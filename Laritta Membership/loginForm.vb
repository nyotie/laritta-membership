﻿Imports MySql.Data.MySqlClient

Public Class loginForm

    Private Sub loginForm_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        PictureBox1.ImageLocation = String.Format("{0}\{1}", My.Application.Info.DirectoryPath, My.Settings.login_sprite_name)
        UserName.Text = ""
        UserPassword.Text = ""
        LabelControl3.Visible = False
        status_login = False
        UserName.Focus()
    End Sub

    Private Sub TextEdit1_GotFocus(ByVal sender As Object, ByVal e As EventArgs) Handles UserName.GotFocus
        UserName.Select(0, UserName.Text.Length)
    End Sub

    Private Sub TextEdit1_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles UserName.KeyDown
        If Not My.Computer.Name = "NYOTO-PC" Then Exit Sub
        If e.KeyCode = Keys.F5 Then
            UserName.Text = "admin"
            UserPassword.Text = "703011585"
            tryLogin()
        End If
    End Sub

    Private Sub TextEdit1_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles UserName.KeyPress
        If e.KeyChar = ChrW(13) Then
            UserPassword.Focus()
        End If
    End Sub

    Private Sub TextEdit2_GotFocus(ByVal sender As Object, ByVal e As EventArgs) Handles UserPassword.GotFocus
        UserPassword.Select(0, UserPassword.Text.Length)
    End Sub

    Private Sub TextEdit2_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles UserPassword.KeyPress
        If e.KeyChar = ChrW(13) Then
            SimpleButton1.Focus()
            tryLogin()
        End If
    End Sub

    Private Sub tryLogin()
        If id_cabang IsNot Nothing Then
            If UserName.Text <> "" And UserPassword.Text <> "" Then
                Try
                    If xSet.Tables("GetDataUserLogin") IsNot Nothing Then xSet.Tables("GetDataUserLogin").Clear()
                    SQLquery = String.Format("SELECT idStaff, Nama, a.idSecurityGroup, idCabang, a.inactive FROM mstaff a LEFT JOIN msecuritygroup b ON a.idSecurityGroup=b.idSecurityGroup " _
                                            & "WHERE username='{0}' AND password='{1}'", UserName.Text, UserPassword.Text, id_cabang)
                    ExDb.ExecQuery(1, SQLquery, xSet, "GetDataUserLogin") 'TRY AT ONLINE, IF NOT MULTISERVER IT WILL AUTOMATICALLY GO FOR LOCAL

                    If xSet.Tables("GetDataUserLogin").Rows(0).Item("inactive") = True Then
                        MsgWarning("User sudah tidak aktif")
                        Return
                    End If
                    staff_id = xSet.Tables("GetDataUserLogin").Rows(0).Item("idStaff").ToString
                    staff_name = xSet.Tables("GetDataUserLogin").Rows(0).Item("Nama").ToString
                    staff_group = xSet.Tables("GetDataUserLogin").Rows(0).Item("idSecurityGroup").ToString
                    status_login = True

                    If status_login = True Then Close()
                Catch ex As Exception
                    LabelControl3.Visible = True
                    status_login = False
                End Try
            End If
        Else
            MsgBox("Aplikasi tidak dapat berjalan di komputer ini", MsgBoxStyle.OkOnly = MsgBoxStyle.Critical, "Warning!")
        End If
    End Sub

    Private Sub SimpleButton1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles SimpleButton1.Click
        tryLogin()
    End Sub

    Private Sub SimpleButton2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles SimpleButton2.Click
        Close()
    End Sub
End Class