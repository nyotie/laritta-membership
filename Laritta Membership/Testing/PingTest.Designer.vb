﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PingTest
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ButPing = New DevExpress.XtraEditors.SimpleButton()
        Me.IPTarget = New DevExpress.XtraEditors.TextEdit()
        Me.HasilPing = New DevExpress.XtraEditors.MemoEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.IPTarget.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.HasilPing.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ButPing
        '
        Me.ButPing.Location = New System.Drawing.Point(92, 97)
        Me.ButPing.Name = "ButPing"
        Me.ButPing.Size = New System.Drawing.Size(75, 23)
        Me.ButPing.TabIndex = 0
        Me.ButPing.Text = "ping"
        '
        'IPTarget
        '
        Me.IPTarget.Location = New System.Drawing.Point(92, 12)
        Me.IPTarget.Name = "IPTarget"
        Me.IPTarget.Size = New System.Drawing.Size(234, 20)
        Me.IPTarget.TabIndex = 1
        '
        'HasilPing
        '
        Me.HasilPing.Location = New System.Drawing.Point(92, 38)
        Me.HasilPing.Name = "HasilPing"
        Me.HasilPing.Size = New System.Drawing.Size(234, 53)
        Me.HasilPing.TabIndex = 2
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(18, 15)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(41, 13)
        Me.LabelControl1.TabIndex = 3
        Me.LabelControl1.Text = "ip target"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(18, 41)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(50, 13)
        Me.LabelControl2.TabIndex = 3
        Me.LabelControl2.Text = "ping result"
        '
        'PingTest
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(349, 132)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.HasilPing)
        Me.Controls.Add(Me.IPTarget)
        Me.Controls.Add(Me.ButPing)
        Me.Name = "PingTest"
        Me.Text = "PingTest"
        CType(Me.IPTarget.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.HasilPing.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ButPing As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents IPTarget As DevExpress.XtraEditors.TextEdit
    Friend WithEvents HasilPing As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
End Class
