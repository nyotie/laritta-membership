﻿Imports DevExpress.XtraReports.UI
Imports System.Management
Imports System

Public Class CallFromString
    Dim ObjectName As String
    Dim T As Type

    Private Sub ShowReport_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ShowReport.Click
        ObjectName = rptName.Text
        T = Type.GetType(Replace(Reflection.Assembly.GetExecutingAssembly.GetName.Name & "." & ObjectName, " ", "_"))
        Dim f = DirectCast(Activator.CreateInstance(T), XtraReport)

        Try
            Dim yyy As ReportPrintTool = New ReportPrintTool(f)
            'or ReportDesignTool to use with yyy.ShowDesigner()
            yyy.ShowRibbonPreviewDialog()
        Finally
            f.Dispose()
        End Try
    End Sub

    Private Sub ShowForm_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ShowForm.Click
        ObjectName = frmName.Text
        T = Type.GetType(Replace(Reflection.Assembly.GetExecutingAssembly.GetName.Name & "." & ObjectName, " ", "_"))
        Dim f = DirectCast(Activator.CreateInstance(T), DevExpress.XtraEditors.XtraForm)

        Try
            f.ShowDialog()
        Finally
            f.Dispose()
        End Try
    End Sub

    Private Sub GetPrinters_Click(sender As System.Object, e As System.EventArgs) Handles GetPrinters.Click
        Dim scope As ManagementScope = New ManagementScope("\root\cimv2")
        scope.Connect()
        Using mosearcher As ManagementObjectSearcher = New ManagementObjectSearcher("SELECT * FROM Win32_Printer")
            For Each printer As ManagementObject In mosearcher.Get
                PrinterLists.Properties.Items.Add(printer("Name").ToString()) ' & " - Status : " & printer("WorkOffline").ToString())
            Next
        End Using
    End Sub
End Class