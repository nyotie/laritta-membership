﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CallFromString
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.rptName = New DevExpress.XtraEditors.TextEdit()
        Me.ShowReport = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.frmName = New DevExpress.XtraEditors.TextEdit()
        Me.ShowForm = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.PrinterLists = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.GetPrinters = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.rptName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.frmName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PrinterLists.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'rptName
        '
        Me.rptName.Location = New System.Drawing.Point(107, 21)
        Me.rptName.Name = "rptName"
        Me.rptName.Size = New System.Drawing.Size(100, 20)
        Me.rptName.TabIndex = 0
        '
        'ShowReport
        '
        Me.ShowReport.Location = New System.Drawing.Point(107, 47)
        Me.ShowReport.Name = "ShowReport"
        Me.ShowReport.Size = New System.Drawing.Size(75, 23)
        Me.ShowReport.TabIndex = 1
        Me.ShowReport.Text = "Show Report"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(35, 24)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(63, 13)
        Me.LabelControl1.TabIndex = 2
        Me.LabelControl1.Text = "Report Name"
        '
        'frmName
        '
        Me.frmName.Location = New System.Drawing.Point(107, 91)
        Me.frmName.Name = "frmName"
        Me.frmName.Size = New System.Drawing.Size(100, 20)
        Me.frmName.TabIndex = 0
        '
        'ShowForm
        '
        Me.ShowForm.Location = New System.Drawing.Point(107, 117)
        Me.ShowForm.Name = "ShowForm"
        Me.ShowForm.Size = New System.Drawing.Size(75, 23)
        Me.ShowForm.TabIndex = 1
        Me.ShowForm.Text = "Show Form"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(35, 94)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(54, 13)
        Me.LabelControl2.TabIndex = 2
        Me.LabelControl2.Text = "Form Name"
        '
        'PrinterLists
        '
        Me.PrinterLists.Location = New System.Drawing.Point(107, 159)
        Me.PrinterLists.Name = "PrinterLists"
        Me.PrinterLists.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PrinterLists.Size = New System.Drawing.Size(100, 20)
        Me.PrinterLists.TabIndex = 3
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(35, 162)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(48, 13)
        Me.LabelControl3.TabIndex = 2
        Me.LabelControl3.Text = "Printer list"
        '
        'GetPrinters
        '
        Me.GetPrinters.Location = New System.Drawing.Point(107, 185)
        Me.GetPrinters.Name = "GetPrinters"
        Me.GetPrinters.Size = New System.Drawing.Size(75, 23)
        Me.GetPrinters.TabIndex = 1
        Me.GetPrinters.Text = "Get printers"
        '
        'CallFromString
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 261)
        Me.Controls.Add(Me.PrinterLists)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.GetPrinters)
        Me.Controls.Add(Me.ShowForm)
        Me.Controls.Add(Me.frmName)
        Me.Controls.Add(Me.ShowReport)
        Me.Controls.Add(Me.rptName)
        Me.Name = "CallFromString"
        Me.Text = "XtraForm1"
        CType(Me.rptName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.frmName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PrinterLists.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents rptName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ShowReport As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents frmName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ShowForm As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PrinterLists As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GetPrinters As DevExpress.XtraEditors.SimpleButton
End Class
