﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class xr1MemberActivities
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim SimpleDiagram3 As DevExpress.XtraCharts.SimpleDiagram = New DevExpress.XtraCharts.SimpleDiagram()
        Dim Series3 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
        Dim PieSeriesLabel3 As DevExpress.XtraCharts.PieSeriesLabel = New DevExpress.XtraCharts.PieSeriesLabel()
        Dim PieSeriesView5 As DevExpress.XtraCharts.PieSeriesView = New DevExpress.XtraCharts.PieSeriesView()
        Dim PieSeriesView6 As DevExpress.XtraCharts.PieSeriesView = New DevExpress.XtraCharts.PieSeriesView()
        Dim ChartTitle3 As DevExpress.XtraCharts.ChartTitle = New DevExpress.XtraCharts.ChartTitle()
        Dim SimpleDiagram2 As DevExpress.XtraCharts.SimpleDiagram = New DevExpress.XtraCharts.SimpleDiagram()
        Dim Series2 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
        Dim PieSeriesLabel2 As DevExpress.XtraCharts.PieSeriesLabel = New DevExpress.XtraCharts.PieSeriesLabel()
        Dim PieSeriesView3 As DevExpress.XtraCharts.PieSeriesView = New DevExpress.XtraCharts.PieSeriesView()
        Dim PieSeriesView4 As DevExpress.XtraCharts.PieSeriesView = New DevExpress.XtraCharts.PieSeriesView()
        Dim ChartTitle2 As DevExpress.XtraCharts.ChartTitle = New DevExpress.XtraCharts.ChartTitle()
        Dim SimpleDiagram1 As DevExpress.XtraCharts.SimpleDiagram = New DevExpress.XtraCharts.SimpleDiagram()
        Dim Series1 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
        Dim PieSeriesLabel1 As DevExpress.XtraCharts.PieSeriesLabel = New DevExpress.XtraCharts.PieSeriesLabel()
        Dim PieSeriesView1 As DevExpress.XtraCharts.PieSeriesView = New DevExpress.XtraCharts.PieSeriesView()
        Dim PieSeriesView2 As DevExpress.XtraCharts.PieSeriesView = New DevExpress.XtraCharts.PieSeriesView()
        Dim ChartTitle1 As DevExpress.XtraCharts.ChartTitle = New DevExpress.XtraCharts.ChartTitle()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.Set_Laporan1 = New Laritta_Membership.Set_Laporan()
        Me.Laporan1TableAdapter = New Laritta_Membership.Set_LaporanTableAdapters.Laporan1TableAdapter()
        Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
        Me.XrPivotGrid1 = New DevExpress.XtraReports.UI.XRPivotGrid()
        Me.fieldidcustomer1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldnamatipemembership1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldnamaCust1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldasal1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldmoney1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldpoints1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldtrans1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.XrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrPageInfo2 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
        Me.Parameter1 = New DevExpress.XtraReports.Parameters.Parameter()
        Me.Parameter2 = New DevExpress.XtraReports.Parameters.Parameter()
        Me.Parameter3 = New DevExpress.XtraReports.Parameters.Parameter()
        Me.Parameter4 = New DevExpress.XtraReports.Parameters.Parameter()
        Me.Parameter5 = New DevExpress.XtraReports.Parameters.Parameter()
        Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
        Me.ChartTrans = New DevExpress.XtraReports.UI.XRChart()
        Me.Parameter6 = New DevExpress.XtraReports.Parameters.Parameter()
        Me.Parameter7 = New DevExpress.XtraReports.Parameters.Parameter()
        Me.ChartMoney = New DevExpress.XtraReports.UI.XRChart()
        Me.ChartPoints = New DevExpress.XtraReports.UI.XRChart()
        Me.Parameter8 = New DevExpress.XtraReports.Parameters.Parameter()
        CType(Me.Set_Laporan1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ChartTrans, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(SimpleDiagram3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Series3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PieSeriesLabel3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PieSeriesView5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PieSeriesView6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ChartMoney, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(SimpleDiagram2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Series2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PieSeriesLabel2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PieSeriesView3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PieSeriesView4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ChartPoints, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(SimpleDiagram1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Series1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PieSeriesLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PieSeriesView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PieSeriesView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Dpi = 254.0!
        Me.Detail.HeightF = 0.0!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254.0!)
        Me.Detail.StylePriority.UseTextAlignment = False
        '
        'TopMargin
        '
        Me.TopMargin.Dpi = 254.0!
        Me.TopMargin.HeightF = 70.0!
        Me.TopMargin.Name = "TopMargin"
        Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254.0!)
        Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'BottomMargin
        '
        Me.BottomMargin.Dpi = 254.0!
        Me.BottomMargin.HeightF = 70.0!
        Me.BottomMargin.Name = "BottomMargin"
        Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254.0!)
        Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'Set_Laporan1
        '
        Me.Set_Laporan1.DataSetName = "Set_Laporan"
        Me.Set_Laporan1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Laporan1TableAdapter
        '
        Me.Laporan1TableAdapter.ClearBeforeFill = True
        '
        'ReportHeader
        '
        Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPivotGrid1, Me.XrPageInfo1, Me.XrLabel1})
        Me.ReportHeader.Dpi = 254.0!
        Me.ReportHeader.HeightF = 432.6908!
        Me.ReportHeader.Name = "ReportHeader"
        '
        'XrPivotGrid1
        '
        Me.XrPivotGrid1.Appearance.Cell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.XrPivotGrid1.Appearance.CustomTotalCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.XrPivotGrid1.Appearance.FieldHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrPivotGrid1.Appearance.FieldHeader.TextHorizontalAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.XrPivotGrid1.Appearance.FieldHeader.TextVerticalAlignment = DevExpress.Utils.VertAlignment.Center
        Me.XrPivotGrid1.Appearance.FieldValue.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.XrPivotGrid1.Appearance.FieldValue.TextHorizontalAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.XrPivotGrid1.Appearance.FieldValue.WordWrap = True
        Me.XrPivotGrid1.Appearance.FieldValueGrandTotal.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.XrPivotGrid1.Appearance.FieldValueGrandTotal.TextHorizontalAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.XrPivotGrid1.Appearance.FieldValueGrandTotal.TextVerticalAlignment = DevExpress.Utils.VertAlignment.Center
        Me.XrPivotGrid1.Appearance.FieldValueTotal.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.XrPivotGrid1.Appearance.FieldValueTotal.TextHorizontalAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.XrPivotGrid1.Appearance.FieldValueTotal.TextVerticalAlignment = DevExpress.Utils.VertAlignment.Center
        Me.XrPivotGrid1.Appearance.GrandTotalCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.XrPivotGrid1.Appearance.Lines.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.XrPivotGrid1.Appearance.TotalCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.XrPivotGrid1.Appearance.TotalCell.TextHorizontalAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.XrPivotGrid1.Appearance.TotalCell.TextVerticalAlignment = DevExpress.Utils.VertAlignment.Center
        Me.XrPivotGrid1.DataAdapter = Me.Laporan1TableAdapter
        Me.XrPivotGrid1.DataMember = "Laporan1"
        Me.XrPivotGrid1.DataSource = Me.Set_Laporan1
        Me.XrPivotGrid1.Dpi = 254.0!
        Me.XrPivotGrid1.Fields.AddRange(New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField() {Me.fieldidcustomer1, Me.fieldnamatipemembership1, Me.fieldnamaCust1, Me.fieldasal1, Me.fieldmoney1, Me.fieldpoints1, Me.fieldtrans1})
        Me.XrPivotGrid1.LocationFloat = New DevExpress.Utils.PointFloat(25.00001!, 128.42!)
        Me.XrPivotGrid1.Name = "XrPivotGrid1"
        Me.XrPivotGrid1.OptionsPrint.FilterSeparatorBarPadding = 3
        Me.XrPivotGrid1.OptionsView.ShowColumnHeaders = False
        Me.XrPivotGrid1.OptionsView.ShowDataHeaders = False
        Me.XrPivotGrid1.SizeF = New System.Drawing.SizeF(2604.0!, 279.2708!)
        '
        'fieldidcustomer1
        '
        Me.fieldidcustomer1.AreaIndex = 0
        Me.fieldidcustomer1.Caption = "id_customer"
        Me.fieldidcustomer1.FieldName = "id_customer"
        Me.fieldidcustomer1.Name = "fieldidcustomer1"
        Me.fieldidcustomer1.Visible = False
        '
        'fieldnamatipemembership1
        '
        Me.fieldnamatipemembership1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea
        Me.fieldnamatipemembership1.AreaIndex = 1
        Me.fieldnamatipemembership1.Caption = "Tipe member"
        Me.fieldnamatipemembership1.FieldName = "nama_tipemembership"
        Me.fieldnamatipemembership1.Name = "fieldnamatipemembership1"
        Me.fieldnamatipemembership1.Width = 150
        '
        'fieldnamaCust1
        '
        Me.fieldnamaCust1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea
        Me.fieldnamaCust1.AreaIndex = 0
        Me.fieldnamaCust1.Caption = "Nama member"
        Me.fieldnamaCust1.FieldName = "namaCust"
        Me.fieldnamaCust1.Name = "fieldnamaCust1"
        Me.fieldnamaCust1.Width = 200
        '
        'fieldasal1
        '
        Me.fieldasal1.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea
        Me.fieldasal1.AreaIndex = 0
        Me.fieldasal1.Caption = "Asal"
        Me.fieldasal1.FieldName = "asal"
        Me.fieldasal1.Name = "fieldasal1"
        Me.fieldasal1.SortOrder = DevExpress.XtraPivotGrid.PivotSortOrder.Descending
        '
        'fieldmoney1
        '
        Me.fieldmoney1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea
        Me.fieldmoney1.AreaIndex = 2
        Me.fieldmoney1.Caption = "Money"
        Me.fieldmoney1.CellFormat.FormatString = "n0"
        Me.fieldmoney1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldmoney1.FieldName = "money"
        Me.fieldmoney1.Name = "fieldmoney1"
        Me.fieldmoney1.ValueFormat.FormatString = "n0"
        Me.fieldmoney1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldmoney1.Width = 107
        '
        'fieldpoints1
        '
        Me.fieldpoints1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea
        Me.fieldpoints1.AreaIndex = 1
        Me.fieldpoints1.Caption = "Points"
        Me.fieldpoints1.CellFormat.FormatString = "n0"
        Me.fieldpoints1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldpoints1.FieldName = "points"
        Me.fieldpoints1.Name = "fieldpoints1"
        Me.fieldpoints1.ValueFormat.FormatString = "n0"
        Me.fieldpoints1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldpoints1.Width = 69
        '
        'fieldtrans1
        '
        Me.fieldtrans1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea
        Me.fieldtrans1.AreaIndex = 0
        Me.fieldtrans1.Caption = "Trans"
        Me.fieldtrans1.CellFormat.FormatString = "n0"
        Me.fieldtrans1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldtrans1.FieldName = "trans"
        Me.fieldtrans1.Name = "fieldtrans1"
        Me.fieldtrans1.ValueFormat.FormatString = "n0"
        Me.fieldtrans1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldtrans1.Width = 61
        '
        'XrPageInfo1
        '
        Me.XrPageInfo1.Dpi = 254.0!
        Me.XrPageInfo1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrPageInfo1.Format = "Tgl cetak : {0:d MMMM yyyy, HH:mm}"
        Me.XrPageInfo1.LocationFloat = New DevExpress.Utils.PointFloat(25.00001!, 70.00001!)
        Me.XrPageInfo1.Name = "XrPageInfo1"
        Me.XrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime
        Me.XrPageInfo1.SizeF = New System.Drawing.SizeF(865.1874!, 58.42001!)
        Me.XrPageInfo1.StylePriority.UseFont = False
        Me.XrPageInfo1.StylePriority.UseTextAlignment = False
        Me.XrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel1
        '
        Me.XrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrLabel1.Dpi = 254.0!
        Me.XrLabel1.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(25.00001!, 0.0!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(1969.0!, 70.0!)
        Me.XrLabel1.StylePriority.UseBorders = False
        Me.XrLabel1.StylePriority.UseFont = False
        Me.XrLabel1.Text = "Laporan Member Performance"
        '
        'XrPageInfo2
        '
        Me.XrPageInfo2.Dpi = 254.0!
        Me.XrPageInfo2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrPageInfo2.LocationFloat = New DevExpress.Utils.PointFloat(2400.0!, 0.0!)
        Me.XrPageInfo2.Name = "XrPageInfo2"
        Me.XrPageInfo2.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrPageInfo2.SizeF = New System.Drawing.SizeF(254.0!, 58.42!)
        Me.XrPageInfo2.StylePriority.UseFont = False
        Me.XrPageInfo2.StylePriority.UseTextAlignment = False
        Me.XrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPageInfo2})
        Me.PageFooter.Dpi = 254.0!
        Me.PageFooter.HeightF = 58.42!
        Me.PageFooter.Name = "PageFooter"
        '
        'Parameter1
        '
        Me.Parameter1.Description = "Tgl Start"
        Me.Parameter1.Name = "Parameter1"
        Me.Parameter1.Type = GetType(Date)
        Me.Parameter1.ValueInfo = "2015-05-01"
        '
        'Parameter2
        '
        Me.Parameter2.Description = "Tgl End"
        Me.Parameter2.Name = "Parameter2"
        Me.Parameter2.Type = GetType(Date)
        Me.Parameter2.ValueInfo = "2015-05-01"
        '
        'Parameter3
        '
        Me.Parameter3.Description = "Money bot"
        Me.Parameter3.Name = "Parameter3"
        Me.Parameter3.Type = GetType(Integer)
        Me.Parameter3.ValueInfo = "0"
        '
        'Parameter4
        '
        Me.Parameter4.Description = "Money top"
        Me.Parameter4.Name = "Parameter4"
        Me.Parameter4.Type = GetType(Integer)
        Me.Parameter4.ValueInfo = "0"
        '
        'Parameter5
        '
        Me.Parameter5.Description = "Point"
        Me.Parameter5.Name = "Parameter5"
        Me.Parameter5.Type = GetType(Integer)
        Me.Parameter5.ValueInfo = "0"
        '
        'ReportFooter
        '
        Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.ChartPoints, Me.ChartMoney, Me.ChartTrans})
        Me.ReportFooter.Dpi = 254.0!
        Me.ReportFooter.HeightF = 2400.0!
        Me.ReportFooter.KeepTogether = True
        Me.ReportFooter.Name = "ReportFooter"
        '
        'ChartTrans
        '
        Me.ChartTrans.BorderColor = System.Drawing.Color.Black
        Me.ChartTrans.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.ChartTrans.DataMember = "Laporan1"
        Me.ChartTrans.DataSource = Me.Set_Laporan1
        SimpleDiagram3.EqualPieSize = False
        Me.ChartTrans.Diagram = SimpleDiagram3
        Me.ChartTrans.Dpi = 254.0!
        Me.ChartTrans.Legend.AlignmentHorizontal = DevExpress.XtraCharts.LegendAlignmentHorizontal.Left
        Me.ChartTrans.Legend.EquallySpacedItems = False
        Me.ChartTrans.Legend.Visibility = DevExpress.Utils.DefaultBoolean.[True]
        Me.ChartTrans.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.ChartTrans.Name = "ChartTrans"
        Series3.ArgumentDataMember = "namaCust"
        Series3.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Qualitative
        Series3.CheckedInLegend = False
        PieSeriesLabel3.TextPattern = "{A}:{VP:0%}"
        Series3.Label = PieSeriesLabel3
        Series3.LegendText = "Transaksi"
        Series3.Name = "Transaction"
        Series3.SummaryFunction = "SUM([trans])"
        Series3.TopNOptions.Count = 6
        Series3.TopNOptions.Enabled = True
        Series3.TopNOptions.OthersArgument = "Others"
        PieSeriesView5.RuntimeExploding = False
        PieSeriesView5.SweepDirection = DevExpress.XtraCharts.PieSweepDirection.Counterclockwise
        Series3.View = PieSeriesView5
        Me.ChartTrans.SeriesSerializable = New DevExpress.XtraCharts.Series() {Series3}
        Me.ChartTrans.SeriesTemplate.ArgumentDataMember = "asal"
        Me.ChartTrans.SeriesTemplate.ValueDataMembersSerializable = "money"
        PieSeriesView6.RuntimeExploding = False
        PieSeriesView6.SweepDirection = DevExpress.XtraCharts.PieSweepDirection.Counterclockwise
        Me.ChartTrans.SeriesTemplate.View = PieSeriesView6
        Me.ChartTrans.SizeF = New System.Drawing.SizeF(2629.0!, 800.0!)
        Me.ChartTrans.StylePriority.UseBorders = False
        ChartTitle3.Text = "Member Performance : Transactions"
        Me.ChartTrans.Titles.AddRange(New DevExpress.XtraCharts.ChartTitle() {ChartTitle3})
        '
        'Parameter6
        '
        Me.Parameter6.Description = "Trans bot"
        Me.Parameter6.Name = "Parameter6"
        Me.Parameter6.Type = GetType(Integer)
        Me.Parameter6.ValueInfo = "0"
        '
        'Parameter7
        '
        Me.Parameter7.Description = "Trans top"
        Me.Parameter7.Name = "Parameter7"
        Me.Parameter7.Type = GetType(Integer)
        Me.Parameter7.ValueInfo = "0"
        '
        'ChartMoney
        '
        Me.ChartMoney.BorderColor = System.Drawing.Color.Black
        Me.ChartMoney.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.ChartMoney.DataMember = "Laporan1"
        Me.ChartMoney.DataSource = Me.Set_Laporan1
        SimpleDiagram2.EqualPieSize = False
        Me.ChartMoney.Diagram = SimpleDiagram2
        Me.ChartMoney.Dpi = 254.0!
        Me.ChartMoney.Legend.AlignmentHorizontal = DevExpress.XtraCharts.LegendAlignmentHorizontal.Left
        Me.ChartMoney.Legend.EquallySpacedItems = False
        Me.ChartMoney.Legend.Visibility = DevExpress.Utils.DefaultBoolean.[True]
        Me.ChartMoney.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 800.0!)
        Me.ChartMoney.Name = "ChartMoney"
        Series2.ArgumentDataMember = "namaCust"
        Series2.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Qualitative
        PieSeriesLabel2.TextPattern = "{A}:{VP:0%}"
        Series2.Label = PieSeriesLabel2
        Series2.LegendText = "Money"
        Series2.Name = "Money"
        Series2.SummaryFunction = "SUM([money])"
        Series2.TopNOptions.Count = 6
        Series2.TopNOptions.Enabled = True
        Series2.TopNOptions.OthersArgument = "Others"
        PieSeriesView3.RuntimeExploding = False
        PieSeriesView3.SweepDirection = DevExpress.XtraCharts.PieSweepDirection.Counterclockwise
        Series2.View = PieSeriesView3
        Me.ChartMoney.SeriesSerializable = New DevExpress.XtraCharts.Series() {Series2}
        Me.ChartMoney.SeriesTemplate.ArgumentDataMember = "asal"
        Me.ChartMoney.SeriesTemplate.ValueDataMembersSerializable = "money"
        PieSeriesView4.RuntimeExploding = False
        PieSeriesView4.SweepDirection = DevExpress.XtraCharts.PieSweepDirection.Counterclockwise
        Me.ChartMoney.SeriesTemplate.View = PieSeriesView4
        Me.ChartMoney.SizeF = New System.Drawing.SizeF(2629.0!, 800.0!)
        Me.ChartMoney.StylePriority.UseBorders = False
        ChartTitle2.Text = "Member Performance : Money"
        Me.ChartMoney.Titles.AddRange(New DevExpress.XtraCharts.ChartTitle() {ChartTitle2})
        '
        'ChartPoints
        '
        Me.ChartPoints.BorderColor = System.Drawing.Color.Black
        Me.ChartPoints.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.ChartPoints.DataMember = "Laporan1"
        Me.ChartPoints.DataSource = Me.Set_Laporan1
        SimpleDiagram1.EqualPieSize = False
        Me.ChartPoints.Diagram = SimpleDiagram1
        Me.ChartPoints.Dpi = 254.0!
        Me.ChartPoints.Legend.AlignmentHorizontal = DevExpress.XtraCharts.LegendAlignmentHorizontal.Left
        Me.ChartPoints.Legend.EquallySpacedItems = False
        Me.ChartPoints.Legend.Visibility = DevExpress.Utils.DefaultBoolean.[True]
        Me.ChartPoints.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 1600.0!)
        Me.ChartPoints.Name = "ChartPoints"
        Series1.ArgumentDataMember = "namaCust"
        Series1.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Qualitative
        PieSeriesLabel1.TextPattern = "{A}:{VP:0%}"
        Series1.Label = PieSeriesLabel1
        Series1.LegendText = "Point"
        Series1.Name = "Point"
        Series1.SummaryFunction = "SUM([points])"
        Series1.TopNOptions.Count = 6
        Series1.TopNOptions.Enabled = True
        Series1.TopNOptions.OthersArgument = "Others"
        PieSeriesView1.RuntimeExploding = False
        PieSeriesView1.SweepDirection = DevExpress.XtraCharts.PieSweepDirection.Counterclockwise
        Series1.View = PieSeriesView1
        Me.ChartPoints.SeriesSerializable = New DevExpress.XtraCharts.Series() {Series1}
        Me.ChartPoints.SeriesTemplate.ArgumentDataMember = "asal"
        Me.ChartPoints.SeriesTemplate.ValueDataMembersSerializable = "money"
        PieSeriesView2.RuntimeExploding = False
        PieSeriesView2.SweepDirection = DevExpress.XtraCharts.PieSweepDirection.Counterclockwise
        Me.ChartPoints.SeriesTemplate.View = PieSeriesView2
        Me.ChartPoints.SizeF = New System.Drawing.SizeF(2629.0!, 800.0!)
        Me.ChartPoints.StylePriority.UseBorders = False
        ChartTitle1.Text = "Member Performance : Points"
        Me.ChartPoints.Titles.AddRange(New DevExpress.XtraCharts.ChartTitle() {ChartTitle1})
        '
        'Parameter8
        '
        Me.Parameter8.Description = "Top-N Chart"
        Me.Parameter8.Name = "Parameter8"
        Me.Parameter8.Type = GetType(Integer)
        Me.Parameter8.ValueInfo = "0"
        '
        'xr1MemberActivities
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.ReportHeader, Me.PageFooter, Me.ReportFooter})
        Me.Bookmark = "Member Performance"
        Me.DataMember = "Laporan1"
        Me.DataSource = Me.Set_Laporan1
        Me.DisplayName = "Member Performance"
        Me.Dpi = 254.0!
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Landscape = True
        Me.Margins = New System.Drawing.Printing.Margins(70, 70, 70, 70)
        Me.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.PageHeight = 2159
        Me.PageWidth = 2794
        Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.Parameter1, Me.Parameter2, Me.Parameter3, Me.Parameter4, Me.Parameter5, Me.Parameter6, Me.Parameter7, Me.Parameter8})
        Me.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.ShowPrintMarginsWarning = False
        Me.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.Version = "14.2"
        CType(Me.Set_Laporan1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(SimpleDiagram3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PieSeriesLabel3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PieSeriesView5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Series3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PieSeriesView6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ChartTrans, System.ComponentModel.ISupportInitialize).EndInit()
        CType(SimpleDiagram2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PieSeriesLabel2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PieSeriesView3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Series2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PieSeriesView4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ChartMoney, System.ComponentModel.ISupportInitialize).EndInit()
        CType(SimpleDiagram1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PieSeriesLabel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PieSeriesView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Series1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PieSeriesView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ChartPoints, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents Set_Laporan1 As Laritta_Membership.Set_Laporan
    Friend WithEvents Laporan1TableAdapter As Laritta_Membership.Set_LaporanTableAdapters.Laporan1TableAdapter
    Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
    Friend WithEvents XrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPageInfo2 As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Friend WithEvents XrPivotGrid1 As DevExpress.XtraReports.UI.XRPivotGrid
    Friend WithEvents fieldidcustomer1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldnamatipemembership1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldnamaCust1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldasal1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldmoney1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldpoints1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldtrans1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
    Friend WithEvents Parameter1 As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents Parameter3 As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents Parameter4 As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents Parameter5 As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents ChartTrans As DevExpress.XtraReports.UI.XRChart
    Friend WithEvents Parameter2 As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents Parameter6 As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents Parameter7 As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents ChartPoints As DevExpress.XtraReports.UI.XRChart
    Friend WithEvents ChartMoney As DevExpress.XtraReports.UI.XRChart
    Friend WithEvents Parameter8 As DevExpress.XtraReports.Parameters.Parameter
End Class
