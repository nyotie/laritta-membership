﻿Imports DevExpress.Utils
Imports DevExpress.XtraEditors
Imports DevExpress.XtraPivotGrid

Public Class xr1MemberActivities

    Private Sub xr1MemberActivities_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs) Handles Me.BeforePrint

        'Me.fieldBulan1.SortOrder = DevExpress.XtraPivotGrid.PivotSortOrder.Descending
    End Sub

    Private Sub ThisReport_ParametersRequestBeforeShow(sender As Object, e As DevExpress.XtraReports.Parameters.ParametersRequestEventArgs) Handles Me.ParametersRequestBeforeShow
        Dim Info As DevExpress.XtraReports.Parameters.ParameterInfo

        For Each Info In e.ParametersInformation
            If Info.Parameter.Name = "Parameter1" Or Info.Parameter.Name = "Parameter2" Then
                Dim Tanggalan As New DateEdit
                Tanggalan.Properties.Mask.MaskType = Mask.MaskType.DateTime
                Tanggalan.Properties.Mask.EditMask = "dd MMM yyyy"
                Tanggalan.Properties.Mask.UseMaskAsDisplayFormat = True

                Info.Editor = Tanggalan
            Else
                Dim Angkaan As New TextEdit
                Angkaan.Properties.Mask.MaskType = Mask.MaskType.Numeric
                Angkaan.Properties.Mask.EditMask = "n0"
                Angkaan.Properties.Mask.UseMaskAsDisplayFormat = True

                Info.Editor = Angkaan
            End If
        Next

        Parameter1.Value = Today.Date
        Parameter2.Value = Today.Date
    End Sub

    Private Sub ThisReport_ParametersRequestSubmit(sender As Object, e As DevExpress.XtraReports.Parameters.ParametersRequestEventArgs) Handles Me.ParametersRequestSubmit
        Laporan1TableAdapter.Fill(Set_Laporan1.Laporan1,
                                  Format(Parameter1.Value, "yyyy/MM/dd"), Format(Parameter2.Value, "yyyy/MM/dd"),
                                  Parameter3.Value, Parameter4.Value,
                                  Parameter5.Value,
                                  Parameter6.Value, Parameter7.Value)

        If Parameter8.Value <= 0 Then Parameter8.Value = 6
        ChartTrans.Series(0).TopNOptions.Count = Parameter8.Value
        ChartMoney.Series(0).TopNOptions.Count = Parameter8.Value
        ChartPoints.Series(0).TopNOptions.Count = Parameter8.Value
    End Sub

    Private Sub XrPivotGrid1_CustomFieldSort(sender As Object, e As DevExpress.XtraReports.UI.PivotGrid.PivotGridCustomFieldSortEventArgs) Handles XrPivotGrid1.CustomFieldSort
        If e.Field.FieldName = "Result" Then
            If e.Value1 Is Nothing OrElse e.Value2 Is Nothing Then
                Return
            End If
            e.Handled = True
            Dim s1 As String = e.Value1.ToString().Replace("Result ", String.Empty)
            Dim s2 As String = e.Value2.ToString().Replace("Result ", String.Empty)
            If Convert.ToInt32(s1) > Convert.ToInt32(s2) Then
                e.Result = 1
            Else
                If Convert.ToInt32(s1) = Convert.ToInt32(s2) Then
                    e.Result = Comparer(Of Int32).Default.Compare(Convert.ToInt32(s1), _
                      Convert.ToInt32(s2))
                Else
                    e.Result = -1
                End If
            End If
        End If
    End Sub
End Class