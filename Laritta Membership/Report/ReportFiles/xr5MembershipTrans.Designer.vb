﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class xr5MembershipTrans
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.Set_Laporan1 = New Laritta_Membership.Set_Laporan()
        Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
        Me.LabelPeriode = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.XrPageInfo2 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
        Me.Parameter1 = New DevExpress.XtraReports.Parameters.Parameter()
        Me.Parameter2 = New DevExpress.XtraReports.Parameters.Parameter()
        Me.Laporan5TableAdapter = New Laritta_Membership.Set_LaporanTableAdapters.Laporan5TableAdapter()
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrPivotGrid1 = New DevExpress.XtraReports.UI.XRPivotGrid()
        Me.fieldTipe1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldnamaCabang1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldTransNilai1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldTransBayar1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        CType(Me.Set_Laporan1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Dpi = 254.0!
        Me.Detail.HeightF = 0.0!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254.0!)
        Me.Detail.StylePriority.UseTextAlignment = False
        '
        'TopMargin
        '
        Me.TopMargin.Dpi = 254.0!
        Me.TopMargin.HeightF = 70.0!
        Me.TopMargin.Name = "TopMargin"
        Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254.0!)
        Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'BottomMargin
        '
        Me.BottomMargin.Dpi = 254.0!
        Me.BottomMargin.HeightF = 70.0!
        Me.BottomMargin.Name = "BottomMargin"
        Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254.0!)
        Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'Set_Laporan1
        '
        Me.Set_Laporan1.DataSetName = "Set_Laporan"
        Me.Set_Laporan1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ReportHeader
        '
        Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.LabelPeriode, Me.XrLabel1})
        Me.ReportHeader.Dpi = 254.0!
        Me.ReportHeader.HeightF = 149.5866!
        Me.ReportHeader.Name = "ReportHeader"
        '
        'LabelPeriode
        '
        Me.LabelPeriode.Dpi = 254.0!
        Me.LabelPeriode.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.LabelPeriode.LocationFloat = New DevExpress.Utils.PointFloat(25.0!, 69.99998!)
        Me.LabelPeriode.Name = "LabelPeriode"
        Me.LabelPeriode.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.LabelPeriode.SizeF = New System.Drawing.SizeF(1969.0!, 58.42001!)
        Me.LabelPeriode.StylePriority.UseFont = False
        Me.LabelPeriode.StylePriority.UseTextAlignment = False
        Me.LabelPeriode.Text = "LabelPeriode"
        Me.LabelPeriode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel1
        '
        Me.XrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrLabel1.Dpi = 254.0!
        Me.XrLabel1.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(25.00001!, 0.0!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(1969.0!, 70.0!)
        Me.XrLabel1.StylePriority.UseBorders = False
        Me.XrLabel1.StylePriority.UseFont = False
        Me.XrLabel1.Text = "Rekap Transaksi Membership"
        '
        'XrPageInfo1
        '
        Me.XrPageInfo1.Dpi = 254.0!
        Me.XrPageInfo1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrPageInfo1.Format = "Tgl cetak : {0:d MMMM yyyy, HH:mm}"
        Me.XrPageInfo1.LocationFloat = New DevExpress.Utils.PointFloat(25.00001!, 0.0!)
        Me.XrPageInfo1.Name = "XrPageInfo1"
        Me.XrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime
        Me.XrPageInfo1.SizeF = New System.Drawing.SizeF(865.1874!, 58.42001!)
        Me.XrPageInfo1.StylePriority.UseFont = False
        Me.XrPageInfo1.StylePriority.UseTextAlignment = False
        Me.XrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrPageInfo2
        '
        Me.XrPageInfo2.Dpi = 254.0!
        Me.XrPageInfo2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrPageInfo2.LocationFloat = New DevExpress.Utils.PointFloat(2400.0!, 0.0!)
        Me.XrPageInfo2.Name = "XrPageInfo2"
        Me.XrPageInfo2.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrPageInfo2.SizeF = New System.Drawing.SizeF(254.0!, 58.42!)
        Me.XrPageInfo2.StylePriority.UseFont = False
        Me.XrPageInfo2.StylePriority.UseTextAlignment = False
        Me.XrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPageInfo2, Me.XrPageInfo1})
        Me.PageFooter.Dpi = 254.0!
        Me.PageFooter.HeightF = 58.42001!
        Me.PageFooter.Name = "PageFooter"
        '
        'Parameter1
        '
        Me.Parameter1.Description = "Tgl Start"
        Me.Parameter1.Name = "Parameter1"
        Me.Parameter1.Type = GetType(Date)
        Me.Parameter1.ValueInfo = "2015-05-01"
        '
        'Parameter2
        '
        Me.Parameter2.Description = "Tgl End"
        Me.Parameter2.Name = "Parameter2"
        Me.Parameter2.Type = GetType(Date)
        Me.Parameter2.ValueInfo = "2015-05-01"
        '
        'Laporan5TableAdapter
        '
        Me.Laporan5TableAdapter.ClearBeforeFill = True
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPivotGrid1})
        Me.GroupHeader1.Dpi = 254.0!
        Me.GroupHeader1.HeightF = 337.75!
        Me.GroupHeader1.Name = "GroupHeader1"
        '
        'XrPivotGrid1
        '
        Me.XrPivotGrid1.Appearance.Cell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.XrPivotGrid1.Appearance.CustomTotalCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.XrPivotGrid1.Appearance.FieldHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrPivotGrid1.Appearance.FieldHeader.TextHorizontalAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.XrPivotGrid1.Appearance.FieldHeader.TextVerticalAlignment = DevExpress.Utils.VertAlignment.Center
        Me.XrPivotGrid1.Appearance.FieldValue.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.XrPivotGrid1.Appearance.FieldValue.TextHorizontalAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.XrPivotGrid1.Appearance.FieldValue.WordWrap = True
        Me.XrPivotGrid1.Appearance.FieldValueGrandTotal.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.XrPivotGrid1.Appearance.FieldValueGrandTotal.TextHorizontalAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.XrPivotGrid1.Appearance.FieldValueGrandTotal.TextVerticalAlignment = DevExpress.Utils.VertAlignment.Center
        Me.XrPivotGrid1.Appearance.FieldValueTotal.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.XrPivotGrid1.Appearance.FieldValueTotal.TextHorizontalAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.XrPivotGrid1.Appearance.FieldValueTotal.TextVerticalAlignment = DevExpress.Utils.VertAlignment.Center
        Me.XrPivotGrid1.Appearance.GrandTotalCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.XrPivotGrid1.Appearance.Lines.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.XrPivotGrid1.Appearance.TotalCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.XrPivotGrid1.Appearance.TotalCell.TextHorizontalAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.XrPivotGrid1.Appearance.TotalCell.TextVerticalAlignment = DevExpress.Utils.VertAlignment.Center
        Me.XrPivotGrid1.DataAdapter = Me.Laporan5TableAdapter
        Me.XrPivotGrid1.DataMember = "Laporan5"
        Me.XrPivotGrid1.DataSource = Me.Set_Laporan1
        Me.XrPivotGrid1.Dpi = 254.0!
        Me.XrPivotGrid1.Fields.AddRange(New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField() {Me.fieldnamaCabang1, Me.fieldTipe1, Me.fieldTransNilai1, Me.fieldTransBayar1})
        Me.XrPivotGrid1.LocationFloat = New DevExpress.Utils.PointFloat(25.0!, 0.0!)
        Me.XrPivotGrid1.Name = "XrPivotGrid1"
        Me.XrPivotGrid1.OptionsPrint.FilterSeparatorBarPadding = 3
        Me.XrPivotGrid1.OptionsView.ShowColumnHeaders = False
        Me.XrPivotGrid1.OptionsView.ShowDataHeaders = False
        Me.XrPivotGrid1.OptionsView.ShowFilterHeaders = False
        Me.XrPivotGrid1.SizeF = New System.Drawing.SizeF(2604.0!, 337.75!)
        '
        'fieldTipe1
        '
        Me.fieldTipe1.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea
        Me.fieldTipe1.AreaIndex = 0
        Me.fieldTipe1.Caption = "Tipe"
        Me.fieldTipe1.FieldName = "Tipe"
        Me.fieldTipe1.Name = "fieldTipe1"
        '
        'fieldnamaCabang1
        '
        Me.fieldnamaCabang1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea
        Me.fieldnamaCabang1.AreaIndex = 0
        Me.fieldnamaCabang1.Caption = "Nama Cabang"
        Me.fieldnamaCabang1.FieldName = "namaCabang"
        Me.fieldnamaCabang1.Name = "fieldnamaCabang1"
        Me.fieldnamaCabang1.Width = 150
        '
        'fieldTransNilai1
        '
        Me.fieldTransNilai1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea
        Me.fieldTransNilai1.AreaIndex = 0
        Me.fieldTransNilai1.Caption = "Trans Nilai"
        Me.fieldTransNilai1.CellFormat.FormatString = "n0"
        Me.fieldTransNilai1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldTransNilai1.FieldName = "TransNilai"
        Me.fieldTransNilai1.Name = "fieldTransNilai1"
        Me.fieldTransNilai1.TotalCellFormat.FormatString = "n0"
        Me.fieldTransNilai1.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldTransNilai1.TotalValueFormat.FormatString = "n0"
        Me.fieldTransNilai1.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldTransNilai1.ValueFormat.FormatString = "n0"
        Me.fieldTransNilai1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        '
        'fieldTransBayar1
        '
        Me.fieldTransBayar1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea
        Me.fieldTransBayar1.AreaIndex = 1
        Me.fieldTransBayar1.Caption = "Trans Bayar"
        Me.fieldTransBayar1.CellFormat.FormatString = "n0"
        Me.fieldTransBayar1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldTransBayar1.FieldName = "TransBayar"
        Me.fieldTransBayar1.Name = "fieldTransBayar1"
        Me.fieldTransBayar1.TotalCellFormat.FormatString = "n0"
        Me.fieldTransBayar1.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldTransBayar1.TotalValueFormat.FormatString = "n0"
        Me.fieldTransBayar1.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldTransBayar1.ValueFormat.FormatString = "n0"
        Me.fieldTransBayar1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        '
        'xr5MembershipTrans
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.ReportHeader, Me.PageFooter, Me.GroupHeader1})
        Me.Bookmark = "Rekap Transaksi Membership"
        Me.DataMember = "Laporan5"
        Me.DataSource = Me.Set_Laporan1
        Me.DisplayName = "Rekap Transaksi Membership"
        Me.Dpi = 254.0!
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Landscape = True
        Me.Margins = New System.Drawing.Printing.Margins(70, 70, 70, 70)
        Me.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.PageHeight = 2159
        Me.PageWidth = 2794
        Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.Parameter1, Me.Parameter2})
        Me.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.ShowPrintMarginsWarning = False
        Me.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.Version = "14.2"
        Me.VerticalContentSplitting = DevExpress.XtraPrinting.VerticalContentSplitting.Smart
        CType(Me.Set_Laporan1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents Set_Laporan1 As Laritta_Membership.Set_Laporan
    Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
    Friend WithEvents XrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPageInfo2 As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Friend WithEvents Parameter1 As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents Parameter2 As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents LabelPeriode As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents Laporan5TableAdapter As Laritta_Membership.Set_LaporanTableAdapters.Laporan5TableAdapter
    Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrPivotGrid1 As DevExpress.XtraReports.UI.XRPivotGrid
    Friend WithEvents fieldnamaCabang1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldTipe1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldTransNilai1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldTransBayar1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
End Class
