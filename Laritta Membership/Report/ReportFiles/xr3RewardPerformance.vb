﻿Imports DevExpress.Utils
Imports DevExpress.XtraEditors

Public Class xr3RewardPerformance

    Private Sub ThisReport_BeforePrint(sender As Object, e As Printing.PrintEventArgs) Handles Me.BeforePrint

    End Sub

    Private Sub ThisReport_ParametersRequestBeforeShow(sender As Object, e As DevExpress.XtraReports.Parameters.ParametersRequestEventArgs) Handles Me.ParametersRequestBeforeShow
        Dim Info As DevExpress.XtraReports.Parameters.ParameterInfo

        For Each Info In e.ParametersInformation
            Dim Tanggalan As New DateEdit
            Tanggalan.Properties.Mask.MaskType = Mask.MaskType.DateTime
            Tanggalan.Properties.Mask.EditMask = "dd MMM yyyy"
            Tanggalan.Properties.Mask.UseMaskAsDisplayFormat = True

            Info.Editor = Tanggalan
        Next

        Parameter1.Value = Today.Date
        Parameter2.Value = Today.Date
    End Sub

    Private Sub ThisReport_ParametersRequestSubmit(sender As Object, e As DevExpress.XtraReports.Parameters.ParametersRequestEventArgs) Handles Me.ParametersRequestSubmit
        If Parameter1.Value Is Nothing Or Parameter2.Value Is Nothing Then Return
        LabelPeriode.Text = String.Format("Periode:{0} ~ {1}", Format(Parameter1.Value, "dd MMMM yyyy"), Format(Parameter2.Value, "dd MMMM yyyy"))
        Laporan3TableAdapter.Fill(Set_Laporan1.Laporan3, Format(Parameter1.Value, "yyyy/MM/dd"), Format(Parameter2.Value, "yyyy/MM/dd"))
    End Sub
End Class