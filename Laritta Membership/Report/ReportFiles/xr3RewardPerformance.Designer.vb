﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class xr3RewardPerformance
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim XyDiagram1 As DevExpress.XtraCharts.XYDiagram = New DevExpress.XtraCharts.XYDiagram()
        Dim Series1 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
        Dim SideBySideBarSeriesLabel1 As DevExpress.XtraCharts.SideBySideBarSeriesLabel = New DevExpress.XtraCharts.SideBySideBarSeriesLabel()
        Dim ChartTitle1 As DevExpress.XtraCharts.ChartTitle = New DevExpress.XtraCharts.ChartTitle()
        Dim XyDiagram2 As DevExpress.XtraCharts.XYDiagram = New DevExpress.XtraCharts.XYDiagram()
        Dim Series2 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
        Dim FullStackedBarSeriesLabel1 As DevExpress.XtraCharts.FullStackedBarSeriesLabel = New DevExpress.XtraCharts.FullStackedBarSeriesLabel()
        Dim FullStackedBarSeriesView1 As DevExpress.XtraCharts.FullStackedBarSeriesView = New DevExpress.XtraCharts.FullStackedBarSeriesView()
        Dim Series3 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
        Dim FullStackedBarSeriesLabel2 As DevExpress.XtraCharts.FullStackedBarSeriesLabel = New DevExpress.XtraCharts.FullStackedBarSeriesLabel()
        Dim FullStackedBarSeriesView2 As DevExpress.XtraCharts.FullStackedBarSeriesView = New DevExpress.XtraCharts.FullStackedBarSeriesView()
        Dim FullStackedBarSeriesView3 As DevExpress.XtraCharts.FullStackedBarSeriesView = New DevExpress.XtraCharts.FullStackedBarSeriesView()
        Dim ChartTitle2 As DevExpress.XtraCharts.ChartTitle = New DevExpress.XtraCharts.ChartTitle()
        Dim SimpleDiagram1 As DevExpress.XtraCharts.SimpleDiagram = New DevExpress.XtraCharts.SimpleDiagram()
        Dim Series4 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
        Dim PieSeriesLabel1 As DevExpress.XtraCharts.PieSeriesLabel = New DevExpress.XtraCharts.PieSeriesLabel()
        Dim PieSeriesView1 As DevExpress.XtraCharts.PieSeriesView = New DevExpress.XtraCharts.PieSeriesView()
        Dim SeriesTitle1 As DevExpress.XtraCharts.SeriesTitle = New DevExpress.XtraCharts.SeriesTitle()
        Dim Series5 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
        Dim PieSeriesLabel2 As DevExpress.XtraCharts.PieSeriesLabel = New DevExpress.XtraCharts.PieSeriesLabel()
        Dim PieSeriesView2 As DevExpress.XtraCharts.PieSeriesView = New DevExpress.XtraCharts.PieSeriesView()
        Dim SeriesTitle2 As DevExpress.XtraCharts.SeriesTitle = New DevExpress.XtraCharts.SeriesTitle()
        Dim Series6 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
        Dim PieSeriesLabel3 As DevExpress.XtraCharts.PieSeriesLabel = New DevExpress.XtraCharts.PieSeriesLabel()
        Dim PieSeriesView3 As DevExpress.XtraCharts.PieSeriesView = New DevExpress.XtraCharts.PieSeriesView()
        Dim SeriesTitle3 As DevExpress.XtraCharts.SeriesTitle = New DevExpress.XtraCharts.SeriesTitle()
        Dim PieSeriesView4 As DevExpress.XtraCharts.PieSeriesView = New DevExpress.XtraCharts.PieSeriesView()
        Dim ChartTitle3 As DevExpress.XtraCharts.ChartTitle = New DevExpress.XtraCharts.ChartTitle()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.Set_Laporan1 = New Laritta_Membership.Set_Laporan()
        Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
        Me.LabelPeriode = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrPivotGrid1 = New DevExpress.XtraReports.UI.XRPivotGrid()
        Me.Laporan3TableAdapter = New Laritta_Membership.Set_LaporanTableAdapters.Laporan3TableAdapter()
        Me.fieldTipe1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldReward1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldTaken1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldValue1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldCabang1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldTahun = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldBulan1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldRequest1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldRedeem1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.XrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.XrPageInfo2 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
        Me.Parameter1 = New DevExpress.XtraReports.Parameters.Parameter()
        Me.Parameter2 = New DevExpress.XtraReports.Parameters.Parameter()
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
        Me.XrChart2 = New DevExpress.XtraReports.UI.XRChart()
        Me.XrChart1 = New DevExpress.XtraReports.UI.XRChart()
        Me.XrChart3 = New DevExpress.XtraReports.UI.XRChart()
        CType(Me.Set_Laporan1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrChart2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(XyDiagram1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Series1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(SideBySideBarSeriesLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrChart1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(XyDiagram2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Series2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(FullStackedBarSeriesLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(FullStackedBarSeriesView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Series3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(FullStackedBarSeriesLabel2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(FullStackedBarSeriesView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(FullStackedBarSeriesView3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrChart3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(SimpleDiagram1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Series4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PieSeriesLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PieSeriesView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Series5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PieSeriesLabel2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PieSeriesView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Series6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PieSeriesLabel3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PieSeriesView3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PieSeriesView4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Dpi = 254.0!
        Me.Detail.HeightF = 0.0!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254.0!)
        Me.Detail.StylePriority.UseTextAlignment = False
        '
        'TopMargin
        '
        Me.TopMargin.Dpi = 254.0!
        Me.TopMargin.HeightF = 70.0!
        Me.TopMargin.Name = "TopMargin"
        Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254.0!)
        Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'BottomMargin
        '
        Me.BottomMargin.Dpi = 254.0!
        Me.BottomMargin.HeightF = 70.0!
        Me.BottomMargin.Name = "BottomMargin"
        Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254.0!)
        Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'Set_Laporan1
        '
        Me.Set_Laporan1.DataSetName = "Set_Laporan"
        Me.Set_Laporan1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ReportHeader
        '
        Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.LabelPeriode, Me.XrLabel1})
        Me.ReportHeader.Dpi = 254.0!
        Me.ReportHeader.HeightF = 149.5866!
        Me.ReportHeader.Name = "ReportHeader"
        '
        'LabelPeriode
        '
        Me.LabelPeriode.Dpi = 254.0!
        Me.LabelPeriode.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.LabelPeriode.LocationFloat = New DevExpress.Utils.PointFloat(25.0!, 69.99998!)
        Me.LabelPeriode.Name = "LabelPeriode"
        Me.LabelPeriode.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.LabelPeriode.SizeF = New System.Drawing.SizeF(1969.0!, 58.42001!)
        Me.LabelPeriode.StylePriority.UseFont = False
        Me.LabelPeriode.StylePriority.UseTextAlignment = False
        Me.LabelPeriode.Text = "LabelPeriode"
        Me.LabelPeriode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel1
        '
        Me.XrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrLabel1.Dpi = 254.0!
        Me.XrLabel1.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(25.00001!, 0.0!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(1969.0!, 70.0!)
        Me.XrLabel1.StylePriority.UseBorders = False
        Me.XrLabel1.StylePriority.UseFont = False
        Me.XrLabel1.Text = "Laporan Reward Performance"
        '
        'XrPivotGrid1
        '
        Me.XrPivotGrid1.Appearance.Cell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.XrPivotGrid1.Appearance.CustomTotalCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.XrPivotGrid1.Appearance.FieldHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrPivotGrid1.Appearance.FieldHeader.TextHorizontalAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.XrPivotGrid1.Appearance.FieldHeader.TextVerticalAlignment = DevExpress.Utils.VertAlignment.Center
        Me.XrPivotGrid1.Appearance.FieldValue.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.XrPivotGrid1.Appearance.FieldValue.TextHorizontalAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.XrPivotGrid1.Appearance.FieldValue.WordWrap = True
        Me.XrPivotGrid1.Appearance.FieldValueGrandTotal.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.XrPivotGrid1.Appearance.FieldValueGrandTotal.TextHorizontalAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.XrPivotGrid1.Appearance.FieldValueGrandTotal.TextVerticalAlignment = DevExpress.Utils.VertAlignment.Center
        Me.XrPivotGrid1.Appearance.FieldValueTotal.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.XrPivotGrid1.Appearance.FieldValueTotal.TextHorizontalAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.XrPivotGrid1.Appearance.FieldValueTotal.TextVerticalAlignment = DevExpress.Utils.VertAlignment.Center
        Me.XrPivotGrid1.Appearance.GrandTotalCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.XrPivotGrid1.Appearance.Lines.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.XrPivotGrid1.Appearance.TotalCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.XrPivotGrid1.Appearance.TotalCell.TextHorizontalAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.XrPivotGrid1.Appearance.TotalCell.TextVerticalAlignment = DevExpress.Utils.VertAlignment.Center
        Me.XrPivotGrid1.DataAdapter = Me.Laporan3TableAdapter
        Me.XrPivotGrid1.DataMember = "Laporan3"
        Me.XrPivotGrid1.DataSource = Me.Set_Laporan1
        Me.XrPivotGrid1.Dpi = 254.0!
        Me.XrPivotGrid1.Fields.AddRange(New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField() {Me.fieldTipe1, Me.fieldReward1, Me.fieldTaken1, Me.fieldValue1, Me.fieldCabang1, Me.fieldTahun, Me.fieldBulan1, Me.fieldRequest1, Me.fieldRedeem1})
        Me.XrPivotGrid1.LocationFloat = New DevExpress.Utils.PointFloat(25.00009!, 58.42004!)
        Me.XrPivotGrid1.Name = "XrPivotGrid1"
        Me.XrPivotGrid1.OptionsPrint.FilterSeparatorBarPadding = 3
        Me.XrPivotGrid1.OptionsView.ShowColumnHeaders = False
        Me.XrPivotGrid1.OptionsView.ShowDataHeaders = False
        Me.XrPivotGrid1.SizeF = New System.Drawing.SizeF(2604.0!, 337.75!)
        '
        'Laporan3TableAdapter
        '
        Me.Laporan3TableAdapter.ClearBeforeFill = True
        '
        'fieldTipe1
        '
        Me.fieldTipe1.AreaIndex = 0
        Me.fieldTipe1.Caption = "Tipe"
        Me.fieldTipe1.FieldName = "Tipe"
        Me.fieldTipe1.Name = "fieldTipe1"
        Me.fieldTipe1.Visible = False
        '
        'fieldReward1
        '
        Me.fieldReward1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea
        Me.fieldReward1.AreaIndex = 0
        Me.fieldReward1.Caption = "Reward"
        Me.fieldReward1.FieldName = "Reward"
        Me.fieldReward1.Name = "fieldReward1"
        Me.fieldReward1.Width = 250
        '
        'fieldTaken1
        '
        Me.fieldTaken1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea
        Me.fieldTaken1.AreaIndex = 2
        Me.fieldTaken1.Caption = "Taken"
        Me.fieldTaken1.CellFormat.FormatString = "n0"
        Me.fieldTaken1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldTaken1.FieldName = "Taken"
        Me.fieldTaken1.Name = "fieldTaken1"
        Me.fieldTaken1.TotalCellFormat.FormatString = "n0"
        Me.fieldTaken1.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldTaken1.TotalValueFormat.FormatString = "n0"
        Me.fieldTaken1.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldTaken1.ValueFormat.FormatString = "n0"
        Me.fieldTaken1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldTaken1.Width = 80
        '
        'fieldValue1
        '
        Me.fieldValue1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea
        Me.fieldValue1.AreaIndex = 3
        Me.fieldValue1.Caption = "Value"
        Me.fieldValue1.CellFormat.FormatString = "n0"
        Me.fieldValue1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldValue1.FieldName = "Value"
        Me.fieldValue1.Name = "fieldValue1"
        Me.fieldValue1.TotalCellFormat.FormatString = "n0"
        Me.fieldValue1.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldValue1.TotalValueFormat.FormatString = "n0"
        Me.fieldValue1.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldValue1.ValueFormat.FormatString = "n0"
        Me.fieldValue1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        '
        'fieldCabang1
        '
        Me.fieldCabang1.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea
        Me.fieldCabang1.AreaIndex = 0
        Me.fieldCabang1.Caption = "Outlet"
        Me.fieldCabang1.FieldName = "Cabang"
        Me.fieldCabang1.Name = "fieldCabang1"
        '
        'fieldTahun
        '
        Me.fieldTahun.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea
        Me.fieldTahun.AreaIndex = 1
        Me.fieldTahun.Caption = "Year"
        Me.fieldTahun.CellFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.fieldTahun.FieldName = "Tahun"
        Me.fieldTahun.Name = "fieldTahun"
        Me.fieldTahun.Options.ShowCustomTotals = False
        Me.fieldTahun.Options.ShowTotals = False
        Me.fieldTahun.ValueFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        '
        'fieldBulan1
        '
        Me.fieldBulan1.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea
        Me.fieldBulan1.AreaIndex = 2
        Me.fieldBulan1.Caption = "Month"
        Me.fieldBulan1.CellFormat.FormatString = "MMMM"
        Me.fieldBulan1.CellFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.fieldBulan1.FieldName = "Bulan"
        Me.fieldBulan1.Name = "fieldBulan1"
        Me.fieldBulan1.Options.ShowCustomTotals = False
        Me.fieldBulan1.Options.ShowTotals = False
        Me.fieldBulan1.ValueFormat.FormatString = "MMMM"
        Me.fieldBulan1.ValueFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        '
        'fieldRequest1
        '
        Me.fieldRequest1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea
        Me.fieldRequest1.AreaIndex = 0
        Me.fieldRequest1.Caption = "Request"
        Me.fieldRequest1.CellFormat.FormatString = "n0"
        Me.fieldRequest1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldRequest1.FieldName = "Request"
        Me.fieldRequest1.Name = "fieldRequest1"
        Me.fieldRequest1.TotalCellFormat.FormatString = "n0"
        Me.fieldRequest1.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldRequest1.TotalValueFormat.FormatString = "n0"
        Me.fieldRequest1.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldRequest1.ValueFormat.FormatString = "n0"
        Me.fieldRequest1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldRequest1.Width = 60
        '
        'fieldRedeem1
        '
        Me.fieldRedeem1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea
        Me.fieldRedeem1.AreaIndex = 1
        Me.fieldRedeem1.Caption = "Redeem"
        Me.fieldRedeem1.CellFormat.FormatString = "n0"
        Me.fieldRedeem1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldRedeem1.FieldName = "Redeem"
        Me.fieldRedeem1.Name = "fieldRedeem1"
        Me.fieldRedeem1.TotalCellFormat.FormatString = "n0"
        Me.fieldRedeem1.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldRedeem1.TotalValueFormat.FormatString = "n0"
        Me.fieldRedeem1.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldRedeem1.ValueFormat.FormatString = "n0"
        Me.fieldRedeem1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldRedeem1.Width = 60
        '
        'XrPageInfo1
        '
        Me.XrPageInfo1.Dpi = 254.0!
        Me.XrPageInfo1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrPageInfo1.Format = "Tgl cetak : {0:d MMMM yyyy, HH:mm}"
        Me.XrPageInfo1.LocationFloat = New DevExpress.Utils.PointFloat(25.00001!, 0.0!)
        Me.XrPageInfo1.Name = "XrPageInfo1"
        Me.XrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime
        Me.XrPageInfo1.SizeF = New System.Drawing.SizeF(865.1874!, 58.42001!)
        Me.XrPageInfo1.StylePriority.UseFont = False
        Me.XrPageInfo1.StylePriority.UseTextAlignment = False
        Me.XrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrPageInfo2
        '
        Me.XrPageInfo2.Dpi = 254.0!
        Me.XrPageInfo2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrPageInfo2.LocationFloat = New DevExpress.Utils.PointFloat(2400.0!, 0.0!)
        Me.XrPageInfo2.Name = "XrPageInfo2"
        Me.XrPageInfo2.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrPageInfo2.SizeF = New System.Drawing.SizeF(254.0!, 58.42!)
        Me.XrPageInfo2.StylePriority.UseFont = False
        Me.XrPageInfo2.StylePriority.UseTextAlignment = False
        Me.XrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPageInfo2, Me.XrPageInfo1})
        Me.PageFooter.Dpi = 254.0!
        Me.PageFooter.HeightF = 58.42001!
        Me.PageFooter.Name = "PageFooter"
        '
        'Parameter1
        '
        Me.Parameter1.Description = "Tgl Start"
        Me.Parameter1.Name = "Parameter1"
        Me.Parameter1.Type = GetType(Date)
        Me.Parameter1.ValueInfo = "2015-05-01"
        '
        'Parameter2
        '
        Me.Parameter2.Description = "Tgl End"
        Me.Parameter2.Name = "Parameter2"
        Me.Parameter2.Type = GetType(Date)
        Me.Parameter2.ValueInfo = "2015-05-01"
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPivotGrid1, Me.XrLabel2})
        Me.GroupHeader1.Dpi = 254.0!
        Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("Tipe", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
        Me.GroupHeader1.HeightF = 421.17!
        Me.GroupHeader1.Name = "GroupHeader1"
        '
        'XrLabel2
        '
        Me.XrLabel2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Laporan3.Tipe", "Tipe : {0}")})
        Me.XrLabel2.Dpi = 254.0!
        Me.XrLabel2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(25.00001!, 0.0!)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(1969.0!, 58.42!)
        Me.XrLabel2.StylePriority.UseFont = False
        '
        'GroupFooter1
        '
        Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrChart2, Me.XrChart1, Me.XrChart3})
        Me.GroupFooter1.Dpi = 254.0!
        Me.GroupFooter1.HeightF = 2000.0!
        Me.GroupFooter1.Name = "GroupFooter1"
        '
        'XrChart2
        '
        Me.XrChart2.AnchorHorizontal = CType((DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left Or DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right), DevExpress.XtraReports.UI.HorizontalAnchorStyles)
        Me.XrChart2.BorderColor = System.Drawing.Color.Black
        Me.XrChart2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrChart2.DataAdapter = Me.Laporan3TableAdapter
        Me.XrChart2.DataMember = "Laporan3"
        Me.XrChart2.DataSource = Me.Set_Laporan1
        XyDiagram1.AxisX.VisibleInPanesSerializable = "-1"
        XyDiagram1.AxisY.VisibleInPanesSerializable = "-1"
        XyDiagram1.DefaultPane.EnableAxisXScrolling = DevExpress.Utils.DefaultBoolean.[False]
        XyDiagram1.DefaultPane.EnableAxisXZooming = DevExpress.Utils.DefaultBoolean.[False]
        XyDiagram1.DefaultPane.EnableAxisYScrolling = DevExpress.Utils.DefaultBoolean.[False]
        XyDiagram1.DefaultPane.EnableAxisYZooming = DevExpress.Utils.DefaultBoolean.[False]
        Me.XrChart2.Diagram = XyDiagram1
        Me.XrChart2.Dpi = 254.0!
        Me.XrChart2.Legend.AlignmentHorizontal = DevExpress.XtraCharts.LegendAlignmentHorizontal.LeftOutside
        Me.XrChart2.Legend.UseCheckBoxes = True
        Me.XrChart2.Legend.Visibility = DevExpress.Utils.DefaultBoolean.[False]
        Me.XrChart2.LocationFloat = New DevExpress.Utils.PointFloat(12.5!, 1300.0!)
        Me.XrChart2.Name = "XrChart2"
        Series1.ArgumentDataMember = "Reward"
        Series1.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Qualitative
        SideBySideBarSeriesLabel1.TextPattern = "{V:n0}"
        Series1.Label = SideBySideBarSeriesLabel1
        Series1.LegendText = "Value"
        Series1.Name = "Value"
        Series1.SummaryFunction = "SUM([Value])"
        Series1.TopNOptions.Count = 10
        Series1.TopNOptions.Enabled = True
        Series1.TopNOptions.OthersArgument = "Others"
        Me.XrChart2.SeriesSerializable = New DevExpress.XtraCharts.Series() {Series1}
        Me.XrChart2.SeriesTemplate.ArgumentDataMember = "asal"
        Me.XrChart2.SizeF = New System.Drawing.SizeF(2629.0!, 700.0!)
        Me.XrChart2.StylePriority.UseBorders = False
        ChartTitle1.Dock = DevExpress.XtraCharts.ChartTitleDockStyle.Bottom
        ChartTitle1.Font = New System.Drawing.Font("Tahoma", 14.0!)
        ChartTitle1.Text = "Value"
        Me.XrChart2.Titles.AddRange(New DevExpress.XtraCharts.ChartTitle() {ChartTitle1})
        '
        'XrChart1
        '
        Me.XrChart1.AnchorHorizontal = CType((DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left Or DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right), DevExpress.XtraReports.UI.HorizontalAnchorStyles)
        Me.XrChart1.BorderColor = System.Drawing.Color.Black
        Me.XrChart1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrChart1.DataAdapter = Me.Laporan3TableAdapter
        Me.XrChart1.DataMember = "Laporan3"
        Me.XrChart1.DataSource = Me.Set_Laporan1
        XyDiagram2.AxisX.VisibleInPanesSerializable = "-1"
        XyDiagram2.AxisY.VisibleInPanesSerializable = "-1"
        XyDiagram2.DefaultPane.EnableAxisXScrolling = DevExpress.Utils.DefaultBoolean.[False]
        XyDiagram2.DefaultPane.EnableAxisXZooming = DevExpress.Utils.DefaultBoolean.[False]
        XyDiagram2.DefaultPane.EnableAxisYScrolling = DevExpress.Utils.DefaultBoolean.[False]
        XyDiagram2.DefaultPane.EnableAxisYZooming = DevExpress.Utils.DefaultBoolean.[False]
        Me.XrChart1.Diagram = XyDiagram2
        Me.XrChart1.Dpi = 254.0!
        Me.XrChart1.Legend.Direction = DevExpress.XtraCharts.LegendDirection.BottomToTop
        Me.XrChart1.Legend.Visibility = DevExpress.Utils.DefaultBoolean.[True]
        Me.XrChart1.LocationFloat = New DevExpress.Utils.PointFloat(12.5!, 600.0!)
        Me.XrChart1.Name = "XrChart1"
        Series2.ArgumentDataMember = "Reward"
        Series2.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Qualitative
        FullStackedBarSeriesLabel1.TextPattern = "{V:n0}"
        Series2.Label = FullStackedBarSeriesLabel1
        Series2.LegendText = "Request"
        Series2.Name = "Request"
        Series2.SummaryFunction = "SUM([Request])"
        Series2.TopNOptions.Count = 10
        Series2.TopNOptions.Enabled = True
        Series2.TopNOptions.OthersArgument = "Others"
        Series2.View = FullStackedBarSeriesView1
        Series3.ArgumentDataMember = "Reward"
        Series3.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Qualitative
        FullStackedBarSeriesLabel2.TextPattern = "{V:n0}"
        Series3.Label = FullStackedBarSeriesLabel2
        Series3.LegendText = "Redeem"
        Series3.Name = "Redeem"
        Series3.SummaryFunction = "SUM([Redeem])"
        Series3.TopNOptions.Count = 10
        Series3.TopNOptions.Enabled = True
        Series3.TopNOptions.OthersArgument = "Others"
        Series3.View = FullStackedBarSeriesView2
        Me.XrChart1.SeriesSerializable = New DevExpress.XtraCharts.Series() {Series2, Series3}
        Me.XrChart1.SeriesTemplate.ArgumentDataMember = "asal"
        Me.XrChart1.SeriesTemplate.View = FullStackedBarSeriesView3
        Me.XrChart1.SizeF = New System.Drawing.SizeF(2629.0!, 700.0!)
        Me.XrChart1.StylePriority.UseBorders = False
        ChartTitle2.Dock = DevExpress.XtraCharts.ChartTitleDockStyle.Bottom
        ChartTitle2.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ChartTitle2.Text = "Request vs Redeem"
        Me.XrChart1.Titles.AddRange(New DevExpress.XtraCharts.ChartTitle() {ChartTitle2})
        '
        'XrChart3
        '
        Me.XrChart3.AnchorHorizontal = CType((DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left Or DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right), DevExpress.XtraReports.UI.HorizontalAnchorStyles)
        Me.XrChart3.BorderColor = System.Drawing.Color.Black
        Me.XrChart3.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrChart3.DataAdapter = Me.Laporan3TableAdapter
        Me.XrChart3.DataMember = "Laporan3"
        Me.XrChart3.DataSource = Me.Set_Laporan1
        SimpleDiagram1.EqualPieSize = False
        Me.XrChart3.Diagram = SimpleDiagram1
        Me.XrChart3.Dpi = 254.0!
        Me.XrChart3.Legend.AlignmentHorizontal = DevExpress.XtraCharts.LegendAlignmentHorizontal.LeftOutside
        Me.XrChart3.Legend.UseCheckBoxes = True
        Me.XrChart3.Legend.Visibility = DevExpress.Utils.DefaultBoolean.[False]
        Me.XrChart3.LocationFloat = New DevExpress.Utils.PointFloat(12.5!, 0.0!)
        Me.XrChart3.Name = "XrChart3"
        Series4.ArgumentDataMember = "Cabang"
        Series4.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Qualitative
        PieSeriesLabel1.TextPattern = "{A}:{VP:0%}"
        Series4.Label = PieSeriesLabel1
        Series4.LegendText = "Request"
        Series4.Name = "Request"
        Series4.SummaryFunction = "SUM([Request])"
        Series4.TopNOptions.Count = 6
        Series4.TopNOptions.ShowOthers = False
        PieSeriesView1.RuntimeExploding = False
        PieSeriesView1.SweepDirection = DevExpress.XtraCharts.PieSweepDirection.Counterclockwise
        PieSeriesView1.Titles.AddRange(New DevExpress.XtraCharts.SeriesTitle() {SeriesTitle1})
        Series4.View = PieSeriesView1
        Series5.ArgumentDataMember = "Cabang"
        Series5.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Qualitative
        PieSeriesLabel2.TextPattern = "{A}:{VP:0%}"
        Series5.Label = PieSeriesLabel2
        Series5.LegendText = "Redeem"
        Series5.Name = "Redeem"
        Series5.SummaryFunction = "SUM([Redeem])"
        Series5.TopNOptions.Count = 6
        Series5.TopNOptions.ShowOthers = False
        PieSeriesView2.RuntimeExploding = False
        PieSeriesView2.SweepDirection = DevExpress.XtraCharts.PieSweepDirection.Counterclockwise
        PieSeriesView2.Titles.AddRange(New DevExpress.XtraCharts.SeriesTitle() {SeriesTitle2})
        Series5.View = PieSeriesView2
        Series6.ArgumentDataMember = "Cabang"
        Series6.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Qualitative
        PieSeriesLabel3.TextPattern = "{A}:{VP:0%}"
        Series6.Label = PieSeriesLabel3
        Series6.LegendText = "Value"
        Series6.Name = "Value"
        Series6.SummaryFunction = "SUM([Value])"
        PieSeriesView3.RuntimeExploding = False
        PieSeriesView3.SweepDirection = DevExpress.XtraCharts.PieSweepDirection.Counterclockwise
        PieSeriesView3.Titles.AddRange(New DevExpress.XtraCharts.SeriesTitle() {SeriesTitle3})
        Series6.View = PieSeriesView3
        Me.XrChart3.SeriesSerializable = New DevExpress.XtraCharts.Series() {Series4, Series5, Series6}
        Me.XrChart3.SeriesTemplate.ArgumentDataMember = "asal"
        PieSeriesView4.RuntimeExploding = False
        PieSeriesView4.SweepDirection = DevExpress.XtraCharts.PieSweepDirection.Counterclockwise
        Me.XrChart3.SeriesTemplate.View = PieSeriesView4
        Me.XrChart3.SizeF = New System.Drawing.SizeF(2629.0!, 600.0!)
        Me.XrChart3.StylePriority.UseBorders = False
        ChartTitle3.Text = ""
        Me.XrChart3.Titles.AddRange(New DevExpress.XtraCharts.ChartTitle() {ChartTitle3})
        '
        'xr3RewardPerformance
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.ReportHeader, Me.PageFooter, Me.GroupHeader1, Me.GroupFooter1})
        Me.Bookmark = "Reward Performance"
        Me.DataMember = "Laporan3"
        Me.DataSource = Me.Set_Laporan1
        Me.DisplayName = "Reward Performance"
        Me.Dpi = 254.0!
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Landscape = True
        Me.Margins = New System.Drawing.Printing.Margins(70, 70, 70, 70)
        Me.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.PageHeight = 2159
        Me.PageWidth = 2794
        Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.Parameter1, Me.Parameter2})
        Me.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.ShowPrintMarginsWarning = False
        Me.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.Version = "14.2"
        Me.VerticalContentSplitting = DevExpress.XtraPrinting.VerticalContentSplitting.Smart
        CType(Me.Set_Laporan1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(XyDiagram1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(SideBySideBarSeriesLabel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Series1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrChart2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(XyDiagram2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(FullStackedBarSeriesLabel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(FullStackedBarSeriesView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Series2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(FullStackedBarSeriesLabel2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(FullStackedBarSeriesView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Series3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(FullStackedBarSeriesView3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrChart1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(SimpleDiagram1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PieSeriesLabel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PieSeriesView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Series4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PieSeriesLabel2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PieSeriesView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Series5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PieSeriesLabel3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PieSeriesView3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Series6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PieSeriesView4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrChart3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents Set_Laporan1 As Laritta_Membership.Set_Laporan
    Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
    Friend WithEvents XrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPageInfo2 As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Friend WithEvents XrPivotGrid1 As DevExpress.XtraReports.UI.XRPivotGrid
    Friend WithEvents Parameter1 As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents Parameter2 As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents LabelPeriode As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents Laporan3TableAdapter As Laritta_Membership.Set_LaporanTableAdapters.Laporan3TableAdapter
    Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents fieldTipe1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldReward1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldTaken1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldValue1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldCabang1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldBulan1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldRequest1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldRedeem1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
    Friend WithEvents XrChart3 As DevExpress.XtraReports.UI.XRChart
    Friend WithEvents fieldTahun As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents XrChart1 As DevExpress.XtraReports.UI.XRChart
    Friend WithEvents XrChart2 As DevExpress.XtraReports.UI.XRChart
End Class
