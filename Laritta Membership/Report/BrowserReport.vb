﻿Imports DevExpress.XtraReports.UI
Imports DevExpress.Utils

Public Class BrowserReport
    Dim T As Type

    Private Sub BrowserReport_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("DaftarReport")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub BrowserReport_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        SQLquery = "SELECT rep_id ID, rep_kategori Kategori, rep_name Nama, rep_info Info, rep_vbfile FILEREPORT FROM mbsm_reportlist ORDER BY rep_kategori, rep_name;"
        'SQLquery = String.Format("SELECT a.idreport ID, namareport Nama, judulreport Report, ISNULL(b.AllowRead,0) AllowRead " & _
        '            "FROM m_report a LEFT JOIN PermissionsReport b ON a.idReport=b.idReport AND b.id_securitygroup={0} " & _
        '            "WHERE a.status=1 ", staff_group)
        ExDb.ExecQuery(1, SQLquery, xSet, "DaftarReport")

        GridControl1.DataSource = xSet.Tables("DaftarReport").DefaultView

        GridView1.Columns("ID").Visible = False
        GridView1.Columns("FILEREPORT").Visible = False
        GridView1.Columns("Info").ColumnEdit = RepoMemo

        GridView1.Columns("Kategori").Width = GridControl1.Width * 0.2
        GridView1.Columns("Nama").Width = GridControl1.Width * 0.3
        GridView1.Columns("Info").Width = GridControl1.Width * 0.5

        For Each coll As DataColumn In xSet.Tables("DaftarReport").Columns
            GridView1.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Next

        AddHandler GridView1.DoubleClick, AddressOf ButShow_Click
    End Sub

    Private Sub butRefresh_Click(sender As Object, e As EventArgs) Handles butRefresh.Click
        SQLquery = "SELECT rep_id ID, rep_kategori Kategori, rep_name Nama, rep_info Info, rep_vbfile FILEREPORT FROM mbsm_reportlist ORDER BY rep_kategori, rep_name;"
        'SQLquery = String.Format("SELECT a.idreport ID, namareport Nama, judulreport Report, ISNULL(b.AllowRead,0) AllowRead " & _
        '            "FROM m_report a LEFT JOIN PermissionsReport b ON a.idReport=b.idReport AND b.id_securitygroup={0} " & _
        '            "WHERE a.status=1 ", staff_group)
        ExDb.ExecQuery(1, SQLquery, xSet, "DaftarReport")
    End Sub

    Private Sub ButShow_Click(sender As Object, e As EventArgs) Handles ButShow.Click
        'If ViewDaftarReport.GetFocusedRowCellValue("AllowRead") = False Then
        '    msgboxWarning("Security Group anda tidak memiliki hak untuk mengakses report ini")
        'Else

        T = Type.GetType(Replace(Reflection.Assembly.GetExecutingAssembly.GetName.Name & "." & GridView1.GetFocusedRowCellValue("FILEREPORT"), " ", "_"))
        Dim f = DirectCast(Activator.CreateInstance(T), XtraReport)
        Try
            Dim thisReport As ReportPrintTool = New ReportPrintTool(f)
            thisReport.ShowRibbonPreviewDialog()
        Catch ex As Exception
            MsgErrorDev()
        Finally
            f.Dispose()
        End Try

        'End If
    End Sub

    Private Sub butClose_Click(sender As Object, e As EventArgs) Handles butClose.Click
        Close()
    End Sub
End Class