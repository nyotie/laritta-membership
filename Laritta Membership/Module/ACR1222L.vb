﻿
Namespace RFID_Tools

    Public Class ACR1222L
        Public Property ReaderEstablished As Boolean
        Public Property CardConnected As Boolean
        Public Property ErrorInfo As String
        Public Property ReaderName As String
        Public Property ReaderList As String()
        Public Property MyContext As IntPtr = New IntPtr 'context is filled at establish reader lists
        Public Property ReturnCode As Integer

        Public Property TrxByte As Byte()
        Private RcxByte As Byte()

        Private ReaderCount, SendLength, ReceivedLength As Integer
        Private hCard As IntPtr = New IntPtr

        Private pProtocol As Integer = (ModWinsCard.SCARD_PROTOCOL_T0 Or ModWinsCard.SCARD_PROTOCOL_T1)
        Private shareMode As Integer = ModWinsCard.SCARD_SHARE_SHARED
        Private pdwActiveProtocol As Integer
        Private ReadOnly operationControlCode As UInteger = 0

        Public Sub New()
            MyBase.New()
            operationControlCode = (CType(ModWinsCard.FILE_DEVICE_SMARTCARD, UInteger) + (3500 * 4))
        End Sub

        Public Sub New(ByVal rdrName As String)
            MyBase.New()
            ReaderName = rdrName
            operationControlCode = (CType(ModWinsCard.FILE_DEVICE_SMARTCARD, UInteger) + (3500 * 4))
        End Sub

#Region "Private Area"
        Private Sub ClearBuffers()
            If TrxByte.Length > 0 Then Array.Clear(TrxByte, 0, TrxByte.Length)
            If RcxByte.Length > 0 Then Array.Clear(RcxByte, 0, RcxByte.Length)
        End Sub

        Private Function ExecScardControl() As Boolean
            Try
                Dim pioSendRequest As ModWinsCard.SCARD_IO_REQUEST
                Dim tmpStr As String = ""

                pioSendRequest.dwProtocol = 0
                pioSendRequest.cbPciLength = 8

                ReturnCode = ModWinsCard.SCardControl(hCard, operationControlCode, TrxByte, SendLength, RcxByte, ReceivedLength, 0)
                If ReturnCode <> ModWinsCard.SCARD_S_SUCCESS Then
                    Throw New RFID_Exception(ReturnCode, ReaderEstablished, CardConnected, ErrorInfo)
                Else
                    For i As Integer = 0 To ReceivedLength - 1
                        tmpStr = tmpStr & String.Format("{0:X2}", RcxByte(i))
                        If i <> ReceivedLength - 1 Then tmpStr &= " "
                    Next
                End If

                If tmpStr = "90 00" Then Return True Else Return False
            Catch ex As Exception
                Return False
            End Try
        End Function
#End Region

#Region "Connection"
        Public Sub Establish(ByRef OutherContext As IntPtr)
            Dim ReturnData As Byte()
            Dim ReturnString As String = ""

            ' Connect to selected reader using hContext handle and obtain hCard handle
            ReturnCode = ModWinsCard.SCardEstablishContext(ModWinsCard.SCARD_SCOPE_USER, 0, 0, MyContext)
            If Not ReturnCode = ModWinsCard.SCARD_S_SUCCESS Then Throw New RFID_Exception(ReturnCode, ReaderEstablished, CardConnected, ErrorInfo)

            ReturnCode = ModWinsCard.SCardListReaders(MyContext, Nothing, Nothing, ReaderCount)
            If Not ReturnCode = ModWinsCard.SCARD_S_SUCCESS Then Throw New RFID_Exception(ReturnCode, ReaderEstablished, CardConnected, ErrorInfo)

            ReturnData = New Byte(ReaderCount) {}
            ReturnCode = ModWinsCard.SCardListReaders(MyContext, Nothing, ReturnData, ReaderCount)

            For i As Integer = 0 To ReaderCount - 3
                ReturnString &= IIf(ReturnData(i) = 0, "\", CType(ChrW(ReturnData(i)), Char))
            Next

            ReaderList = ReturnString.Split("\")
            OutherContext = MyContext
            ReaderEstablished = True
        End Sub

        Public Sub EasyConnection()
            If MyContext = 0 Or Not ReaderEstablished Then Throw New RFID_Exception(ReturnCode, ReaderEstablished, CardConnected, ErrorInfo)
            Connect(MyContext, ReaderName, ModWinsCard.SCARD_SHARE_DIRECT, ModWinsCard.SCARD_PROTOCOL_UNDEFINED)
        End Sub

        Public Overloads Sub Connect(ByVal tempHContext As IntPtr, ByVal tempReaderName As String, ByVal tempShareMode As Integer, ByVal tempPreferedProtocol As Integer)
            If Not ReaderEstablished Then Throw New RFID_Exception(ReturnCode, ReaderEstablished, CardConnected, ErrorInfo)
            ReturnCode = ModWinsCard.SCardConnect(tempHContext, tempReaderName, tempShareMode, tempPreferedProtocol, hCard, pdwActiveProtocol)
            If ReturnCode <> ModWinsCard.SCARD_S_SUCCESS Then Throw New RFID_Exception(ReturnCode, ReaderEstablished, CardConnected, ErrorInfo)

            CardConnected = True
            ReaderName = tempReaderName
            shareMode = tempShareMode
            pProtocol = tempPreferedProtocol
        End Sub

        Public Sub Disconnect()
            If CardConnected Then
                ReturnCode = ModWinsCard.SCardDisconnect(hCard, ModWinsCard.SCARD_UNPOWER_CARD)
                If ReturnCode <> ModWinsCard.SCARD_S_SUCCESS Then
                    Throw New RFID_Exception(ReturnCode, ReaderEstablished, CardConnected, ErrorInfo)
                End If
            End If

            ReaderEstablished = False
            CardConnected = False
            MyContext = 0
        End Sub
#End Region

#Region "LCD"
        Public Sub Backlight(ByVal TurnON As Boolean)
            TrxByte = New Byte(4) {}
            RcxByte = New Byte(2) {}

            TrxByte(0) = &HFF
            TrxByte(1) = &H0
            TrxByte(2) = &H64
            TrxByte(3) = CType(TurnON, Byte)
            TrxByte(4) = &H0

            SendLength = 5
            ReceivedLength = 2

            If Not ExecScardControl() Then Throw New RFID_Exception(ReturnCode, ReaderEstablished, CardConnected, ErrorInfo)
        End Sub

        Public Sub ClearLCD()
            TrxByte = New Byte(4) {}
            RcxByte = New Byte(2) {}

            TrxByte(0) = &HFF
            TrxByte(1) = &H0
            TrxByte(2) = &H60
            TrxByte(3) = &H0
            TrxByte(4) = &H0

            SendLength = 5
            ReceivedLength = 2

            If Not ExecScardControl() Then Throw New RFID_Exception(ReturnCode, ReaderEstablished, CardConnected, ErrorInfo)
        End Sub

        Public Sub DisplayLCD(ByVal LineNum As Integer, ByVal strmsg As String)
            Dim msg() As Byte = System.Text.ASCIIEncoding.ASCII.GetBytes(strmsg)
            If (msg.Length > 16) Then Array.Resize(msg, 16)

            TrxByte = New Byte(4 + msg.Length) {}
            RcxByte = New Byte(2) {}

            TrxByte(0) = &HFF
            TrxByte(1) = &H0
            TrxByte(2) = &H68
            TrxByte(3) = IIf(LineNum = 1, 0, 64)
            TrxByte(4) = CType(msg.Length, Byte)

            Array.Copy(msg, 0, TrxByte, 5, msg.Length)
            SendLength = TrxByte.Length
            ReceivedLength = 2

            If Not ExecScardControl() Then Throw New RFID_Exception(ReturnCode, ReaderEstablished, CardConnected, ErrorInfo)
        End Sub
#End Region

    End Class

    Public Class MiFareCard
        Public Property ReaderEstablished As Boolean
        Public Property CardConnected As Boolean
        Public Property ErrorInfo As String
        Public Property ReaderName As String
        Public Property ReaderList As String()
        Public Property ReturnCode As Integer

        Public Property MyContext As IntPtr = New IntPtr 'context is filled at establish reader lists

        Public Property SendBuff As Byte() = New Byte((263) - 1) {}
        Public Property RecvBuff As Byte() = New Byte((263) - 1) {}

        Public ReaderCount, SendLength, ReceivedLength As Integer

        Private hCard As IntPtr = New IntPtr
        Private ActiveProtocol As Integer = 0

        Public Sub New()
            MyBase.New()
        End Sub

        Public Sub New(ByVal rdrName As String)
            MyBase.New()
            ReaderName = rdrName
        End Sub

#Region "Private Area"
        Private Sub ClearBuffers()
            Array.Clear(RecvBuff, 0, RecvBuff.Length)
            Array.Clear(SendBuff, 0, SendBuff.Length)
        End Sub

        Private Function ExecScardTransmit(ByVal reqType As Integer) As Boolean
            Dim pioSendRequest As ModWinsCard.SCARD_IO_REQUEST
            Dim indx As Integer = 0
            Dim tmpStr As String = ""

            pioSendRequest.dwProtocol = ActiveProtocol
            pioSendRequest.cbPciLength = 8

            ReturnCode = ModWinsCard.SCardTransmit(hCard, pioSendRequest, SendBuff, SendLength, pioSendRequest, RecvBuff, ReceivedLength)
            If (ReturnCode <> ModWinsCard.SCARD_S_SUCCESS) Then
                Return False
            Else
                Select Case reqType
                    Case 0
                        indx = (ReceivedLength - 2)
                        Do While (indx <= (ReceivedLength - 1))
                            tmpStr = (tmpStr + (" " + String.Format("{0:X2}", RecvBuff(indx))))
                            indx = (indx + 1)
                        Loop
                    Case 1
                        indx = (ReceivedLength - 2)
                        Do While (indx <= (ReceivedLength - 1))
                            tmpStr = (tmpStr + String.Format("{0:X2}", RecvBuff(indx)))
                            indx = (indx + 1)
                        Loop
                    Case 2
                        For i As Integer = ReceivedLength - 2 To ReceivedLength - 1
                            tmpStr = (tmpStr + (" " + String.Format("{0:X2}", RecvBuff(i))))
                        Next
                End Select
            End If

            If (tmpStr = " 90 00") Then Return True Else Return False
        End Function
#End Region

#Region "Connection Area"
        Public Sub Establish(ByRef OutherContext As IntPtr)
            Dim ReturnData As Byte()
            Dim ReturnString As String = ""

            ' Connect to selected reader using hContext handle and obtain hCard handle
            ReturnCode = ModWinsCard.SCardEstablishContext(ModWinsCard.SCARD_SCOPE_USER, 0, 0, MyContext)
            If Not ReturnCode = ModWinsCard.SCARD_S_SUCCESS Then Throw New RFID_Exception(ReturnCode, ReaderEstablished, CardConnected, ErrorInfo)

            ReturnCode = ModWinsCard.SCardListReaders(MyContext, Nothing, Nothing, ReaderCount)
            If Not ReturnCode = ModWinsCard.SCARD_S_SUCCESS Then Throw New RFID_Exception(ReturnCode, ReaderEstablished, CardConnected, ErrorInfo)

            ReturnData = New Byte(ReaderCount) {}
            ReturnCode = ModWinsCard.SCardListReaders(MyContext, Nothing, ReturnData, ReaderCount)

            For i As Integer = 0 To ReaderCount - 3
                ReturnString &= IIf(ReturnData(i) = 0, "\", CType(ChrW(ReturnData(i)), Char))
            Next

            ReaderList = ReturnString.Split("\")
            OutherContext = MyContext
            ReaderEstablished = True
        End Sub

        Public Sub EasyConnection()
            If MyContext = 0 Or Not ReaderEstablished Then Throw New RFID_Exception(ReturnCode, ReaderEstablished, CardConnected, ErrorInfo)
            Connect(MyContext, ReaderName)
        End Sub

        Public Sub Connect(ByVal tempHContext As IntPtr, ByVal tempReaderName As String)
            If CardConnected Then ReturnCode = ModWinsCard.SCardDisconnect(hCard, ModWinsCard.SCARD_UNPOWER_CARD)
            ReturnCode = ModWinsCard.SCardConnect(tempHContext, tempReaderName, ModWinsCard.SCARD_SHARE_EXCLUSIVE, ModWinsCard.SCARD_PROTOCOL_T1, hCard, ActiveProtocol)
            If Not ReturnCode = ModWinsCard.SCARD_S_SUCCESS Then Throw New RFID_Exception(ReturnCode, ReaderEstablished, CardConnected, ErrorInfo)

            CardConnected = True
        End Sub

        Public Sub Disconnect()
            If CardConnected Then
                ReturnCode = ModWinsCard.SCardDisconnect(hCard, ModWinsCard.SCARD_UNPOWER_CARD)
                If ReturnCode <> ModWinsCard.SCARD_S_SUCCESS Then Throw New RFID_Exception(ReturnCode, ReaderEstablished, CardConnected, ErrorInfo)
            End If
            CardConnected = False
        End Sub
#End Region

#Region "Keys Area"
        Public Sub LoadKeys(ByVal KeyType As MiFareKey.KeyType, ByVal MyKey() As Byte)
            If MyKey Is Nothing Then Throw New RFID_Exception("Keys not yet defined")

            ClearBuffers()
            SendBuff(0) = &HFF                                                                      ' CLA 0xFF
            SendBuff(1) = &H82                                                                      ' INS 0x82
            SendBuff(2) = &H0                                                                       ' P1 : volatile memory 0x00
            SendBuff(3) = MiFareKey.KeyNum(KeyType)                                                 ' P2 : Memory location
            SendBuff(4) = &H6                                                                       ' P3 0x06
            
            Array.Copy(MyKey, 0, SendBuff, 5, MyKey.Length)
            SendLength = 11
            ReceivedLength = 2

            'load keys to volatile mem
            If Not ExecScardTransmit(0) Then Throw New RFID_Exception(ReturnCode, ReaderEstablished, CardConnected, ErrorInfo)
        End Sub

        Public Sub Authentication(ByVal BlockNo As Integer, ByVal KeyType As MiFareKey.KeyType)
            ClearBuffers()
            SendBuff(0) = &HFF                                                                      ' CLA 0xFF
            SendBuff(1) = &H86                                                                      ' INS: for stored key input 0x86
            SendBuff(2) = &H0                                                                       ' P1: same for all source types 0x00
            SendBuff(3) = &H0                                                                       ' P2: for stored key input 0x00
            SendBuff(4) = &H5                                                                       ' P3: for stored key input 0x05
            SendBuff(5) = &H1                                                                       ' Byte 1: version number 0x01
            SendBuff(6) = &H0                                                                       ' Byte 2: 0x00
            SendBuff(7) = CType(BlockNo, Byte)                                                      ' Byte 3: Block No. to authenticated
            SendBuff(8) = KeyType                                                                   ' Byte 4 : Key type
            SendBuff(9) = MiFareKey.KeyNum(KeyType)                                                 ' Byte 5 : Key number
            SendLength = 10
            ReceivedLength = 2

            If ExecScardTransmit(0) Then
                'return 90 = completed successfully
                If (ReturnCode <> ModWinsCard.SCARD_S_SUCCESS) Then
                    Throw New RFID_Exception(ReturnCode, ReaderEstablished, CardConnected, ErrorInfo)
                End If
                If (RecvBuff((ReceivedLength - 2)) <> 144) Then
                    Throw New RFID_Exception(ReturnCode, ReaderEstablished, CardConnected, ErrorInfo)
                End If
            Else
                'return 63 = operation failed
                Throw New RFID_Exception(ReturnCode, ReaderEstablished, CardConnected, ErrorInfo)
            End If
        End Sub
#End Region

#Region "Carding"
        Public Function GetUID() As String
            Try
                ClearBuffers()
                SendBuff(0) = &HFF                                                                      ' CLA 0xFF
                SendBuff(1) = &HCA                                                                      ' INS 0xCA
                SendBuff(2) = &H0                                                                       ' P1
                SendBuff(3) = &H0                                                                       ' P2
                SendBuff(4) = &H0                                                                       ' Le

                SendLength = 5
                ReceivedLength = 6

                Dim CardUID As String = ""
                If ExecScardTransmit(2) Then
                    For i As Integer = 0 To ReceivedLength - 3
                        CardUID = CardUID + String.Format("{0:X2}", RecvBuff(i))
                        If i < ReceivedLength - 3 Then CardUID &= " "
                    Next
                Else
                    Throw New RFID_Exception(ReturnCode, ReaderEstablished, CardConnected, ErrorInfo)
                End If

                Return CardUID
            Catch ex As RFID_Exception
                Return ""
            End Try
        End Function

        Public Sub BlockRead(ByVal BlockNo As Integer, ByVal BlockLength As Integer)
            ClearBuffers()

            SendBuff(0) = &HFF                                                                      ' CLA 0xFF
            SendBuff(1) = &HB0                                                                      ' INS: for stored key input 0x86
            SendBuff(2) = &H0                                                                       ' P1: same for all source types 0x00
            SendBuff(3) = CType(BlockNo, Byte)                                                      ' P2: for stored key input 0x00
            SendBuff(4) = CType(BlockLength, Byte)                                                  ' P3: for stored key input 0x05
            SendLength = 5
            ReceivedLength = BlockLength + 2

            If Not ExecScardTransmit(2) Then Throw New RFID_Exception(ReturnCode, ReaderEstablished, CardConnected, ErrorInfo)
        End Sub

        Public Sub BlockWrite(ByVal BlockNo As Integer, ByVal StringValue As String)
            Dim tempstr() As Byte = System.Text.ASCIIEncoding.ASCII.GetBytes(StringValue)
            If (tempstr.Length > 16) Then Array.Resize(tempstr, 16)

            ClearBuffers()
            SendBuff(0) = &HFF                                                                      ' CLA 0xFF
            SendBuff(1) = &HD6                                                                      ' INS 0xD6
            SendBuff(2) = &H0                                                                       ' P1
            SendBuff(3) = CType(BlockNo, Byte)                                                      ' P2 : Starting Block No.
            SendBuff(4) = &H10                                                                      ' P3 : Data length

            Array.Copy(tempstr, 0, SendBuff, 5, tempstr.Length)
            SendLength = SendBuff.Length
            ReceivedLength = 2

            If Not ExecScardTransmit(2) Then Throw New RFID_Exception(ReturnCode, ReaderEstablished, CardConnected, ErrorInfo)
        End Sub

        Public Sub BinaryKeys_Write(ByVal SectorNo As Integer, ByVal KeyA() As Byte, ByVal KeyB() As Byte)
            If SectorNo < 0 Or SectorNo > 15 Then Throw New RFID_Exception("Sector number is out of range")
            If KeyA Is Nothing Or KeyB Is Nothing Then Throw New RFID_Exception("Keys not acceptable")

            Dim TrailerBlock As Byte = CType(((SectorNo * 4) + 3), Byte)
            Dim DataTrailer() As Byte = New Byte(15) {}
            Dim AccessBits() As Byte = New Byte(3) {&HF0, &HFF, &H0, &H69}                          '(my) default trailer access bits -> F0 FF 00 69

            Array.Copy(KeyA, 0, DataTrailer, 0, 6)
            Array.Copy(AccessBits, 0, DataTrailer, 6, 4)
            Array.Copy(KeyB, 0, DataTrailer, 10, 6)

            ClearBuffers()
            SendBuff(0) = &HFF                                                                      ' CLA 0xFF
            SendBuff(1) = &HD6                                                                      ' INS 0xD6
            SendBuff(2) = &H0                                                                       ' P1
            SendBuff(3) = TrailerBlock                                                              ' P2 : Block No.
            SendBuff(4) = &H10                                                                      ' P3 : Data length

            Array.Copy(DataTrailer, 0, SendBuff, 5, DataTrailer.Length)
            SendLength = SendBuff.Length
            ReceivedLength = 2

            If Not ExecScardTransmit(2) Then Throw New RFID_Exception(ReturnCode, ReaderEstablished, CardConnected, ErrorInfo)
        End Sub
#End Region
    End Class

    Public Class RFID_Exception
        Inherits Exception
        Public Property ErrorMessage As String

        Public Sub New()
            MyBase.New()
        End Sub

        Public Sub New(ByVal ErrMesg As String)
            MyBase.New()
            ErrorMessage = ErrMesg
        End Sub

        Public Sub New(ByVal CodeNumber As Long, ByRef RC As Boolean, ByRef CC As Boolean, ByRef EI As String)
            MyBase.New()

            Select Case CodeNumber
                Case ModWinsCard.SCARD_E_NO_READERS_AVAILABLE
                    RC = False
                    CC = False
                Case ModWinsCard.SCARD_W_REMOVED_CARD
                    RC = False
                    CC = False
                Case ModWinsCard.SCARD_W_UNPOWERED_CARD
                    CC = False
            End Select

            EI = ModWinsCard.GetScardErrMsg(CodeNumber)
            ErrorMessage = EI
        End Sub
    End Class

    Public Class MiFareKey
        Public Property KeyA As Byte() = New Byte(5) {}
        Public Property KeyB As Byte() = New Byte(5) {}
        Property Stats As Boolean
        Property DefaultKey As Byte() = New Byte(5) {&HFF, &HFF, &HFF, &HFF, &HFF, &HFF}

        Public Enum KeyType
            TypeA = 96
            TypeB = 97
        End Enum

        Public Shared Function KeyNum(ByVal TypeKey As KeyType) As Byte
            If TypeKey = KeyType.TypeA Then
                Return &H0
            Else
                Return &H1
            End If
        End Function

        Public Sub New()
            MyBase.New()
        End Sub

        Public Sub New(ByVal tempKeyA As String, ByVal tempKeyB As String)
            KeyA = GetBytesFromStrings(tempKeyA)
            KeyB = GetBytesFromStrings(tempKeyB)

            Stats = True
        End Sub

        Public Sub SetKeyA(ByVal tempKeyA As String)
            KeyA = GetBytesFromStrings(tempKeyA)

            If KeyB IsNot Nothing Then
                Stats = True
            End If
        End Sub

        Public Sub SetKeyB(ByVal tempKeyB As String)
            KeyB = GetBytesFromStrings(tempKeyB)

            If KeyA IsNot Nothing Then
                Stats = True
            End If
        End Sub

        Private Shared Function GetBytesFromStrings(ByVal temp_str As String)
            Dim temp2 As String() = temp_str.Split(CType(" ", Char))
            Dim newBytes As Byte() = New Byte(temp2.Length - 1) {}

            For i As Integer = 0 To temp2.Length - 1
                newBytes(i) = Byte.Parse(temp2(i), System.Globalization.NumberStyles.HexNumber)
            Next

            Return newBytes
        End Function
    End Class

End Namespace
