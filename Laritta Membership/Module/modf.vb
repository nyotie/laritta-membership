﻿Imports MySql.Data.MySqlClient
Imports Laritta_Membership.RFID_Tools

Module modf
    'WaitFormDialog
    Public LoadSprite As New LoadingSprites

    'Encryption
    Public Const EncryptKey As String = "01encryptitk02"
    Public Encryptor As New TripleDES(EncryptKey)

    'Variabel SQL
    Public xCon As SqlClient.SqlConnection
    Public ExDb As New ClassDB
    Public xSet As New DataSet
    Public SQLquery As String
    Public sp_Query, param_Query As String

    'Setting
    Public id_cabang As String
    Public MultiServer, OnlineStatus, LocalStatus As Boolean

    'RFID
    Public DefReaderName As String
    Public GlobalReaderContext As IntPtr
    Public MyMiFareKey As MiFareKey = New MiFareKey
    Public MyACR As ACR1222L = New ACR1222L
    Public MyCard As MiFareCard = New MiFareCard

    'Connection String
    Public conn_string_local As String
    Public conn_string_online As String

    'Variabel login
    Public staff_id, staff_group, shiftLogin As Integer
    Public staff_name As String
    Public status_login As Boolean = True

    'Variabel permissions tambahan
    Public GlobalEditVisibility As Boolean
    Public GlobalPrintVisibility As Boolean

    'Variabel Browsers
    Public selmaster_id As Integer
    Public selected_id, selected_no As Integer
    Public id_module, id_jenistrans As Integer
    Public nameCheck As String

    Public Sub Main()
        Dim appProc As Process() = Process.GetProcessesByName(System.IO.Path.GetFileNameWithoutExtension(Process.GetCurrentProcess.MainModule.ModuleName))

        If appProc.Length > 1 Then
            MessageBox.Show("Applikasi sudah berjalan di Desktop.")
        Else
            Try
                My.Application.ChangeCulture("en-US")

                If Not OpenConnection() Then
                    Application.Run(New FrmSettingKoneksi)
                    MsgInfo("Jalankan lagi aplikasi untuk melakukan pengaturan lebih lanjut.")
                    Return
                End If

                Get_MembercardSettings()

                Application.Run(New MainForm)
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try
        End If
    End Sub

    Public Sub MsgInfo(ByVal xtext As String)
        MsgBox(xtext, MsgBoxStyle.Information, "Perhatian")
    End Sub

    Public Sub MsgErrorDev()
        MsgBox("Terdapat error pada saat menyimpan/mengambil data, mintalah IT Person perusahaan anda untuk mencari tahu permasalahan yang terjadi.", MsgBoxStyle.Critical, "PERHATIAN!")
    End Sub

    Public Sub MsgWarning(ByVal xtext As String)
        MsgBox(xtext, MsgBoxStyle.Exclamation, "Perhatian")
    End Sub

    Public Function isDoubleData(ByVal query1 As String) As Boolean
        If Not xSet.Tables("nameCheck") Is Nothing Then xSet.Tables("nameCheck").Clear()
        ExDb.ExecQuery(True, query1, xSet, "nameCheck")
        If xSet.Tables("nameCheck").Rows.Count > 0 Then
            If selmaster_id = 0 Or Not selmaster_id = xSet.Tables("nameCheck").Rows(0).Item(0) Then
                Return False
            End If
        End If

        Return True
    End Function

    Public Function Check_IsOnline() As Boolean
        Try
            Using client = New System.Net.WebClient
                Using stream = client.OpenRead("http://www.google.com")
                    Return True
                End Using
            End Using
        Catch ex As Exception
            Return False
        End Try
    End Function

#Region "GET DATA"
    Public Sub Get_User_Permissions()
        Try
            If Not xSet.Tables("User_ProgPermisisons") Is Nothing Then xSet.Tables("User_ProgPermisisons").Clear()
            SQLquery = String.Format("SELECT a.permission_id, a.permission_name, IFNULL(b.allow_read, 0) allow_read, IFNULL(b.allow_new, 0) allow_new, " & _
                                     "IFNULL(b.allow_edit, 0) allow_edit, IFNULL(b.allow_void, 0) allow_void, IFNULL(b.allow_print, 0) allow_print " & _
                                     "FROM mbsm_permissionlists a LEFT JOIN mbsm_permissionuse b ON a.permission_id=b.permission_id AND b.id_sc={0} " & _
                                     "ORDER BY a.kat_num, a.permission_num;", staff_group)
            ExDb.ExecQuery(1, SQLquery, xSet, "User_ProgPermisisons")
        Catch ex As Exception
            MsgErrorDev()
        End Try
    End Sub

    Public Function Get_DataCompanyAndBranch()
        Try
BranchDataChecker: LoadSprite.ShowLoading("Making lemonade", "Loading")
            If Not xSet.Tables("DataBranch") Is Nothing Then xSet.Tables("DataBranch").Clear()
            SQLquery = "SELECT idCabang ID, isProduksi Produksi, namaCabang Nama, alamatCabang Alamat, telpCabang Telp FROM mcabang WHERE inactive=0 ORDER BY idCabang, namaCabang;"
            ExDb.ExecQuery(1, SQLquery, xSet, "DataBranch")
            LoadSprite.CloseLoading()
            If xSet.Tables("DataBranch").Rows.Count = 0 Then
                selmaster_id = 0
                '  MasterCabang.FirstTime = True
                '  MasterCabang.ShowDialog()
                GoTo BranchDataChecker
            End If

WhatBranchAmIChecker: LoadSprite.ShowLoading("Preparing the party", "Loading")
            id_cabang = FileSettingsHelper.ReadSettings(Application.StartupPath & "\setting.ini", "Cabang", "id_cabang", "")
            LoadSprite.CloseLoading()
            If id_cabang = Nothing Or id_cabang = "" Then
                selmaster_id = 0
                LoadSprite.CloseLoading()
                WhatBranchAmI.ShowDialog()
                GoTo WhatBranchAmIChecker
            End If
        Catch ex As Exception
            LoadSprite.CloseLoading()
            MsgWarning("Terdapat error pada saat mengambil data perusahaan dan cabang!")
            Return False
        End Try

        Return True
    End Function
#End Region

#Region "CONNECTION SETTINGS"
    Public Function OpenConnection() As Boolean
        Try
            LoadSprite.ShowLoading("Initializing", "Testing Koneksi")
            MultiServer = FileSettingsHelper.ReadSettings(Application.StartupPath & "\setting.ini", "Application Settings", "Multiserver", "")
            LoadSprite.CloseLoading()

            '-------------------------------ONLINE-------------------------------
            If MultiServer Then
                LoadSprite.ShowLoading("Mencoba koneksi ke Database Online", "Testing Koneksi")
                If ConnectionChecker("Online Database", conn_string_online) Then
                    OnlineStatus = True
                Else
                    LoadSprite.CloseLoading()
                    MsgWarning("Gagal terhubung dengan Server Online, harap periksa koneksi internet")
                    OnlineStatus = False
                    Return False
                End If
                LoadSprite.CloseLoading()
            End If

            '-------------------------------LOCAL-------------------------------
            LoadSprite.ShowLoading("Mencoba koneksi ke Database Local", "Testing Koneksi")
            If ConnectionChecker("Local Database", conn_string_local) Then
                LocalStatus = True
            Else
                LoadSprite.CloseLoading()
                MsgWarning("Gagal terhubung dengan Server Local!")
                LocalStatus = False
                If Not MultiServer Then Return False
            End If
            LoadSprite.CloseLoading()

            'HOW CAN YOU DO QUERY IF YOU DONT HAVE CONNECTION STRING?
            If Get_DataCompanyAndBranch() = False Then Return False
        Catch ex As Exception
            LoadSprite.CloseLoading()
            MsgWarning("Terdapat masalah pada pengaturan setting. Form pengaturan akan terbuka setelah pesan ini ditutup.")
            Return False
        End Try

        Return True
    End Function

    Public Function ConnectionChecker(ByVal LineConnection As String, ByRef the_packet As String) As Boolean
        Dim ConnectionString As String
        Dim DbName As String = FileSettingsHelper.ReadSettings(Application.StartupPath & "\setting.ini", LineConnection, "DatabaseName", "")
        If DbName = "" Then Return False

        Dim Usr As String = FileSettingsHelper.ReadSettings(Application.StartupPath & "\setting.ini", LineConnection, "User", "")
        Dim Host As String = FileSettingsHelper.ReadSettings(Application.StartupPath & "\setting.ini", LineConnection, "Host", "")
        Dim Pwd As String = FileSettingsHelper.ReadSettings(Application.StartupPath & "\setting.ini", LineConnection, "Password", "")

        If Not Pwd = "" Then
            Pwd = Encryptor.DecryptData(Pwd)
        End If

        'server=localhost;User Id=root;password=vertrigo;Persist Security Info=True;database=db_laritta;Connect Timeout=5
        ConnectionString = String.Format("server={0}; User Id={2}; password={3}; Persist Security Info=True; database={1}; Connect Timeout=30; ", Host, DbName, Usr, Pwd)

        Using xCon = New MySqlConnection(ConnectionString)
            Try
                xCon.Open()
                If xCon.State = ConnectionState.Open Then
                    xCon.Close()
                Else
                    Return False
                End If

                the_packet = ConnectionString
                Return True
            Catch ex As Exception
                Return False
            End Try
        End Using
    End Function
#End Region

#Region "READER AND CARD"
    Private Sub Get_MembercardSettings()
        'get data reader and card from settings.ini
        Dim SavedKeyA, SavedKeyB As String

        DefReaderName = FileSettingsHelper.ReadSettings(Application.StartupPath & "\setting.ini", "Reader and Card", "ReaderDefaultName", "")
        If DefReaderName = "" Then
            MyMiFareKey = New MiFareKey()
        Else
            SavedKeyA = FileSettingsHelper.ReadSettings(Application.StartupPath & "\setting.ini", "Reader and Card", "KeyA", "")
            SavedKeyB = FileSettingsHelper.ReadSettings(Application.StartupPath & "\setting.ini", "Reader and Card", "KeyB", "")

            If Not SavedKeyA = "" Then SavedKeyA = Encryptor.DecryptData(SavedKeyA)
            If Not SavedKeyB = "" Then SavedKeyB = Encryptor.DecryptData(SavedKeyB)

            MyMiFareKey = New MiFareKey(SavedKeyA, SavedKeyB)
        End If

    End Sub
#End Region

End Module