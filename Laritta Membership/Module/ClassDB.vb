﻿Imports MySql.Data.MySqlClient

Public Class ClassDB
    Dim xCom As MySqlCommand
    Dim xAdap As MySqlDataAdapter
    Dim xTrans As MySqlTransaction

    '****************************************************************************************
    '   Name        : ExecQuery
    '   Parameter   : SQL           (String)
    '                 xDataSet      (DataSet)
    '                 xTableName    (String)
    '   Function    : Execute SQL Languange
    '****************************************************************************************

    Public Function ExecQuery(ByVal sv_tujuan As Boolean, ByVal pSQL As String, ByVal pDataSet As DataSet, ByVal pTableName As String) As DataSet
        Dim connection_string As String = If(sv_tujuan And MultiServer, conn_string_online, conn_string_local)
        Try
            Using xCon = New MySqlConnection(connection_string)
                xAdap = New MySqlDataAdapter(pSQL, xCon)
                xAdap.Fill(pDataSet, pTableName)
                xAdap.Dispose()
            End Using

            Return pDataSet
        Catch exx As MySqlException When exx.Message.Contains("Unable to connect to any of the specified MySQL hosts.")
            If Not sv_tujuan Then LocalStatus = False Else OnlineStatus = False
            Return Nothing
        Catch ex As MySqlException
            Return Nothing
        End Try
    End Function

    '****************************************************************************************
    '   Name        : ExecData
    '   Parameter   : SQL           (String)
    '   Function    : Execute SQL Languange For Inser, Update, Delete
    '****************************************************************************************

    Public Sub ExecData(ByVal sv_tujuan As Boolean, ByVal pSQL As String)
        Dim connection_string As String = If(sv_tujuan And MultiServer, conn_string_online, conn_string_local)

        Using xCon = New MySqlConnection(connection_string)
            Try
                xCom = New MySqlCommand(pSQL, xCon)
                xCon.Open()
                xTrans = xCon.BeginTransaction
                xCom.Transaction = xTrans
                xCom.ExecuteNonQuery()
                xTrans.Commit()
                xCon.Close()
                xCom.Dispose()
            Catch exx As MySqlException When exx.Message.Contains("The server was not found or was not accessible.")
                xTrans.Rollback()
                If Not sv_tujuan Then LocalStatus = False Else OnlineStatus = False
                MsgBox(String.Format("Error On : {0}{1}{2}", exx.Source, Chr(13), exx.Message), MessageBoxButtons.OK, MessageBoxIcon.Error)
            Catch ex As MySqlException
                xTrans.Rollback()
                MsgBox(String.Format("Error On : {0}{1}{2}", ex.Source, Chr(13), ex.Message), MessageBoxButtons.OK, MessageBoxIcon.Error)
            Finally
                xCon.Close()
            End Try
        End Using
    End Sub
End Class