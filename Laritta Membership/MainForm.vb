﻿Public Class MainForm

    Private Sub PermissionsApply()
        Get_User_Permissions()

        If status_login Then
            PageMaster.Visible = True
            PageTransaksi.Visible = True
            PageToolsAndSetting.Visible = True
        Else
            PageMaster.Visible = False
            PageTransaksi.Visible = False
            PageToolsAndSetting.Visible = False
            Return
        End If

        'If MultiServer And Not LocalStatus Then
        If PageMaster.Visible = True Then
            MTipeMembership.Enabled = xSet.Tables("User_ProgPermisisons").Select("permission_id=1")(0).Item("allow_read")
            MKota.Enabled = xSet.Tables("User_ProgPermisisons").Select("permission_id=2")(0).Item("allow_read")
            MArea.Enabled = xSet.Tables("User_ProgPermisisons").Select("permission_id=3")(0).Item("allow_read")
            MCustomer.Enabled = xSet.Tables("User_ProgPermisisons").Select("permission_id=4")(0).Item("allow_read")
            MPoint.Enabled = xSet.Tables("User_ProgPermisisons").Select("permission_id=7")(0).Item("allow_read")
            MStamp.Enabled = xSet.Tables("User_ProgPermisisons").Select("permission_id=8")(0).Item("allow_read")
            MGift.Enabled = xSet.Tables("User_ProgPermisisons").Select("permission_id=9")(0).Item("allow_read")
            MPromo.Enabled = xSet.Tables("User_ProgPermisisons").Select("permission_id=10")(0).Item("allow_read")
            MRewardPoint.Enabled = xSet.Tables("User_ProgPermisisons").Select("permission_id=11")(0).Item("allow_read")
            MRewardStamp.Enabled = xSet.Tables("User_ProgPermisisons").Select("permission_id=12")(0).Item("allow_read")
            MMembercard.Enabled = xSet.Tables("User_ProgPermisisons").Select("permission_id=13")(0).Item("allow_read")
        End If
      
        If PageTransaksi.Visible = True Then
            TransCheckMember.Enabled = xSet.Tables("User_ProgPermisisons").Select("permission_id=14")(0).Item("allow_read")
            TransVoidNota.Enabled = xSet.Tables("User_ProgPermisisons").Select("permission_id=22")(0).Item("allow_read")
            'IIf(MultiServer And Not LocalStatus, False, xSet.Tables("User_ProgPermisisons").Select("permission_id=22")(0).Item("allow_read"))
            TransPenggunaanPG.Enabled = xSet.Tables("User_ProgPermisisons").Select("permission_id=23")(0).Item("allow_read")
            TransPenggunaanPS.Enabled = xSet.Tables("User_ProgPermisisons").Select("permission_id=33")(0).Item("allow_read")
            TransMassBlessings.Enabled = xSet.Tables("User_ProgPermisisons").Select("permission_id=24")(0).Item("allow_read")
            TransAuthentication.Enabled = xSet.Tables("User_ProgPermisisons").Select("permission_id=25")(0).Item("allow_read")
            TransSettle.Enabled = xSet.Tables("User_ProgPermisisons").Select("permission_id=35")(0).Item("allow_read")
            TransLogs.Enabled = xSet.Tables("User_ProgPermisisons").Select("permission_id=26")(0).Item("allow_read")
            TransReport.Enabled = xSet.Tables("User_ProgPermisisons").Select("permission_id=27")(0).Item("allow_read")
        End If
        If PageToolsAndSetting.Visible = True Then
            ToolProgPerm.Enabled = xSet.Tables("User_ProgPermisisons").Select("permission_id=28")(0).Item("allow_read")
            ToolRepPerm.Enabled = xSet.Tables("User_ProgPermisisons").Select("permission_id=29")(0).Item("allow_read")
            ToolBlockCard.Enabled = xSet.Tables("User_ProgPermisisons").Select("permission_id=34")(0).Item("allow_read")
            ToolCabang.Enabled = xSet.Tables("User_ProgPermisisons").Select("permission_id=30")(0).Item("allow_read")
            ToolKoneksi.Enabled = xSet.Tables("User_ProgPermisisons").Select("permission_id=31")(0).Item("allow_read")
            ToolKoneksi.Enabled = xSet.Tables("User_ProgPermisisons").Select("permission_id=31")(0).Item("allow_read")
            ToolRFID.Enabled = xSet.Tables("User_ProgPermisisons").Select("permission_id=32")(0).Item("allow_read")
        End If
    End Sub

    Private Sub MainForm_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        Application.Exit()
    End Sub

    Private Sub MainForm_GotFocus(ByVal sender As Object, ByVal e As EventArgs) Handles Me.GotFocus
        If status_login = False Then
            Close()
        Else
            If (MultiServer And Not OnlineStatus) Or (Not MultiServer And Not LocalStatus) Then Close()

            PermissionsApply()
        End If
    End Sub

    Private Sub MainForm_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        For Each ctl As Control In Controls
            Dim ctlMDI As MdiClient = TryCast(ctl, MdiClient)
            If ctlMDI Is Nothing Then
                Continue For
            End If
            ctlMDI.BackColor = BackColor
        Next
    End Sub

    Private Sub MainForm_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        loginForm.ShowDialog()
    End Sub

    Private Sub RibbonControl_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles RibbonControl.ItemClick
        If e.Item.Caption = "Logout" Then
            PageMaster.Visible = False
            PageTransaksi.Visible = False
            PageToolsAndSetting.Visible = False

            For i As Integer = MdiChildren.Length - 1 To 0 Step -1
                MdiChildren(i).Close()
            Next
            loginForm.ShowDialog()

            '-----------------------------------------------------MASTER
        ElseIf e.Item.Caption = "Customer" Then
            ShowLookUpModule(BrowserMasterCustomer, Me, "Master Customer")
        ElseIf e.Item.Caption = "Kota" Then
            ShowLookUpModule(BrowserMasterKota, Me, "Master Kota")
        ElseIf e.Item.Caption = "Area" Then
            ShowLookUpModule(BrowserMasterArea, Me, "Master Area")
        ElseIf e.Item.Caption = "Tipe Membership" Then
            ShowLookUpModule(BrowserMasterTipeMembership, Me, "Master Tipe Membership")
        ElseIf e.Item.Caption = "Membercard" Then
            ShowModule(MasterMembercard, "Membercard")
            MasterMembercard = Nothing

        ElseIf e.Item.Caption = "Point" Then
            ShowLookUpModule(BrowserMasterPoint, Me, "Master Point")
        ElseIf e.Item.Caption = "Stamp" Then
            ShowModule(MasterStamp, "Master Stamp")
            MasterStamp = Nothing
        ElseIf e.Item.Caption = "Gift" Then
            ShowLookUpModule(BrowserMasterGift, Me, "Master Gift")
        ElseIf e.Item.Caption = "Promo" Then
            ShowLookUpModule(BrowserMasterPromo, Me, "Master Promo")

        ElseIf e.Item.Caption = "Reward Point" Then
            ShowLookUpModule(BrowserMasterRewardPoint, Me, "Master Reward Point")
        ElseIf e.Item.Caption = "Reward Stamp" Then
            ShowLookUpModule(BrowserMasterRewardStamp, Me, "Master Reward Stamp")

            '-----------------------------------------------------TRANSAKSI
        ElseIf e.Item.Caption = "Check Member" Then
            ShowModule(CheckMember, "Check Member")
            CheckMember = Nothing
        ElseIf e.Item.Caption = "Void Nota" Then
            ShowModule(VoidNota, "Void Nota")
            VoidNota = Nothing
        ElseIf e.Item.Caption = "Promo dan Gift" Then
            ShowModule(PenggunaanGiftPromo, "Penggunaan Promo dan Gift")
            PenggunaanGiftPromo = Nothing
        ElseIf e.Item.Caption = "Point dan Stamp" Then
            ShowModule(PenggunaanPointStamp, "Penggunaan Point dan Stamp")
            PenggunaanPointStamp = Nothing

        ElseIf e.Item.Caption = "Mass Blessings" Then
            ShowLookUpModule(MassBlessingMemberList, Me, "Mass Blessings")
        ElseIf e.Item.Caption = "Authentication" Then
            ShowLookUpModule(BlessingAuth, Me, "Authentication")

        ElseIf e.Item.Caption = "Settle" Then
            ShowLookUpModule(Settlement, Me, "Settlement")
        ElseIf e.Item.Caption = "Logs" Then
            ShowLookUpModule(EventLogs, Me, "Event Logs")
        ElseIf e.Item.Caption = "Report" Then
            ShowLookUpModule(BrowserReport, Me, "Reports")

            '-----------------------------------------------------TOOLS AND SETTINGS
        ElseIf e.Item.Caption = "Program Permissions" Then
            ShowLookUpModule(ProgPermissions, Me, "Program Permissions")
        ElseIf e.Item.Caption = "Report Permissions" Then
            '=============================================XXXXXXXXXXXXXXXXXXXXXXXXX========================================FILL ME
        ElseIf e.Item.Caption = "Blocking Card" Then
            ShowModule(MembercardBlock, "Blocking Card")
            MembercardBlock = Nothing
        ElseIf e.Item.Caption = "Cabang" Then
            ShowModule(WhatBranchAmI, "Pilih Cabang")
            WhatBranchAmI = Nothing
        ElseIf e.Item.Caption = "Koneksi" Then
            ShowModule(FrmSettingKoneksi, "Setting Koneksi")
            FrmSettingKoneksi = Nothing
        ElseIf e.Item.Caption = "RFID" Then
            ShowModule(MembercardSettings, "Setting RFID")
            MembercardSettings = Nothing

        ElseIf e.Item.Caption = "About" Then
            ShowModule(FrmAbout, "About")
            'MsgBox("Versi applikasi :" & My.Application.Info.Version.ToString, MsgBoxStyle.OkOnly, "About")
        End If
    End Sub

    Private Sub RibbonControl_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RibbonControl.Click

    End Sub
End Class