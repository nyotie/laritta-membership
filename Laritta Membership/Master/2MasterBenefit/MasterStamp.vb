﻿Public Class MasterStamp

    Private Sub MasterStamp_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("DataMasterStamp")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub MasterStamp_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()

        SQLquery = "SELECT syaratperiode_start, syaratperiode_end, rate_rp, rate_stamp, ismultiplied, available_at_outlet, available_at_order, Inactive FROM mbsm_stamp WHERE id_stamp=1"
        ExDb.ExecQuery(1, SQLquery, xSet, "DataMasterStamp")

        If xSet.Tables("DataMasterStamp").Rows.Count > 0 Then
            PeriodeStart.EditValue = xSet.Tables("DataMasterStamp").Rows(0).Item("syaratperiode_start")
            PeriodeEnd.EditValue = xSet.Tables("DataMasterStamp").Rows(0).Item("syaratperiode_end")
            RateRp.EditValue = xSet.Tables("DataMasterStamp").Rows(0).Item("rate_rp")
            CekMultiplied.Checked = xSet.Tables("DataMasterStamp").Rows(0).Item("ismultiplied")
            CekOutlet.Checked = xSet.Tables("DataMasterStamp").Rows(0).Item("available_at_outlet")
            CekOrder.Checked = xSet.Tables("DataMasterStamp").Rows(0).Item("available_at_order")
            CekInaktif.Checked = xSet.Tables("DataMasterStamp").Rows(0).Item("Inactive")

            selmaster_id = 1
        Else
            selmaster_id = 0
        End If
    End Sub

    Private Sub MasterStamp_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        LoadSprite.CloseLoading()
        PeriodeStart.Focus()
    End Sub

    Private Sub ButSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSimpan.Click
        If Not beforeSave() Then Return
        Try
            LoadSprite.ShowLoading()
            If selmaster_id = "0" Then
                sp_Query = "INSERT INTO mbsm_stamp(syaratperiode_start, syaratperiode_end, rate_rp, rate_stamp, ismultiplied, available_at_outlet, available_at_order, Inactive, createby, createdate) "
                param_Query = String.Format("VALUES('{0}', '{1}', {2}, 1, {3}, {4}, {5}, {6}, {7}, NOW()) ",
                                            Format(PeriodeStart.EditValue, "yyyy-MM-dd 00:00:00"),
                                            Format(PeriodeEnd.EditValue, "yyyy-MM-dd 00:00:00"),
                                            RateRp.EditValue,
                                            CekMultiplied.Checked,
                                            CekOutlet.Checked,
                                            CekOrder.Checked,
                                            CekInaktif.Checked,
                                            staff_id)
            Else
                sp_Query = String.Format("UPDATE mbsm_stamp SET syaratperiode_start='{0}', syaratperiode_end='{1}', rate_rp={2}, rate_stamp=1, ismultiplied={3}, available_at_outlet={4}, available_at_order={5}, Inactive={6}, updateBy={7}, updateDate=NOW() ",
                                            Format(PeriodeStart.EditValue, "yyyy-MM-dd 00:00:00"),
                                            Format(PeriodeEnd.EditValue, "yyyy-MM-dd 00:00:00"),
                                            RateRp.EditValue,
                                            CekMultiplied.Checked,
                                            CekOutlet.Checked,
                                            CekOrder.Checked,
                                            CekInaktif.Checked,
                                            staff_id)
                param_Query = "WHERE id_stamp=1"
            End If
            ExDb.ExecData(1, sp_Query & param_Query)

            'If save_online.Checked And MultiServer And OnlineStatus Then
            '    ExDb.ExecData(1, sp_Query & param_Query)
            'End If

            MsgInfo("Data berhasil disimpan, tekan OK untuk melanjutkan.")
            BrowserMasterPoint.isChange = True
        Catch ex As Exception
            MsgErrorDev()
        End Try
        LoadSprite.CloseLoading()
        Close()
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Function beforeSave() As Boolean
        If PeriodeStart.EditValue Is Nothing Or PeriodeEnd.EditValue Is Nothing Or PeriodeStart.EditValue < Now.Date Then
            MsgInfo("Periode mulai dan selesai tidak boleh kosong dan tidak bisa kurang dari hari ini")
            Return False
        End If
        If RateRp.EditValue < 0 Then
            MsgInfo("Rate Rp tidak boleh minus")
            Return False
        End If

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        Return True
    End Function
End Class