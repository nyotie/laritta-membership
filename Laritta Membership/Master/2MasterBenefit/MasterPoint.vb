﻿Imports DevExpress.Utils

Public Class MasterPoint

    Private Sub MasterPoint_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("DataMasterPoint")
        xSet.Tables.Remove("DaftarTipeMembership")
        xSet.Tables.Remove("GetLastInsertPoint")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub MasterPoint_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()

        If Not selmaster_id = 0 Then
            SQLquery = String.Format("SELECT x.id_tipemembership, x.rate_rp, x.rate_point, x.isDefault FROM mbsm_point x WHERE x.id_ratepoint={0}", selmaster_id)
            ExDb.ExecQuery(1, SQLquery, xSet, "DataMasterPoint")
        End If

        Get_KebutuhanDaftar()

        If Not selmaster_id = 0 Then
            DaftarTipeMembership.EditValue = xSet.Tables("DataMasterPoint").Rows(0).Item("id_tipemembership")
            RateRp.EditValue = xSet.Tables("DataMasterPoint").Rows(0).Item("rate_rp")
            RatePoint.EditValue = xSet.Tables("DataMasterPoint").Rows(0).Item("rate_point")
            CekDefault.Checked = xSet.Tables("DataMasterPoint").Rows(0).Item("isDefault")
        End If
    End Sub

    Private Sub MasterPoint_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        LoadSprite.CloseLoading()
        DaftarTipeMembership.Focus()

        If BrowserMasterPoint.isFirstTime Then
            CekDefault.Checked = True
            CekDefault.Enabled = False
        End If
    End Sub

    Private Sub ButSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSimpan.Click
        If Not beforeSave() Then Return
        Try
            LoadSprite.ShowLoading()
            If selmaster_id = "0" Then
                sp_Query = "INSERT INTO mbsm_point(id_tipemembership, rate_rp, rate_point, isDefault, createby, createdate) "
                param_Query = String.Format("VALUES({0}, {1}, {2}, {3}, {4}, NOW()) ", DaftarTipeMembership.EditValue, RateRp.EditValue, RatePoint.EditValue, CekDefault.Checked, staff_id)
            Else
                sp_Query = String.Format("UPDATE mbsm_point SET id_tipemembership={0}, rate_rp={1}, rate_point={2}, isDefault={3}, updateBy={4}, updateDate=NOW()", DaftarTipeMembership.EditValue, RateRp.EditValue, RatePoint.EditValue, CekDefault.Checked, staff_id)
                param_Query = String.Format("WHERE id_ratepoint={0}", selmaster_id)
            End If
            ExDb.ExecData(1, sp_Query & param_Query)

            '----get id rate yang barusan di input dan merupakan default
            If selmaster_id = 0 And CekDefault.Checked And Not BrowserMasterPoint.isFirstTime Then
                SQLquery = "SELECT id_ratepoint FROM mbsm_point WHERE id_tipemembership=" & DaftarTipeMembership.EditValue
                ExDb.ExecQuery(1, SQLquery, xSet, "GetLastInsertPoint")

                selmaster_id = xSet.Tables("GetLastInsertPoint").Rows(0).Item("id_ratepoint")
            End If

            '----if insert, cek if default=true and not firsttime then update | if update, cek if default=true not firsttime then update
            If CekDefault.Checked And Not BrowserMasterPoint.isFirstTime Then
                sp_Query = "UPDATE mbsm_point SET isDefault=0 "
                param_Query = String.Format("WHERE id_ratepoint<>{0}", selmaster_id)
                ExDb.ExecData(1, sp_Query & param_Query)
            End If

            'If save_online.Checked And MultiServer And OnlineStatus Then
            '    ExDb.ExecData(1, sp_Query & param_Query)
            'End If

            MsgInfo("Data berhasil disimpan, tekan OK untuk melanjutkan.")
            BrowserMasterPoint.isChange = True
        Catch ex As Exception
            MsgErrorDev()
        End Try
        LoadSprite.CloseLoading()
        Close()
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Sub Get_KebutuhanDaftar()
        Dim id_tipe As Integer
        If selmaster_id = "0" Then
            id_tipe = 0
        Else
            id_tipe = xSet.Tables("DataMasterPoint").Rows(0).Item("id_tipemembership")
        End If

        SQLquery = String.Format("SELECT a.id_tipemembership ID, a.nama_tipemembership Nama FROM mbsm_tipemembership a " & _
                                 "WHERE a.id_tipemembership NOT IN (SELECT x.id_tipemembership FROM mbsm_point x) OR id_tipemembership={0} AND a.Inactive=0;", id_tipe)
        ExDb.ExecQuery(1, SQLquery, xSet, "DaftarTipeMembership")
        DaftarTipeMembership.Properties.DataSource = xSet.Tables("DaftarTipeMembership").DefaultView

        DaftarTipeMembership.Properties.NullText = ""
        DaftarTipeMembership.Properties.ValueMember = "ID"
        DaftarTipeMembership.Properties.DisplayMember = "Nama"
        DaftarTipeMembership.Properties.ShowClearButton = False
        DaftarTipeMembership.Properties.PopulateViewColumns()

        ViewTipeMembership.Columns("ID").Visible = False

        For Each coll As DataColumn In xSet.Tables("DaftarTipeMembership").Columns
            ViewTipeMembership.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next
    End Sub

    Private Function beforeSave() As Boolean
        If DaftarTipeMembership.EditValue Is Nothing Then
            MsgInfo("Tipe membership belum dipilih")
            Return False
        End If
        If RateRp.EditValue < 0 Then
            MsgInfo("Rate Rp tidak boleh minus")
            Return False
        End If
        If RatePoint.EditValue < 0 Then
            MsgInfo("Rate point tidak boleh minus")
            Return False
        End If

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        Return True
    End Function
End Class