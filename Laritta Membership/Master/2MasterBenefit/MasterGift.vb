﻿Imports DevExpress.Utils

Public Class MasterGift

    Private Sub MasterGift_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("DataMasterGift")
        xSet.Tables.Remove("DaftarSyaratUnlock")
        xSet.Tables.Remove("DaftarParameterWhere")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub MasterGift_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()
        Get_KebutuhanDaftar()

        If Not selmaster_id = 0 Then
            SQLquery = String.Format("SELECT nama_gift, gift_explanation, expired_day, expired_date, repetisi_gift, syarat_none, syarat_tipe, syarat_operator, syarat_tipevalue, syarat_wherevalue, reward_jenis, reward_gift, reward_disc, reward_value, reward_max, exc_at_outlet, exc_at_order, use_at_outlet, use_at_order, use_expired, Inactive FROM mbsm_gift WHERE id_gift={0}", selmaster_id)
            ExDb.ExecQuery(1, SQLquery, xSet, "DataMasterGift")

            NamaGift.EditValue = xSet.Tables("DataMasterGift").Rows(0).Item("nama_gift")
            ExplanationGift.EditValue = xSet.Tables("DataMasterGift").Rows(0).Item("gift_explanation")
            Repetisi.EditValue = xSet.Tables("DataMasterGift").Rows(0).Item("repetisi_gift")
            exp_day.EditValue = xSet.Tables("DataMasterGift").Rows(0).Item("expired_day")
            exp_date.EditValue = IIf(exp_day.EditValue = 0, xSet.Tables("DataMasterGift").Rows(0).Item("expired_date"), Nothing)

            RewardType.EditValue = xSet.Tables("DataMasterGift").Rows(0).Item("reward_jenis")
            RewardGift.EditValue = xSet.Tables("DataMasterGift").Rows(0).Item("reward_gift")
            RewardDisc.Checked = xSet.Tables("DataMasterGift").Rows(0).Item("reward_disc")
            RewardValue.EditValue = xSet.Tables("DataMasterGift").Rows(0).Item("reward_value")
            RewardMax.EditValue = xSet.Tables("DataMasterGift").Rows(0).Item("reward_max")

            at_outlet.EditValue = xSet.Tables("DataMasterGift").Rows(0).Item("exc_at_outlet")
            at_order.EditValue = xSet.Tables("DataMasterGift").Rows(0).Item("exc_at_order")

            syarat_none.Checked = xSet.Tables("DataMasterGift").Rows(0).Item("syarat_none")
            If Not syarat_none.Checked Then
                DaftarSyaratUnlock.EditValue = xSet.Tables("DataMasterGift").Rows(0).Item("syarat_tipe")
                DaftarParameterWhere.EditValue = xSet.Tables("DataMasterGift").Rows(0).Item("syarat_wherevalue")
                SyaratOperator.EditValue = xSet.Tables("DataMasterGift").Rows(0).Item("syarat_operator")
                SyaratNilai.EditValue = xSet.Tables("DataMasterGift").Rows(0).Item("syarat_tipevalue")
            End If

            use_outlet.Checked = xSet.Tables("DataMasterGift").Rows(0).Item("use_at_outlet")
            use_order.Checked = xSet.Tables("DataMasterGift").Rows(0).Item("use_at_order")
            use_expired.EditValue = xSet.Tables("DataMasterGift").Rows(0).Item("use_expired")

            CekInaktif.Checked = xSet.Tables("DataMasterGift").Rows(0).Item("Inactive")

            nameCheck = NamaGift.EditValue
        End If
    End Sub

    Private Sub MasterGift_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        LoadSprite.CloseLoading()
        NamaGift.Focus()
        If selmaster_id = 0 Then
            CekInaktif.Properties.ReadOnly = True
            CekInaktif.Checked = False
        End If
    End Sub

    Private Sub exp_day_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles exp_day.EditValueChanged
        If exp_day.EditValue > 0 Then exp_date.EditValue = Nothing
    End Sub

    Private Sub exp_date_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles exp_date.EditValueChanged
        If exp_date.EditValue IsNot Nothing Then exp_day.EditValue = Nothing
    End Sub

    Private Sub RewardType_EditValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ChangingEventArgs) Handles RewardType.EditValueChanging
        If e.NewValue = "brg" Then
            RewardDisc.Checked = False
            RewardDisc.Enabled = False
        Else
            RewardDisc.Checked = False
            RewardDisc.Enabled = True
        End If
    End Sub

    Private Sub RewardDisc_EditValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ChangingEventArgs) Handles RewardDisc.EditValueChanging
        If e.NewValue = True Then
            'disc %
            RewardValue.EditValue = 0
            RewardValue.Properties.MaxLength = 3
            RewardValue.Properties.Mask.EditMask = "P0"
        Else
            'nominal
            RewardValue.EditValue = 0
            RewardValue.Properties.MaxLength = 0
            RewardValue.Properties.Mask.EditMask = "n0"
        End If
    End Sub

    Private Sub syarat_none_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles syarat_none.CheckedChanged
        If syarat_none.Checked = True Then
            RewardMax.EditValue = 1
            RewardMax.Enabled = False
            at_outlet.Checked = True
            at_outlet.Enabled = False
            at_order.Checked = True
            at_order.Enabled = False

            DaftarSyaratUnlock.EditValue = Nothing
            DaftarParameterWhere.EditValue = Nothing

            PanelSyaratPenukaran.Enabled = False
        Else
            RewardMax.Enabled = True
            at_outlet.Enabled = True
            at_order.Enabled = True

            PanelSyaratPenukaran.Enabled = True
        End If
    End Sub

    Private Sub DaftarSyaratUnlock_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DaftarSyaratUnlock.EditValueChanged
        If IsNothing(DaftarSyaratUnlock.EditValue) Then Return

        Dim this_syarat As String = xSet.Tables("DaftarSyaratUnlock").Select("ID=" & DaftarSyaratUnlock.EditValue)(0).Item("syarat_tabelwhere")
        DaftarParameterWhere.EditValue = Nothing

        If this_syarat = "" Then
            DaftarParameterWhere.Enabled = False
            Return
        Else
            DaftarParameterWhere.Enabled = True
        End If

        If Not IsNothing(xSet.Tables("DaftarParameterWhere")) Then xSet.Tables("DaftarParameterWhere").Clear()

        If this_syarat = "mbarang" Then
            SQLquery = "SELECT idBrg ID, namaBrg Nama FROM mbarang WHERE inactive=0;"
        ElseIf this_syarat = "mkategoribrg2" Then
            'ElseIf ViewSyaratUnlock.GetFocusedRowCellValue("syarat_tabelwhere") = "mkategoribrg2" Then
            SQLquery = "SELECT idKategori2 ID, namaKategori2 Nama FROM mkategoribrg2 WHERE inactive=0;"
        End If
        ExDb.ExecQuery(1, SQLquery, xSet, "DaftarParameterWhere")
        DaftarParameterWhere.Properties.DataSource = xSet.Tables("DaftarParameterWhere").DefaultView
        DaftarParameterWhere.Properties.NullText = ""
        DaftarParameterWhere.Properties.ValueMember = "ID"
        DaftarParameterWhere.Properties.DisplayMember = "Nama"
        DaftarParameterWhere.Properties.ShowClearButton = False
        DaftarParameterWhere.Properties.PopulateViewColumns()

        DaftarParameterWhere.Properties.PopupFormSize = New Size(300, 300)

        ViewParameterWhere.Columns("ID").Visible = False
    End Sub

    Private Sub ButSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSimpan.Click
        If Not beforeSave() Then Return
        Try
            LoadSprite.ShowLoading()
            If selmaster_id = "0" Then
                sp_Query = "INSERT INTO mbsm_gift(nama_gift, gift_explanation, expired_day, expired_date, repetisi_gift, syarat_none, syarat_tipe, syarat_operator, syarat_tipevalue, syarat_wherevalue, reward_jenis, reward_gift, reward_disc, reward_value, reward_max, exc_at_outlet, exc_at_order, use_at_outlet, use_at_order, use_expired, createby, createdate, Inactive) "
                param_Query = String.Format("VALUES ('{0}', '{1}', {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, '{10}', '{11}', {12}, {13}, {14}, {15}, {16}, {17}, {18}, {19}, {20}, NOW(), {21}) ",
                                            NamaGift.Text,
                                            ExplanationGift.Text,
                                            IIf(exp_day.EditValue Is Nothing, 0, exp_day.EditValue),
                                            IIf(exp_day.EditValue = 0, "'" & Format(exp_date.EditValue, "yyyy-MM-dd") & "'", "NULL"),
                                            Repetisi.EditValue,
                                            syarat_none.Checked,
                                            IIf(Not syarat_none.Checked, "'" & DaftarSyaratUnlock.EditValue & "'", "NULL"),
                                            IIf(Not syarat_none.Checked, "'" & SyaratOperator.EditValue & "'", "NULL"),
                                            IIf(Not syarat_none.Checked, SyaratNilai.EditValue, "NULL"),
                                            IIf(Not syarat_none.Checked And DaftarParameterWhere.Enabled, DaftarParameterWhere.EditValue, "NULL"),
                                            RewardType.EditValue,
                                            RewardGift.EditValue,
                                            RewardDisc.Checked,
                                            RewardValue.EditValue,
                                            RewardMax.EditValue,
                                            at_outlet.Checked,
                                            at_order.Checked,
                                            use_outlet.Checked,
                                            use_order.Checked,
                                            use_expired.EditValue,
                                            staff_id,
                                            CekInaktif.Checked)
            Else
                sp_Query = String.Format("UPDATE mbsm_gift SET nama_gift='{0}',gift_explanation='{1}',expired_day={2},expired_date={3},repetisi_gift={4},syarat_none={5}," & _
                                         "syarat_tipe={6},syarat_operator={7},syarat_tipevalue={8},syarat_wherevalue={9},reward_jenis='{10}',reward_gift='{11}',reward_disc={12}, " & _
                                         "reward_value={13},reward_max={14},exc_at_outlet={15},exc_at_order={16},use_at_outlet={17},use_at_order={18},use_expired={19}," & _
                                         "updateby={20},updatedate=NOW(),Inactive={21} ",
                                            NamaGift.Text,
                                            ExplanationGift.Text,
                                            IIf(exp_day.EditValue Is Nothing, 0, exp_day.EditValue),
                                            IIf(exp_day.EditValue = 0, "'" & Format(exp_date.EditValue, "yyyy-MM-dd") & "'", "NULL"),
                                            Repetisi.EditValue,
                                            syarat_none.Checked,
                                            IIf(Not syarat_none.Checked, "'" & DaftarSyaratUnlock.EditValue & "'", "NULL"),
                                            IIf(Not syarat_none.Checked, "'" & SyaratOperator.EditValue & "'", "NULL"),
                                            IIf(Not syarat_none.Checked, SyaratNilai.EditValue, "NULL"),
                                            IIf(Not syarat_none.Checked And DaftarParameterWhere.Enabled, DaftarParameterWhere.EditValue, "NULL"),
                                            RewardType.EditValue,
                                            RewardGift.EditValue,
                                            RewardDisc.Checked,
                                            RewardValue.EditValue,
                                            RewardMax.EditValue,
                                            at_outlet.Checked,
                                            at_order.Checked,
                                            use_outlet.Checked,
                                            use_order.Checked,
                                            use_expired.EditValue,
                                            staff_id,
                                            CekInaktif.Checked)
                param_Query = String.Format("WHERE id_gift={0}", selmaster_id)
            End If

            ExDb.ExecData(1, sp_Query & param_Query)
            MsgInfo("Data berhasil disimpan, tekan OK untuk melanjutkan.")
            BrowserMasterGift.isChange = True
        Catch ex As Exception
            MsgErrorDev()
            MsgBox(sp_Query & param_Query)
        End Try
        LoadSprite.CloseLoading()
        Close()
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Sub Get_KebutuhanDaftar()
        '--------------------------------Tipe Syarat
        SQLquery = "SELECT id_syaratunlock ID, tipe_syaratunlock Tipe, IFNULL(syarat_tabelwhere,'') syarat_tabelwhere, operator FROM mbsm_syaratunlock WHERE inactive=0;"
        ExDb.ExecQuery(1, SQLquery, xSet, "DaftarSyaratUnlock")
        DaftarSyaratUnlock.Properties.DataSource = xSet.Tables("DaftarSyaratUnlock").DefaultView

        DaftarSyaratUnlock.Properties.NullText = ""
        DaftarSyaratUnlock.Properties.ValueMember = "ID"
        DaftarSyaratUnlock.Properties.DisplayMember = "Tipe"
        DaftarSyaratUnlock.Properties.ShowClearButton = False
        DaftarSyaratUnlock.Properties.PopulateViewColumns()

        DaftarSyaratUnlock.Properties.PopupFormSize = New Size(150, 300)

        ViewSyaratUnlock.Columns("ID").Visible = False
        ViewSyaratUnlock.Columns("syarat_tabelwhere").Visible = False
        ViewSyaratUnlock.Columns("operator").Visible = False

        For Each coll As DataColumn In xSet.Tables("DaftarSyaratUnlock").Columns
            ViewSyaratUnlock.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next
    End Sub

    Private Function beforeSave() As Boolean
        If NamaGift.Text.Length < 3 Or RewardGift.Text.Length < 3 Then
            MsgInfo("Nama gift / reward gift tidak boleh kosong atau terlalu pendek!")
            Return False
        End If

        If ExplanationGift.Text.Contains("'") Or RewardGift.Text.Contains("'") Or NamaGift.Text.Contains("'") Then
            MsgInfo("Inputan tidak boleh mengandung tanda petik (')!")
            Return False
        End If

        If Repetisi.EditValue < 1 Or exp_day.EditValue < 0 Then
            MsgInfo("Repetisi tidak boleh kurang dari 1 dan Expired date tidak boleh negatif!")
            Return False
        End If

        If syarat_none.Checked = False And (RewardValue.EditValue < 0 Or SyaratNilai.EditValue <= 0) Then
            MsgInfo("Nilai reward dan nilai syarat tidak boleh negatif!")
            Return False
        End If

        If exp_day.EditValue = 0 And exp_date.EditValue <= Today.Date Then
            MsgInfo("Tanggal expired tidak boleh kurang / sama dengan hari ini!")
            Return False
        End If

        If at_order.Checked = False And at_outlet.Checked = False Then
            MsgInfo("Harap pilih gift berlaku di outlet/order/keduanya!")
            Return False
        End If

        If use_outlet.Checked = False And use_order.Checked = False Then
            MsgInfo("Harap pilih gift dapat digunakan di outlet/order/keduanya!")
            Return False
        End If

        If use_expired.EditValue <= 0 Then
            MsgInfo("Jumlah expired setelah penukaran tidak boleh kurang atau sama dengan 0!")
            Return False
        End If

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        If NamaGift.Text <> nameCheck Then
            If Not isDoubleData(String.Format("SELECT id_gift ID FROM mbsm_gift WHERE nama_gift='{0}'", NamaGift.Text)) Then
                MsgWarning("Nama gift sudah ada dalam daftar!")
                Return False
            End If
        End If

        Return True
    End Function
End Class