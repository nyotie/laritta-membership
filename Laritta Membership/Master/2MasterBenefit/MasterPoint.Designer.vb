﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MasterPoint
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.CekDefault = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.DaftarTipeMembership = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.ViewTipeMembership = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.RatePoint = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.RateRp = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.ButBatal = New DevExpress.XtraEditors.SimpleButton()
        Me.ButSimpan = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.CekDefault.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DaftarTipeMembership.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewTipeMembership, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RatePoint.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RateRp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelControl1.Controls.Add(Me.CekDefault)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Controls.Add(Me.DaftarTipeMembership)
        Me.PanelControl1.Controls.Add(Me.LabelControl2)
        Me.PanelControl1.Controls.Add(Me.RatePoint)
        Me.PanelControl1.Controls.Add(Me.LabelControl4)
        Me.PanelControl1.Controls.Add(Me.RateRp)
        Me.PanelControl1.Controls.Add(Me.LabelControl5)
        Me.PanelControl1.Controls.Add(Me.LabelControl3)
        Me.PanelControl1.Location = New System.Drawing.Point(12, 12)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(357, 101)
        Me.PanelControl1.TabIndex = 1
        '
        'CekDefault
        '
        Me.CekDefault.Location = New System.Drawing.Point(103, 69)
        Me.CekDefault.Name = "CekDefault"
        Me.CekDefault.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.CekDefault.Properties.Appearance.Options.UseFont = True
        Me.CekDefault.Properties.Caption = "Jadikan default"
        Me.CekDefault.Size = New System.Drawing.Size(114, 19)
        Me.CekDefault.TabIndex = 4
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl1.Location = New System.Drawing.Point(13, 18)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(81, 14)
        Me.LabelControl1.TabIndex = 1
        Me.LabelControl1.Text = "Tipe member"
        '
        'DaftarTipeMembership
        '
        Me.DaftarTipeMembership.Location = New System.Drawing.Point(105, 15)
        Me.DaftarTipeMembership.Name = "DaftarTipeMembership"
        Me.DaftarTipeMembership.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.DaftarTipeMembership.Properties.Appearance.Options.UseFont = True
        Me.DaftarTipeMembership.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DaftarTipeMembership.Properties.NullText = ""
        Me.DaftarTipeMembership.Properties.View = Me.ViewTipeMembership
        Me.DaftarTipeMembership.Size = New System.Drawing.Size(239, 21)
        Me.DaftarTipeMembership.TabIndex = 1
        '
        'ViewTipeMembership
        '
        Me.ViewTipeMembership.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.ViewTipeMembership.Name = "ViewTipeMembership"
        Me.ViewTipeMembership.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.ViewTipeMembership.OptionsView.ShowGroupPanel = False
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl2.Location = New System.Drawing.Point(13, 45)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(29, 14)
        Me.LabelControl2.TabIndex = 1
        Me.LabelControl2.Text = "Rate"
        '
        'RatePoint
        '
        Me.RatePoint.Location = New System.Drawing.Point(237, 42)
        Me.RatePoint.Name = "RatePoint"
        Me.RatePoint.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.RatePoint.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.RatePoint.Properties.Appearance.Options.UseFont = True
        Me.RatePoint.Properties.Appearance.Options.UseTextOptions = True
        Me.RatePoint.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.RatePoint.Properties.Mask.EditMask = "n2"
        Me.RatePoint.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.RatePoint.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.RatePoint.Properties.NullText = "0.00"
        Me.RatePoint.Size = New System.Drawing.Size(43, 21)
        Me.RatePoint.TabIndex = 3
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl4.Location = New System.Drawing.Point(286, 45)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(32, 14)
        Me.LabelControl4.TabIndex = 1
        Me.LabelControl4.Text = "point"
        '
        'RateRp
        '
        Me.RateRp.Location = New System.Drawing.Point(123, 42)
        Me.RateRp.Name = "RateRp"
        Me.RateRp.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.RateRp.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.RateRp.Properties.Appearance.Options.UseFont = True
        Me.RateRp.Properties.Appearance.Options.UseTextOptions = True
        Me.RateRp.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.RateRp.Properties.Mask.EditMask = "n0"
        Me.RateRp.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.RateRp.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.RateRp.Properties.NullText = "0"
        Me.RateRp.Size = New System.Drawing.Size(94, 21)
        Me.RateRp.TabIndex = 2
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl5.Location = New System.Drawing.Point(100, 45)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(20, 14)
        Me.LabelControl5.TabIndex = 1
        Me.LabelControl5.Text = "Rp."
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl3.Location = New System.Drawing.Point(223, 45)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(9, 14)
        Me.LabelControl3.TabIndex = 1
        Me.LabelControl3.Text = "="
        '
        'ButBatal
        '
        Me.ButBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButBatal.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButBatal.Appearance.Options.UseFont = True
        Me.ButBatal.Image = Global.Laritta_Membership.My.Resources.Resources.back_32
        Me.ButBatal.Location = New System.Drawing.Point(279, 119)
        Me.ButBatal.Name = "ButBatal"
        Me.ButBatal.Size = New System.Drawing.Size(90, 40)
        Me.ButBatal.TabIndex = 6
        Me.ButBatal.Text = "&Batal"
        '
        'ButSimpan
        '
        Me.ButSimpan.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButSimpan.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButSimpan.Appearance.Options.UseFont = True
        Me.ButSimpan.Image = Global.Laritta_Membership.My.Resources.Resources.apply_32
        Me.ButSimpan.Location = New System.Drawing.Point(183, 119)
        Me.ButSimpan.Name = "ButSimpan"
        Me.ButSimpan.Size = New System.Drawing.Size(90, 40)
        Me.ButSimpan.TabIndex = 5
        Me.ButSimpan.Text = "&Simpan"
        '
        'MasterPoint
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(381, 171)
        Me.Controls.Add(Me.ButBatal)
        Me.Controls.Add(Me.ButSimpan)
        Me.Controls.Add(Me.PanelControl1)
        Me.Name = "MasterPoint"
        Me.Text = "MasterPoint"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.CekDefault.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DaftarTipeMembership.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewTipeMembership, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RatePoint.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RateRp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents RateRp As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CekDefault As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents DaftarTipeMembership As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents ViewTipeMembership As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents RatePoint As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ButBatal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButSimpan As DevExpress.XtraEditors.SimpleButton
End Class
