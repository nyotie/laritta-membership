﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MasterStamp
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.PeriodeEnd = New DevExpress.XtraEditors.DateEdit()
        Me.PeriodeStart = New DevExpress.XtraEditors.DateEdit()
        Me.CekOrder = New DevExpress.XtraEditors.CheckEdit()
        Me.CekMultiplied = New DevExpress.XtraEditors.CheckEdit()
        Me.CekOutlet = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.RateRp = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.LineShape2 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.LineShape1 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.ButBatal = New DevExpress.XtraEditors.SimpleButton()
        Me.ButSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.CekInaktif = New DevExpress.XtraEditors.CheckEdit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.PeriodeEnd.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PeriodeEnd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PeriodeStart.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PeriodeStart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CekOrder.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CekMultiplied.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CekOutlet.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RateRp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CekInaktif.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelControl1.Controls.Add(Me.LabelControl6)
        Me.PanelControl1.Controls.Add(Me.PeriodeEnd)
        Me.PanelControl1.Controls.Add(Me.PeriodeStart)
        Me.PanelControl1.Controls.Add(Me.CekOrder)
        Me.PanelControl1.Controls.Add(Me.CekMultiplied)
        Me.PanelControl1.Controls.Add(Me.CekOutlet)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Controls.Add(Me.LabelControl2)
        Me.PanelControl1.Controls.Add(Me.LabelControl3)
        Me.PanelControl1.Controls.Add(Me.LabelControl4)
        Me.PanelControl1.Controls.Add(Me.RateRp)
        Me.PanelControl1.Controls.Add(Me.LabelControl5)
        Me.PanelControl1.Controls.Add(Me.ShapeContainer1)
        Me.PanelControl1.Location = New System.Drawing.Point(12, 37)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(393, 166)
        Me.PanelControl1.TabIndex = 1
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl6.Location = New System.Drawing.Point(18, 117)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(40, 14)
        Me.LabelControl6.TabIndex = 1
        Me.LabelControl6.Text = "Lokasi"
        '
        'PeriodeEnd
        '
        Me.PeriodeEnd.EditValue = Nothing
        Me.PeriodeEnd.Location = New System.Drawing.Point(248, 15)
        Me.PeriodeEnd.Name = "PeriodeEnd"
        Me.PeriodeEnd.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.PeriodeEnd.Properties.Appearance.Options.UseFont = True
        Me.PeriodeEnd.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PeriodeEnd.Properties.Mask.EditMask = "dd MMM yyyy"
        Me.PeriodeEnd.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.PeriodeEnd.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.PeriodeEnd.Size = New System.Drawing.Size(133, 21)
        Me.PeriodeEnd.TabIndex = 2
        '
        'PeriodeStart
        '
        Me.PeriodeStart.EditValue = Nothing
        Me.PeriodeStart.Location = New System.Drawing.Point(98, 15)
        Me.PeriodeStart.Name = "PeriodeStart"
        Me.PeriodeStart.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.PeriodeStart.Properties.Appearance.Options.UseFont = True
        Me.PeriodeStart.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PeriodeStart.Properties.Mask.EditMask = "dd MMM yyyy"
        Me.PeriodeStart.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.PeriodeStart.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.PeriodeStart.Size = New System.Drawing.Size(133, 21)
        Me.PeriodeStart.TabIndex = 1
        '
        'CekOrder
        '
        Me.CekOrder.Location = New System.Drawing.Point(96, 139)
        Me.CekOrder.Name = "CekOrder"
        Me.CekOrder.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.CekOrder.Properties.Appearance.Options.UseFont = True
        Me.CekOrder.Properties.Caption = "Order"
        Me.CekOrder.Size = New System.Drawing.Size(135, 19)
        Me.CekOrder.TabIndex = 6
        '
        'CekMultiplied
        '
        Me.CekMultiplied.Location = New System.Drawing.Point(96, 78)
        Me.CekMultiplied.Name = "CekMultiplied"
        Me.CekMultiplied.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.CekMultiplied.Properties.Appearance.Options.UseFont = True
        Me.CekMultiplied.Properties.Caption = "Berlaku kelipatan"
        Me.CekMultiplied.Size = New System.Drawing.Size(135, 19)
        Me.CekMultiplied.TabIndex = 4
        '
        'CekOutlet
        '
        Me.CekOutlet.Location = New System.Drawing.Point(96, 114)
        Me.CekOutlet.Name = "CekOutlet"
        Me.CekOutlet.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.CekOutlet.Properties.Appearance.Options.UseFont = True
        Me.CekOutlet.Properties.Caption = "Outlet"
        Me.CekOutlet.Size = New System.Drawing.Size(135, 19)
        Me.CekOutlet.TabIndex = 5
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl1.Location = New System.Drawing.Point(18, 18)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(48, 14)
        Me.LabelControl1.TabIndex = 1
        Me.LabelControl1.Text = "Periode"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl2.Location = New System.Drawing.Point(17, 54)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(29, 14)
        Me.LabelControl2.TabIndex = 1
        Me.LabelControl2.Text = "Rate"
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl3.Location = New System.Drawing.Point(237, 18)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(5, 14)
        Me.LabelControl3.TabIndex = 1
        Me.LabelControl3.Text = "-"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl4.Location = New System.Drawing.Point(224, 54)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(44, 14)
        Me.LabelControl4.TabIndex = 1
        Me.LabelControl4.Text = "/stamp"
        '
        'RateRp
        '
        Me.RateRp.Location = New System.Drawing.Point(124, 51)
        Me.RateRp.Name = "RateRp"
        Me.RateRp.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.RateRp.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.RateRp.Properties.Appearance.Options.UseFont = True
        Me.RateRp.Properties.Appearance.Options.UseTextOptions = True
        Me.RateRp.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.RateRp.Properties.Mask.EditMask = "n0"
        Me.RateRp.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.RateRp.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.RateRp.Properties.NullText = "0"
        Me.RateRp.Size = New System.Drawing.Size(94, 21)
        Me.RateRp.TabIndex = 3
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl5.Location = New System.Drawing.Point(98, 54)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(20, 14)
        Me.LabelControl5.TabIndex = 1
        Me.LabelControl5.Text = "Rp."
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(2, 2)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape2, Me.LineShape1})
        Me.ShapeContainer1.Size = New System.Drawing.Size(389, 162)
        Me.ShapeContainer1.TabIndex = 6
        Me.ShapeContainer1.TabStop = False
        '
        'LineShape2
        '
        Me.LineShape2.BorderColor = System.Drawing.SystemColors.ButtonShadow
        Me.LineShape2.Name = "LineShape2"
        Me.LineShape2.X1 = 15
        Me.LineShape2.X2 = 379
        Me.LineShape2.Y1 = 101
        Me.LineShape2.Y2 = 101
        '
        'LineShape1
        '
        Me.LineShape1.BorderColor = System.Drawing.SystemColors.ButtonShadow
        Me.LineShape1.Name = "LineShape1"
        Me.LineShape1.X1 = 15
        Me.LineShape1.X2 = 379
        Me.LineShape1.Y1 = 40
        Me.LineShape1.Y2 = 40
        '
        'ButBatal
        '
        Me.ButBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButBatal.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButBatal.Appearance.Options.UseFont = True
        Me.ButBatal.Image = Global.Laritta_Membership.My.Resources.Resources.back_32
        Me.ButBatal.Location = New System.Drawing.Point(315, 209)
        Me.ButBatal.Name = "ButBatal"
        Me.ButBatal.Size = New System.Drawing.Size(90, 40)
        Me.ButBatal.TabIndex = 8
        Me.ButBatal.Text = "&Batal"
        '
        'ButSimpan
        '
        Me.ButSimpan.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButSimpan.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButSimpan.Appearance.Options.UseFont = True
        Me.ButSimpan.Image = Global.Laritta_Membership.My.Resources.Resources.apply_32
        Me.ButSimpan.Location = New System.Drawing.Point(219, 209)
        Me.ButSimpan.Name = "ButSimpan"
        Me.ButSimpan.Size = New System.Drawing.Size(90, 40)
        Me.ButSimpan.TabIndex = 7
        Me.ButSimpan.Text = "&Simpan"
        '
        'CekInaktif
        '
        Me.CekInaktif.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CekInaktif.Location = New System.Drawing.Point(315, 12)
        Me.CekInaktif.Name = "CekInaktif"
        Me.CekInaktif.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.CekInaktif.Properties.Appearance.Options.UseFont = True
        Me.CekInaktif.Properties.Caption = "Tidak aktif"
        Me.CekInaktif.Size = New System.Drawing.Size(90, 19)
        Me.CekInaktif.TabIndex = 0
        '
        'MasterStamp
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(417, 261)
        Me.Controls.Add(Me.CekInaktif)
        Me.Controls.Add(Me.ButBatal)
        Me.Controls.Add(Me.ButSimpan)
        Me.Controls.Add(Me.PanelControl1)
        Me.Name = "MasterStamp"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.PeriodeEnd.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PeriodeEnd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PeriodeStart.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PeriodeStart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CekOrder.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CekMultiplied.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CekOutlet.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RateRp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CekInaktif.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents RateRp As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CekOutlet As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ButBatal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PeriodeEnd As DevExpress.XtraEditors.DateEdit
    Friend WithEvents PeriodeStart As DevExpress.XtraEditors.DateEdit
    Friend WithEvents CekOrder As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CekMultiplied As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents LineShape1 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LineShape2 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents CekInaktif As DevExpress.XtraEditors.CheckEdit
End Class
