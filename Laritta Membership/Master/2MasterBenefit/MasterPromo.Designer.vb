﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MasterPromo
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.at_order = New DevExpress.XtraEditors.CheckEdit()
        Me.at_outlet = New DevExpress.XtraEditors.CheckEdit()
        Me.syarat_none = New DevExpress.XtraEditors.CheckEdit()
        Me.periode_end = New DevExpress.XtraEditors.DateEdit()
        Me.RewardPromo = New DevExpress.XtraEditors.TextEdit()
        Me.RewardValue = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.NamaPromo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.CekInaktif = New DevExpress.XtraEditors.CheckEdit()
        Me.ButBatal = New DevExpress.XtraEditors.SimpleButton()
        Me.ButSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.ExplanationPromo = New DevExpress.XtraEditors.MemoEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.DaftarParameterWhere = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.ViewParameterWhere = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.SyaratNilai = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelSyaratPenukaran = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.SyaratOperator = New DevExpress.XtraEditors.TextEdit()
        Me.DaftarSyaratUnlock = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.ViewSyaratUnlock = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.use_order = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.use_outlet = New DevExpress.XtraEditors.CheckEdit()
        Me.use_expired = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.LineShape3 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.LineShape1 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.RewardDisc = New DevExpress.XtraEditors.CheckEdit()
        Me.RewardType = New DevExpress.XtraEditors.RadioGroup()
        Me.Repetisi = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.CekMultiplied = New DevExpress.XtraEditors.CheckEdit()
        Me.DaftarTipeMembership = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.ViewTipeMembership = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.periode_start = New DevExpress.XtraEditors.DateEdit()
        Me.RewardMax = New DevExpress.XtraEditors.TextEdit()
        Me.ShapeContainer2 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.LineShape2 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        CType(Me.at_order.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.at_outlet.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.syarat_none.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.periode_end.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.periode_end.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RewardPromo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RewardValue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NamaPromo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CekInaktif.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ExplanationPromo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DaftarParameterWhere.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewParameterWhere, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SyaratNilai.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.PanelSyaratPenukaran, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelSyaratPenukaran.SuspendLayout()
        CType(Me.SyaratOperator.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DaftarSyaratUnlock.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewSyaratUnlock, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.use_order.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.use_outlet.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.use_expired.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.RewardDisc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RewardType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Repetisi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CekMultiplied.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DaftarTipeMembership.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewTipeMembership, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.periode_start.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.periode_start.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RewardMax.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'at_order
        '
        Me.at_order.Location = New System.Drawing.Point(206, 350)
        Me.at_order.Name = "at_order"
        Me.at_order.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.at_order.Properties.Appearance.Options.UseFont = True
        Me.at_order.Properties.Caption = "Order"
        Me.at_order.Size = New System.Drawing.Size(75, 19)
        Me.at_order.TabIndex = 12
        '
        'at_outlet
        '
        Me.at_outlet.Location = New System.Drawing.Point(125, 350)
        Me.at_outlet.Name = "at_outlet"
        Me.at_outlet.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.at_outlet.Properties.Appearance.Options.UseFont = True
        Me.at_outlet.Properties.Caption = "Outlet"
        Me.at_outlet.Size = New System.Drawing.Size(75, 19)
        Me.at_outlet.TabIndex = 11
        '
        'syarat_none
        '
        Me.syarat_none.Location = New System.Drawing.Point(16, 69)
        Me.syarat_none.Name = "syarat_none"
        Me.syarat_none.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.syarat_none.Properties.Appearance.Options.UseFont = True
        Me.syarat_none.Properties.Caption = "none"
        Me.syarat_none.Size = New System.Drawing.Size(75, 19)
        Me.syarat_none.TabIndex = 0
        '
        'periode_end
        '
        Me.periode_end.EditValue = Nothing
        Me.periode_end.Location = New System.Drawing.Point(268, 199)
        Me.periode_end.Name = "periode_end"
        Me.periode_end.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.periode_end.Properties.Appearance.Options.UseFont = True
        Me.periode_end.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.periode_end.Properties.Mask.EditMask = "dd MMMM yyyy"
        Me.periode_end.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.periode_end.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.periode_end.Size = New System.Drawing.Size(125, 21)
        Me.periode_end.TabIndex = 5
        '
        'RewardPromo
        '
        Me.RewardPromo.Location = New System.Drawing.Point(127, 268)
        Me.RewardPromo.Name = "RewardPromo"
        Me.RewardPromo.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.RewardPromo.Properties.Appearance.Options.UseFont = True
        Me.RewardPromo.Size = New System.Drawing.Size(266, 21)
        Me.RewardPromo.TabIndex = 7
        '
        'RewardValue
        '
        Me.RewardValue.EditValue = ""
        Me.RewardValue.Location = New System.Drawing.Point(127, 295)
        Me.RewardValue.Name = "RewardValue"
        Me.RewardValue.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.RewardValue.Properties.Appearance.Options.UseFont = True
        Me.RewardValue.Properties.Appearance.Options.UseTextOptions = True
        Me.RewardValue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.RewardValue.Properties.Mask.EditMask = "n0"
        Me.RewardValue.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.RewardValue.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.RewardValue.Properties.NullText = "0"
        Me.RewardValue.Size = New System.Drawing.Size(111, 21)
        Me.RewardValue.TabIndex = 8
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl9.Location = New System.Drawing.Point(14, 271)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(48, 14)
        Me.LabelControl9.TabIndex = 0
        Me.LabelControl9.Text = "Reward"
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl6.Location = New System.Drawing.Point(14, 352)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(62, 14)
        Me.LabelControl6.TabIndex = 0
        Me.LabelControl6.Text = "Berlaku di"
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl5.Location = New System.Drawing.Point(14, 298)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(75, 14)
        Me.LabelControl5.TabIndex = 0
        Me.LabelControl5.Text = "Nilai reward"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl4.Location = New System.Drawing.Point(258, 203)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(5, 14)
        Me.LabelControl4.TabIndex = 0
        Me.LabelControl4.Text = "-"
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl3.Location = New System.Drawing.Point(14, 202)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(48, 14)
        Me.LabelControl3.TabIndex = 0
        Me.LabelControl3.Text = "Periode"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl2.Location = New System.Drawing.Point(14, 175)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(50, 14)
        Me.LabelControl2.TabIndex = 0
        Me.LabelControl2.Text = "Repetisi"
        '
        'NamaPromo
        '
        Me.NamaPromo.Location = New System.Drawing.Point(127, 62)
        Me.NamaPromo.Name = "NamaPromo"
        Me.NamaPromo.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.NamaPromo.Properties.Appearance.Options.UseFont = True
        Me.NamaPromo.Size = New System.Drawing.Size(266, 21)
        Me.NamaPromo.TabIndex = 1
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl1.Location = New System.Drawing.Point(14, 65)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(80, 14)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Nama promo"
        '
        'CekInaktif
        '
        Me.CekInaktif.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CekInaktif.Location = New System.Drawing.Point(671, 12)
        Me.CekInaktif.Name = "CekInaktif"
        Me.CekInaktif.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.CekInaktif.Properties.Appearance.Options.UseFont = True
        Me.CekInaktif.Properties.Caption = "Tidak aktif"
        Me.CekInaktif.Size = New System.Drawing.Size(91, 19)
        Me.CekInaktif.TabIndex = 0
        '
        'ButBatal
        '
        Me.ButBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButBatal.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButBatal.Appearance.Options.UseFont = True
        Me.ButBatal.Image = Global.Laritta_Membership.My.Resources.Resources.back_32
        Me.ButBatal.Location = New System.Drawing.Point(672, 446)
        Me.ButBatal.Name = "ButBatal"
        Me.ButBatal.Size = New System.Drawing.Size(90, 40)
        Me.ButBatal.TabIndex = 4
        Me.ButBatal.Text = "&Batal"
        '
        'ButSimpan
        '
        Me.ButSimpan.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButSimpan.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButSimpan.Appearance.Options.UseFont = True
        Me.ButSimpan.Image = Global.Laritta_Membership.My.Resources.Resources.apply_32
        Me.ButSimpan.Location = New System.Drawing.Point(576, 446)
        Me.ButSimpan.Name = "ButSimpan"
        Me.ButSimpan.Size = New System.Drawing.Size(90, 40)
        Me.ButSimpan.TabIndex = 3
        Me.ButSimpan.Text = "&Simpan"
        '
        'ExplanationPromo
        '
        Me.ExplanationPromo.Location = New System.Drawing.Point(127, 89)
        Me.ExplanationPromo.Name = "ExplanationPromo"
        Me.ExplanationPromo.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ExplanationPromo.Properties.Appearance.Options.UseFont = True
        Me.ExplanationPromo.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.ExplanationPromo.Size = New System.Drawing.Size(266, 77)
        Me.ExplanationPromo.TabIndex = 2
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl11.Location = New System.Drawing.Point(14, 92)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(70, 14)
        Me.LabelControl11.TabIndex = 0
        Me.LabelControl11.Text = "Penjelasan"
        '
        'DaftarParameterWhere
        '
        Me.DaftarParameterWhere.Enabled = False
        Me.DaftarParameterWhere.Location = New System.Drawing.Point(110, 42)
        Me.DaftarParameterWhere.Name = "DaftarParameterWhere"
        Me.DaftarParameterWhere.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.DaftarParameterWhere.Properties.Appearance.Options.UseFont = True
        Me.DaftarParameterWhere.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DaftarParameterWhere.Properties.NullText = ""
        Me.DaftarParameterWhere.Properties.View = Me.ViewParameterWhere
        Me.DaftarParameterWhere.Size = New System.Drawing.Size(181, 21)
        Me.DaftarParameterWhere.TabIndex = 2
        '
        'ViewParameterWhere
        '
        Me.ViewParameterWhere.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.ViewParameterWhere.Name = "ViewParameterWhere"
        Me.ViewParameterWhere.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.ViewParameterWhere.OptionsView.ShowGroupPanel = False
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl12.Location = New System.Drawing.Point(18, 42)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(109, 14)
        Me.LabelControl12.TabIndex = 0
        Me.LabelControl12.Text = "Syarat penukaran"
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl13.Location = New System.Drawing.Point(13, 46)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(66, 14)
        Me.LabelControl13.TabIndex = 0
        Me.LabelControl13.Text = "Parameter"
        '
        'LabelControl14
        '
        Me.LabelControl14.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl14.Location = New System.Drawing.Point(13, 73)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(57, 14)
        Me.LabelControl14.TabIndex = 0
        Me.LabelControl14.Text = "Operator"
        '
        'SyaratNilai
        '
        Me.SyaratNilai.Location = New System.Drawing.Point(110, 96)
        Me.SyaratNilai.Name = "SyaratNilai"
        Me.SyaratNilai.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.SyaratNilai.Properties.Appearance.Options.UseFont = True
        Me.SyaratNilai.Properties.Appearance.Options.UseTextOptions = True
        Me.SyaratNilai.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.SyaratNilai.Properties.Mask.EditMask = "n2"
        Me.SyaratNilai.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.SyaratNilai.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.SyaratNilai.Properties.NullText = "0.00"
        Me.SyaratNilai.Size = New System.Drawing.Size(153, 21)
        Me.SyaratNilai.TabIndex = 4
        '
        'LabelControl15
        '
        Me.LabelControl15.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl15.Location = New System.Drawing.Point(13, 100)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(70, 14)
        Me.LabelControl15.TabIndex = 0
        Me.LabelControl15.Text = "Nilai syarat"
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.Controls.Add(Me.LabelControl19)
        Me.GroupControl1.Controls.Add(Me.LabelControl12)
        Me.GroupControl1.Controls.Add(Me.PanelSyaratPenukaran)
        Me.GroupControl1.Controls.Add(Me.use_order)
        Me.GroupControl1.Controls.Add(Me.LabelControl8)
        Me.GroupControl1.Controls.Add(Me.use_outlet)
        Me.GroupControl1.Controls.Add(Me.use_expired)
        Me.GroupControl1.Controls.Add(Me.LabelControl17)
        Me.GroupControl1.Controls.Add(Me.LabelControl16)
        Me.GroupControl1.Controls.Add(Me.syarat_none)
        Me.GroupControl1.Controls.Add(Me.ShapeContainer1)
        Me.GroupControl1.Location = New System.Drawing.Point(428, 37)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(334, 403)
        Me.GroupControl1.TabIndex = 2
        Me.GroupControl1.Text = "Syarat"
        '
        'LabelControl19
        '
        Me.LabelControl19.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl19.Location = New System.Drawing.Point(18, 248)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(121, 14)
        Me.LabelControl19.TabIndex = 0
        Me.LabelControl19.Text = "Syarat penggunaan"
        '
        'PanelSyaratPenukaran
        '
        Me.PanelSyaratPenukaran.Controls.Add(Me.LabelControl15)
        Me.PanelSyaratPenukaran.Controls.Add(Me.LabelControl20)
        Me.PanelSyaratPenukaran.Controls.Add(Me.LabelControl14)
        Me.PanelSyaratPenukaran.Controls.Add(Me.LabelControl13)
        Me.PanelSyaratPenukaran.Controls.Add(Me.DaftarParameterWhere)
        Me.PanelSyaratPenukaran.Controls.Add(Me.SyaratNilai)
        Me.PanelSyaratPenukaran.Controls.Add(Me.SyaratOperator)
        Me.PanelSyaratPenukaran.Controls.Add(Me.DaftarSyaratUnlock)
        Me.PanelSyaratPenukaran.Location = New System.Drawing.Point(18, 87)
        Me.PanelSyaratPenukaran.Name = "PanelSyaratPenukaran"
        Me.PanelSyaratPenukaran.Size = New System.Drawing.Size(304, 134)
        Me.PanelSyaratPenukaran.TabIndex = 1
        '
        'LabelControl20
        '
        Me.LabelControl20.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl20.Location = New System.Drawing.Point(11, 18)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(70, 14)
        Me.LabelControl20.TabIndex = 0
        Me.LabelControl20.Text = "Tipe syarat"
        '
        'SyaratOperator
        '
        Me.SyaratOperator.EditValue = ">"
        Me.SyaratOperator.Location = New System.Drawing.Point(110, 69)
        Me.SyaratOperator.Name = "SyaratOperator"
        Me.SyaratOperator.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.SyaratOperator.Properties.Appearance.Options.UseFont = True
        Me.SyaratOperator.Properties.Mask.EditMask = "(>|=)=?"
        Me.SyaratOperator.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.SyaratOperator.Size = New System.Drawing.Size(57, 21)
        Me.SyaratOperator.TabIndex = 3
        '
        'DaftarSyaratUnlock
        '
        Me.DaftarSyaratUnlock.Location = New System.Drawing.Point(110, 15)
        Me.DaftarSyaratUnlock.Name = "DaftarSyaratUnlock"
        Me.DaftarSyaratUnlock.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.DaftarSyaratUnlock.Properties.Appearance.Options.UseFont = True
        Me.DaftarSyaratUnlock.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DaftarSyaratUnlock.Properties.NullText = ""
        Me.DaftarSyaratUnlock.Properties.View = Me.ViewSyaratUnlock
        Me.DaftarSyaratUnlock.Size = New System.Drawing.Size(181, 21)
        Me.DaftarSyaratUnlock.TabIndex = 1
        '
        'ViewSyaratUnlock
        '
        Me.ViewSyaratUnlock.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.ViewSyaratUnlock.Name = "ViewSyaratUnlock"
        Me.ViewSyaratUnlock.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.ViewSyaratUnlock.OptionsView.ShowGroupPanel = False
        '
        'use_order
        '
        Me.use_order.Location = New System.Drawing.Point(194, 273)
        Me.use_order.Name = "use_order"
        Me.use_order.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.use_order.Properties.Appearance.Options.UseFont = True
        Me.use_order.Properties.Caption = "Order"
        Me.use_order.Size = New System.Drawing.Size(75, 19)
        Me.use_order.TabIndex = 3
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl8.Location = New System.Drawing.Point(18, 276)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(62, 14)
        Me.LabelControl8.TabIndex = 12
        Me.LabelControl8.Text = "Berlaku di"
        '
        'use_outlet
        '
        Me.use_outlet.Location = New System.Drawing.Point(113, 272)
        Me.use_outlet.Name = "use_outlet"
        Me.use_outlet.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.use_outlet.Properties.Appearance.Options.UseFont = True
        Me.use_outlet.Properties.Caption = "Outlet"
        Me.use_outlet.Size = New System.Drawing.Size(75, 19)
        Me.use_outlet.TabIndex = 2
        '
        'use_expired
        '
        Me.use_expired.EditValue = "1"
        Me.use_expired.Location = New System.Drawing.Point(115, 297)
        Me.use_expired.Name = "use_expired"
        Me.use_expired.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.use_expired.Properties.Appearance.Options.UseFont = True
        Me.use_expired.Properties.Appearance.Options.UseTextOptions = True
        Me.use_expired.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.use_expired.Properties.Mask.EditMask = "n0"
        Me.use_expired.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.use_expired.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.use_expired.Properties.MaxLength = 3
        Me.use_expired.Properties.NullText = "0"
        Me.use_expired.Size = New System.Drawing.Size(41, 21)
        Me.use_expired.TabIndex = 4
        '
        'LabelControl17
        '
        Me.LabelControl17.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl17.Location = New System.Drawing.Point(162, 300)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(147, 14)
        Me.LabelControl17.TabIndex = 22
        Me.LabelControl17.Text = "hari setelah penukaran"
        '
        'LabelControl16
        '
        Me.LabelControl16.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl16.Location = New System.Drawing.Point(18, 300)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(47, 14)
        Me.LabelControl16.TabIndex = 12
        Me.LabelControl16.Text = "Expired"
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(2, 23)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape3, Me.LineShape1})
        Me.ShapeContainer1.Size = New System.Drawing.Size(330, 378)
        Me.ShapeContainer1.TabIndex = 0
        Me.ShapeContainer1.TabStop = False
        '
        'LineShape3
        '
        Me.LineShape3.BorderColor = System.Drawing.SystemColors.ButtonShadow
        Me.LineShape3.Name = "LineShape3"
        Me.LineShape3.X1 = 10
        Me.LineShape3.X2 = 320
        Me.LineShape3.Y1 = 240
        Me.LineShape3.Y2 = 240
        '
        'LineShape1
        '
        Me.LineShape1.BorderColor = System.Drawing.SystemColors.ButtonShadow
        Me.LineShape1.Name = "LineShape1"
        Me.LineShape1.X1 = 10
        Me.LineShape1.X2 = 320
        Me.LineShape1.Y1 = 35
        Me.LineShape1.Y2 = 35
        '
        'GroupControl2
        '
        Me.GroupControl2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupControl2.AppearanceCaption.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.GroupControl2.AppearanceCaption.Options.UseFont = True
        Me.GroupControl2.Controls.Add(Me.RewardDisc)
        Me.GroupControl2.Controls.Add(Me.RewardType)
        Me.GroupControl2.Controls.Add(Me.Repetisi)
        Me.GroupControl2.Controls.Add(Me.CekMultiplied)
        Me.GroupControl2.Controls.Add(Me.DaftarTipeMembership)
        Me.GroupControl2.Controls.Add(Me.ExplanationPromo)
        Me.GroupControl2.Controls.Add(Me.LabelControl9)
        Me.GroupControl2.Controls.Add(Me.LabelControl7)
        Me.GroupControl2.Controls.Add(Me.LabelControl1)
        Me.GroupControl2.Controls.Add(Me.LabelControl18)
        Me.GroupControl2.Controls.Add(Me.LabelControl11)
        Me.GroupControl2.Controls.Add(Me.NamaPromo)
        Me.GroupControl2.Controls.Add(Me.LabelControl10)
        Me.GroupControl2.Controls.Add(Me.LabelControl5)
        Me.GroupControl2.Controls.Add(Me.periode_start)
        Me.GroupControl2.Controls.Add(Me.periode_end)
        Me.GroupControl2.Controls.Add(Me.LabelControl6)
        Me.GroupControl2.Controls.Add(Me.LabelControl2)
        Me.GroupControl2.Controls.Add(Me.LabelControl3)
        Me.GroupControl2.Controls.Add(Me.RewardMax)
        Me.GroupControl2.Controls.Add(Me.LabelControl4)
        Me.GroupControl2.Controls.Add(Me.at_order)
        Me.GroupControl2.Controls.Add(Me.RewardValue)
        Me.GroupControl2.Controls.Add(Me.RewardPromo)
        Me.GroupControl2.Controls.Add(Me.at_outlet)
        Me.GroupControl2.Controls.Add(Me.ShapeContainer2)
        Me.GroupControl2.Location = New System.Drawing.Point(12, 37)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(410, 403)
        Me.GroupControl2.TabIndex = 1
        Me.GroupControl2.Text = "Informasi Promo"
        '
        'RewardDisc
        '
        Me.RewardDisc.Enabled = False
        Me.RewardDisc.Location = New System.Drawing.Point(244, 295)
        Me.RewardDisc.Name = "RewardDisc"
        Me.RewardDisc.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.RewardDisc.Properties.Appearance.Options.UseFont = True
        Me.RewardDisc.Properties.Caption = "%"
        Me.RewardDisc.Size = New System.Drawing.Size(45, 19)
        Me.RewardDisc.TabIndex = 9
        '
        'RewardType
        '
        Me.RewardType.EditValue = "brg"
        Me.RewardType.Location = New System.Drawing.Point(127, 237)
        Me.RewardType.Name = "RewardType"
        Me.RewardType.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem("brg", "Barang"), New DevExpress.XtraEditors.Controls.RadioGroupItem("disc", "Diskon")})
        Me.RewardType.Size = New System.Drawing.Size(162, 25)
        Me.RewardType.TabIndex = 6
        '
        'Repetisi
        '
        Me.Repetisi.Location = New System.Drawing.Point(127, 172)
        Me.Repetisi.Name = "Repetisi"
        Me.Repetisi.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.Repetisi.Properties.Appearance.Options.UseFont = True
        Me.Repetisi.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.Repetisi.Properties.Items.AddRange(New Object() {"Harian", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu", "Mingguan", "Bulanan", "Tahunan", "Transaksi"})
        Me.Repetisi.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.Repetisi.Size = New System.Drawing.Size(125, 21)
        Me.Repetisi.TabIndex = 3
        '
        'CekMultiplied
        '
        Me.CekMultiplied.Location = New System.Drawing.Point(125, 375)
        Me.CekMultiplied.Name = "CekMultiplied"
        Me.CekMultiplied.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.CekMultiplied.Properties.Appearance.Options.UseFont = True
        Me.CekMultiplied.Properties.Caption = "Berlaku kelipatan"
        Me.CekMultiplied.Size = New System.Drawing.Size(137, 19)
        Me.CekMultiplied.TabIndex = 13
        '
        'DaftarTipeMembership
        '
        Me.DaftarTipeMembership.Location = New System.Drawing.Point(127, 35)
        Me.DaftarTipeMembership.Name = "DaftarTipeMembership"
        Me.DaftarTipeMembership.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.DaftarTipeMembership.Properties.Appearance.Options.UseFont = True
        Me.DaftarTipeMembership.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DaftarTipeMembership.Properties.NullText = ""
        Me.DaftarTipeMembership.Properties.View = Me.ViewTipeMembership
        Me.DaftarTipeMembership.Size = New System.Drawing.Size(192, 21)
        Me.DaftarTipeMembership.TabIndex = 0
        '
        'ViewTipeMembership
        '
        Me.ViewTipeMembership.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.ViewTipeMembership.Name = "ViewTipeMembership"
        Me.ViewTipeMembership.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.ViewTipeMembership.OptionsView.ShowGroupPanel = False
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl7.Location = New System.Drawing.Point(14, 38)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(107, 14)
        Me.LabelControl7.TabIndex = 0
        Me.LabelControl7.Text = "Tipe membership"
        '
        'LabelControl18
        '
        Me.LabelControl18.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl18.Location = New System.Drawing.Point(14, 325)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(74, 14)
        Me.LabelControl18.TabIndex = 0
        Me.LabelControl18.Text = "Max reward"
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl10.Location = New System.Drawing.Point(14, 243)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(83, 14)
        Me.LabelControl10.TabIndex = 0
        Me.LabelControl10.Text = "Jenis Reward"
        '
        'periode_start
        '
        Me.periode_start.EditValue = Nothing
        Me.periode_start.Location = New System.Drawing.Point(127, 199)
        Me.periode_start.Name = "periode_start"
        Me.periode_start.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.periode_start.Properties.Appearance.Options.UseFont = True
        Me.periode_start.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.periode_start.Properties.Mask.EditMask = "dd MMMM yyyy"
        Me.periode_start.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.periode_start.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.periode_start.Size = New System.Drawing.Size(125, 21)
        Me.periode_start.TabIndex = 4
        '
        'RewardMax
        '
        Me.RewardMax.Location = New System.Drawing.Point(127, 322)
        Me.RewardMax.Name = "RewardMax"
        Me.RewardMax.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.RewardMax.Properties.Appearance.Options.UseFont = True
        Me.RewardMax.Properties.Appearance.Options.UseTextOptions = True
        Me.RewardMax.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.RewardMax.Properties.Mask.EditMask = "n0"
        Me.RewardMax.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.RewardMax.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.RewardMax.Properties.NullText = "0"
        Me.RewardMax.Size = New System.Drawing.Size(111, 21)
        Me.RewardMax.TabIndex = 10
        '
        'ShapeContainer2
        '
        Me.ShapeContainer2.Location = New System.Drawing.Point(2, 23)
        Me.ShapeContainer2.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer2.Name = "ShapeContainer2"
        Me.ShapeContainer2.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape2})
        Me.ShapeContainer2.Size = New System.Drawing.Size(406, 378)
        Me.ShapeContainer2.TabIndex = 14
        Me.ShapeContainer2.TabStop = False
        '
        'LineShape2
        '
        Me.LineShape2.BorderColor = System.Drawing.SystemColors.ButtonShadow
        Me.LineShape2.Name = "LineShape2"
        Me.LineShape2.X1 = 8
        Me.LineShape2.X2 = 398
        Me.LineShape2.Y1 = 205
        Me.LineShape2.Y2 = 205
        '
        'MasterPromo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(774, 498)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.ButBatal)
        Me.Controls.Add(Me.ButSimpan)
        Me.Controls.Add(Me.CekInaktif)
        Me.Controls.Add(Me.GroupControl2)
        Me.Name = "MasterPromo"
        CType(Me.at_order.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.at_outlet.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.syarat_none.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.periode_end.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.periode_end.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RewardPromo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RewardValue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NamaPromo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CekInaktif.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ExplanationPromo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DaftarParameterWhere.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewParameterWhere, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SyaratNilai.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.PanelSyaratPenukaran, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelSyaratPenukaran.ResumeLayout(False)
        Me.PanelSyaratPenukaran.PerformLayout()
        CType(Me.SyaratOperator.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DaftarSyaratUnlock.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewSyaratUnlock, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.use_order.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.use_outlet.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.use_expired.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.RewardDisc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RewardType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Repetisi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CekMultiplied.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DaftarTipeMembership.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewTipeMembership, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.periode_start.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.periode_start.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RewardMax.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents CekInaktif As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ButBatal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents at_order As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents at_outlet As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents syarat_none As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents periode_end As DevExpress.XtraEditors.DateEdit
    Friend WithEvents RewardValue As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents NamaPromo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents RewardPromo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ExplanationPromo As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DaftarParameterWhere As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents ViewParameterWhere As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SyaratNilai As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents SyaratOperator As DevExpress.XtraEditors.TextEdit
    Friend WithEvents DaftarSyaratUnlock As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents ViewSyaratUnlock As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents periode_start As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DaftarTipeMembership As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents ViewTipeMembership As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Repetisi As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents CekMultiplied As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents use_order As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents use_outlet As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents use_expired As DevExpress.XtraEditors.TextEdit
    Friend WithEvents RewardType As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents RewardMax As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ShapeContainer2 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents LineShape2 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents PanelSyaratPenukaran As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents LineShape3 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents LineShape1 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents RewardDisc As DevExpress.XtraEditors.CheckEdit
End Class
