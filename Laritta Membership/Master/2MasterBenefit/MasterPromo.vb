﻿Imports DevExpress.Utils

Public Class MasterPromo

    Private Sub MasterPromo_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("DataMasterPromo")
        xSet.Tables.Remove("DaftarTipeMembership")
        xSet.Tables.Remove("DaftarSyaratUnlock")
        xSet.Tables.Remove("DaftarParameterWhere")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub MasterPromo_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()
        Get_KebutuhanDaftar()

        If Not selmaster_id = 0 Then
            SQLquery = String.Format("SELECT id_tipemembership, nama_promo, promo_explanation, syaratperiode_start, syaratperiode_end, repetisi_promo, syarat_none, syarat_tipe, syarat_operator, syarat_tipevalue, syarat_wherevalue, reward_jenis, reward_promo, reward_disc, reward_value, reward_max, exc_at_outlet, exc_at_order, ismultiplied, use_at_outlet, use_at_order, use_expired, Inactive FROM mbsm_promo WHERE id_promo={0}", selmaster_id)
            ExDb.ExecQuery(1, SQLquery, xSet, "DataMasterPromo")

            DaftarTipeMembership.EditValue = xSet.Tables("DataMasterPromo").Rows(0).Item("id_tipemembership")
            NamaPromo.EditValue = xSet.Tables("DataMasterPromo").Rows(0).Item("nama_promo")
            ExplanationPromo.EditValue = xSet.Tables("DataMasterPromo").Rows(0).Item("promo_explanation")
            Repetisi.SelectedIndex = xSet.Tables("DataMasterPromo").Rows(0).Item("repetisi_promo")
            periode_start.EditValue = xSet.Tables("DataMasterPromo").Rows(0).Item("syaratperiode_start")
            periode_end.EditValue = xSet.Tables("DataMasterPromo").Rows(0).Item("syaratperiode_end")

            RewardType.EditValue = xSet.Tables("DataMasterPromo").Rows(0).Item("reward_jenis")
            RewardPromo.EditValue = xSet.Tables("DataMasterPromo").Rows(0).Item("reward_promo")
            RewardDisc.Checked = xSet.Tables("DataMasterPromo").Rows(0).Item("reward_disc")
            RewardValue.EditValue = xSet.Tables("DataMasterPromo").Rows(0).Item("reward_value")
            RewardMax.EditValue = xSet.Tables("DataMasterPromo").Rows(0).Item("reward_max")

            CekMultiplied.Checked = xSet.Tables("DataMasterPromo").Rows(0).Item("ismultiplied")
            at_outlet.Checked = xSet.Tables("DataMasterPromo").Rows(0).Item("exc_at_outlet")
            at_order.Checked = xSet.Tables("DataMasterPromo").Rows(0).Item("exc_at_order")

            syarat_none.Checked = xSet.Tables("DataMasterPromo").Rows(0).Item("syarat_none")
            If Not syarat_none.Checked Then
                DaftarSyaratUnlock.EditValue = xSet.Tables("DataMasterPromo").Rows(0).Item("syarat_tipe")
                DaftarParameterWhere.EditValue = xSet.Tables("DataMasterPromo").Rows(0).Item("syarat_wherevalue")
                SyaratOperator.EditValue = xSet.Tables("DataMasterPromo").Rows(0).Item("syarat_operator")
                SyaratNilai.EditValue = xSet.Tables("DataMasterPromo").Rows(0).Item("syarat_tipevalue")
            End If

            use_outlet.Checked = xSet.Tables("DataMasterPromo").Rows(0).Item("use_at_outlet")
            use_order.Checked = xSet.Tables("DataMasterPromo").Rows(0).Item("use_at_order")
            use_expired.EditValue = xSet.Tables("DataMasterPromo").Rows(0).Item("use_expired")

            CekInaktif.Checked = xSet.Tables("DataMasterPromo").Rows(0).Item("Inactive")

            nameCheck = NamaPromo.EditValue
        End If
    End Sub

    Private Sub MasterPromo_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        LoadSprite.CloseLoading()
        DaftarTipeMembership.Focus()
        If selmaster_id = 0 Then
            CekInaktif.Properties.ReadOnly = True
            CekInaktif.Checked = False
            periode_start.EditValue = DateAdd(DateInterval.Day, 1, Today.Date)
            periode_end.EditValue = DateAdd(DateInterval.Day, 8, Today.Date)
        End If
    End Sub

    Private Sub RewardType_EditValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ChangingEventArgs) Handles RewardType.EditValueChanging
        If e.NewValue = "brg" Then
            RewardDisc.Checked = False
            RewardDisc.Enabled = False
        Else
            RewardDisc.Checked = False
            RewardDisc.Enabled = True
        End If
    End Sub

    Private Sub RewardDisc_EditValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ChangingEventArgs) Handles RewardDisc.EditValueChanging
        If e.NewValue = True Then
            'disc %
            RewardValue.EditValue = 0
            RewardValue.Properties.MaxLength = 3
            RewardValue.Properties.Mask.EditMask = "P0"
        Else
            'nominal
            RewardValue.EditValue = 0
            RewardValue.Properties.MaxLength = 0
            RewardValue.Properties.Mask.EditMask = "n0"
        End If
    End Sub

    Private Sub syarat_none_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles syarat_none.CheckedChanged
        Try
            If syarat_none.Checked = True Then
                RewardMax.EditValue = 1
                RewardMax.Enabled = False
                CekMultiplied.Checked = False
                CekMultiplied.Enabled = False
                at_outlet.Checked = True
                at_outlet.Enabled = False
                at_order.Checked = True
                at_order.Enabled = False

                DaftarSyaratUnlock.EditValue = Nothing
                DaftarParameterWhere.EditValue = Nothing

                PanelSyaratPenukaran.Enabled = False
            Else
                RewardMax.Enabled = True
                CekMultiplied.Enabled = True
                at_outlet.Enabled = True
                at_order.Enabled = True

                PanelSyaratPenukaran.Enabled = True
            End If
        Catch ex As Exception
            MsgErrorDev()
        End Try
    End Sub

    Private Sub DaftarSyaratUnlock_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DaftarSyaratUnlock.EditValueChanged
        If DaftarSyaratUnlock.EditValue = Nothing Then GoTo disableme

        Dim this_syarat As String = xSet.Tables("DaftarSyaratUnlock").Select("ID=" & DaftarSyaratUnlock.EditValue)(0).Item("syarat_tabelwhere")
        DaftarParameterWhere.EditValue = Nothing

        If this_syarat = "" Then
disableme:  DaftarParameterWhere.Enabled = False
            Return
        Else
            DaftarParameterWhere.Enabled = True
        End If

        If Not xSet.Tables("DaftarParameterWhere") Is Nothing Then xSet.Tables("DaftarParameterWhere").Clear()
        If this_syarat = "mbarang" Then
            SQLquery = "SELECT idBrg ID, namaBrg Nama FROM mbarang WHERE inactive=0;"
        ElseIf this_syarat = "mkategoribrg2" Then
            'ElseIf ViewSyaratUnlock.GetFocusedRowCellValue("syarat_tabelwhere") = "mkategoribrg2" Then
            SQLquery = "SELECT idKategori2 ID, namaKategori2 Nama FROM mkategoribrg2 WHERE inactive=0;"
        End If
        ExDb.ExecQuery(1, SQLquery, xSet, "DaftarParameterWhere")
        DaftarParameterWhere.Properties.DataSource = xSet.Tables("DaftarParameterWhere").DefaultView
        DaftarParameterWhere.Properties.NullText = ""
        DaftarParameterWhere.Properties.ValueMember = "ID"
        DaftarParameterWhere.Properties.DisplayMember = "Nama"
        DaftarParameterWhere.Properties.ShowClearButton = False
        DaftarParameterWhere.Properties.PopulateViewColumns()

        DaftarParameterWhere.Properties.PopupFormSize = New Size(300, 300)

        ViewParameterWhere.Columns("ID").Visible = False
    End Sub

    Private Sub ButSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSimpan.Click
        If Not beforeSave() Then Return
        Try
            LoadSprite.ShowLoading()
            If selmaster_id = "0" Then
                sp_Query = "INSERT INTO mbsm_promo(id_tipemembership, nama_promo, promo_explanation, syaratperiode_start, syaratperiode_end, repetisi_promo, syarat_none, " & _
                    "syarat_tipe, syarat_operator, syarat_tipevalue, syarat_wherevalue, reward_jenis, reward_promo, reward_disc, reward_value, reward_max, exc_at_outlet, exc_at_order, ismultiplied, " & _
                    "use_at_outlet, use_at_order, use_expired, createby, createdate, Inactive) "
                param_Query = String.Format("VALUES ({0}, '{1}', '{2}', '{3}', '{4}', {5}, {6}, {7}, {8}, {9}, {10}, '{11}', '{12}', {13}, {14}, {15}, {16}, {17}, {18}, {19}, {20}, {21}, {22}, NOW(), {23}) ",
                                            DaftarTipeMembership.EditValue,
                                            NamaPromo.Text,
                                            ExplanationPromo.Text,
                                            Format(periode_start.EditValue, "yyyy-MM-dd"),
                                            Format(periode_end.EditValue, "yyyy-MM-dd"),
                                            Repetisi.SelectedIndex,
                                            syarat_none.Checked,
                                            IIf(Not syarat_none.Checked, "'" & DaftarSyaratUnlock.EditValue & "'", "NULL"),
                                            IIf(Not syarat_none.Checked, "'" & SyaratOperator.EditValue & "'", "NULL"),
                                            IIf(Not syarat_none.Checked, SyaratNilai.EditValue, "NULL"),
                                            IIf(Not syarat_none.Checked And DaftarParameterWhere.Enabled, DaftarParameterWhere.EditValue, "NULL"),
                                            RewardType.EditValue,
                                            RewardPromo.EditValue,
                                            RewardDisc.Checked,
                                            RewardValue.EditValue,
                                            RewardMax.EditValue,
                                            at_outlet.Checked,
                                            at_order.Checked,
                                            CekMultiplied.Checked,
                                            use_outlet.Checked,
                                            use_order.Checked,
                                            use_expired.EditValue,
                                            staff_id,
                                            CekInaktif.Checked)
            Else
                sp_Query = String.Format("UPDATE mbsm_promo SET id_tipemembership={0},nama_promo='{1}',promo_explanation='{2}',syaratperiode_start='{3}',syaratperiode_end='{4}'," & _
                                         "repetisi_promo={5},syarat_none={6},syarat_tipe={7},syarat_operator={8},syarat_tipevalue={9},syarat_wherevalue={10},reward_jenis='{11}',reward_promo='{12}'," & _
                                         "reward_disc={13},reward_value={14},reward_max={15},exc_at_outlet={16},exc_at_order={17}, ismultiplied={18},use_at_outlet={19},use_at_order={20},use_expired={21}," & _
                                         "updateby={22},updatedate=NOW(),Inactive={23} ",
                                            DaftarTipeMembership.EditValue,
                                            NamaPromo.Text,
                                            ExplanationPromo.Text,
                                            Format(periode_start.EditValue, "yyyy-MM-dd"),
                                            Format(periode_end.EditValue, "yyyy-MM-dd"),
                                            Repetisi.SelectedIndex,
                                            syarat_none.Checked,
                                            IIf(Not syarat_none.Checked, "'" & DaftarSyaratUnlock.EditValue & "'", "NULL"),
                                            IIf(Not syarat_none.Checked, "'" & SyaratOperator.EditValue & "'", "NULL"),
                                            IIf(Not syarat_none.Checked, SyaratNilai.EditValue, "NULL"),
                                            IIf(Not syarat_none.Checked And DaftarParameterWhere.Enabled, DaftarParameterWhere.EditValue, "NULL"),
                                            RewardType.EditValue,
                                            RewardPromo.EditValue,
                                            RewardDisc.Checked,
                                            RewardValue.EditValue,
                                            RewardMax.EditValue,
                                            at_outlet.Checked,
                                            at_order.Checked,
                                            CekMultiplied.Checked,
                                            use_outlet.Checked,
                                            use_order.Checked,
                                            use_expired.EditValue,
                                            staff_id,
                                            CekInaktif.Checked)
                param_Query = String.Format("WHERE id_promo={0}", selmaster_id)
            End If

            ExDb.ExecData(1, sp_Query & param_Query)
            MsgInfo("Data berhasil disimpan, tekan OK untuk melanjutkan.")
            BrowserMasterPromo.isChange = True
        Catch ex As Exception
            MsgErrorDev()
        End Try
        LoadSprite.CloseLoading()
        Close()
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Sub Get_KebutuhanDaftar()
        '--------------------------------Tipe Membership
        SQLquery = "SELECT id_tipemembership ID, nama_tipemembership Nama, level_tipemembership Level, syarat_krt Syarat, harga_krt Harga FROM mbsm_tipemembership a WHERE a.Inactive=0;"
        ExDb.ExecQuery(1, SQLquery, xSet, "DaftarTipeMembership")
        DaftarTipeMembership.Properties.DataSource = xSet.Tables("DaftarTipeMembership").DefaultView

        DaftarTipeMembership.Properties.NullText = ""
        DaftarTipeMembership.Properties.ValueMember = "ID"
        DaftarTipeMembership.Properties.DisplayMember = "Nama"
        DaftarTipeMembership.Properties.ShowClearButton = False
        DaftarTipeMembership.Properties.PopulateViewColumns()

        ViewTipeMembership.Columns("ID").Visible = False
        ViewTipeMembership.Columns("Syarat").DisplayFormat.FormatType = FormatType.Numeric
        ViewTipeMembership.Columns("Syarat").DisplayFormat.FormatString = "Rp{0:n0}"
        ViewTipeMembership.Columns("Harga").DisplayFormat.FormatType = FormatType.Numeric
        ViewTipeMembership.Columns("Harga").DisplayFormat.FormatString = "Rp{0:n0}"

        For Each coll As DataColumn In xSet.Tables("DaftarTipeMembership").Columns
            ViewTipeMembership.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        '--------------------------------Tipe Syarat
        SQLquery = "SELECT id_syaratunlock ID, tipe_syaratunlock Tipe, IFNULL(syarat_tabelwhere,'') syarat_tabelwhere, operator FROM mbsm_syaratunlock WHERE inactive=0;"
        ExDb.ExecQuery(1, SQLquery, xSet, "DaftarSyaratUnlock")
        DaftarSyaratUnlock.Properties.DataSource = xSet.Tables("DaftarSyaratUnlock").DefaultView

        DaftarSyaratUnlock.Properties.NullText = ""
        DaftarSyaratUnlock.Properties.ValueMember = "ID"
        DaftarSyaratUnlock.Properties.DisplayMember = "Tipe"
        DaftarSyaratUnlock.Properties.ShowClearButton = False
        DaftarSyaratUnlock.Properties.PopulateViewColumns()

        DaftarSyaratUnlock.Properties.PopupFormSize = New Size(150, 300)

        ViewSyaratUnlock.Columns("ID").Visible = False
        ViewSyaratUnlock.Columns("syarat_tabelwhere").Visible = False
        ViewSyaratUnlock.Columns("operator").Visible = False

        For Each coll As DataColumn In xSet.Tables("DaftarSyaratUnlock").Columns
            ViewSyaratUnlock.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next
    End Sub

    Private Function beforeSave() As Boolean
        If DaftarTipeMembership.EditValue Is Nothing Then
            MsgInfo("Tipe membership belum di pilih")
            DaftarTipeMembership.Focus()
            Return False
        End If

        If NamaPromo.Text.Length < 3 Or RewardPromo.Text.Length < 3 Then
            MsgInfo("Nama promo / reward promo tidak boleh kosong atau terlalu pendek!")
            Return False
        End If

        If ExplanationPromo.Text.Contains("'") Or RewardPromo.Text.Contains("'") Or NamaPromo.Text.Contains("'") Then
            MsgInfo("Inputan tidak boleh mengandung tanda petik (')!")
            Return False
        End If

        If Repetisi.SelectedIndex = -1 Then
            MsgInfo("Repetisi belum di isi")
            Repetisi.Focus()
            Return False
        End If

        If periode_start.EditValue Is Nothing Or periode_end.EditValue Is Nothing Then
            MsgInfo("Periode belum di isi")
            periode_start.Focus()
            Return False
        End If

        If periode_start.EditValue < Today.Date Or periode_end.EditValue < Today.Date Then
            MsgInfo("Periode tidak boleh kurang / sama dengan hari ini!")
            periode_start.Focus()
            Return False
        End If

        If periode_end.EditValue < periode_start.EditValue Then
            MsgInfo("Periode akhir tidak boleh kurang dari hari ini!")
            periode_end.Focus()
            Return False
        End If

        If syarat_none.Checked = False And (RewardValue.EditValue < 0 Or SyaratNilai.EditValue <= 0) Then
            MsgInfo("Nilai reward dan nilai syarat tidak boleh negatif!")
            Return False
        End If

        If at_order.Checked = False And at_outlet.Checked = False Then
            MsgInfo("Harap pilih promo dapat ditukarkan di outlet/order/keduanya!")
            Return False
        End If

        If use_outlet.Checked = False And use_order.Checked = False Then
            MsgInfo("Harap pilih promo dapat digunakan di outlet/order/keduanya!")
            Return False
        End If

        If use_expired.EditValue <= 0 Then
            MsgInfo("Jumlah expired setelah penukaran tidak boleh kurang atau sama dengan 0!")
            Return False
        End If

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        If NamaPromo.Text <> nameCheck Then
            If Not isDoubleData(String.Format("SELECT id_promo ID FROM mbsm_promo WHERE nama_promo='{0}'", NamaPromo.Text)) Then
                MsgWarning("Nama promo sudah ada dalam daftar!")
                Return False
            End If
        End If

        Return True
    End Function
End Class