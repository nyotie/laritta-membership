﻿Imports DevExpress.Utils
Imports System.Globalization
Imports Laritta_Membership.RFID_Tools

Public Class MasterMembercard
    Dim CardUID As String

    Private Sub MasterMembercard_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("JoinedMemberLists")
        MassBlessing = Nothing
        MyCard.Disconnect()

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub MasterMembercard_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()

        'check mifarekey is ready or not
        If Not MyMiFareKey.Stats Then
            StatusMessage.Text = "MiFare Keys is not initialized yet!"
            ButOK.Enabled = False
            Return
        End If

        'check reader is connected or not
        Try
            MyCard.Establish(GlobalReaderContext)
            If MyCard.ReaderEstablished Then
                StatusMessage.Text = "Reader is ready"
            Else
                StatusMessage.Text = "Reader not found"
            End If
        Catch ex As RFID_Exception
            StatusMessage.Text = "Reader not found"
        Catch ex As Exception
            MsgErrorDev()
        End Try

        'get member lists
        Get_KebutuhanDaftar()
    End Sub

    Private Sub MasterMembercard_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        LoadSprite.CloseLoading()
    End Sub

    Private Sub DaftarMemberTerdaftar_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DaftarMemberTerdaftar.EditValueChanged
        Try
            KodeMember.Text = DaftarMemberTerdaftar.Properties.View.GetFocusedRowCellValue("Kode")
        Catch ex As Exception
            KodeMember.Text = ""
        End Try
    End Sub

    Private Sub ButCheck_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButCheck.Click
        If Not MyMiFareKey.Stats Then Return

        Dim state As String = ""

        Try
            state = "Reader"
            If Not MyCard.ReaderEstablished Then MyCard.Establish(GlobalReaderContext)
            StatusMessage.Text = state & " is ready"

            'try to connect to card
            state = "Card"
            MyCard.Connect(GlobalReaderContext, DefReaderName)
            StatusMessage.Text = state & " is ready"
        Catch ex As RFID_Exception
            StatusMessage.Text = state & " not found"
        End Try

        Try

            '============================================================using sector 1 as hq, trailer=7, block=4,5,6
            'try to load DEFAULT keys to buffer
            state = "Load Default Keys"
            MyCard.LoadKeys(MiFareKey.KeyType.TypeA, MyMiFareKey.DefaultKey)

            'try to authenticate with DEFAULT keys into trailer sector 2 (block7)
            state = "Auth"
            MyCard.Authentication(7, MiFareKey.KeyType.TypeA)

            'if success > do binarywrite | if no > skip

            'write secret keys to trailer sector
            MyCard.BinaryKeys_Write(1, MyMiFareKey.KeyA, MyMiFareKey.KeyB)
            '============================================================
        Catch ex As Exception
            StatusMessage.Text = "Default key not accepted"
        End Try


        Try
            'try to load custom key B (to write purpose)
            state = "Load Custom Keys"
            MyCard.LoadKeys(MiFareKey.KeyType.TypeB, MyMiFareKey.KeyB)

            'try to authenticate with CUSTOM keys into block 4 where keys are going to be stored
            state = "Auth"
            MyCard.Authentication(4, MiFareKey.KeyType.TypeB)

            '============================================================
            'get uid
            state = "UID"
            CardUID = MyCard.GetUID()
            StatusMessage.Text = "Card is ready"
        Catch ex As Exception
            StatusMessage.Text = "Card is not recognized"
        End Try
    End Sub

    Private Sub ButOK_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButOK.Click
        If Not KodeMember.EditValue = "" And StatusMessage.Text = "Card is ready" And MyCard.ReaderEstablished And MyCard.CardConnected Then
            If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then Return

            Try
                LoadSprite.ShowLoading("Card", "Inserting code to card..", Me)

                MyCard.BlockWrite(4, KodeMember.EditValue) 'block 4 is used as container to store member's code
                DaftarMemberTerdaftar.EditValue = Nothing
                KodeMember.Text = ""
                StatusMessage.Text = ""
                LoadSprite.CloseLoading()

                LoadSprite.ShowLoading("Database", "Updating database..", Me)
                'save carduid to db and update having card+1
                SQLquery = String.Format("UPDATE mcustomer SET kode_kartuUID='{0}', xtimes_changecard=xtimes_changecard+1 WHERE idCust={1}; ",
                                         CardUID, DaftarMemberTerdaftar.Properties.View.GetFocusedRowCellValue("ID"))
                ExDb.ExecData(1, SQLquery)

                MsgInfo("Success!")
            Catch ex As RFID_Exception
                MsgWarning(ex.ErrorMessage)
                StatusMessage.Text = "Operation failed, please retry Check"
            Catch ex As Exception
                MsgErrorDev()
                StatusMessage.Text = "Success but cant update to database"
            End Try
            LoadSprite.CloseLoading()
        End If
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Sub Get_KebutuhanDaftar()
        If Not xSet.Tables("JoinedMemberLists") Is Nothing Then xSet.Tables("JoinedMemberLists").Clear()
        SQLquery = "SELECT idCust ID, d.nama_tipemembership Tipe, namaCust Nama, kode_member Kode, CASE WHEN sex=0 THEN 'Cowo' ELSE 'Cewe' END Sex, BirthDate Ultah, PhoneMobile HP, " & _
                  "CASE WHEN kode_kartuUID IS NULL THEN 'False' ELSE 'True' END AS Kartu, IFNULL(kode_kartuUID,0) kode_kartuUID " & _
                  "FROM mcustomer a INNER JOIN mbsm_tipemembership d ON a.id_tipemembership=d.id_tipemembership WHERE a.id_tipemembership IS NOT NULL AND a.inactive=0;"

        If xSet.Tables("JoinedMemberLists") Is Nothing Then
            ExDb.ExecQuery(1, SQLquery, xSet, "JoinedMemberLists")
            DaftarMemberTerdaftar.Properties.DataSource = xSet.Tables("JoinedMemberLists").DefaultView

            DaftarMemberTerdaftar.Properties.NullText = ""
            DaftarMemberTerdaftar.Properties.ValueMember = "ID"
            DaftarMemberTerdaftar.Properties.DisplayMember = "Nama"
            DaftarMemberTerdaftar.Properties.ShowClearButton = False
            DaftarMemberTerdaftar.Properties.PopulateViewColumns()

            ViewMemberTerdaftar.Columns("ID").Visible = False
            ViewMemberTerdaftar.Columns("Kode").Visible = False
            ViewMemberTerdaftar.Columns("kode_kartuUID").Visible = False
            ViewMemberTerdaftar.Columns("Ultah").DisplayFormat.FormatType = FormatType.DateTime
            ViewMemberTerdaftar.Columns("Ultah").DisplayFormat.FormatString = "dd MMM yyyy"

            ViewMemberTerdaftar.Columns("Tipe").Width = 70
            ViewMemberTerdaftar.Columns("Nama").Width = 120
            ViewMemberTerdaftar.Columns("Sex").Width = 40
            ViewMemberTerdaftar.Columns("HP").Width = 90

            ViewMemberTerdaftar.Columns("Kartu").ColumnEdit = HavingCard

            For Each coll As DataColumn In xSet.Tables("JoinedMemberLists").Columns
                ViewMemberTerdaftar.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
            Next
        Else
            ExDb.ExecQuery(1, SQLquery, xSet, "JoinedMemberLists")
        End If
    End Sub
End Class