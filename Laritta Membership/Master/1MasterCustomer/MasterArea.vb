﻿Imports DevExpress.Utils

Public Class MasterArea

    Private Sub MasterArea_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("DataMasterArea")
        xSet.Tables.Remove("DaftarKota")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub MasterArea_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        LoadSprite.CloseLoading()
        LookKota.Focus()
        If selmaster_id = 0 Then
            CekInaktif.Properties.ReadOnly = True
            CekInaktif.Checked = False
        End If
    End Sub

    Private Sub MasterArea_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()
        Get_KebutuhanDaftar()

        If Not selmaster_id = 0 Then
            SQLquery = String.Format("SELECT id_kota, nama_area, Inactive FROM mbsm_locarea WHERE id_area={0}", selmaster_id)
            ExDb.ExecQuery(1, SQLquery, xSet, "DataMasterArea")

            LookKota.EditValue = xSet.Tables("DataMasterArea").Rows(0).Item("id_kota")
            NamaArea.EditValue = xSet.Tables("DataMasterArea").Rows(0).Item("nama_area")
            CekInaktif.Checked = xSet.Tables("DataMasterArea").Rows(0).Item("Inactive")

            nameCheck = NamaArea.EditValue
        End If
    End Sub

    Private Sub ButSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSimpan.Click
        If Not beforeSave() Then Return
        Try
            LoadSprite.ShowLoading()
            If selmaster_id = "0" Then
                sp_Query = "INSERT INTO mbsm_locarea(id_kota, nama_area, createby, createdate) "
                param_Query = String.Format("VALUES('{0}', '{1}', '{2}', NOW()) ", LookKota.EditValue, NamaArea.Text, staff_id)
            Else
                sp_Query = String.Format("UPDATE mbsm_locarea SET id_kota={0}, nama_area='{1}', inactive={2}, updateBy={3}, updateDate=NOW() ", LookKota.EditValue, NamaArea.Text, CekInaktif.Checked, staff_id)
                param_Query = String.Format("WHERE id_area={0}", selmaster_id)
            End If

            ExDb.ExecData(1, sp_Query & param_Query)
            MsgInfo("Data berhasil disimpan, tekan OK untuk melanjutkan.")
            BrowserMasterArea.isChange = True
        Catch ex As Exception
            MsgErrorDev()
        End Try
        LoadSprite.CloseLoading()
        Close()
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Sub Get_KebutuhanDaftar()
        '--------------------------------Kota
        SQLquery = "SELECT id_kota ID, nama_kota Nama FROM mbsm_lockota WHERE Inactive=0;"
        ExDb.ExecQuery(1, SQLquery, xSet, "DaftarKota")
        LookKota.Properties.DataSource = xSet.Tables("DaftarKota").DefaultView

        LookKota.Properties.NullText = ""
        LookKota.Properties.ValueMember = "ID"
        LookKota.Properties.DisplayMember = "Nama"
        LookKota.Properties.ShowClearButton = False
        LookKota.Properties.PopulateViewColumns()

        LookKota.Properties.PopupFormSize = New Size(400, 300)

        ViewKota.Columns("ID").Visible = False

        For Each coll As DataColumn In xSet.Tables("DaftarKota").Columns
            ViewKota.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next
    End Sub

    Private Function beforeSave() As Boolean
        If NamaArea.Text.Length < 3 Or NamaArea.Text.Contains("'") Then
            MsgInfo("Nama area terlalu pendek atau mengandung tanda petik (')")
            Return False
        End If

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        If NamaArea.Text <> nameCheck Then
            If Not isDoubleData(String.Format("SELECT id_area ID FROM mbsm_locarea WHERE nama_area='{0}'", NamaArea.Text)) Then
                MsgWarning("Nama area sudah ada dalam daftar!")
                Return False
            End If
        End If

        Return True
    End Function
End Class