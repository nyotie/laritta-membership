﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MasterRegisterMember
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.OutletTotalLunas = New DevExpress.XtraEditors.TextEdit()
        Me.xTrans = New DevExpress.XtraEditors.TextEdit()
        Me.Email = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.TglLahir = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.NamaLengkap = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.KTP_NIK = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.DaftarTipeMembership = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.ViewTipeMembership = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.SyaratKartu = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.HargaKartu = New DevExpress.XtraEditors.TextEdit()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.KodeKartu = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.KodeMember = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.ButBeli = New DevExpress.XtraEditors.SimpleButton()
        Me.ButFreeMember = New DevExpress.XtraEditors.SimpleButton()
        Me.ButBatal = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.OrderTotalBeli = New DevExpress.XtraEditors.TextEdit()
        Me.OrderTotalLunas = New DevExpress.XtraEditors.TextEdit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.OutletTotalLunas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xTrans.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Email.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TglLahir.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NamaLengkap.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.KTP_NIK.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DaftarTipeMembership.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewTipeMembership, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SyaratKartu.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.HargaKartu.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.KodeKartu.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.KodeMember.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.OrderTotalBeli.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.OrderTotalLunas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.Controls.Add(Me.OrderTotalLunas)
        Me.GroupControl1.Controls.Add(Me.OrderTotalBeli)
        Me.GroupControl1.Controls.Add(Me.OutletTotalLunas)
        Me.GroupControl1.Controls.Add(Me.LabelControl21)
        Me.GroupControl1.Controls.Add(Me.xTrans)
        Me.GroupControl1.Controls.Add(Me.LabelControl20)
        Me.GroupControl1.Controls.Add(Me.Email)
        Me.GroupControl1.Controls.Add(Me.LabelControl9)
        Me.GroupControl1.Controls.Add(Me.LabelControl19)
        Me.GroupControl1.Controls.Add(Me.LabelControl8)
        Me.GroupControl1.Controls.Add(Me.LabelControl18)
        Me.GroupControl1.Controls.Add(Me.LabelControl13)
        Me.GroupControl1.Controls.Add(Me.LabelControl10)
        Me.GroupControl1.Controls.Add(Me.LabelControl4)
        Me.GroupControl1.Controls.Add(Me.TglLahir)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Controls.Add(Me.NamaLengkap)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.KTP_NIK)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(503, 248)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Informasi calon member"
        '
        'OutletTotalLunas
        '
        Me.OutletTotalLunas.EditValue = "0"
        Me.OutletTotalLunas.Location = New System.Drawing.Point(226, 218)
        Me.OutletTotalLunas.Name = "OutletTotalLunas"
        Me.OutletTotalLunas.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.OutletTotalLunas.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.OutletTotalLunas.Properties.Appearance.Options.UseFont = True
        Me.OutletTotalLunas.Properties.Appearance.Options.UseTextOptions = True
        Me.OutletTotalLunas.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.OutletTotalLunas.Properties.Mask.EditMask = "n0"
        Me.OutletTotalLunas.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.OutletTotalLunas.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.OutletTotalLunas.Size = New System.Drawing.Size(126, 21)
        Me.OutletTotalLunas.TabIndex = 7
        Me.OutletTotalLunas.TabStop = False
        '
        'xTrans
        '
        Me.xTrans.Location = New System.Drawing.Point(203, 140)
        Me.xTrans.Name = "xTrans"
        Me.xTrans.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.xTrans.Properties.Appearance.Options.UseFont = True
        Me.xTrans.Properties.Appearance.Options.UseTextOptions = True
        Me.xTrans.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.xTrans.Properties.Mask.EditMask = "n0"
        Me.xTrans.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.xTrans.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.xTrans.Properties.ReadOnly = True
        Me.xTrans.Size = New System.Drawing.Size(55, 21)
        Me.xTrans.TabIndex = 4
        Me.xTrans.TabStop = False
        '
        'Email
        '
        Me.Email.Location = New System.Drawing.Point(203, 113)
        Me.Email.Name = "Email"
        Me.Email.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.Email.Properties.Appearance.Options.UseFont = True
        Me.Email.Properties.Mask.EditMask = "[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}"
        Me.Email.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.Email.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.Email.Properties.ReadOnly = True
        Me.Email.Size = New System.Drawing.Size(149, 21)
        Me.Email.TabIndex = 3
        Me.Email.TabStop = False
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl9.Location = New System.Drawing.Point(18, 221)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(134, 14)
        Me.LabelControl9.TabIndex = 0
        Me.LabelControl9.Text = "Total transaksi outlet"
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl8.Location = New System.Drawing.Point(18, 143)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(57, 14)
        Me.LabelControl8.TabIndex = 0
        Me.LabelControl8.Text = "Jml order"
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl13.Location = New System.Drawing.Point(264, 144)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl13.TabIndex = 0
        Me.LabelControl13.Text = "x"
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl10.Location = New System.Drawing.Point(203, 222)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(20, 14)
        Me.LabelControl10.TabIndex = 0
        Me.LabelControl10.Text = "Rp."
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl4.Location = New System.Drawing.Point(18, 115)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(33, 14)
        Me.LabelControl4.TabIndex = 0
        Me.LabelControl4.Text = "Email"
        '
        'TglLahir
        '
        Me.TglLahir.Location = New System.Drawing.Point(203, 86)
        Me.TglLahir.Name = "TglLahir"
        Me.TglLahir.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.TglLahir.Properties.Appearance.Options.UseFont = True
        Me.TglLahir.Properties.Mask.EditMask = "dd MMMM yyyy"
        Me.TglLahir.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TglLahir.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TglLahir.Properties.ReadOnly = True
        Me.TglLahir.Size = New System.Drawing.Size(149, 21)
        Me.TglLahir.TabIndex = 2
        Me.TglLahir.TabStop = False
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl3.Location = New System.Drawing.Point(18, 89)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(81, 14)
        Me.LabelControl3.TabIndex = 0
        Me.LabelControl3.Text = "Tanggal lahir"
        '
        'NamaLengkap
        '
        Me.NamaLengkap.Location = New System.Drawing.Point(203, 59)
        Me.NamaLengkap.Name = "NamaLengkap"
        Me.NamaLengkap.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.NamaLengkap.Properties.Appearance.Options.UseFont = True
        Me.NamaLengkap.Properties.Mask.EditMask = "[A-Z0-9() ]+"
        Me.NamaLengkap.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.NamaLengkap.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.NamaLengkap.Properties.ReadOnly = True
        Me.NamaLengkap.Size = New System.Drawing.Size(251, 21)
        Me.NamaLengkap.TabIndex = 1
        Me.NamaLengkap.TabStop = False
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl2.Location = New System.Drawing.Point(18, 62)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(98, 14)
        Me.LabelControl2.TabIndex = 0
        Me.LabelControl2.Text = "Nama customer"
        '
        'KTP_NIK
        '
        Me.KTP_NIK.Location = New System.Drawing.Point(203, 32)
        Me.KTP_NIK.Name = "KTP_NIK"
        Me.KTP_NIK.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.KTP_NIK.Properties.Appearance.Options.UseFont = True
        Me.KTP_NIK.Properties.Mask.EditMask = "[0-9.]+"
        Me.KTP_NIK.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.KTP_NIK.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.KTP_NIK.Properties.ReadOnly = True
        Me.KTP_NIK.Size = New System.Drawing.Size(149, 21)
        Me.KTP_NIK.TabIndex = 0
        Me.KTP_NIK.TabStop = False
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl1.Location = New System.Drawing.Point(18, 35)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(68, 14)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Nomor KTP"
        '
        'DaftarTipeMembership
        '
        Me.DaftarTipeMembership.Location = New System.Drawing.Point(203, 34)
        Me.DaftarTipeMembership.Name = "DaftarTipeMembership"
        Me.DaftarTipeMembership.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.DaftarTipeMembership.Properties.Appearance.Options.UseFont = True
        Me.DaftarTipeMembership.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DaftarTipeMembership.Properties.NullText = ""
        Me.DaftarTipeMembership.Properties.View = Me.ViewTipeMembership
        Me.DaftarTipeMembership.Size = New System.Drawing.Size(192, 21)
        Me.DaftarTipeMembership.TabIndex = 0
        '
        'ViewTipeMembership
        '
        Me.ViewTipeMembership.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.ViewTipeMembership.Name = "ViewTipeMembership"
        Me.ViewTipeMembership.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.ViewTipeMembership.OptionsView.ShowGroupPanel = False
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl5.Location = New System.Drawing.Point(16, 37)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(107, 14)
        Me.LabelControl5.TabIndex = 0
        Me.LabelControl5.Text = "Tipe membership"
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl6.Location = New System.Drawing.Point(16, 64)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(110, 14)
        Me.LabelControl6.TabIndex = 0
        Me.LabelControl6.Text = "Syarat pembelian"
        '
        'SyaratKartu
        '
        Me.SyaratKartu.Location = New System.Drawing.Point(224, 61)
        Me.SyaratKartu.Name = "SyaratKartu"
        Me.SyaratKartu.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.SyaratKartu.Properties.Appearance.Options.UseFont = True
        Me.SyaratKartu.Properties.Appearance.Options.UseTextOptions = True
        Me.SyaratKartu.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.SyaratKartu.Properties.Mask.EditMask = "n2"
        Me.SyaratKartu.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.SyaratKartu.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.SyaratKartu.Properties.ReadOnly = True
        Me.SyaratKartu.Size = New System.Drawing.Size(126, 21)
        Me.SyaratKartu.TabIndex = 1
        Me.SyaratKartu.TabStop = False
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl7.Location = New System.Drawing.Point(16, 91)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(75, 14)
        Me.LabelControl7.TabIndex = 0
        Me.LabelControl7.Text = "Harga kartu"
        '
        'HargaKartu
        '
        Me.HargaKartu.Location = New System.Drawing.Point(224, 88)
        Me.HargaKartu.Name = "HargaKartu"
        Me.HargaKartu.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.HargaKartu.Properties.Appearance.Options.UseFont = True
        Me.HargaKartu.Properties.Appearance.Options.UseTextOptions = True
        Me.HargaKartu.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.HargaKartu.Properties.Mask.EditMask = "n2"
        Me.HargaKartu.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.HargaKartu.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.HargaKartu.Properties.ReadOnly = True
        Me.HargaKartu.Size = New System.Drawing.Size(126, 21)
        Me.HargaKartu.TabIndex = 2
        Me.HargaKartu.TabStop = False
        '
        'GroupControl2
        '
        Me.GroupControl2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl2.AppearanceCaption.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.GroupControl2.AppearanceCaption.Options.UseFont = True
        Me.GroupControl2.Controls.Add(Me.DaftarTipeMembership)
        Me.GroupControl2.Controls.Add(Me.KodeKartu)
        Me.GroupControl2.Controls.Add(Me.LabelControl17)
        Me.GroupControl2.Controls.Add(Me.KodeMember)
        Me.GroupControl2.Controls.Add(Me.LabelControl14)
        Me.GroupControl2.Controls.Add(Me.LabelControl7)
        Me.GroupControl2.Controls.Add(Me.LabelControl6)
        Me.GroupControl2.Controls.Add(Me.HargaKartu)
        Me.GroupControl2.Controls.Add(Me.SyaratKartu)
        Me.GroupControl2.Controls.Add(Me.LabelControl12)
        Me.GroupControl2.Controls.Add(Me.LabelControl11)
        Me.GroupControl2.Controls.Add(Me.LabelControl5)
        Me.GroupControl2.Location = New System.Drawing.Point(12, 266)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(503, 172)
        Me.GroupControl2.TabIndex = 1
        Me.GroupControl2.Text = "Jenis membership"
        '
        'KodeKartu
        '
        Me.KodeKartu.Location = New System.Drawing.Point(199, 142)
        Me.KodeKartu.Name = "KodeKartu"
        Me.KodeKartu.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.KodeKartu.Properties.Appearance.Options.UseFont = True
        Me.KodeKartu.Properties.ReadOnly = True
        Me.KodeKartu.Size = New System.Drawing.Size(253, 21)
        Me.KodeKartu.TabIndex = 4
        Me.KodeKartu.TabStop = False
        '
        'LabelControl17
        '
        Me.LabelControl17.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl17.Location = New System.Drawing.Point(16, 145)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(99, 14)
        Me.LabelControl17.TabIndex = 0
        Me.LabelControl17.Text = "Kode unik kartu"
        '
        'KodeMember
        '
        Me.KodeMember.Location = New System.Drawing.Point(201, 115)
        Me.KodeMember.Name = "KodeMember"
        Me.KodeMember.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.KodeMember.Properties.Appearance.Options.UseFont = True
        Me.KodeMember.Properties.ReadOnly = True
        Me.KodeMember.Size = New System.Drawing.Size(194, 21)
        Me.KodeMember.TabIndex = 3
        Me.KodeMember.TabStop = False
        '
        'LabelControl14
        '
        Me.LabelControl14.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl14.Location = New System.Drawing.Point(16, 118)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(150, 14)
        Me.LabelControl14.TabIndex = 0
        Me.LabelControl14.Text = "Perkiraan kode member"
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl12.Location = New System.Drawing.Point(201, 92)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(20, 14)
        Me.LabelControl12.TabIndex = 0
        Me.LabelControl12.Text = "Rp."
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl11.Location = New System.Drawing.Point(201, 65)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(20, 14)
        Me.LabelControl11.TabIndex = 0
        Me.LabelControl11.Text = "Rp."
        '
        'ButBeli
        '
        Me.ButBeli.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButBeli.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButBeli.Appearance.Options.UseFont = True
        Me.ButBeli.Enabled = False
        Me.ButBeli.Image = Global.Laritta_Membership.My.Resources.Resources.member_buy_32
        Me.ButBeli.Location = New System.Drawing.Point(220, 444)
        Me.ButBeli.Name = "ButBeli"
        Me.ButBeli.Size = New System.Drawing.Size(90, 40)
        Me.ButBeli.TabIndex = 2
        Me.ButBeli.Text = "Beli"
        '
        'ButFreeMember
        '
        Me.ButFreeMember.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButFreeMember.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButFreeMember.Appearance.Options.UseFont = True
        Me.ButFreeMember.Enabled = False
        Me.ButFreeMember.Image = Global.Laritta_Membership.My.Resources.Resources.member_upgrade_32
        Me.ButFreeMember.Location = New System.Drawing.Point(316, 444)
        Me.ButFreeMember.Name = "ButFreeMember"
        Me.ButFreeMember.Size = New System.Drawing.Size(103, 40)
        Me.ButFreeMember.TabIndex = 3
        Me.ButFreeMember.Text = "Gratis"
        '
        'ButBatal
        '
        Me.ButBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButBatal.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButBatal.Appearance.Options.UseFont = True
        Me.ButBatal.Image = Global.Laritta_Membership.My.Resources.Resources.back_32
        Me.ButBatal.Location = New System.Drawing.Point(425, 444)
        Me.ButBatal.Name = "ButBatal"
        Me.ButBatal.Size = New System.Drawing.Size(90, 40)
        Me.ButBatal.TabIndex = 4
        Me.ButBatal.Text = "&Batal"
        '
        'LabelControl18
        '
        Me.LabelControl18.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl18.Location = New System.Drawing.Point(203, 168)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(20, 14)
        Me.LabelControl18.TabIndex = 0
        Me.LabelControl18.Text = "Rp."
        '
        'LabelControl19
        '
        Me.LabelControl19.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl19.Location = New System.Drawing.Point(203, 195)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(20, 14)
        Me.LabelControl19.TabIndex = 0
        Me.LabelControl19.Text = "Rp."
        '
        'LabelControl20
        '
        Me.LabelControl20.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl20.Location = New System.Drawing.Point(18, 167)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(131, 14)
        Me.LabelControl20.TabIndex = 0
        Me.LabelControl20.Text = "Total transaksi order"
        '
        'LabelControl21
        '
        Me.LabelControl21.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl21.Location = New System.Drawing.Point(18, 194)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(136, 14)
        Me.LabelControl21.TabIndex = 0
        Me.LabelControl21.Text = "Transaksi order lunas"
        '
        'OrderTotalBeli
        '
        Me.OrderTotalBeli.Location = New System.Drawing.Point(226, 164)
        Me.OrderTotalBeli.Name = "OrderTotalBeli"
        Me.OrderTotalBeli.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.OrderTotalBeli.Properties.Appearance.Options.UseFont = True
        Me.OrderTotalBeli.Properties.Appearance.Options.UseTextOptions = True
        Me.OrderTotalBeli.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.OrderTotalBeli.Properties.Mask.EditMask = "n2"
        Me.OrderTotalBeli.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.OrderTotalBeli.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.OrderTotalBeli.Properties.ReadOnly = True
        Me.OrderTotalBeli.Size = New System.Drawing.Size(126, 21)
        Me.OrderTotalBeli.TabIndex = 5
        Me.OrderTotalBeli.TabStop = False
        '
        'OrderTotalLunas
        '
        Me.OrderTotalLunas.Location = New System.Drawing.Point(226, 191)
        Me.OrderTotalLunas.Name = "OrderTotalLunas"
        Me.OrderTotalLunas.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.OrderTotalLunas.Properties.Appearance.Options.UseFont = True
        Me.OrderTotalLunas.Properties.Appearance.Options.UseTextOptions = True
        Me.OrderTotalLunas.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.OrderTotalLunas.Properties.Mask.EditMask = "n2"
        Me.OrderTotalLunas.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.OrderTotalLunas.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.OrderTotalLunas.Properties.ReadOnly = True
        Me.OrderTotalLunas.Size = New System.Drawing.Size(126, 21)
        Me.OrderTotalLunas.TabIndex = 6
        Me.OrderTotalLunas.TabStop = False
        '
        'MasterRegisterMember
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(527, 496)
        Me.Controls.Add(Me.ButBatal)
        Me.Controls.Add(Me.ButFreeMember)
        Me.Controls.Add(Me.ButBeli)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.GroupControl1)
        Me.Name = "MasterRegisterMember"
        Me.Text = "MasterRegisterMember"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.OutletTotalLunas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xTrans.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Email.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TglLahir.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NamaLengkap.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.KTP_NIK.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DaftarTipeMembership.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewTipeMembership, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SyaratKartu.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.HargaKartu.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.KodeKartu.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.KodeMember.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.OrderTotalBeli.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.OrderTotalLunas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Email As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TglLahir As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents NamaLengkap As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents KTP_NIK As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DaftarTipeMembership As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents ViewTipeMembership As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents HargaKartu As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SyaratKartu As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents OutletTotalLunas As DevExpress.XtraEditors.TextEdit
    Friend WithEvents xTrans As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ButBeli As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButFreeMember As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButBatal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents KodeMember As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents KodeKartu As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents OrderTotalLunas As DevExpress.XtraEditors.TextEdit
    Friend WithEvents OrderTotalBeli As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
End Class
