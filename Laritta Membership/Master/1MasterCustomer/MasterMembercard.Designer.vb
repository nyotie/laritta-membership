﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MasterMembercard
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.KodeMember = New DevExpress.XtraEditors.TextEdit()
        Me.LabelKodeMember = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.DaftarMemberTerdaftar = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.ViewMemberTerdaftar = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.StatusMessage = New DevExpress.XtraEditors.TextEdit()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.LineShape1 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.ButCheck = New DevExpress.XtraEditors.SimpleButton()
        Me.ButBatal = New DevExpress.XtraEditors.SimpleButton()
        Me.ButOK = New DevExpress.XtraEditors.SimpleButton()
        Me.HavingCard = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        CType(Me.KodeMember.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DaftarMemberTerdaftar.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewMemberTerdaftar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StatusMessage.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.HavingCard, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'KodeMember
        '
        Me.KodeMember.Location = New System.Drawing.Point(115, 63)
        Me.KodeMember.Name = "KodeMember"
        Me.KodeMember.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.KodeMember.Properties.Appearance.Options.UseFont = True
        Me.KodeMember.Properties.Mask.EditMask = "[0-9]{8} [0-9]{4}"
        Me.KodeMember.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.KodeMember.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.KodeMember.Properties.MaxLength = 50
        Me.KodeMember.Properties.ReadOnly = True
        Me.KodeMember.Size = New System.Drawing.Size(282, 21)
        Me.KodeMember.TabIndex = 1
        '
        'LabelKodeMember
        '
        Me.LabelKodeMember.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelKodeMember.Location = New System.Drawing.Point(17, 66)
        Me.LabelKodeMember.Name = "LabelKodeMember"
        Me.LabelKodeMember.Size = New System.Drawing.Size(86, 14)
        Me.LabelKodeMember.TabIndex = 26
        Me.LabelKodeMember.Text = "Kode Member"
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl9.Location = New System.Drawing.Point(17, 39)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(50, 14)
        Me.LabelControl9.TabIndex = 29
        Me.LabelControl9.Text = "Member"
        '
        'DaftarMemberTerdaftar
        '
        Me.DaftarMemberTerdaftar.Location = New System.Drawing.Point(115, 37)
        Me.DaftarMemberTerdaftar.Name = "DaftarMemberTerdaftar"
        Me.DaftarMemberTerdaftar.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.DaftarMemberTerdaftar.Properties.Appearance.Options.UseFont = True
        Me.DaftarMemberTerdaftar.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DaftarMemberTerdaftar.Properties.NullText = ""
        Me.DaftarMemberTerdaftar.Properties.PopupSizeable = False
        Me.DaftarMemberTerdaftar.Properties.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.HavingCard})
        Me.DaftarMemberTerdaftar.Properties.View = Me.ViewMemberTerdaftar
        Me.DaftarMemberTerdaftar.Size = New System.Drawing.Size(282, 21)
        Me.DaftarMemberTerdaftar.TabIndex = 0
        '
        'ViewMemberTerdaftar
        '
        Me.ViewMemberTerdaftar.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.ViewMemberTerdaftar.Name = "ViewMemberTerdaftar"
        Me.ViewMemberTerdaftar.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.ViewMemberTerdaftar.OptionsView.ShowGroupPanel = False
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl1.Location = New System.Drawing.Point(16, 108)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(41, 14)
        Me.LabelControl1.TabIndex = 26
        Me.LabelControl1.Text = "Status"
        '
        'StatusMessage
        '
        Me.StatusMessage.Location = New System.Drawing.Point(114, 105)
        Me.StatusMessage.Name = "StatusMessage"
        Me.StatusMessage.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.StatusMessage.Properties.Appearance.Options.UseFont = True
        Me.StatusMessage.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.StatusMessage.Properties.MaxLength = 50
        Me.StatusMessage.Properties.ReadOnly = True
        Me.StatusMessage.Size = New System.Drawing.Size(283, 21)
        Me.StatusMessage.TabIndex = 2
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.Controls.Add(Me.DaftarMemberTerdaftar)
        Me.GroupControl1.Controls.Add(Me.LabelKodeMember)
        Me.GroupControl1.Controls.Add(Me.KodeMember)
        Me.GroupControl1.Controls.Add(Me.LabelControl9)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Controls.Add(Me.StatusMessage)
        Me.GroupControl1.Controls.Add(Me.ShapeContainer1)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(404, 136)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Membercard"
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(2, 23)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape1})
        Me.ShapeContainer1.Size = New System.Drawing.Size(400, 111)
        Me.ShapeContainer1.TabIndex = 31
        Me.ShapeContainer1.TabStop = False
        '
        'LineShape1
        '
        Me.LineShape1.BorderColor = System.Drawing.SystemColors.ButtonShadow
        Me.LineShape1.Name = "LineShape1"
        Me.LineShape1.X1 = 13
        Me.LineShape1.X2 = 390
        Me.LineShape1.Y1 = 71
        Me.LineShape1.Y2 = 71
        '
        'ButCheck
        '
        Me.ButCheck.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButCheck.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButCheck.Appearance.Options.UseFont = True
        Me.ButCheck.Image = Global.Laritta_Membership.My.Resources.Resources.reset_32
        Me.ButCheck.Location = New System.Drawing.Point(12, 154)
        Me.ButCheck.Name = "ButCheck"
        Me.ButCheck.Size = New System.Drawing.Size(90, 40)
        Me.ButCheck.TabIndex = 3
        Me.ButCheck.Text = "Check"
        '
        'ButBatal
        '
        Me.ButBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButBatal.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButBatal.Appearance.Options.UseFont = True
        Me.ButBatal.Image = Global.Laritta_Membership.My.Resources.Resources.back_32
        Me.ButBatal.Location = New System.Drawing.Point(326, 154)
        Me.ButBatal.Name = "ButBatal"
        Me.ButBatal.Size = New System.Drawing.Size(90, 40)
        Me.ButBatal.TabIndex = 2
        Me.ButBatal.Text = "&Batal"
        '
        'ButOK
        '
        Me.ButOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButOK.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButOK.Appearance.Options.UseFont = True
        Me.ButOK.Image = Global.Laritta_Membership.My.Resources.Resources.apply_32
        Me.ButOK.Location = New System.Drawing.Point(230, 154)
        Me.ButOK.Name = "ButOK"
        Me.ButOK.Size = New System.Drawing.Size(90, 40)
        Me.ButOK.TabIndex = 1
        Me.ButOK.Text = "&OK"
        '
        'HavingCard
        '
        Me.HavingCard.AutoHeight = False
        Me.HavingCard.Name = "HavingCard"
        Me.HavingCard.ValueChecked = "True"
        Me.HavingCard.ValueGrayed = "False"
        Me.HavingCard.ValueUnchecked = "False"
        '
        'MasterMembercard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(428, 206)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.ButBatal)
        Me.Controls.Add(Me.ButCheck)
        Me.Controls.Add(Me.ButOK)
        Me.Name = "MasterMembercard"
        Me.Text = "MasterMembercard"
        CType(Me.KodeMember.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DaftarMemberTerdaftar.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewMemberTerdaftar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StatusMessage.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.HavingCard, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents KodeMember As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelKodeMember As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DaftarMemberTerdaftar As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents ViewMemberTerdaftar As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ButBatal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents ButCheck As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents LineShape1 As Microsoft.VisualBasic.PowerPacks.LineShape
    Private WithEvents StatusMessage As DevExpress.XtraEditors.TextEdit
    Friend WithEvents HavingCard As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
End Class
