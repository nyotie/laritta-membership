﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MasterCustomer
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.NamaLengkap = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl25 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.KTP_NIK = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.LookArea = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.ViewArea = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LookKota = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.ViewKota = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.Agama = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.StatusKawin = New DevExpress.XtraEditors.RadioGroup()
        Me.Sex = New DevExpress.XtraEditors.RadioGroup()
        Me.TglLahir = New DevExpress.XtraEditors.DateEdit()
        Me.LiveAddress = New DevExpress.XtraEditors.TextEdit()
        Me.Hobi = New DevExpress.XtraEditors.TextEdit()
        Me.Email = New DevExpress.XtraEditors.TextEdit()
        Me.TelpHP = New DevExpress.XtraEditors.TextEdit()
        Me.TelpRumah = New DevExpress.XtraEditors.TextEdit()
        Me.TmptLahir = New DevExpress.XtraEditors.TextEdit()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.LineShape1 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.KTPAddress = New DevExpress.XtraEditors.TextEdit()
        Me.KTPKecamatan = New DevExpress.XtraEditors.TextEdit()
        Me.KTPKelurahan = New DevExpress.XtraEditors.TextEdit()
        Me.KTPRtrw = New DevExpress.XtraEditors.TextEdit()
        Me.KTPKota = New DevExpress.XtraEditors.TextEdit()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.TelpKantor = New DevExpress.XtraEditors.TextEdit()
        Me.AddressOffice = New DevExpress.XtraEditors.TextEdit()
        Me.Perusahaan = New DevExpress.XtraEditors.TextEdit()
        Me.Pekerjaan = New DevExpress.XtraEditors.TextEdit()
        Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl()
        Me.LookKategori = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.ViewKategori = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.CekBlacklist = New DevExpress.XtraEditors.CheckEdit()
        Me.CekInaktif = New DevExpress.XtraEditors.CheckEdit()
        Me.KodeMember = New DevExpress.XtraEditors.TextEdit()
        Me.Catatan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelKodeMember = New DevExpress.XtraEditors.LabelControl()
        Me.ButSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.ButBatal = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.NamaLengkap.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.KTP_NIK.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LookArea.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewArea, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LookKota.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewKota, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Agama.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StatusKawin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sex.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TglLahir.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TglLahir.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LiveAddress.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Hobi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Email.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TelpHP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TelpRumah.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TmptLahir.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.KTPAddress.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.KTPKecamatan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.KTPKelurahan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.KTPRtrw.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.KTPKota.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.TelpKantor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AddressOffice.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Perusahaan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Pekerjaan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.LookKategori.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewKategori, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CekBlacklist.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CekInaktif.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.KodeMember.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Catatan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Location = New System.Drawing.Point(20, 59)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(98, 14)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Nama lengkap*"
        '
        'NamaLengkap
        '
        Me.NamaLengkap.Location = New System.Drawing.Point(158, 56)
        Me.NamaLengkap.Name = "NamaLengkap"
        Me.NamaLengkap.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NamaLengkap.Properties.Appearance.Options.UseFont = True
        Me.NamaLengkap.Properties.Mask.EditMask = "[A-Z() .,]+"
        Me.NamaLengkap.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.NamaLengkap.Size = New System.Drawing.Size(239, 21)
        Me.NamaLengkap.TabIndex = 2
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl2.Location = New System.Drawing.Point(20, 86)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(86, 14)
        Me.LabelControl2.TabIndex = 0
        Me.LabelControl2.Text = "Tempat lahir*"
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl3.Location = New System.Drawing.Point(20, 113)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(89, 14)
        Me.LabelControl3.TabIndex = 0
        Me.LabelControl3.Text = "Tanggal lahir*"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl4.Location = New System.Drawing.Point(20, 140)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(83, 14)
        Me.LabelControl4.TabIndex = 0
        Me.LabelControl4.Text = "Jenis kelamin"
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl5.Location = New System.Drawing.Point(20, 355)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(102, 14)
        Me.LabelControl5.TabIndex = 0
        Me.LabelControl5.Text = "Tempat tinggal*"
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl6.Location = New System.Drawing.Point(20, 382)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(37, 14)
        Me.LabelControl6.TabIndex = 0
        Me.LabelControl6.Text = "Kota*"
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl7.Location = New System.Drawing.Point(12, 479)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(81, 14)
        Me.LabelControl7.TabIndex = 0
        Me.LabelControl7.Text = "(*) wajib diisi"
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl8.Location = New System.Drawing.Point(25, 32)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(51, 14)
        Me.LabelControl8.TabIndex = 0
        Me.LabelControl8.Text = "Alamat*"
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl9.Location = New System.Drawing.Point(25, 86)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(49, 14)
        Me.LabelControl9.TabIndex = 0
        Me.LabelControl9.Text = "RT/RW*"
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl10.Location = New System.Drawing.Point(25, 113)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(72, 14)
        Me.LabelControl10.TabIndex = 0
        Me.LabelControl10.Text = "Kelurahan*"
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl11.Location = New System.Drawing.Point(25, 140)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(78, 14)
        Me.LabelControl11.TabIndex = 0
        Me.LabelControl11.Text = "Kecamatan*"
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl12.Location = New System.Drawing.Point(25, 59)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(37, 14)
        Me.LabelControl12.TabIndex = 0
        Me.LabelControl12.Text = "Kota*"
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl13.Location = New System.Drawing.Point(20, 170)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(78, 14)
        Me.LabelControl13.TabIndex = 0
        Me.LabelControl13.Text = "Telp rumah*"
        '
        'LabelControl14
        '
        Me.LabelControl14.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl14.Location = New System.Drawing.Point(20, 197)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(55, 14)
        Me.LabelControl14.TabIndex = 0
        Me.LabelControl14.Text = "Telp HP*"
        '
        'LabelControl15
        '
        Me.LabelControl15.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl15.Location = New System.Drawing.Point(20, 224)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(51, 14)
        Me.LabelControl15.TabIndex = 0
        Me.LabelControl15.Text = "Agama*"
        '
        'LabelControl16
        '
        Me.LabelControl16.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl16.Location = New System.Drawing.Point(20, 252)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(119, 14)
        Me.LabelControl16.TabIndex = 0
        Me.LabelControl16.Text = "Status perkawinan"
        '
        'LabelControl17
        '
        Me.LabelControl17.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl17.Location = New System.Drawing.Point(25, 32)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(64, 14)
        Me.LabelControl17.TabIndex = 0
        Me.LabelControl17.Text = "Pekerjaan"
        '
        'LabelControl18
        '
        Me.LabelControl18.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl18.Location = New System.Drawing.Point(25, 59)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(76, 14)
        Me.LabelControl18.TabIndex = 0
        Me.LabelControl18.Text = "Perusahaan"
        '
        'LabelControl19
        '
        Me.LabelControl19.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl19.Location = New System.Drawing.Point(25, 86)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(88, 14)
        Me.LabelControl19.TabIndex = 0
        Me.LabelControl19.Text = "Alamat kantor"
        '
        'LabelControl20
        '
        Me.LabelControl20.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl20.Location = New System.Drawing.Point(25, 113)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(71, 14)
        Me.LabelControl20.TabIndex = 0
        Me.LabelControl20.Text = "Telp kantor"
        '
        'LabelControl22
        '
        Me.LabelControl22.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl22.Location = New System.Drawing.Point(20, 281)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(33, 14)
        Me.LabelControl22.TabIndex = 0
        Me.LabelControl22.Text = "Email"
        '
        'LabelControl23
        '
        Me.LabelControl23.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl23.Location = New System.Drawing.Point(20, 308)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(36, 14)
        Me.LabelControl23.TabIndex = 0
        Me.LabelControl23.Text = "Hobi*"
        '
        'LabelControl24
        '
        Me.LabelControl24.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl24.Location = New System.Drawing.Point(25, 62)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(37, 14)
        Me.LabelControl24.TabIndex = 0
        Me.LabelControl24.Text = "Notes"
        '
        'LabelControl25
        '
        Me.LabelControl25.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl25.Location = New System.Drawing.Point(25, 35)
        Me.LabelControl25.Name = "LabelControl25"
        Me.LabelControl25.Size = New System.Drawing.Size(61, 14)
        Me.LabelControl25.TabIndex = 0
        Me.LabelControl25.Text = "Kategori*"
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.Controls.Add(Me.KTP_NIK)
        Me.GroupControl1.Controls.Add(Me.LabelControl21)
        Me.GroupControl1.Controls.Add(Me.LookArea)
        Me.GroupControl1.Controls.Add(Me.LookKota)
        Me.GroupControl1.Controls.Add(Me.Agama)
        Me.GroupControl1.Controls.Add(Me.StatusKawin)
        Me.GroupControl1.Controls.Add(Me.Sex)
        Me.GroupControl1.Controls.Add(Me.TglLahir)
        Me.GroupControl1.Controls.Add(Me.LabelControl5)
        Me.GroupControl1.Controls.Add(Me.LabelControl6)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Controls.Add(Me.LiveAddress)
        Me.GroupControl1.Controls.Add(Me.Hobi)
        Me.GroupControl1.Controls.Add(Me.Email)
        Me.GroupControl1.Controls.Add(Me.TelpHP)
        Me.GroupControl1.Controls.Add(Me.TelpRumah)
        Me.GroupControl1.Controls.Add(Me.TmptLahir)
        Me.GroupControl1.Controls.Add(Me.NamaLengkap)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Controls.Add(Me.LabelControl4)
        Me.GroupControl1.Controls.Add(Me.LabelControl13)
        Me.GroupControl1.Controls.Add(Me.LabelControl14)
        Me.GroupControl1.Controls.Add(Me.LabelControl15)
        Me.GroupControl1.Controls.Add(Me.LabelControl16)
        Me.GroupControl1.Controls.Add(Me.LabelControl22)
        Me.GroupControl1.Controls.Add(Me.LabelControl23)
        Me.GroupControl1.Controls.Add(Me.ShapeContainer1)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(413, 466)
        Me.GroupControl1.TabIndex = 1
        Me.GroupControl1.Text = "Data diri"
        '
        'KTP_NIK
        '
        Me.KTP_NIK.Location = New System.Drawing.Point(158, 29)
        Me.KTP_NIK.Name = "KTP_NIK"
        Me.KTP_NIK.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.KTP_NIK.Properties.Appearance.Options.UseFont = True
        Me.KTP_NIK.Properties.Mask.EditMask = "[0-9.]+"
        Me.KTP_NIK.Size = New System.Drawing.Size(239, 21)
        Me.KTP_NIK.TabIndex = 1
        '
        'LabelControl21
        '
        Me.LabelControl21.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl21.Location = New System.Drawing.Point(20, 32)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(76, 14)
        Me.LabelControl21.TabIndex = 0
        Me.LabelControl21.Text = "Nomor KTP*"
        '
        'LookArea
        '
        Me.LookArea.Enabled = False
        Me.LookArea.Location = New System.Drawing.Point(158, 406)
        Me.LookArea.Name = "LookArea"
        Me.LookArea.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LookArea.Properties.Appearance.Options.UseFont = True
        Me.LookArea.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.LookArea.Properties.NullText = ""
        Me.LookArea.Properties.View = Me.ViewArea
        Me.LookArea.Size = New System.Drawing.Size(192, 21)
        Me.LookArea.TabIndex = 14
        '
        'ViewArea
        '
        Me.ViewArea.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.ViewArea.Name = "ViewArea"
        Me.ViewArea.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.ViewArea.OptionsView.ShowGroupPanel = False
        '
        'LookKota
        '
        Me.LookKota.Location = New System.Drawing.Point(158, 379)
        Me.LookKota.Name = "LookKota"
        Me.LookKota.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LookKota.Properties.Appearance.Options.UseFont = True
        Me.LookKota.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.LookKota.Properties.NullText = ""
        Me.LookKota.Properties.View = Me.ViewKota
        Me.LookKota.Size = New System.Drawing.Size(192, 21)
        Me.LookKota.TabIndex = 13
        '
        'ViewKota
        '
        Me.ViewKota.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.ViewKota.Name = "ViewKota"
        Me.ViewKota.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.ViewKota.OptionsView.ShowGroupPanel = False
        '
        'Agama
        '
        Me.Agama.Location = New System.Drawing.Point(158, 221)
        Me.Agama.Name = "Agama"
        Me.Agama.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Agama.Properties.Appearance.Options.UseFont = True
        Me.Agama.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.Agama.Properties.Items.AddRange(New Object() {"Islam", "Katolik", "Kristen", "Budha", "Hindu"})
        Me.Agama.Size = New System.Drawing.Size(149, 21)
        Me.Agama.TabIndex = 8
        '
        'StatusKawin
        '
        Me.StatusKawin.EditValue = False
        Me.StatusKawin.Location = New System.Drawing.Point(158, 248)
        Me.StatusKawin.Name = "StatusKawin"
        Me.StatusKawin.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatusKawin.Properties.Appearance.Options.UseFont = True
        Me.StatusKawin.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(False, "Single"), New DevExpress.XtraEditors.Controls.RadioGroupItem(True, "Married")})
        Me.StatusKawin.Size = New System.Drawing.Size(149, 24)
        Me.StatusKawin.TabIndex = 9
        '
        'Sex
        '
        Me.Sex.EditValue = False
        Me.Sex.Location = New System.Drawing.Point(158, 137)
        Me.Sex.Name = "Sex"
        Me.Sex.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Sex.Properties.Appearance.Options.UseFont = True
        Me.Sex.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(False, "Pria"), New DevExpress.XtraEditors.Controls.RadioGroupItem(True, "Wanita")})
        Me.Sex.Size = New System.Drawing.Size(149, 24)
        Me.Sex.TabIndex = 5
        '
        'TglLahir
        '
        Me.TglLahir.EditValue = Nothing
        Me.TglLahir.Location = New System.Drawing.Point(158, 110)
        Me.TglLahir.Name = "TglLahir"
        Me.TglLahir.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TglLahir.Properties.Appearance.Options.UseFont = True
        Me.TglLahir.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.TglLahir.Properties.Mask.EditMask = "dd MMMM yyyy"
        Me.TglLahir.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TglLahir.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.TglLahir.Size = New System.Drawing.Size(192, 21)
        Me.TglLahir.TabIndex = 4
        '
        'LiveAddress
        '
        Me.LiveAddress.Location = New System.Drawing.Point(158, 352)
        Me.LiveAddress.Name = "LiveAddress"
        Me.LiveAddress.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LiveAddress.Properties.Appearance.Options.UseFont = True
        Me.LiveAddress.Properties.Mask.EditMask = "[A-Z0-9 .,()]+"
        Me.LiveAddress.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.LiveAddress.Size = New System.Drawing.Size(239, 21)
        Me.LiveAddress.TabIndex = 12
        '
        'Hobi
        '
        Me.Hobi.Location = New System.Drawing.Point(158, 305)
        Me.Hobi.Name = "Hobi"
        Me.Hobi.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Hobi.Properties.Appearance.Options.UseFont = True
        Me.Hobi.Properties.Mask.EditMask = "[A-Z0 ,]+"
        Me.Hobi.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.Hobi.Size = New System.Drawing.Size(192, 21)
        Me.Hobi.TabIndex = 11
        '
        'Email
        '
        Me.Email.Location = New System.Drawing.Point(158, 278)
        Me.Email.Name = "Email"
        Me.Email.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Email.Properties.Appearance.Options.UseFont = True
        Me.Email.Properties.Mask.EditMask = "[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}"
        Me.Email.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.Email.Size = New System.Drawing.Size(192, 21)
        Me.Email.TabIndex = 10
        '
        'TelpHP
        '
        Me.TelpHP.Location = New System.Drawing.Point(158, 194)
        Me.TelpHP.Name = "TelpHP"
        Me.TelpHP.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TelpHP.Properties.Appearance.Options.UseFont = True
        Me.TelpHP.Properties.Mask.EditMask = "[0-9,]+"
        Me.TelpHP.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TelpHP.Size = New System.Drawing.Size(192, 21)
        Me.TelpHP.TabIndex = 7
        '
        'TelpRumah
        '
        Me.TelpRumah.Location = New System.Drawing.Point(158, 167)
        Me.TelpRumah.Name = "TelpRumah"
        Me.TelpRumah.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TelpRumah.Properties.Appearance.Options.UseFont = True
        Me.TelpRumah.Properties.Mask.EditMask = "[0-9,]+"
        Me.TelpRumah.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TelpRumah.Size = New System.Drawing.Size(192, 21)
        Me.TelpRumah.TabIndex = 6
        '
        'TmptLahir
        '
        Me.TmptLahir.Location = New System.Drawing.Point(158, 83)
        Me.TmptLahir.Name = "TmptLahir"
        Me.TmptLahir.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TmptLahir.Properties.Appearance.Options.UseFont = True
        Me.TmptLahir.Properties.Mask.EditMask = "[A-Z ]+"
        Me.TmptLahir.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TmptLahir.Size = New System.Drawing.Size(149, 21)
        Me.TmptLahir.TabIndex = 3
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(2, 23)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape1})
        Me.ShapeContainer1.Size = New System.Drawing.Size(409, 441)
        Me.ShapeContainer1.TabIndex = 6
        Me.ShapeContainer1.TabStop = False
        '
        'LineShape1
        '
        Me.LineShape1.BorderColor = System.Drawing.SystemColors.ButtonShadow
        Me.LineShape1.Name = "LineShape1"
        Me.LineShape1.X1 = 16
        Me.LineShape1.X2 = 400
        Me.LineShape1.Y1 = 313
        Me.LineShape1.Y2 = 313
        '
        'GroupControl2
        '
        Me.GroupControl2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl2.AppearanceCaption.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.GroupControl2.AppearanceCaption.Options.UseFont = True
        Me.GroupControl2.Controls.Add(Me.LabelControl8)
        Me.GroupControl2.Controls.Add(Me.LabelControl11)
        Me.GroupControl2.Controls.Add(Me.LabelControl12)
        Me.GroupControl2.Controls.Add(Me.LabelControl10)
        Me.GroupControl2.Controls.Add(Me.LabelControl9)
        Me.GroupControl2.Controls.Add(Me.KTPAddress)
        Me.GroupControl2.Controls.Add(Me.KTPKecamatan)
        Me.GroupControl2.Controls.Add(Me.KTPKelurahan)
        Me.GroupControl2.Controls.Add(Me.KTPRtrw)
        Me.GroupControl2.Controls.Add(Me.KTPKota)
        Me.GroupControl2.Location = New System.Drawing.Point(431, 12)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(378, 168)
        Me.GroupControl2.TabIndex = 15
        Me.GroupControl2.Text = "Data KTP"
        '
        'KTPAddress
        '
        Me.KTPAddress.Location = New System.Drawing.Point(123, 29)
        Me.KTPAddress.Name = "KTPAddress"
        Me.KTPAddress.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.KTPAddress.Properties.Appearance.Options.UseFont = True
        Me.KTPAddress.Properties.Mask.EditMask = "[A-Z0-9 .,]+"
        Me.KTPAddress.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.KTPAddress.Size = New System.Drawing.Size(239, 21)
        Me.KTPAddress.TabIndex = 15
        '
        'KTPKecamatan
        '
        Me.KTPKecamatan.Location = New System.Drawing.Point(123, 137)
        Me.KTPKecamatan.Name = "KTPKecamatan"
        Me.KTPKecamatan.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.KTPKecamatan.Properties.Appearance.Options.UseFont = True
        Me.KTPKecamatan.Properties.Mask.EditMask = "[A-Z0-9 .,]+"
        Me.KTPKecamatan.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.KTPKecamatan.Properties.MaxLength = 50
        Me.KTPKecamatan.Size = New System.Drawing.Size(176, 21)
        Me.KTPKecamatan.TabIndex = 19
        '
        'KTPKelurahan
        '
        Me.KTPKelurahan.Location = New System.Drawing.Point(123, 110)
        Me.KTPKelurahan.Name = "KTPKelurahan"
        Me.KTPKelurahan.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.KTPKelurahan.Properties.Appearance.Options.UseFont = True
        Me.KTPKelurahan.Properties.Mask.EditMask = "[A-Z0-9 .,]+"
        Me.KTPKelurahan.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.KTPKelurahan.Properties.MaxLength = 50
        Me.KTPKelurahan.Size = New System.Drawing.Size(176, 21)
        Me.KTPKelurahan.TabIndex = 18
        '
        'KTPRtrw
        '
        Me.KTPRtrw.Location = New System.Drawing.Point(123, 83)
        Me.KTPRtrw.Name = "KTPRtrw"
        Me.KTPRtrw.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.KTPRtrw.Properties.Appearance.Options.UseFont = True
        Me.KTPRtrw.Properties.Mask.EditMask = "[0-9/]+"
        Me.KTPRtrw.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.KTPRtrw.Properties.MaxLength = 6
        Me.KTPRtrw.Size = New System.Drawing.Size(70, 21)
        Me.KTPRtrw.TabIndex = 17
        '
        'KTPKota
        '
        Me.KTPKota.Location = New System.Drawing.Point(123, 56)
        Me.KTPKota.Name = "KTPKota"
        Me.KTPKota.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.KTPKota.Properties.Appearance.Options.UseFont = True
        Me.KTPKota.Properties.Mask.EditMask = "[A-Z ]+"
        Me.KTPKota.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.KTPKota.Properties.MaxLength = 50
        Me.KTPKota.Size = New System.Drawing.Size(176, 21)
        Me.KTPKota.TabIndex = 16
        '
        'GroupControl3
        '
        Me.GroupControl3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl3.AppearanceCaption.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.GroupControl3.AppearanceCaption.Options.UseFont = True
        Me.GroupControl3.Controls.Add(Me.LabelControl17)
        Me.GroupControl3.Controls.Add(Me.LabelControl18)
        Me.GroupControl3.Controls.Add(Me.LabelControl19)
        Me.GroupControl3.Controls.Add(Me.LabelControl20)
        Me.GroupControl3.Controls.Add(Me.TelpKantor)
        Me.GroupControl3.Controls.Add(Me.AddressOffice)
        Me.GroupControl3.Controls.Add(Me.Perusahaan)
        Me.GroupControl3.Controls.Add(Me.Pekerjaan)
        Me.GroupControl3.Location = New System.Drawing.Point(431, 186)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(378, 140)
        Me.GroupControl3.TabIndex = 20
        Me.GroupControl3.Text = "Data pekerjaan"
        '
        'TelpKantor
        '
        Me.TelpKantor.Location = New System.Drawing.Point(123, 110)
        Me.TelpKantor.Name = "TelpKantor"
        Me.TelpKantor.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.TelpKantor.Properties.Appearance.Options.UseFont = True
        Me.TelpKantor.Properties.Mask.EditMask = "[0-9,]+"
        Me.TelpKantor.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TelpKantor.Properties.MaxLength = 50
        Me.TelpKantor.Size = New System.Drawing.Size(185, 21)
        Me.TelpKantor.TabIndex = 23
        '
        'AddressOffice
        '
        Me.AddressOffice.Location = New System.Drawing.Point(123, 83)
        Me.AddressOffice.Name = "AddressOffice"
        Me.AddressOffice.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.AddressOffice.Properties.Appearance.Options.UseFont = True
        Me.AddressOffice.Properties.Mask.EditMask = "[A-Z0-9 .,]+"
        Me.AddressOffice.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.AddressOffice.Properties.MaxLength = 50
        Me.AddressOffice.Size = New System.Drawing.Size(239, 21)
        Me.AddressOffice.TabIndex = 22
        '
        'Perusahaan
        '
        Me.Perusahaan.Location = New System.Drawing.Point(123, 56)
        Me.Perusahaan.Name = "Perusahaan"
        Me.Perusahaan.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.Perusahaan.Properties.Appearance.Options.UseFont = True
        Me.Perusahaan.Properties.Mask.EditMask = "[A-Z0-9 .,]+"
        Me.Perusahaan.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.Perusahaan.Properties.MaxLength = 50
        Me.Perusahaan.Size = New System.Drawing.Size(185, 21)
        Me.Perusahaan.TabIndex = 21
        '
        'Pekerjaan
        '
        Me.Pekerjaan.Location = New System.Drawing.Point(123, 29)
        Me.Pekerjaan.Name = "Pekerjaan"
        Me.Pekerjaan.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.Pekerjaan.Properties.Appearance.Options.UseFont = True
        Me.Pekerjaan.Properties.Mask.EditMask = "[A-Z0-9 .,]+"
        Me.Pekerjaan.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.Pekerjaan.Properties.MaxLength = 50
        Me.Pekerjaan.Size = New System.Drawing.Size(185, 21)
        Me.Pekerjaan.TabIndex = 20
        '
        'GroupControl4
        '
        Me.GroupControl4.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl4.AppearanceCaption.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.GroupControl4.AppearanceCaption.Options.UseFont = True
        Me.GroupControl4.Controls.Add(Me.LookKategori)
        Me.GroupControl4.Controls.Add(Me.CekBlacklist)
        Me.GroupControl4.Controls.Add(Me.CekInaktif)
        Me.GroupControl4.Controls.Add(Me.LabelControl25)
        Me.GroupControl4.Controls.Add(Me.KodeMember)
        Me.GroupControl4.Controls.Add(Me.Catatan)
        Me.GroupControl4.Controls.Add(Me.LabelKodeMember)
        Me.GroupControl4.Controls.Add(Me.LabelControl24)
        Me.GroupControl4.Location = New System.Drawing.Point(431, 332)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(378, 146)
        Me.GroupControl4.TabIndex = 28
        Me.GroupControl4.Text = "Lain-lain"
        '
        'LookKategori
        '
        Me.LookKategori.Location = New System.Drawing.Point(123, 32)
        Me.LookKategori.Name = "LookKategori"
        Me.LookKategori.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LookKategori.Properties.Appearance.Options.UseFont = True
        Me.LookKategori.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.LookKategori.Properties.NullText = ""
        Me.LookKategori.Properties.View = Me.ViewKategori
        Me.LookKategori.Size = New System.Drawing.Size(185, 21)
        Me.LookKategori.TabIndex = 24
        '
        'ViewKategori
        '
        Me.ViewKategori.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.ViewKategori.Name = "ViewKategori"
        Me.ViewKategori.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.ViewKategori.OptionsView.ShowGroupPanel = False
        '
        'CekBlacklist
        '
        Me.CekBlacklist.Location = New System.Drawing.Point(121, 86)
        Me.CekBlacklist.Name = "CekBlacklist"
        Me.CekBlacklist.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.CekBlacklist.Properties.Appearance.Options.UseFont = True
        Me.CekBlacklist.Properties.Caption = "Blacklist"
        Me.CekBlacklist.Size = New System.Drawing.Size(94, 19)
        Me.CekBlacklist.TabIndex = 26
        '
        'CekInaktif
        '
        Me.CekInaktif.Location = New System.Drawing.Point(221, 86)
        Me.CekInaktif.Name = "CekInaktif"
        Me.CekInaktif.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.CekInaktif.Properties.Appearance.Options.UseFont = True
        Me.CekInaktif.Properties.Caption = "Tidak aktif"
        Me.CekInaktif.Size = New System.Drawing.Size(94, 19)
        Me.CekInaktif.TabIndex = 27
        '
        'KodeMember
        '
        Me.KodeMember.Location = New System.Drawing.Point(123, 111)
        Me.KodeMember.Name = "KodeMember"
        Me.KodeMember.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.KodeMember.Properties.Appearance.Options.UseFont = True
        Me.KodeMember.Properties.Mask.EditMask = "[0-9]{8} [0-9]{4}"
        Me.KodeMember.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.KodeMember.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.KodeMember.Properties.MaxLength = 50
        Me.KodeMember.Properties.ReadOnly = True
        Me.KodeMember.Size = New System.Drawing.Size(239, 21)
        Me.KodeMember.TabIndex = 25
        Me.KodeMember.Visible = False
        '
        'Catatan
        '
        Me.Catatan.Location = New System.Drawing.Point(123, 59)
        Me.Catatan.Name = "Catatan"
        Me.Catatan.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.Catatan.Properties.Appearance.Options.UseFont = True
        Me.Catatan.Properties.Mask.EditMask = "[A-Z0-9 .,()]+"
        Me.Catatan.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.Catatan.Properties.MaxLength = 50
        Me.Catatan.Size = New System.Drawing.Size(239, 21)
        Me.Catatan.TabIndex = 25
        '
        'LabelKodeMember
        '
        Me.LabelKodeMember.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelKodeMember.Location = New System.Drawing.Point(25, 114)
        Me.LabelKodeMember.Name = "LabelKodeMember"
        Me.LabelKodeMember.Size = New System.Drawing.Size(86, 14)
        Me.LabelKodeMember.TabIndex = 0
        Me.LabelKodeMember.Text = "Kode Member"
        Me.LabelKodeMember.Visible = False
        '
        'ButSimpan
        '
        Me.ButSimpan.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButSimpan.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButSimpan.Appearance.Options.UseFont = True
        Me.ButSimpan.Image = Global.Laritta_Membership.My.Resources.Resources.apply_32
        Me.ButSimpan.Location = New System.Drawing.Point(623, 486)
        Me.ButSimpan.Name = "ButSimpan"
        Me.ButSimpan.Size = New System.Drawing.Size(90, 40)
        Me.ButSimpan.TabIndex = 29
        Me.ButSimpan.Text = "&Simpan"
        '
        'ButBatal
        '
        Me.ButBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButBatal.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButBatal.Appearance.Options.UseFont = True
        Me.ButBatal.Image = Global.Laritta_Membership.My.Resources.Resources.back_32
        Me.ButBatal.Location = New System.Drawing.Point(719, 486)
        Me.ButBatal.Name = "ButBatal"
        Me.ButBatal.Size = New System.Drawing.Size(90, 40)
        Me.ButBatal.TabIndex = 30
        Me.ButBatal.Text = "&Batal"
        '
        'MasterCustomer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(821, 538)
        Me.Controls.Add(Me.ButBatal)
        Me.Controls.Add(Me.ButSimpan)
        Me.Controls.Add(Me.GroupControl4)
        Me.Controls.Add(Me.GroupControl3)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.LabelControl7)
        Me.Name = "MasterCustomer"
        Me.Text = "MasterCustomer"
        CType(Me.NamaLengkap.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.KTP_NIK.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LookArea.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewArea, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LookKota.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewKota, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Agama.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StatusKawin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sex.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TglLahir.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TglLahir.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LiveAddress.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Hobi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Email.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TelpHP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TelpRumah.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TmptLahir.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.KTPAddress.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.KTPKecamatan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.KTPKelurahan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.KTPRtrw.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.KTPKota.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.TelpKantor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AddressOffice.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Perusahaan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Pekerjaan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        Me.GroupControl4.PerformLayout()
        CType(Me.LookKategori.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewKategori, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CekBlacklist.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CekInaktif.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.KodeMember.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Catatan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents NamaLengkap As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl25 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents TglLahir As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Hobi As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Email As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TelpRumah As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TmptLahir As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LookArea As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents ViewArea As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LookKota As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents ViewKota As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Agama As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents StatusKawin As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents Sex As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LiveAddress As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TelpHP As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents LineShape1 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents KTPAddress As DevExpress.XtraEditors.TextEdit
    Friend WithEvents KTPKecamatan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents KTPKelurahan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents KTPRtrw As DevExpress.XtraEditors.TextEdit
    Friend WithEvents KTPKota As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TelpKantor As DevExpress.XtraEditors.TextEdit
    Friend WithEvents AddressOffice As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Perusahaan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Pekerjaan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LookKategori As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents CekBlacklist As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CekInaktif As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents Catatan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ButSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButBatal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ViewKategori As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents KTP_NIK As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents KodeMember As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelKodeMember As DevExpress.XtraEditors.LabelControl
End Class
