﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MasterTipeMembership
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DaftarRewardGift = New DevExpress.XtraGrid.GridControl()
        Me.ViewRewardGift = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.DaftarGift = New DevExpress.XtraEditors.Repository.RepositoryItemSearchLookUpEdit()
        Me.ViewDaftarGift = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.isHave = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.HargaKartu = New DevExpress.XtraEditors.TextEdit()
        Me.SyaratBeli = New DevExpress.XtraEditors.TextEdit()
        Me.LevelTipe = New DevExpress.XtraEditors.TextEdit()
        Me.NamaTipe = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.ButBatal = New DevExpress.XtraEditors.SimpleButton()
        Me.ButSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.CekInaktif = New DevExpress.XtraEditors.CheckEdit()
        CType(Me.DaftarRewardGift, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewRewardGift, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DaftarGift, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewDaftarGift, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.isHave, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.HargaKartu.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SyaratBeli.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LevelTipe.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NamaTipe.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.CekInaktif.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DaftarRewardGift
        '
        Me.DaftarRewardGift.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DaftarRewardGift.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.DaftarRewardGift.Location = New System.Drawing.Point(166, 26)
        Me.DaftarRewardGift.MainView = Me.ViewRewardGift
        Me.DaftarRewardGift.Name = "DaftarRewardGift"
        Me.DaftarRewardGift.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.DaftarGift, Me.isHave})
        Me.DaftarRewardGift.Size = New System.Drawing.Size(589, 292)
        Me.DaftarRewardGift.TabIndex = 6
        Me.DaftarRewardGift.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.ViewRewardGift})
        '
        'ViewRewardGift
        '
        Me.ViewRewardGift.Appearance.HeaderPanel.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ViewRewardGift.Appearance.HeaderPanel.Options.UseFont = True
        Me.ViewRewardGift.Appearance.Row.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ViewRewardGift.Appearance.Row.Options.UseFont = True
        Me.ViewRewardGift.ColumnPanelRowHeight = 40
        Me.ViewRewardGift.GridControl = Me.DaftarRewardGift
        Me.ViewRewardGift.Name = "ViewRewardGift"
        Me.ViewRewardGift.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewRewardGift.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewRewardGift.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewRewardGift.OptionsCustomization.AllowColumnMoving = False
        Me.ViewRewardGift.OptionsCustomization.AllowColumnResizing = False
        Me.ViewRewardGift.OptionsCustomization.AllowFilter = False
        Me.ViewRewardGift.OptionsCustomization.AllowGroup = False
        Me.ViewRewardGift.OptionsCustomization.AllowQuickHideColumns = False
        Me.ViewRewardGift.OptionsCustomization.AllowSort = False
        Me.ViewRewardGift.OptionsView.ShowGroupPanel = False
        Me.ViewRewardGift.RowHeight = 30
        '
        'DaftarGift
        '
        Me.DaftarGift.AutoHeight = False
        Me.DaftarGift.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DaftarGift.Name = "DaftarGift"
        Me.DaftarGift.View = Me.ViewDaftarGift
        '
        'ViewDaftarGift
        '
        Me.ViewDaftarGift.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.ViewDaftarGift.Name = "ViewDaftarGift"
        Me.ViewDaftarGift.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.ViewDaftarGift.OptionsView.ShowGroupPanel = False
        '
        'isHave
        '
        Me.isHave.AutoHeight = False
        Me.isHave.Name = "isHave"
        '
        'HargaKartu
        '
        Me.HargaKartu.EditValue = 0
        Me.HargaKartu.Location = New System.Drawing.Point(184, 109)
        Me.HargaKartu.Name = "HargaKartu"
        Me.HargaKartu.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.HargaKartu.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.HargaKartu.Properties.Appearance.Options.UseFont = True
        Me.HargaKartu.Properties.Appearance.Options.UseTextOptions = True
        Me.HargaKartu.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.HargaKartu.Properties.Mask.EditMask = "n0"
        Me.HargaKartu.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.HargaKartu.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.HargaKartu.Properties.NullText = "0"
        Me.HargaKartu.Size = New System.Drawing.Size(114, 21)
        Me.HargaKartu.TabIndex = 5
        '
        'SyaratBeli
        '
        Me.SyaratBeli.EditValue = 0
        Me.SyaratBeli.Location = New System.Drawing.Point(184, 82)
        Me.SyaratBeli.Name = "SyaratBeli"
        Me.SyaratBeli.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.SyaratBeli.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.SyaratBeli.Properties.Appearance.Options.UseFont = True
        Me.SyaratBeli.Properties.Appearance.Options.UseTextOptions = True
        Me.SyaratBeli.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.SyaratBeli.Properties.Mask.EditMask = "n0"
        Me.SyaratBeli.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.SyaratBeli.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.SyaratBeli.Properties.NullText = "0"
        Me.SyaratBeli.Size = New System.Drawing.Size(114, 21)
        Me.SyaratBeli.TabIndex = 4
        '
        'LevelTipe
        '
        Me.LevelTipe.EditValue = 1
        Me.LevelTipe.Location = New System.Drawing.Point(166, 55)
        Me.LevelTipe.Name = "LevelTipe"
        Me.LevelTipe.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.LevelTipe.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LevelTipe.Properties.Appearance.Options.UseFont = True
        Me.LevelTipe.Properties.Appearance.Options.UseTextOptions = True
        Me.LevelTipe.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LevelTipe.Properties.Mask.EditMask = "[1-4]"
        Me.LevelTipe.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.LevelTipe.Properties.NullText = "1"
        Me.LevelTipe.Size = New System.Drawing.Size(54, 21)
        Me.LevelTipe.TabIndex = 3
        '
        'NamaTipe
        '
        Me.NamaTipe.Location = New System.Drawing.Point(166, 28)
        Me.NamaTipe.Name = "NamaTipe"
        Me.NamaTipe.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.NamaTipe.Properties.Appearance.Options.UseFont = True
        Me.NamaTipe.Size = New System.Drawing.Size(278, 21)
        Me.NamaTipe.TabIndex = 2
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl5.Location = New System.Drawing.Point(15, 38)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(81, 14)
        Me.LabelControl5.TabIndex = 0
        Me.LabelControl5.Text = "Basic reward"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl4.Location = New System.Drawing.Point(14, 113)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(75, 14)
        Me.LabelControl4.TabIndex = 0
        Me.LabelControl4.Text = "Harga kartu"
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl3.Location = New System.Drawing.Point(14, 86)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(110, 14)
        Me.LabelControl3.TabIndex = 0
        Me.LabelControl3.Text = "Syarat pembelian"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl2.Location = New System.Drawing.Point(15, 59)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(114, 14)
        Me.LabelControl2.TabIndex = 0
        Me.LabelControl2.Text = "Level membership"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl1.Location = New System.Drawing.Point(15, 31)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(145, 14)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Nama tipe membership"
        '
        'ButBatal
        '
        Me.ButBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButBatal.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButBatal.Appearance.Options.UseFont = True
        Me.ButBatal.Image = Global.Laritta_Membership.My.Resources.Resources.back_32
        Me.ButBatal.Location = New System.Drawing.Point(682, 509)
        Me.ButBatal.Name = "ButBatal"
        Me.ButBatal.Size = New System.Drawing.Size(90, 40)
        Me.ButBatal.TabIndex = 8
        Me.ButBatal.Text = "&Batal"
        '
        'ButSimpan
        '
        Me.ButSimpan.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButSimpan.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButSimpan.Appearance.Options.UseFont = True
        Me.ButSimpan.Image = Global.Laritta_Membership.My.Resources.Resources.apply_32
        Me.ButSimpan.Location = New System.Drawing.Point(586, 509)
        Me.ButSimpan.Name = "ButSimpan"
        Me.ButSimpan.Size = New System.Drawing.Size(90, 40)
        Me.ButSimpan.TabIndex = 7
        Me.ButSimpan.Text = "&Simpan"
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl6.Location = New System.Drawing.Point(165, 86)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(16, 14)
        Me.LabelControl6.TabIndex = 0
        Me.LabelControl6.Text = "Rp"
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl7.Location = New System.Drawing.Point(165, 113)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(16, 14)
        Me.LabelControl7.TabIndex = 0
        Me.LabelControl7.Text = "Rp"
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.Controls.Add(Me.DaftarRewardGift)
        Me.GroupControl1.Controls.Add(Me.LabelControl5)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 180)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(760, 323)
        Me.GroupControl1.TabIndex = 6
        Me.GroupControl1.Text = "Keuntungan member"
        '
        'GroupControl2
        '
        Me.GroupControl2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl2.AppearanceCaption.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.GroupControl2.AppearanceCaption.Options.UseFont = True
        Me.GroupControl2.Controls.Add(Me.LabelControl1)
        Me.GroupControl2.Controls.Add(Me.HargaKartu)
        Me.GroupControl2.Controls.Add(Me.LabelControl3)
        Me.GroupControl2.Controls.Add(Me.LabelControl6)
        Me.GroupControl2.Controls.Add(Me.SyaratBeli)
        Me.GroupControl2.Controls.Add(Me.LabelControl2)
        Me.GroupControl2.Controls.Add(Me.LabelControl4)
        Me.GroupControl2.Controls.Add(Me.LevelTipe)
        Me.GroupControl2.Controls.Add(Me.LabelControl7)
        Me.GroupControl2.Controls.Add(Me.NamaTipe)
        Me.GroupControl2.Location = New System.Drawing.Point(12, 37)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(760, 137)
        Me.GroupControl2.TabIndex = 1
        Me.GroupControl2.Text = "Informasi dasar"
        '
        'CekInaktif
        '
        Me.CekInaktif.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CekInaktif.Location = New System.Drawing.Point(682, 12)
        Me.CekInaktif.Name = "CekInaktif"
        Me.CekInaktif.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.CekInaktif.Properties.Appearance.Options.UseFont = True
        Me.CekInaktif.Properties.Caption = "Tidak aktif"
        Me.CekInaktif.Size = New System.Drawing.Size(90, 19)
        Me.CekInaktif.TabIndex = 1
        '
        'MasterTipeMembership
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(784, 561)
        Me.Controls.Add(Me.CekInaktif)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.ButBatal)
        Me.Controls.Add(Me.ButSimpan)
        Me.Name = "MasterTipeMembership"
        Me.Text = "MasterTipeMembership"
        CType(Me.DaftarRewardGift, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewRewardGift, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DaftarGift, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewDaftarGift, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.isHave, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.HargaKartu.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SyaratBeli.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LevelTipe.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NamaTipe.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.CekInaktif.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DaftarRewardGift As DevExpress.XtraGrid.GridControl
    Friend WithEvents ViewRewardGift As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents HargaKartu As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SyaratBeli As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LevelTipe As DevExpress.XtraEditors.TextEdit
    Friend WithEvents NamaTipe As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ButBatal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents DaftarGift As DevExpress.XtraEditors.Repository.RepositoryItemSearchLookUpEdit
    Friend WithEvents ViewDaftarGift As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents CekInaktif As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents isHave As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
End Class
