﻿Imports DevExpress.Utils

Public Class MasterCustomer
    Dim NIK_check As String

    Private Sub MasterCustomer_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("DataMasterCustomer")
        xSet.Tables.Remove("DaftarKota")
        xSet.Tables.Remove("DaftarArea")
        xSet.Tables.Remove("DaftarKategori")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub MasterCustomer_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        LoadSprite.CloseLoading()
        KTP_NIK.Focus()

        If xSet.Tables("User_ProgPermisisons").Select("permission_id=6")(0).Item("allow_read") Then
            KodeMember.Properties.PasswordChar = ""
        Else
            KodeMember.Properties.PasswordChar = "*"
        End If

        If selmaster_id = 0 Then
            CekInaktif.Properties.ReadOnly = True
            CekInaktif.Checked = False
        Else
            If xSet.Tables("DataMasterCustomer").Rows(0).Item("isMember") <> 0 Then
                TglLahir.Properties.ReadOnly = True
                LabelKodeMember.Visible = True
                KodeMember.Visible = True
            End If
        End If
    End Sub

    Private Sub MasterCustomer_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()
        Get_KebutuhanDaftar()

        Try
            If Not selmaster_id = 0 Then
                SQLquery = String.Format("SELECT idKategoriCust, namaCust, BirthPlace, BirthDate, IFNULL(sex, False) sex, homeAddress, id_kota, id_area, KTP_NIK, KTP_homeAddress, KTP_Kota, " & _
                                         "KTP_RTRW, KTP_Kelurahan, KTP_Kecamatan, Phone, PhoneMobile, Religion, IFNULL(StatusKawin, False) StatusKawin, " & _
                                         "Pekerjaan, PerusahaanKerja, AlamatKerja, TelpKerja, Email, Hobi, blacklist, notes, Inactive, IFNULL(id_tipemembership,0) isMember, kode_member FROM mcustomer WHERE idCust={0}", selmaster_id)
                ExDb.ExecQuery(1, SQLquery, xSet, "DataMasterCustomer")

                KTP_NIK.EditValue = xSet.Tables("DataMasterCustomer").Rows(0).Item("KTP_NIK").ToString
                NamaLengkap.EditValue = xSet.Tables("DataMasterCustomer").Rows(0).Item("namaCust")
                TmptLahir.EditValue = xSet.Tables("DataMasterCustomer").Rows(0).Item("BirthPlace").ToString
                TglLahir.EditValue = xSet.Tables("DataMasterCustomer").Rows(0).Item("BirthDate")
                Sex.EditValue = CBool(xSet.Tables("DataMasterCustomer").Rows(0).Item("sex"))
                TelpRumah.EditValue = xSet.Tables("DataMasterCustomer").Rows(0).Item("Phone")
                TelpHP.EditValue = xSet.Tables("DataMasterCustomer").Rows(0).Item("PhoneMobile")
                Agama.EditValue = xSet.Tables("DataMasterCustomer").Rows(0).Item("Religion")
                StatusKawin.EditValue = CBool(xSet.Tables("DataMasterCustomer").Rows(0).Item("StatusKawin"))
                Email.EditValue = xSet.Tables("DataMasterCustomer").Rows(0).Item("Email")
                Hobi.EditValue = xSet.Tables("DataMasterCustomer").Rows(0).Item("Hobi")

                LiveAddress.EditValue = xSet.Tables("DataMasterCustomer").Rows(0).Item("homeAddress")
                LookKota.EditValue = xSet.Tables("DataMasterCustomer").Rows(0).Item("id_kota")
                LookArea.EditValue = xSet.Tables("DataMasterCustomer").Rows(0).Item("id_area")

                KTPAddress.EditValue = xSet.Tables("DataMasterCustomer").Rows(0).Item("KTP_homeAddress")
                KTPKota.EditValue = xSet.Tables("DataMasterCustomer").Rows(0).Item("KTP_Kota")
                KTPRtrw.EditValue = xSet.Tables("DataMasterCustomer").Rows(0).Item("KTP_RTRW")
                KTPKelurahan.EditValue = xSet.Tables("DataMasterCustomer").Rows(0).Item("KTP_Kelurahan")
                KTPKecamatan.EditValue = xSet.Tables("DataMasterCustomer").Rows(0).Item("KTP_Kecamatan")

                Pekerjaan.EditValue = xSet.Tables("DataMasterCustomer").Rows(0).Item("Pekerjaan")
                Perusahaan.EditValue = xSet.Tables("DataMasterCustomer").Rows(0).Item("PerusahaanKerja")
                AddressOffice.EditValue = xSet.Tables("DataMasterCustomer").Rows(0).Item("AlamatKerja")
                TelpKantor.EditValue = xSet.Tables("DataMasterCustomer").Rows(0).Item("TelpKerja")

                LookKategori.EditValue = xSet.Tables("DataMasterCustomer").Rows(0).Item("idKategoriCust")
                Catatan.EditValue = xSet.Tables("DataMasterCustomer").Rows(0).Item("notes")
                CekBlacklist.Checked = xSet.Tables("DataMasterCustomer").Rows(0).Item("blacklist")
                CekInaktif.Checked = xSet.Tables("DataMasterCustomer").Rows(0).Item("Inactive")

                KodeMember.EditValue = xSet.Tables("DataMasterCustomer").Rows(0).Item("kode_member")

                nameCheck = NamaLengkap.EditValue
                NIK_check = KTP_NIK.EditValue
            End If

        Catch ex As Exception
            MsgErrorDev()
        End Try
    End Sub

    Private Sub LookKota_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles LookKota.EditValueChanged
        If Not LookKota.EditValue Is Nothing Then
            LookArea.Enabled = True

            Try
                If Not xSet.Tables("DaftarArea") Is Nothing Then xSet.Tables("DaftarArea").Clear()
                SQLquery = String.Format("SELECT id_area ID, nama_area Nama FROM mbsm_locarea WHERE Inactive=0 AND id_kota='{0}' ", LookKota.EditValue)
                If Not selected_id = 0 Then SQLquery += String.Format("OR x.id_penerimaanpartner={0} ", xSet.Tables("mt_returclient").Rows(0).Item("id_penerimaanpartner"))
                ExDb.ExecQuery(1, SQLquery, xSet, "DaftarArea")

                LookArea.Properties.DataSource = xSet.Tables("DaftarArea").DefaultView
                LookArea.Properties.NullText = ""
                LookArea.Properties.ValueMember = "ID"
                LookArea.Properties.DisplayMember = "Nama"
                LookArea.Properties.ShowClearButton = False
                LookArea.Properties.PopulateViewColumns()

                ViewArea.Columns("ID").Visible = False

                For Each coll As DataColumn In xSet.Tables("DaftarArea").Columns
                    ViewArea.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
                Next
            Catch ex As Exception
                xSet.Tables("DaftarArea").Clear()
            End Try
        Else
            LookArea.Enabled = False
        End If
    End Sub

    Private Sub ButSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSimpan.Click
        If Not beforeSave() Then Return
        Try
            LoadSprite.ShowLoading()
            If selmaster_id = "0" Then
                sp_Query = "INSERT INTO mcustomer (idCabang, idKategoriCust, namaCust, BirthPlace, BirthDate, sex, homeAddress, id_kota, id_area, " & _
                    "KTP_NIK, KTP_homeAddress, KTP_Kota, KTP_RTRW, KTP_Kelurahan, KTP_Kecamatan, Phone, PhoneMobile, Religion, StatusKawin, " & _
                    "Pekerjaan, PerusahaanKerja, AlamatKerja, TelpKerja, Email, Hobi, " & _
                    "blacklist, notes, createBy, updateDate, inactive) "
                param_Query = "VALUES(" & id_cabang & ", " & _
                    LookKategori.EditValue & ", " & _
                    "'" & NamaLengkap.Text & "', " & _
                    "'" & TmptLahir.Text & "', " & _
                    "'" & Format(TglLahir.EditValue, "yyyy-MM-dd") & "', " & _
                    Sex.EditValue & ", " & _
                    "'" & LiveAddress.Text & "', " & _
                    LookKota.EditValue & ", " & _
                    IIf(LookArea.Text = "", "NULL", LookArea.EditValue) & ", " & _
                    "REPLACE('" & KTP_NIK.Text & "', ' ', ''), " & _
                    "'" & KTPAddress.Text & "', " & _
                    "'" & KTPKota.Text & "', " & _
                    "'" & KTPRtrw.Text & "', " & _
                    "'" & KTPKelurahan.Text & "', " & _
                    "'" & KTPKecamatan.Text & "', " & _
                    "'" & TelpRumah.Text & "', " & _
                    "'" & TelpHP.Text & "', " & _
                    "'" & Agama.SelectedItem & "', " & _
                    StatusKawin.EditValue & ", " & _
                    "'" & Pekerjaan.Text & "', " & _
                    "'" & Perusahaan.Text & "', " & _
                    "'" & AddressOffice.Text & "', " & _
                    "'" & TelpKantor.Text & "', " & _
                    "'" & Email.Text & "', " & _
                    "'" & Hobi.Text & "', " & _
                    "" & CekBlacklist.Checked & ", " & _
                    "'" & Catatan.Text & "', " & _
                    staff_id & ", " & _
                    "NOW(), " & _
                    CekInaktif.Checked & ")"
            Else
                sp_Query = "UPDATE mcustomer SET " & _
                    "idKategoriCust=" & LookKategori.EditValue & ", " & _
                    "namaCust='" & NamaLengkap.Text & "', " & _
                    "BirthPlace='" & TmptLahir.Text & "', " & _
                    "BirthDate='" & Format(TglLahir.EditValue, "yyyy-MM-dd") & "', " & _
                    "sex=" & Sex.EditValue & ", " & _
                    "homeAddress='" & LiveAddress.Text & "', " & _
                    "id_kota=" & LookKota.EditValue & ", " & _
                    "id_area=" & IIf(LookArea.Text = "", "NULL", LookArea.EditValue) & ", " & _
                    "KTP_NIK='" & KTP_NIK.Text & "', " & _
                    "KTP_homeAddress='" & KTPAddress.Text & "', " & _
                    "KTP_Kota='" & KTPKota.Text & "', " & _
                    "KTP_RTRW='" & KTPRtrw.Text & "', " & _
                    "KTP_Kelurahan='" & KTPKelurahan.Text & "', " & _
                    "KTP_Kecamatan='" & KTPKecamatan.Text & "', " & _
                    "Phone='" & TelpRumah.Text & "', " & _
                    "PhoneMobile='" & TelpHP.Text & "', " & _
                    "Religion='" & Agama.SelectedItem & "', " & _
                    "StatusKawin=" & StatusKawin.EditValue & ", " & _
                    "Pekerjaan='" & Pekerjaan.Text & "', " & _
                    "PerusahaanKerja='" & Perusahaan.Text & "', " & _
                    "AlamatKerja='" & AddressOffice.Text & "', " & _
                    "TelpKerja='" & TelpKantor.Text & "', " & _
                    "Email='" & Email.Text & "', " & _
                    "Hobi='" & Hobi.Text & "', " & _
                    "blacklist=" & CekBlacklist.Checked & ", " & _
                    "notes='" & Catatan.Text & "', " & _
                    "updateBy=" & staff_id & ", " & _
                    "updateDate=NOW(), " & _
                    "inactive=" & CekInaktif.Checked
                param_Query = String.Format(" WHERE idCust={0}", selmaster_id)
            End If

            'If save_online.Checked And MultiServer And OnlineStatus Then
            '    ExDb.ExecData(1, sp_Query & param_Query)
            'End If
            ExDb.ExecData(1, sp_Query & param_Query)
            MsgInfo("Data berhasil disimpan, tekan OK untuk melanjutkan.")
            BrowserMasterCustomer.isChange = True
        Catch ex As Exception
            MsgErrorDev()
        End Try
        LoadSprite.CloseLoading()
        Close()
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Sub Get_KebutuhanDaftar()
        '--------------------------------Kota
        SQLquery = "SELECT id_kota ID, nama_kota Nama FROM mbsm_lockota WHERE Inactive=0;"
        ExDb.ExecQuery(1, SQLquery, xSet, "DaftarKota")
        LookKota.Properties.DataSource = xSet.Tables("DaftarKota").DefaultView

        LookKota.Properties.NullText = ""
        LookKota.Properties.ValueMember = "ID"
        LookKota.Properties.DisplayMember = "Nama"
        LookKota.Properties.ShowClearButton = False
        LookKota.Properties.PopulateViewColumns()

        LookKota.Properties.PopupFormSize = New Size(200, 300)

        ViewKota.Columns("ID").Visible = False

        For Each coll As DataColumn In xSet.Tables("DaftarKota").Columns
            ViewKota.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        '--------------------------------Kategori
        SQLquery = "SELECT idKategoriCust ID, namaKategoriCust Nama FROM mkategoricust WHERE Inactive=0;"
        ExDb.ExecQuery(1, SQLquery, xSet, "DaftarKategori")
        LookKategori.Properties.DataSource = xSet.Tables("DaftarKategori").DefaultView

        LookKategori.Properties.NullText = ""
        LookKategori.Properties.ValueMember = "ID"
        LookKategori.Properties.DisplayMember = "Nama"
        LookKategori.Properties.ShowClearButton = False
        LookKategori.Properties.PopulateViewColumns()

        LookKategori.Properties.PopupFormSize = New Size(200, 300)

        ViewKategori.Columns("ID").Visible = False

        For Each coll As DataColumn In xSet.Tables("DaftarKategori").Columns
            ViewKategori.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

    End Sub

    Private Function beforeSave() As Boolean
        If NamaLengkap.Text.Length < 3 Or Not NamaLengkap.Text.Contains(" ") Then
            MsgInfo("Nama Customer harus dalam 2 kata")
            Return False
        End If

        If KTP_NIK.Text.Length < 5 Or TmptLahir.Text.Length < 3 Or TglLahir.EditValue Is Nothing Or LiveAddress.Text.Length < 5 Or KTPAddress.Text.Length < 5 Or KTPKota.Text.Length < 3 Or KTPRtrw.Text.Length < 2 Or KTPKelurahan.Text.Length < 3 Or KTPKecamatan.Text.Length < 3 Or TelpHP.Text.Length < 3 Or TelpRumah.Text.Length < 3 Or Not Agama.SelectedIndex >= 0 Or Hobi.Text.Length < 3 Or LookKota.EditValue Is Nothing Or LookKategori.EditValue Is Nothing Then
            MsgWarning("Field bertanda bintang (*) tidak boleh ada yang kosong!")
            Return False
        End If

        If TmptLahir.Text.Contains("'") Or LiveAddress.Text.Contains("'") Or KTPAddress.Text.Contains("'") Or KTPKota.Text.Contains("'") Or
            KTPRtrw.Text.Contains("'") Or KTPKelurahan.Text.Contains("'") Or KTPKecamatan.Text.Contains("'") Or TelpHP.Text.Contains("'") Or
            TelpRumah.Text.Contains("'") Or Hobi.Text.Contains("'") Then
            MsgWarning("Huruf (') tidak diterima sebagai inputan")
            Return False
        End If

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        If NamaLengkap.Text <> nameCheck Or NIK_check = KTP_NIK.EditValue Or selmaster_id = 0 Then
            If Not isDoubleData(String.Format("SELECT idCust FROM mcustomer WHERE namacust='{0}' AND KTP_NIK='{1}' LIMIT 1", NamaLengkap.EditValue, KTP_NIK.EditValue)) Then
                MsgWarning(String.Format("Nama '{0}' dengan nomor KTP [{1}] sudah ada dalam daftar!", NamaLengkap.EditValue, KTP_NIK.EditValue))
                Return False
            End If
        End If

        Return True
    End Function
End Class