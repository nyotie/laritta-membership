﻿Imports DevExpress.Utils

Public Class MasterRegisterMember
    Dim kode_member, kode_kartu As String

    Private Sub MasterRegisterMember_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("DataMasterCustomer")
        xSet.Tables.Remove("DaftarTipeMembership")
        xSet.Tables.Remove("GetNewKodeMember")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub MasterRegisterMember_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()
        Get_KebutuhanDaftar()

        SQLquery = String.Format("SELECT KTP_NIK, namaCust, BirthDate, Email, IFNULL(SUM(b.NilaiTotalPesanan),0) TotalBeliOrder, IFNULL(SUM(b.NilaiTotalPembayaran),0) TotalLunasOrder, COUNT(a.idCust) xBeli FROM mcustomer a INNER JOIN set_pesanan b ON a.idCust=b.idCust INNER JOIN mtpesanan c ON b.idPesanan=c.idPesanan AND DATE(C.tglPesan) >= '2014-01-01' WHERE c.Void=0 AND a.idCust={0};", selmaster_id)
        ExDb.ExecQuery(1, SQLquery, xSet, "DataMasterCustomer")

        KTP_NIK.EditValue = xSet.Tables("DataMasterCustomer").Rows(0).Item("KTP_NIK")
        NamaLengkap.EditValue = xSet.Tables("DataMasterCustomer").Rows(0).Item("namaCust")
        TglLahir.EditValue = xSet.Tables("DataMasterCustomer").Rows(0).Item("BirthDate")
        Email.EditValue = xSet.Tables("DataMasterCustomer").Rows(0).Item("Email")
        xTrans.EditValue = xSet.Tables("DataMasterCustomer").Rows(0).Item("xBeli")
        OrderTotalBeli.EditValue = xSet.Tables("DataMasterCustomer").Rows(0).Item("TotalBeliOrder")
        OrderTotalLunas.EditValue = xSet.Tables("DataMasterCustomer").Rows(0).Item("TotalLunasOrder")
    End Sub

    Private Sub MasterRegisterMember_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        LoadSprite.CloseLoading()
        DaftarTipeMembership.Focus()

        If KTP_NIK.Text = "" Or TglLahir.Text = "" Then
            MsgWarning("Lengkapi data customer terlebih dahulu!")
            DaftarTipeMembership.Enabled = False
            KodeMember.EditValue = ""
            KodeKartu.EditValue = ""
        Else
            SQLquery = String.Format("SELECT COUNT(idCust) +1 NewKodeMember FROM mcustomer WHERE id_tipemembership IS NOT NULL AND BirthDate='{0}'", Format(TglLahir.EditValue, "yyyy-MM-dd"))
            ExDb.ExecQuery(1, SQLquery, xSet, "GetNewKodeMember")
            kode_member = String.Format("{0} {1}", Format(TglLahir.EditValue, "yyyyMMdd"), Format(xSet.Tables("GetNewKodeMember").Rows(0).Item("NewKodeMember"), "0000"))
            kode_kartu = Guid.NewGuid.ToString

            KodeMember.EditValue = kode_member
            KodeKartu.EditValue = kode_kartu
        End If
    End Sub

    Private Sub DaftarTipeMembership_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DaftarTipeMembership.EditValueChanged
        If DaftarTipeMembership.EditValue IsNot Nothing Then
            SyaratKartu.EditValue = ViewTipeMembership.GetFocusedRowCellValue("Syarat")
            HargaKartu.EditValue = ViewTipeMembership.GetFocusedRowCellValue("Harga")

            If (OrderTotalLunas.EditValue + OutletTotalLunas.EditValue) >= SyaratKartu.EditValue Then
                ButBeli.Enabled = False
                ButFreeMember.Enabled = True
            Else
                ButBeli.Enabled = True
                ButFreeMember.Enabled = False
            End If
        Else
            SyaratKartu.EditValue = 0
            HargaKartu.EditValue = 0

            KodeMember.Text = ""
            KodeKartu.Text = ""

            ButBeli.Enabled = False
            ButFreeMember.Enabled = False
        End If
        SyaratKartu.EditValue = ViewTipeMembership.GetFocusedRowCellValue("Syarat")
        HargaKartu.EditValue = ViewTipeMembership.GetFocusedRowCellValue("Harga")
    End Sub

    Private Sub OutletTotalLunas_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OutletTotalLunas.EditValueChanged
        If (OrderTotalLunas.EditValue + OutletTotalLunas.EditValue) >= SyaratKartu.EditValue Then
            ButBeli.Enabled = False
            ButFreeMember.Enabled = True
        Else
            ButBeli.Enabled = True
            ButFreeMember.Enabled = False
        End If
    End Sub

    Private Sub ButBeli_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBeli.Click
        Try
            If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
                Return
            End If

            UpdateMasterCustomer(2)

            MsgInfo("Data berhasil disimpan, tekan OK untuk melanjutkan.")
            BrowserMasterCustomer.isChange = True

        Catch ex As Exception
            MsgErrorDev()
        End Try
        LoadSprite.CloseLoading()
        Close()
    End Sub

    Private Sub ButUpgrade_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButFreeMember.Click
        Try
            If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
                Return
            End If

            UpdateMasterCustomer(1)

            MsgInfo("Data berhasil disimpan, tekan OK untuk melanjutkan.")
            BrowserMasterCustomer.isChange = True

        Catch ex As Exception
            MsgErrorDev()
        End Try
        LoadSprite.CloseLoading()
        Close()
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Sub Get_KebutuhanDaftar()
        SQLquery = "SELECT id_tipemembership ID, nama_tipemembership Nama, level_tipemembership Level, syarat_krt Syarat, harga_krt Harga FROM mbsm_tipemembership a WHERE a.Inactive=0;"
        ExDb.ExecQuery(1, SQLquery, xSet, "DaftarTipeMembership")
        DaftarTipeMembership.Properties.DataSource = xSet.Tables("DaftarTipeMembership").DefaultView

        DaftarTipeMembership.Properties.NullText = ""
        DaftarTipeMembership.Properties.ValueMember = "ID"
        DaftarTipeMembership.Properties.DisplayMember = "Nama"
        DaftarTipeMembership.Properties.ShowClearButton = False
        DaftarTipeMembership.Properties.PopulateViewColumns()

        ViewTipeMembership.Columns("ID").Visible = False
        ViewTipeMembership.Columns("Syarat").DisplayFormat.FormatType = FormatType.Numeric
        ViewTipeMembership.Columns("Syarat").DisplayFormat.FormatString = "Rp{0:n0}"
        ViewTipeMembership.Columns("Harga").DisplayFormat.FormatType = FormatType.Numeric
        ViewTipeMembership.Columns("Harga").DisplayFormat.FormatString = "Rp{0:n0}"

        For Each coll As DataColumn In xSet.Tables("DaftarTipeMembership").Columns
            ViewTipeMembership.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next
    End Sub

    Private Sub UpdateMasterCustomer(ByVal sumber As Integer)
        'sumber : 1-upgrade | 2-buy
        LoadSprite.ShowLoading("Baking cookies", "Register Member", Me)
        SQLquery = String.Format("CALL RegisterNewMember({0}, '{1}', {2}, '{3}', {4}, {5}, {6}, {7}, {8}, {9},{10});",
                                 selmaster_id,
                                 Format(TglLahir.EditValue, "yyyy-MM-dd"),
                                 DaftarTipeMembership.EditValue,
                                 kode_kartu,
                                 xTrans.EditValue,
                                 OrderTotalLunas.EditValue,
                                 OutletTotalLunas.EditValue,
                                 id_cabang,
                                 sumber,
                                 IIf(sumber = 1, HargaKartu.EditValue * -1, HargaKartu.EditValue),
                                 staff_id)
        ExDb.ExecData(1, SQLquery)
        LoadSprite.CloseLoading()

        LoadSprite.ShowLoading("Adding topping", "Register Member", Me)
        SQLquery = String.Format("INSERT INTO mbsm_membergifts(id_customer, id_gift, get_date, expired_date, availability) " & _
                    "SELECT {0} id_customer, a.id_gift, NOW(), CASE WHEN b.expired_day=0 THEN b.expired_date ELSE DATE_ADD(CURDATE(),INTERVAL b.expired_day DAY) END, b.repetisi_gift " & _
                    "FROM mbsm_tipemembershipgift a INNER JOIN mbsm_gift b ON a.id_gift=b.id_gift AND b.Inactive=0 WHERE a.id_tipemembership={1};", selmaster_id, DaftarTipeMembership.EditValue)
        ExDb.ExecData(1, SQLquery)
        LoadSprite.CloseLoading()

        'LoadSprite.ShowLoading("Put them to the plate", "Register Member", Me)
        'SQLquery = String.Format("INSERT INTO mbst_log(id_cabang, id_customer, event_date, event_id, event_value, creator) " & _
        '                        "VALUES ({0}, {1}, NOW(), {2}, {3}, {4});", id_cabang, selmaster_id, sumber, IIf(sumber = 1, HargaKartu.EditValue * -1, HargaKartu.EditValue), staff_id)
        'ExDb.ExecData(1, SQLquery)
    End Sub
End Class