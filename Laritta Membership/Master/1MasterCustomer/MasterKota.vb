﻿Public Class MasterKota

    Private Sub MasterKota_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("DataMasterKota")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub MasterKota_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        NamaKota.Focus()
        If selmaster_id = 0 Then
            CekInaktif.Properties.ReadOnly = True
            CekInaktif.Checked = False
        End If
        LoadSprite.CloseLoading()
    End Sub

    Private Sub MasterKota_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()

        If Not selmaster_id = 0 Then
            SQLquery = String.Format("SELECT nama_kota, Inactive FROM mbsm_lockota WHERE id_kota={0}", selmaster_id)
            ExDb.ExecQuery(1, SQLquery, xSet, "DataMasterKota")

            NamaKota.EditValue = xSet.Tables("DataMasterKota").Rows(0).Item("nama_kota")
            CekInaktif.Checked = xSet.Tables("DataMasterKota").Rows(0).Item("Inactive")

            nameCheck = NamaKota.EditValue
        End If
    End Sub

    Private Sub ButSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSimpan.Click
        If Not beforeSave() Then Return
        Try
            LoadSprite.ShowLoading()
            If selmaster_id = "0" Then
                sp_Query = "INSERT INTO mbsm_lockota(nama_kota, createby, createdate) "
                param_Query = String.Format("VALUES('{0}','{1}', NOW()) ", NamaKota.Text, staff_id)
            Else
                sp_Query = String.Format("UPDATE mbsm_lockota SET nama_kota='{0}', inactive={1}, updateBy={2}, updateDate=NOW() ", NamaKota.Text, CekInaktif.Checked, staff_id)
                param_Query = String.Format("WHERE id_kota={0}", selmaster_id)
            End If
            ExDb.ExecData(1, sp_Query & param_Query)
            MsgInfo("Data berhasil disimpan, tekan OK untuk melanjutkan.")
            BrowserMasterKota.isChange = True
        Catch ex As Exception
            MsgErrorDev()
        End Try
        LoadSprite.CloseLoading()
        Close()
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Function beforeSave() As Boolean
        If NamaKota.Text.Length < 3 Or NamaKota.Text.Contains("'") Then
            MsgInfo("Nama kota terlalu pendek atau mengandung tanda petik (')")
            Return False
        End If

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        If NamaKota.Text <> nameCheck Then
            If Not isDoubleData(String.Format("SELECT id_kota ID FROM mbsm_lockota WHERE nama_kota='{0}'", NamaKota.Text)) Then
                MsgWarning("Nama kota sudah ada dalam daftar!")
                Return False
            End If
        End If

        Return True
    End Function
End Class