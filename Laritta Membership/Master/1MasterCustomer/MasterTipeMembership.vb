﻿Imports DevExpress.Utils

Public Class MasterTipeMembership

    Private Sub MasterTipeMembership_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("DataMasterTipeMembership")
        xSet.Tables.Remove("DaftarGift")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub MasterTipeMembership_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()
        Get_KebutuhanDaftar()

        If Not selmaster_id = 0 Then
            SQLquery = String.Format("SELECT nama_tipemembership, level_tipemembership, syarat_krt, harga_krt, Inactive FROM mbsm_tipemembership WHERE id_tipemembership={0}", selmaster_id)
            ExDb.ExecQuery(1, SQLquery, xSet, "DataMasterTipeMembership")

            NamaTipe.EditValue = xSet.Tables("DataMasterTipeMembership").Rows(0).Item("nama_tipemembership")
            LevelTipe.EditValue = xSet.Tables("DataMasterTipeMembership").Rows(0).Item("level_tipemembership")
            SyaratBeli.EditValue = xSet.Tables("DataMasterTipeMembership").Rows(0).Item("syarat_krt")
            HargaKartu.EditValue = xSet.Tables("DataMasterTipeMembership").Rows(0).Item("harga_krt")
            CekInaktif.Checked = xSet.Tables("DataMasterTipeMembership").Rows(0).Item("Inactive")

            nameCheck = NamaTipe.EditValue
        End If
    End Sub

    Private Sub MasterTipeMembership_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        LoadSprite.CloseLoading()
        NamaTipe.Focus()
        If selmaster_id = 0 Then
            CekInaktif.Properties.ReadOnly = True
            CekInaktif.Checked = False
        End If
    End Sub

    Private Sub ButSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSimpan.Click
        If Not beforeSave() Then Return
        Try
            LoadSprite.ShowLoading("Menyimpan Informasi dasar", "Loading", Me)
            SQLquery = ""
            If selmaster_id = "0" Then
                SQLquery = String.Format("INSERT INTO mbsm_tipemembership(nama_tipemembership, level_tipemembership, syarat_krt, harga_krt, createby, createdate) VALUES('{0}', {1}, {2}, {3}, {4}, NOW()); SELECT id_tipemembership ID FROM mbsm_tipemembership WHERE nama_tipemembership='{0}';",
                                         NamaTipe.Text, LevelTipe.EditValue, SyaratBeli.EditValue, HargaKartu.EditValue, staff_id)
                ExDb.ExecQuery(1, SQLquery, xSet, "nameCheck")

                selmaster_id = xSet.Tables("nameCheck").Rows(0).Item(0)
                SQLquery = ""
            Else
                SQLquery = String.Format("UPDATE mbsm_tipemembership SET nama_tipemembership='{1}', level_tipemembership={2}, syarat_krt={3}, harga_krt={4}, inactive={5}, updateBy={6}, updateDate=NOW() WHERE id_tipemembership={0}; ",
                                         selmaster_id, NamaTipe.Text, LevelTipe.EditValue, SyaratBeli.EditValue, HargaKartu.EditValue, CekInaktif.Checked, staff_id)
                SQLquery &= String.Format("DELETE FROM mbsm_tipemembershipgift WHERE id_tipemembership={0}; ", selmaster_id)
                ExDb.ExecData(1, SQLquery)
            End If
            LoadSprite.CloseLoading()

            LoadSprite.ShowLoading("Menyimpan Keuntungan member", "Loading", Me)
            If xSet.Tables("DaftarGift").Select("isHave=True").Length > 0 Then
                SQLquery &= "INSERT INTO mbsm_tipemembershipgift VALUES"
                For Each row As DataRow In xSet.Tables("DaftarGift").Select("isHave = True")
                    SQLquery &= String.Format("({0}, {1}), ", selmaster_id, row.Item("ID"))
                Next
                SQLquery = Mid(SQLquery, 1, SQLquery.Length - 2) & "; "
                ExDb.ExecData(1, SQLquery)
            End If

            MsgInfo("Data berhasil disimpan, tekan OK untuk melanjutkan.")
            BrowserMasterTipeMembership.isChange = True
        Catch ex As Exception
            MsgErrorDev()
        End Try
        LoadSprite.CloseLoading()
        Close()
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Sub Get_KebutuhanDaftar()
        '--------------------------------Gift
        If Not xSet.Tables("DaftarGift") Is Nothing Then xSet.Tables("DaftarGift").Clear()
        SQLquery = String.Format("SELECT CASE WHEN b.id_tipemembership IS NOT NULL THEN 'True' ELSE 'False' END isHave, a.id_gift ID, a.nama_gift Nama, CASE WHEN a.expired_day=0 THEN DATE_FORMAT(expired_date, '%d %b %Y') ELSE CONCAT(a.expired_day, ' hari') END Expiring, a.repetisi_gift Repetisi, a.reward_value Nilai, a.syarat_none Syarat " & _
                    "FROM mbsm_gift a LEFT JOIN mbsm_tipemembershipgift b ON a.id_gift=b.id_gift AND b.id_tipemembership={0} " & _
                    "WHERE a.Inactive=0", selmaster_id)
        ExDb.ExecQuery(1, SQLquery, xSet, "DaftarGift")

        '----------------------------Grid Settings
        DaftarRewardGift.DataSource = xSet.Tables("DaftarGift").DefaultView

        For Each coll As DataColumn In xSet.Tables("DaftarGift").Columns
            ViewRewardGift.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        isHave.ValueChecked = "True"
        isHave.ValueUnchecked = "False"
        isHave.ValueGrayed = "False"

        DaftarRewardGift.RepositoryItems.Add(isHave)

        ViewRewardGift.Columns("isHave").ColumnEdit = isHave
        ViewRewardGift.Columns("ID").Visible = False
        ViewRewardGift.Columns("isHave").Caption = "..."
        ViewRewardGift.Columns("Nilai").DisplayFormat.FormatType = FormatType.Numeric
        ViewRewardGift.Columns("Nilai").DisplayFormat.FormatString = "Rp{0:n0}"

        ViewRewardGift.Columns("Nama").OptionsColumn.AllowEdit = False
        ViewRewardGift.Columns("Expiring").OptionsColumn.AllowEdit = False
        ViewRewardGift.Columns("Repetisi").OptionsColumn.AllowEdit = False
        ViewRewardGift.Columns("Nilai").OptionsColumn.AllowEdit = False
        ViewRewardGift.Columns("Syarat").OptionsColumn.AllowEdit = False

        ViewRewardGift.Columns("isHave").Width = 60
        ViewRewardGift.Columns("Nama").Width = 250
        ViewRewardGift.Columns("Expiring").Width = 150
        ViewRewardGift.Columns("Repetisi").Width = 100
        ViewRewardGift.Columns("Nilai").Width = 150
        ViewRewardGift.Columns("Syarat").Width = 80

        'ViewRewardGift.BestFitColumns()
    End Sub

    Private Function beforeSave() As Boolean
        If NamaTipe.Text.Length < 3 Or NamaTipe.Text.Contains("'") Then
            MsgInfo("Nama tipe membership terlalu pendek atau mengandung tanda petik (')")
            Return False
        End If

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        If NamaTipe.Text <> nameCheck Then
            If Not isDoubleData(String.Format("SELECT id_tipemembership ID FROM mbsm_tipemembership WHERE nama_tipemembership='{0}'", NamaTipe.Text)) Then
                MsgWarning("Nama tipe membership sudah ada dalam daftar!")
                Return False
            End If
        End If

        Return True
    End Function
End Class