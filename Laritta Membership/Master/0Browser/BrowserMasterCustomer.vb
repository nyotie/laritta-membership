﻿Imports DevExpress.Utils
Imports DevExpress.XtraBars

Public Class BrowserMasterCustomer
    Public isChange As Boolean

    Private Sub BrowserMaster_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("BMasterCustomer")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub BrowserMaster_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        '-----start setting user permissions
        Dim pRow As DataRow = xSet.Tables("User_ProgPermisisons").Select("permission_id=4")(0)
        butBaru.Enabled = IIf(MultiServer And Not OnlineStatus, False, pRow.Item("allow_new"))
        butKoreksi.Enabled = pRow.Item("allow_edit")
        butVoid.Enabled = pRow.Item("allow_void")
        ButExport.Enabled = pRow.Item("allow_print")
        ButRegisterMember.Enabled = xSet.Tables("User_ProgPermisisons").Select("permission_id=5")(0).Item("allow_read")
        '-----end of setting user permissions

        butBaru.Focus()
    End Sub

    Private Sub BrowserMaster_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        isChange = True
        'refreshingGrid()

        PanelCustomerProperties.Location = New Point(GridControl1.Width / 2 - 155 + GridControl1.Location.X, GridControl1.Height / 2 - 95 + GridControl1.Location.Y)
        AddHandler GridView1.DoubleClick, AddressOf butKoreksi_Click
    End Sub

    Private Sub butBaru_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butBaru.Click
        selmaster_id = 0

        ShowModule(MasterCustomer, "Master Customer")
        MasterCustomer = Nothing

        refreshingGrid()
    End Sub

    Private Sub butKoreksi_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butKoreksi.Click
        If GridView1.RowCount < 1 Or butKoreksi.Visible = False Then Return
        selmaster_id = GridView1.GetFocusedRowCellValue("ID")
        If selmaster_id = 0 Then Return

        ShowModule(MasterCustomer, "Master Customer")
        MasterCustomer = Nothing

        refreshingGrid()
    End Sub

    Private Sub butVoid_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butVoid.Click
        If Not GridView1.RowCount > 0 Then Return

        Dim QuestionString As String = String.Format("Yakin ingin mengubah status {0} menjadi {1}?", GridView1.GetFocusedRowCellValue("Nama"), IIf(GridView1.GetFocusedRowCellValue("Status") = True, "Aktif", "Tidak Aktif"))
        If MsgBox(QuestionString, MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then Return

        SQLquery = String.Format("UPDATE mcustomer SET Inactive={1}, updateBy={2}, updateDate=NOW() WHERE idCust={0}; ", GridView1.GetFocusedRowCellValue("ID"), IIf(GridView1.GetFocusedRowCellValue("Status") = True, 0, 1), staff_id)
        ExDb.ExecData(1, SQLquery)

        MsgInfo("Data berhasil disimpan, tekan OK untuk melanjutkan.")
        isChange = True
        refreshingGrid()
    End Sub

    Private Sub butRefresh_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butRefresh.Click
        isChange = True
        refreshingGrid()
    End Sub

    Private Sub ButRegisterMember_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButRegisterMember.Click
        If GridView1.RowCount < 1 Or butKoreksi.Visible = False Then Return
        If GridView1.GetFocusedRowCellValue("Member") = 1 Or GridView1.GetFocusedRowCellValue("Status") = True Then Return
        selmaster_id = GridView1.GetFocusedRowCellValue("ID")

        ShowModule(MasterRegisterMember, "Register Membership")
        MasterRegisterMember = Nothing

        refreshingGrid()
    End Sub

    Private Sub ButExport_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButExport.Click
        PopupMenu1.ShowPopup(Control.MousePosition)
    End Sub

    Private Sub ExportPDF_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles ExportPDF.ItemClick
        Using saveFileDialog1 As New SaveFileDialog() With {.Filter = "PDF Files|*.pdf", .Title = "Save a PDF File"}
            saveFileDialog1.ShowDialog()
            If saveFileDialog1.FileName <> "" Then
                GridView1.OptionsPrint.ExpandAllDetails = True
                GridView1.ExportToPdf(saveFileDialog1.FileName)
                MsgInfo("Export file success")
            End If
        End Using
    End Sub

    Private Sub ExportXLS_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles ExportXLS.ItemClick
        Using saveFileDialog1 As New SaveFileDialog() With {.Filter = "XLS Files|*.xls", .Title = "Save a XLS File"}
            saveFileDialog1.ShowDialog()
            If saveFileDialog1.FileName <> "" Then
                GridView1.OptionsPrint.ExpandAllDetails = True
                GridView1.ExportToXls(saveFileDialog1.FileName)
                MsgInfo("Export file success")
            End If
        End Using
    End Sub

    Private Sub ExportXLSX_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles ExportXLSX.ItemClick
        Using saveFileDialog1 As New SaveFileDialog() With {.Filter = "XLSX Files|*.xlsx", .Title = "Save a XLSX File"}
            saveFileDialog1.ShowDialog()
            If saveFileDialog1.FileName <> "" Then
                GridView1.OptionsPrint.ExpandAllDetails = True
                GridView1.ExportToXlsx(saveFileDialog1.FileName)
                MsgInfo("Export file success")
            End If
        End Using
    End Sub

    Private Sub butClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butClose.Click
        Close()
    End Sub

    Private Sub refreshingGrid()
        Dim idxRow As Integer = GridView1.FocusedRowHandle

        If isChange = True Then isChange = False Else Return

        '----------------------------Query
        If Not xSet.Tables("BMasterCustomer") Is Nothing Then xSet.Tables("BMasterCustomer").Clear()
        SQLquery = "SELECT idCust ID, b.namaKategoriCust Kategori, a.namaCust Nama, a.BirthDate Ultah, a.homeAddress Alamat, a.Phone Telp, CASE WHEN a.id_tipemembership IS NULL THEN False ELSE True END AS Member, a.Inactive Status FROM mcustomer a INNER JOIN mkategoricust b ON a.idKategoriCust=b.idKategoriCust "
        If butCheckShowAllData.Checked = False And Not SearchKeyword.Text = "" Then
            SQLquery += String.Format("WHERE (b.namaKategoriCust LIKE '%{0}%' OR a.namaCust LIKE '%{0}%' OR a.homeAddress LIKE '%{0}%' OR a.Phone LIKE '%{0}%') ", SearchKeyword.Text)
        ElseIf butCheckShowAllData.Checked = False And SearchKeyword.Text = "" Then
            butCheckShowAllData.Checked = True
        End If

        If butCheckShowAllData.Checked = True And butCheckShowInactive.Checked = False Then
            SQLquery += "WHERE a.inactive=0 "
        ElseIf butCheckShowAllData.Checked = False And butCheckShowInactive.Checked = False Then
            SQLquery += "AND a.inactive=0 "
        End If
        SQLquery += "ORDER BY b.namaKategoriCust, a.namaCust"
        ExDb.ExecQuery(1, SQLquery, xSet, "BMasterCustomer")

        '----------------------------Grid Settings
        GridControl1.DataSource = xSet.Tables("BMasterCustomer").DefaultView

        For Each coll As DataColumn In xSet.Tables("BMasterCustomer").Columns
            GridView1.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next
        GridView1.Columns("ID").Visible = False

        GridView1.Columns("Member").ColumnEdit = ColumnCheckEdit
        GridView1.Columns("Ultah").DisplayFormat.FormatType = FormatType.DateTime
        GridView1.Columns("Ultah").DisplayFormat.FormatString = "dd MMM yyyy"

        GridView1.Columns("Nama").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count
        GridView1.Columns("Nama").SummaryItem.DisplayFormat = "Jumlah:{0:n0}"

        GridView1.OptionsView.ColumnAutoWidth = False

        GridView1.Columns("Kategori").Width = 150
        GridView1.Columns("Nama").Width = 200
        GridView1.Columns("Ultah").Width = 100
        GridView1.Columns("Alamat").Width = 600
        GridView1.Columns("Telp").Width = 200
        GridView1.Columns("Member").Width = 80
        GridView1.Columns("Status").Width = 80

        GridView1.FocusedRowHandle = idxRow

        For Each coll As DataColumn In xSet.Tables("BMasterCustomer").Columns
            GridView1.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next
    End Sub

    Private Sub GridView1_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles GridView1.MouseDown
        If e.Button = Windows.Forms.MouseButtons.Right Then
            Dim hi As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo = GridView1.CalcHitInfo(e.Location)
            If hi.InRow Then
                PopupMenu2.ShowPopup(Cursor.Position)
            End If
        End If
    End Sub

    Private Sub CustomerProperties_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles CustomerProperties.ItemClick
        If GridView1.RowCount < 1 Then Return
        selmaster_id = GridView1.GetFocusedRowCellValue("ID")
        If selmaster_id = 0 Then Return

        If Not xSet.Tables("CustProperties") Is Nothing Then xSet.Tables("CustProperties").Clear()
        SQLquery = "SELECT b.Nama AS Creator, c.Nama AS Editor, a.updateDate Tanggal FROM mcustomer a INNER JOIN mstaff b ON a.createBy=b.idStaff LEFT JOIN mstaff c ON a.updateBy=c.idStaff WHERE idCust=" & selmaster_id
        ExDb.ExecQuery(1, SQLquery, xSet, "CustProperties")

        If xSet.Tables("CustProperties").Rows.Count > 0 Then
            NamaCreator.Text = xSet.Tables("CustProperties").Rows(0).Item(0).ToString
            NamaEditor.Text = xSet.Tables("CustProperties").Rows(0).Item(1).ToString
            TanggalEdit.Text = xSet.Tables("CustProperties").Rows(0).Item(2)

            PanelCustomerProperties.Visible = True
            GridControl1.Enabled = False
            PanelControl1.Enabled = False
            ButCloseProperties.Focus()
        End If
    End Sub

    Private Sub ButCloseProperties_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButCloseProperties.Click
        NamaCreator.Text = ""
        NamaEditor.Text = ""
        TanggalEdit.Text = ""

        PanelCustomerProperties.Visible = False
        GridControl1.Enabled = True
        PanelControl1.Enabled = True
    End Sub

    Private Sub butCheckShowAllData_CheckedChanged(sender As Object, e As EventArgs) Handles butCheckShowAllData.CheckedChanged
        If butCheckShowAllData.Checked = True Then
            SearchKeyword.Text = ""
            SearchKeyword.Properties.ReadOnly = True
        Else
            SearchKeyword.Properties.ReadOnly = False
        End If
    End Sub
End Class