﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrowserMasterCustomer
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.butBaru = New DevExpress.XtraEditors.SimpleButton()
        Me.butKoreksi = New DevExpress.XtraEditors.SimpleButton()
        Me.ButRegisterMember = New DevExpress.XtraEditors.SimpleButton()
        Me.ButExport = New DevExpress.XtraEditors.SimpleButton()
        Me.butClose = New DevExpress.XtraEditors.SimpleButton()
        Me.butVoid = New DevExpress.XtraEditors.SimpleButton()
        Me.butCheckShowInactive = New DevExpress.XtraEditors.CheckEdit()
        Me.butRefresh = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.ColumnCheckEdit = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.BarManager1 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.PopupMenu1 = New DevExpress.XtraBars.PopupMenu(Me.components)
        Me.ExportPDF = New DevExpress.XtraBars.BarButtonItem()
        Me.ExportXLS = New DevExpress.XtraBars.BarButtonItem()
        Me.ExportXLSX = New DevExpress.XtraBars.BarButtonItem()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.CustomerProperties = New DevExpress.XtraBars.BarButtonItem()
        Me.PopupMenu2 = New DevExpress.XtraBars.PopupMenu(Me.components)
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.TanggalEdit = New DevExpress.XtraEditors.TextEdit()
        Me.NamaEditor = New DevExpress.XtraEditors.TextEdit()
        Me.NamaCreator = New DevExpress.XtraEditors.TextEdit()
        Me.ButCloseProperties = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelCustomerProperties = New DevExpress.XtraEditors.GroupControl()
        Me.PanelControl3 = New DevExpress.XtraEditors.PanelControl()
        Me.SearchKeyword = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.butCheckShowAllData = New DevExpress.XtraEditors.CheckEdit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.butCheckShowInactive.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ColumnCheckEdit, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupMenu1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupMenu2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.TanggalEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NamaEditor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NamaCreator.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelCustomerProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelCustomerProperties.SuspendLayout()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl3.SuspendLayout()
        CType(Me.SearchKeyword.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.butCheckShowAllData.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelControl1.Controls.Add(Me.butBaru)
        Me.PanelControl1.Controls.Add(Me.butKoreksi)
        Me.PanelControl1.Controls.Add(Me.ButRegisterMember)
        Me.PanelControl1.Controls.Add(Me.ButExport)
        Me.PanelControl1.Controls.Add(Me.butClose)
        Me.PanelControl1.Controls.Add(Me.butVoid)
        Me.PanelControl1.Location = New System.Drawing.Point(12, 347)
        Me.PanelControl1.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(966, 53)
        Me.PanelControl1.TabIndex = 2
        '
        'butBaru
        '
        Me.butBaru.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butBaru.Appearance.Options.UseFont = True
        Me.butBaru.Image = Global.Laritta_Membership.My.Resources.Resources.new_32
        Me.butBaru.Location = New System.Drawing.Point(6, 6)
        Me.butBaru.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.butBaru.Name = "butBaru"
        Me.butBaru.Size = New System.Drawing.Size(100, 41)
        Me.butBaru.TabIndex = 0
        Me.butBaru.Text = "Baru"
        '
        'butKoreksi
        '
        Me.butKoreksi.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butKoreksi.Appearance.Options.UseFont = True
        Me.butKoreksi.Image = Global.Laritta_Membership.My.Resources.Resources.edit_32
        Me.butKoreksi.Location = New System.Drawing.Point(114, 5)
        Me.butKoreksi.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.butKoreksi.Name = "butKoreksi"
        Me.butKoreksi.Size = New System.Drawing.Size(100, 41)
        Me.butKoreksi.TabIndex = 1
        Me.butKoreksi.Text = "Koreksi"
        '
        'ButRegisterMember
        '
        Me.ButRegisterMember.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButRegisterMember.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButRegisterMember.Appearance.Options.UseFont = True
        Me.ButRegisterMember.Image = Global.Laritta_Membership.My.Resources.Resources.member_32
        Me.ButRegisterMember.Location = New System.Drawing.Point(656, 6)
        Me.ButRegisterMember.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.ButRegisterMember.Name = "ButRegisterMember"
        Me.ButRegisterMember.Size = New System.Drawing.Size(108, 40)
        Me.ButRegisterMember.TabIndex = 3
        Me.ButRegisterMember.Text = "Register" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Member"
        '
        'ButExport
        '
        Me.ButExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButExport.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButExport.Appearance.Options.UseFont = True
        Me.ButExport.Image = Global.Laritta_Membership.My.Resources.Resources.export_32
        Me.ButExport.Location = New System.Drawing.Point(772, 6)
        Me.ButExport.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.ButExport.Name = "ButExport"
        Me.BarManager1.SetPopupContextMenu(Me.ButExport, Me.PopupMenu1)
        Me.ButExport.Size = New System.Drawing.Size(90, 40)
        Me.ButExport.TabIndex = 4
        Me.ButExport.Text = "Export"
        '
        'butClose
        '
        Me.butClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.butClose.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butClose.Appearance.Options.UseFont = True
        Me.butClose.Image = Global.Laritta_Membership.My.Resources.Resources.close_32
        Me.butClose.Location = New System.Drawing.Point(870, 6)
        Me.butClose.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.butClose.Name = "butClose"
        Me.butClose.Size = New System.Drawing.Size(90, 40)
        Me.butClose.TabIndex = 5
        Me.butClose.Text = "Tutup"
        '
        'butVoid
        '
        Me.butVoid.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butVoid.Appearance.Options.UseFont = True
        Me.butVoid.Image = Global.Laritta_Membership.My.Resources.Resources.nonaktif_32
        Me.butVoid.Location = New System.Drawing.Point(222, 6)
        Me.butVoid.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.butVoid.Name = "butVoid"
        Me.butVoid.Size = New System.Drawing.Size(128, 41)
        Me.butVoid.TabIndex = 2
        Me.butVoid.Text = "Non/Aktifkan"
        '
        'butCheckShowInactive
        '
        Me.butCheckShowInactive.Location = New System.Drawing.Point(6, 54)
        Me.butCheckShowInactive.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.butCheckShowInactive.Name = "butCheckShowInactive"
        Me.butCheckShowInactive.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butCheckShowInactive.Properties.Appearance.Options.UseFont = True
        Me.butCheckShowInactive.Properties.Caption = "Tampilkan juga data tidak aktif"
        Me.butCheckShowInactive.Size = New System.Drawing.Size(400, 19)
        Me.butCheckShowInactive.TabIndex = 3
        '
        'butRefresh
        '
        Me.butRefresh.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butRefresh.Appearance.Options.UseFont = True
        Me.butRefresh.Image = Global.Laritta_Membership.My.Resources.Resources.refresh_32
        Me.butRefresh.Location = New System.Drawing.Point(413, 5)
        Me.butRefresh.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.butRefresh.Name = "butRefresh"
        Me.butRefresh.Size = New System.Drawing.Size(124, 68)
        Me.butRefresh.TabIndex = 4
        Me.butRefresh.Text = "Refresh"
        '
        'GridControl1
        '
        Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl1.Location = New System.Drawing.Point(12, 102)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.ColumnCheckEdit})
        Me.GridControl1.Size = New System.Drawing.Size(967, 239)
        Me.GridControl1.TabIndex = 1
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Appearance.HeaderPanel.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridView1.Appearance.HeaderPanel.Options.UseFont = True
        Me.GridView1.Appearance.Row.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridView1.Appearance.Row.Options.UseFont = True
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsView.EnableAppearanceEvenRow = True
        Me.GridView1.OptionsView.ShowAutoFilterRow = True
        Me.GridView1.OptionsView.ShowFooter = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'ColumnCheckEdit
        '
        Me.ColumnCheckEdit.AutoHeight = False
        Me.ColumnCheckEdit.Name = "ColumnCheckEdit"
        Me.ColumnCheckEdit.ValueChecked = 1
        Me.ColumnCheckEdit.ValueUnchecked = 0
        '
        'BarManager1
        '
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.ExportPDF, Me.ExportXLS, Me.ExportXLSX, Me.CustomerProperties})
        Me.BarManager1.MaxItemId = 5
        '
        'PopupMenu1
        '
        Me.PopupMenu1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.ExportPDF), New DevExpress.XtraBars.LinkPersistInfo(Me.ExportXLS), New DevExpress.XtraBars.LinkPersistInfo(Me.ExportXLSX)})
        Me.PopupMenu1.Manager = Me.BarManager1
        Me.PopupMenu1.Name = "PopupMenu1"
        '
        'ExportPDF
        '
        Me.ExportPDF.Caption = "Export to PDF"
        Me.ExportPDF.Id = 0
        Me.ExportPDF.Name = "ExportPDF"
        '
        'ExportXLS
        '
        Me.ExportXLS.Caption = "Export to XLS"
        Me.ExportXLS.Id = 1
        Me.ExportXLS.Name = "ExportXLS"
        '
        'ExportXLSX
        '
        Me.ExportXLSX.Caption = "Export to XLSX"
        Me.ExportXLSX.Id = 2
        Me.ExportXLSX.Name = "ExportXLSX"
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Size = New System.Drawing.Size(991, 0)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 412)
        Me.barDockControlBottom.Size = New System.Drawing.Size(991, 0)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 412)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(991, 0)
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 412)
        '
        'CustomerProperties
        '
        Me.CustomerProperties.Caption = "Properties"
        Me.CustomerProperties.Id = 4
        Me.CustomerProperties.Name = "CustomerProperties"
        '
        'PopupMenu2
        '
        Me.PopupMenu2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.CustomerProperties)})
        Me.PopupMenu2.Manager = Me.BarManager1
        Me.PopupMenu2.Name = "PopupMenu2"
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.LabelControl3)
        Me.PanelControl2.Controls.Add(Me.LabelControl2)
        Me.PanelControl2.Controls.Add(Me.LabelControl1)
        Me.PanelControl2.Controls.Add(Me.TanggalEdit)
        Me.PanelControl2.Controls.Add(Me.NamaEditor)
        Me.PanelControl2.Controls.Add(Me.NamaCreator)
        Me.PanelControl2.Location = New System.Drawing.Point(5, 25)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(298, 115)
        Me.PanelControl2.TabIndex = 0
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl3.Location = New System.Drawing.Point(21, 74)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(50, 14)
        Me.LabelControl3.TabIndex = 1
        Me.LabelControl3.Text = "Tanggal"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl2.Location = New System.Drawing.Point(21, 47)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(105, 14)
        Me.LabelControl2.TabIndex = 1
        Me.LabelControl2.Text = "Pengoreksi akhir"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl1.Location = New System.Drawing.Point(21, 21)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(56, 14)
        Me.LabelControl1.TabIndex = 1
        Me.LabelControl1.Text = "Pembuat"
        '
        'TanggalEdit
        '
        Me.TanggalEdit.Location = New System.Drawing.Point(136, 71)
        Me.TanggalEdit.Name = "TanggalEdit"
        Me.TanggalEdit.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.TanggalEdit.Properties.Appearance.Options.UseFont = True
        Me.TanggalEdit.Properties.Mask.EditMask = "dd MMM yyyy, HH:mm"
        Me.TanggalEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TanggalEdit.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TanggalEdit.Properties.ReadOnly = True
        Me.TanggalEdit.Size = New System.Drawing.Size(149, 21)
        Me.TanggalEdit.TabIndex = 2
        Me.TanggalEdit.TabStop = False
        '
        'NamaEditor
        '
        Me.NamaEditor.Location = New System.Drawing.Point(136, 44)
        Me.NamaEditor.Name = "NamaEditor"
        Me.NamaEditor.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.NamaEditor.Properties.Appearance.Options.UseFont = True
        Me.NamaEditor.Properties.ReadOnly = True
        Me.NamaEditor.Size = New System.Drawing.Size(149, 21)
        Me.NamaEditor.TabIndex = 1
        Me.NamaEditor.TabStop = False
        '
        'NamaCreator
        '
        Me.NamaCreator.Location = New System.Drawing.Point(136, 17)
        Me.NamaCreator.MenuManager = Me.BarManager1
        Me.NamaCreator.Name = "NamaCreator"
        Me.NamaCreator.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.NamaCreator.Properties.Appearance.Options.UseFont = True
        Me.NamaCreator.Properties.ReadOnly = True
        Me.NamaCreator.Size = New System.Drawing.Size(149, 21)
        Me.NamaCreator.TabIndex = 0
        Me.NamaCreator.TabStop = False
        '
        'ButCloseProperties
        '
        Me.ButCloseProperties.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButCloseProperties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButCloseProperties.Appearance.Options.UseFont = True
        Me.ButCloseProperties.Image = Global.Laritta_Membership.My.Resources.Resources.close_32
        Me.ButCloseProperties.Location = New System.Drawing.Point(214, 145)
        Me.ButCloseProperties.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.ButCloseProperties.Name = "ButCloseProperties"
        Me.ButCloseProperties.Size = New System.Drawing.Size(90, 40)
        Me.ButCloseProperties.TabIndex = 1
        Me.ButCloseProperties.Text = "Tutup"
        '
        'PanelCustomerProperties
        '
        Me.PanelCustomerProperties.AppearanceCaption.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.PanelCustomerProperties.AppearanceCaption.Options.UseFont = True
        Me.PanelCustomerProperties.Controls.Add(Me.ButCloseProperties)
        Me.PanelCustomerProperties.Controls.Add(Me.PanelControl2)
        Me.PanelCustomerProperties.Location = New System.Drawing.Point(340, 126)
        Me.PanelCustomerProperties.Name = "PanelCustomerProperties"
        Me.PanelCustomerProperties.Size = New System.Drawing.Size(310, 190)
        Me.PanelCustomerProperties.TabIndex = 3
        Me.PanelCustomerProperties.Text = "Properties"
        Me.PanelCustomerProperties.Visible = False
        '
        'PanelControl3
        '
        Me.PanelControl3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelControl3.Controls.Add(Me.SearchKeyword)
        Me.PanelControl3.Controls.Add(Me.LabelControl4)
        Me.PanelControl3.Controls.Add(Me.butCheckShowAllData)
        Me.PanelControl3.Controls.Add(Me.butCheckShowInactive)
        Me.PanelControl3.Controls.Add(Me.butRefresh)
        Me.PanelControl3.Location = New System.Drawing.Point(12, 12)
        Me.PanelControl3.Name = "PanelControl3"
        Me.PanelControl3.Size = New System.Drawing.Size(967, 84)
        Me.PanelControl3.TabIndex = 0
        '
        'SearchKeyword
        '
        Me.SearchKeyword.Location = New System.Drawing.Point(89, 27)
        Me.SearchKeyword.MenuManager = Me.BarManager1
        Me.SearchKeyword.Name = "SearchKeyword"
        Me.SearchKeyword.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.SearchKeyword.Properties.Appearance.Options.UseFont = True
        Me.SearchKeyword.Size = New System.Drawing.Size(317, 21)
        Me.SearchKeyword.TabIndex = 2
        Me.SearchKeyword.TabStop = False
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl4.Location = New System.Drawing.Point(28, 30)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(55, 14)
        Me.LabelControl4.TabIndex = 1
        Me.LabelControl4.Text = "Keyword"
        '
        'butCheckShowAllData
        '
        Me.butCheckShowAllData.Location = New System.Drawing.Point(6, 5)
        Me.butCheckShowAllData.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.butCheckShowAllData.Name = "butCheckShowAllData"
        Me.butCheckShowAllData.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butCheckShowAllData.Properties.Appearance.Options.UseFont = True
        Me.butCheckShowAllData.Properties.Caption = "Tampilkan semua data"
        Me.butCheckShowAllData.Size = New System.Drawing.Size(400, 19)
        Me.butCheckShowAllData.TabIndex = 0
        '
        'BrowserMasterCustomer
        '
        Me.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(991, 412)
        Me.Controls.Add(Me.PanelControl3)
        Me.Controls.Add(Me.PanelCustomerProperties)
        Me.Controls.Add(Me.GridControl1)
        Me.Controls.Add(Me.PanelControl1)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "BrowserMasterCustomer"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Master"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.butCheckShowInactive.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ColumnCheckEdit, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupMenu1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupMenu2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        CType(Me.TanggalEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NamaEditor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NamaCreator.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelCustomerProperties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelCustomerProperties.ResumeLayout(False)
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl3.ResumeLayout(False)
        Me.PanelControl3.PerformLayout()
        CType(Me.SearchKeyword.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.butCheckShowAllData.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents butCheckShowInactive As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents butBaru As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butKoreksi As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butClose As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butVoid As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butRefresh As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents ButExport As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents ExportPDF As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ExportXLS As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ExportXLSX As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents PopupMenu1 As DevExpress.XtraBars.PopupMenu
    Friend WithEvents ColumnCheckEdit As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents ButRegisterMember As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents CustomerProperties As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents PopupMenu2 As DevExpress.XtraBars.PopupMenu
    Friend WithEvents PanelCustomerProperties As DevExpress.XtraEditors.GroupControl
    Friend WithEvents ButCloseProperties As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TanggalEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents NamaEditor As DevExpress.XtraEditors.TextEdit
    Friend WithEvents NamaCreator As DevExpress.XtraEditors.TextEdit
    Friend WithEvents PanelControl3 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents butCheckShowAllData As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents SearchKeyword As DevExpress.XtraEditors.TextEdit
End Class
