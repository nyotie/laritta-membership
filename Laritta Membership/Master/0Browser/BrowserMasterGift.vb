﻿Imports DevExpress.Utils
Imports DevExpress.XtraBars

Public Class BrowserMasterGift
    Public isChange As Boolean

    Private Sub BrowserMasterGift_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("BMasterGift")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub BrowserMasterGift_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        '-----start setting user permissions
        Dim drow As DataRow = xSet.Tables("User_ProgPermisisons").Select("permission_id=9")(0)
        butBaru.Enabled = drow.Item("allow_new")
        butKoreksi.Enabled = drow.Item("allow_edit")
        butVoid.Enabled = drow.Item("allow_void")
        ButExport.Enabled = drow.Item("allow_print")
        '-----end of setting user permissions

        butBaru.Focus()
    End Sub

    Private Sub BrowserMasterGift_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        isChange = True
        refreshingGrid()

        AddHandler GridView1.DoubleClick, AddressOf butKoreksi_Click
    End Sub

    Private Sub butBaru_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butBaru.Click
        selmaster_id = 0

        ShowModule(MasterGift, "Master Gift")
        MasterGift = Nothing

        refreshingGrid()
    End Sub

    Private Sub butKoreksi_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butKoreksi.Click
        If Not GridView1.RowCount > 0 And butKoreksi.Visible = True Then Return
        selmaster_id = GridView1.GetFocusedRowCellValue("ID")
        If selmaster_id = 0 Then Return

        ShowModule(MasterGift, "Master Gift")
        MasterGift = Nothing

        refreshingGrid()
    End Sub

    Private Sub butVoid_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butVoid.Click
        If Not GridView1.RowCount > 0 Then Return

        Dim QuestionString As String = String.Format("Yakin ingin mengubah status {0} menjadi {1}?", GridView1.GetFocusedRowCellValue("Nama"), IIf(GridView1.GetFocusedRowCellValue("Status") = True, "Aktif", "Tidak Aktif"))
        If MsgBox(QuestionString, MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then Return

        SQLquery = String.Format("UPDATE mbsm_gift SET Inactive={1}, updateBy={2}, updateDate=NOW() WHERE id_gift={0}; ", GridView1.GetFocusedRowCellValue("ID"), IIf(GridView1.GetFocusedRowCellValue("Status") = True, 0, 1), staff_id)
        ExDb.ExecData(1, SQLquery)

        MsgInfo("Data berhasil disimpan, tekan OK untuk melanjutkan.")
        isChange = True
        refreshingGrid()
    End Sub

    Private Sub butRefresh_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butRefresh.Click
        isChange = True
        refreshingGrid()
    End Sub

    Private Sub ButExport_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButExport.Click
        PopupMenu1.ShowPopup(Control.MousePosition)
    End Sub

    Private Sub ExportPDF_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles ExportPDF.ItemClick
        Using saveFileDialog1 As New SaveFileDialog() With {.Filter = "PDF Files|*.pdf", .Title = "Save a PDF File"}
            saveFileDialog1.ShowDialog()
            If saveFileDialog1.FileName <> "" Then
                GridView1.OptionsPrint.ExpandAllDetails = True
                GridView1.ExportToPdf(saveFileDialog1.FileName)
                MsgInfo("Export file success")
            End If
        End Using
    End Sub

    Private Sub ExportXLS_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles ExportXLS.ItemClick
        Using saveFileDialog1 As New SaveFileDialog() With {.Filter = "XLS Files|*.xls", .Title = "Save a XLS File"}
            saveFileDialog1.ShowDialog()
            If saveFileDialog1.FileName <> "" Then
                GridView1.OptionsPrint.ExpandAllDetails = True
                GridView1.ExportToXls(saveFileDialog1.FileName)
                MsgInfo("Export file success")
            End If
        End Using
    End Sub

    Private Sub ExportXLSX_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles ExportXLSX.ItemClick
        Using saveFileDialog1 As New SaveFileDialog() With {.Filter = "XLSX Files|*.xlsx", .Title = "Save a XLSX File"}
            saveFileDialog1.ShowDialog()
            If saveFileDialog1.FileName <> "" Then
                GridView1.OptionsPrint.ExpandAllDetails = True
                GridView1.ExportToXlsx(saveFileDialog1.FileName)
                MsgInfo("Export file success")
            End If
        End Using
    End Sub

    Private Sub butClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butClose.Click
        Close()
    End Sub

    Private Sub reset_buttons()
        butBaru.Text = "Baru"
        butKoreksi.Text = "Koreksi"
        butVoid.Text = "Non/Aktifkan"

        If MultiServer = True And OnlineStatus = False Then
            butBaru.Enabled = False
        Else
            butBaru.Enabled = True
        End If

        butKoreksi.Enabled = True
        butVoid.Enabled = True
    End Sub

    Private Sub refreshingGrid()
        reset_buttons()
        Dim idxRow As Integer = GridView1.FocusedRowHandle

        If isChange = True Then isChange = False Else Return

        '----------------------------Query
        If Not xSet.Tables("BMasterGift") Is Nothing Then xSet.Tables("BMasterGift").Clear()
        SQLquery = "SELECT id_gift ID, nama_gift Nama, expired_day 'Hari expired', expired_date 'Tgl expired', repetisi_gift Repetisi, syarat_none 'Tanpa syarat', use_at_outlet Outlet, use_at_order 'Order', inactive Status FROM mbsm_gift a "
        If butCheckShow.Checked = False Then SQLquery += "WHERE a.inactive=0 "
        SQLquery += "ORDER BY nama_gift"
        ExDb.ExecQuery(1, SQLquery, xSet, "BMasterGift")

        '----------------------------Grid Settings
        GridControl1.DataSource = xSet.Tables("BMasterGift").DefaultView

        For Each coll As DataColumn In xSet.Tables("BMasterGift").Columns
            GridView1.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next
        GridView1.Columns("ID").Visible = False
        'GridView1.Columns("use_at_order").Caption = "Order"
        GridView1.Columns("Tgl expired").DisplayFormat.FormatType = FormatType.DateTime
        GridView1.Columns("Tgl expired").DisplayFormat.FormatString = "dd MMM yyyy"
        GridView1.Columns("Nama").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count
        GridView1.Columns("Nama").SummaryItem.DisplayFormat = "Jumlah:{0:n0}"
        GridView1.BestFitColumns()
        GridView1.FocusedRowHandle = idxRow
    End Sub
End Class