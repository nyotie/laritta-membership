﻿Imports DevExpress.Utils
Imports DevExpress.XtraBars

Public Class BrowserMasterPoint
    Public isChange, isFirstTime As Boolean

    Private Sub BrowserMasterPoint_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("BMasterPoint")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub BrowserMasterPoint_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        '-----start setting user permissions
        Dim drow As DataRow = xSet.Tables("User_ProgPermisisons").Select("permission_id=7")(0)
        butBaru.Enabled = drow.Item("allow_new")
        butKoreksi.Enabled = drow.Item("allow_edit")
        'butVoid.Enabled = drow.Item("allow_void")
        ButExport.Enabled = drow.Item("allow_print")
        '-----end of setting user permissions

        butBaru.Focus()
    End Sub

    Private Sub BrowserMasterPoint_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        isChange = True
        refreshingGrid()

        AddHandler GridView1.DoubleClick, AddressOf butKoreksi_Click
    End Sub

    Private Sub butBaru_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butBaru.Click
        selmaster_id = 0

        If GridView1.RowCount = 0 Then isFirstTime = True Else isFirstTime = False
        ShowModule(MasterPoint, "Master Point")
        MasterPoint = Nothing

        refreshingGrid()
    End Sub

    Private Sub butKoreksi_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butKoreksi.Click
        If Not GridView1.RowCount > 0 And butKoreksi.Visible = True Then Return
        selmaster_id = GridView1.GetFocusedRowCellValue("ID")
        If selmaster_id = 0 Then Return

        If GridView1.RowCount = 1 Then isFirstTime = True Else isFirstTime = False
        ShowModule(MasterPoint, "Master Point")
        MasterPoint = Nothing

        refreshingGrid()
    End Sub

    Private Sub butVoid_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butVoid.Click
        If Not GridView1.RowCount > 0 Then Return

    End Sub

    Private Sub butRefresh_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butRefresh.Click
        isChange = True
        refreshingGrid()
    End Sub

    Private Sub ButExport_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButExport.Click
        PopupMenu1.ShowPopup(Control.MousePosition)
    End Sub

    Private Sub ExportPDF_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles ExportPDF.ItemClick
        Using saveFileDialog1 As New SaveFileDialog() With {.Filter = "PDF Files|*.pdf", .Title = "Save a PDF File"}
            saveFileDialog1.ShowDialog()
            If saveFileDialog1.FileName <> "" Then
                GridView1.OptionsPrint.ExpandAllDetails = True
                GridView1.ExportToPdf(saveFileDialog1.FileName)
                MsgInfo("Export file success")
            End If
        End Using
    End Sub

    Private Sub ExportXLS_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles ExportXLS.ItemClick
        Using saveFileDialog1 As New SaveFileDialog() With {.Filter = "XLS Files|*.xls", .Title = "Save a XLS File"}
            saveFileDialog1.ShowDialog()
            If saveFileDialog1.FileName <> "" Then
                GridView1.OptionsPrint.ExpandAllDetails = True
                GridView1.ExportToXls(saveFileDialog1.FileName)
                MsgInfo("Export file success")
            End If
        End Using
    End Sub

    Private Sub ExportXLSX_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles ExportXLSX.ItemClick
        Using saveFileDialog1 As New SaveFileDialog() With {.Filter = "XLSX Files|*.xlsx", .Title = "Save a XLSX File"}
            saveFileDialog1.ShowDialog()
            If saveFileDialog1.FileName <> "" Then
                GridView1.OptionsPrint.ExpandAllDetails = True
                GridView1.ExportToXlsx(saveFileDialog1.FileName)
                MsgInfo("Export file success")
            End If
        End Using
    End Sub

    Private Sub butClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butClose.Click
        Close()
    End Sub

    Private Sub reset_buttons()
        butBaru.Text = "Baru"
        butKoreksi.Text = "Koreksi"
        butVoid.Text = "Non/Aktifkan"

        If MultiServer = True And OnlineStatus = False Then
            butBaru.Enabled = False
        Else
            butBaru.Enabled = True
        End If

        butKoreksi.Enabled = True
        butVoid.Enabled = True
    End Sub

    Private Sub refreshingGrid()
        reset_buttons()
        Dim idxRow As Integer = GridView1.FocusedRowHandle

        If isChange = True Then isChange = False Else Return

        '----------------------------Query
        If Not xSet.Tables("BMasterPoint") Is Nothing Then xSet.Tables("BMasterPoint").Clear()
        SQLquery = "SELECT id_ratepoint ID, nama_tipemembership, rate_rp, rate_point, isDefault FROM mbsm_point a INNER JOIN mbsm_tipemembership b ON a.id_tipemembership=b.id_tipemembership "
        'If butCheckShow.Checked = False Then SQLquery += "WHERE a.inactive=0 "
        SQLquery += "ORDER BY b.level_tipemembership"
        ExDb.ExecQuery(1, SQLquery, xSet, "BMasterPoint")

        '----------------------------Grid Settings
        GridControl1.DataSource = xSet.Tables("BMasterPoint").DefaultView
        GridView1.Columns("ID").Visible = False
        GridView1.Columns("nama_tipemembership").Caption = "Tipe Membership"
        GridView1.Columns("rate_rp").Caption = "Rate Rp"
        GridView1.Columns("rate_point").Caption = "Rate Point"
        GridView1.Columns("isDefault").Caption = "Default"

        GridView1.Columns("nama_tipemembership").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count
        GridView1.Columns("nama_tipemembership").SummaryItem.DisplayFormat = "Jumlah:{0:n0}"

        GridView1.Columns("rate_rp").DisplayFormat.FormatType = FormatType.Numeric
        GridView1.Columns("rate_rp").DisplayFormat.FormatString = "Rp{0:n0}"
        GridView1.Columns("rate_point").DisplayFormat.FormatType = FormatType.Numeric
        GridView1.Columns("rate_point").DisplayFormat.FormatString = "{0:n2}"

        For Each coll As DataColumn In xSet.Tables("BMasterPoint").Columns
            GridView1.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next
        GridView1.BestFitColumns()
        GridView1.FocusedRowHandle = idxRow
    End Sub
End Class