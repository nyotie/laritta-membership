﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MasterRewardPoint
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.ExpiredDate = New DevExpress.XtraEditors.DateEdit()
        Me.RewardValue = New DevExpress.XtraEditors.TextEdit()
        Me.KebutuhanPoint = New DevExpress.XtraEditors.TextEdit()
        Me.SpesifikasiReward = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.NamaReward = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.ButBatal = New DevExpress.XtraEditors.SimpleButton()
        Me.ButSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.CekInaktif = New DevExpress.XtraEditors.CheckEdit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.ExpiredDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ExpiredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RewardValue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.KebutuhanPoint.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SpesifikasiReward.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NamaReward.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CekInaktif.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelControl1.Controls.Add(Me.LabelControl6)
        Me.PanelControl1.Controls.Add(Me.ExpiredDate)
        Me.PanelControl1.Controls.Add(Me.RewardValue)
        Me.PanelControl1.Controls.Add(Me.KebutuhanPoint)
        Me.PanelControl1.Controls.Add(Me.SpesifikasiReward)
        Me.PanelControl1.Controls.Add(Me.LabelControl5)
        Me.PanelControl1.Controls.Add(Me.NamaReward)
        Me.PanelControl1.Controls.Add(Me.LabelControl4)
        Me.PanelControl1.Controls.Add(Me.LabelControl3)
        Me.PanelControl1.Controls.Add(Me.LabelControl2)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Location = New System.Drawing.Point(12, 37)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(421, 153)
        Me.PanelControl1.TabIndex = 2
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl6.Location = New System.Drawing.Point(143, 125)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(20, 14)
        Me.LabelControl6.TabIndex = 3
        Me.LabelControl6.Text = "Rp."
        '
        'ExpiredDate
        '
        Me.ExpiredDate.EditValue = Nothing
        Me.ExpiredDate.Location = New System.Drawing.Point(143, 68)
        Me.ExpiredDate.Name = "ExpiredDate"
        Me.ExpiredDate.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ExpiredDate.Properties.Appearance.Options.UseFont = True
        Me.ExpiredDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ExpiredDate.Properties.Mask.EditMask = "dd MMMM yyyy"
        Me.ExpiredDate.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.ExpiredDate.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.ExpiredDate.Size = New System.Drawing.Size(141, 21)
        Me.ExpiredDate.TabIndex = 4
        '
        'RewardValue
        '
        Me.RewardValue.Location = New System.Drawing.Point(169, 122)
        Me.RewardValue.Name = "RewardValue"
        Me.RewardValue.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.RewardValue.Properties.Appearance.Options.UseFont = True
        Me.RewardValue.Properties.Appearance.Options.UseTextOptions = True
        Me.RewardValue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.RewardValue.Properties.Mask.EditMask = "n0"
        Me.RewardValue.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.RewardValue.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.RewardValue.Properties.NullText = "0"
        Me.RewardValue.Size = New System.Drawing.Size(115, 21)
        Me.RewardValue.TabIndex = 6
        '
        'KebutuhanPoint
        '
        Me.KebutuhanPoint.Location = New System.Drawing.Point(143, 95)
        Me.KebutuhanPoint.Name = "KebutuhanPoint"
        Me.KebutuhanPoint.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.KebutuhanPoint.Properties.Appearance.Options.UseFont = True
        Me.KebutuhanPoint.Properties.Appearance.Options.UseTextOptions = True
        Me.KebutuhanPoint.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.KebutuhanPoint.Properties.Mask.EditMask = "n0"
        Me.KebutuhanPoint.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.KebutuhanPoint.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.KebutuhanPoint.Properties.NullText = "0"
        Me.KebutuhanPoint.Size = New System.Drawing.Size(141, 21)
        Me.KebutuhanPoint.TabIndex = 5
        '
        'SpesifikasiReward
        '
        Me.SpesifikasiReward.Location = New System.Drawing.Point(143, 41)
        Me.SpesifikasiReward.Name = "SpesifikasiReward"
        Me.SpesifikasiReward.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.SpesifikasiReward.Properties.Appearance.Options.UseFont = True
        Me.SpesifikasiReward.Size = New System.Drawing.Size(271, 21)
        Me.SpesifikasiReward.TabIndex = 3
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl5.Location = New System.Drawing.Point(20, 98)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(105, 14)
        Me.LabelControl5.TabIndex = 0
        Me.LabelControl5.Text = "Kebutuhan point"
        '
        'NamaReward
        '
        Me.NamaReward.Location = New System.Drawing.Point(143, 14)
        Me.NamaReward.Name = "NamaReward"
        Me.NamaReward.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.NamaReward.Properties.Appearance.Options.UseFont = True
        Me.NamaReward.Properties.MaxLength = 50
        Me.NamaReward.Size = New System.Drawing.Size(271, 21)
        Me.NamaReward.TabIndex = 2
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl4.Location = New System.Drawing.Point(20, 125)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(75, 14)
        Me.LabelControl4.TabIndex = 0
        Me.LabelControl4.Text = "Nilai reward"
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl3.Location = New System.Drawing.Point(20, 71)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(105, 14)
        Me.LabelControl3.TabIndex = 0
        Me.LabelControl3.Text = "Tanggal expired "
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl2.Location = New System.Drawing.Point(20, 44)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(66, 14)
        Me.LabelControl2.TabIndex = 0
        Me.LabelControl2.Text = "Spesifikasi"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl1.Location = New System.Drawing.Point(20, 17)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(85, 14)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Nama reward"
        '
        'ButBatal
        '
        Me.ButBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButBatal.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButBatal.Appearance.Options.UseFont = True
        Me.ButBatal.Image = Global.Laritta_Membership.My.Resources.Resources.back_32
        Me.ButBatal.Location = New System.Drawing.Point(343, 196)
        Me.ButBatal.Name = "ButBatal"
        Me.ButBatal.Size = New System.Drawing.Size(90, 40)
        Me.ButBatal.TabIndex = 8
        Me.ButBatal.Text = "&Batal"
        '
        'ButSimpan
        '
        Me.ButSimpan.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButSimpan.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButSimpan.Appearance.Options.UseFont = True
        Me.ButSimpan.Image = Global.Laritta_Membership.My.Resources.Resources.apply_32
        Me.ButSimpan.Location = New System.Drawing.Point(247, 196)
        Me.ButSimpan.Name = "ButSimpan"
        Me.ButSimpan.Size = New System.Drawing.Size(90, 40)
        Me.ButSimpan.TabIndex = 7
        Me.ButSimpan.Text = "&Simpan"
        '
        'CekInaktif
        '
        Me.CekInaktif.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CekInaktif.Location = New System.Drawing.Point(343, 12)
        Me.CekInaktif.Name = "CekInaktif"
        Me.CekInaktif.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.CekInaktif.Properties.Appearance.Options.UseFont = True
        Me.CekInaktif.Properties.Caption = "Tidak aktif"
        Me.CekInaktif.Size = New System.Drawing.Size(90, 19)
        Me.CekInaktif.TabIndex = 1
        '
        'MasterRewardPoint
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(445, 248)
        Me.Controls.Add(Me.CekInaktif)
        Me.Controls.Add(Me.ButBatal)
        Me.Controls.Add(Me.ButSimpan)
        Me.Controls.Add(Me.PanelControl1)
        Me.Name = "MasterRewardPoint"
        Me.Text = "MasterRewardPoint"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.ExpiredDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ExpiredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RewardValue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.KebutuhanPoint.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SpesifikasiReward.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NamaReward.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CekInaktif.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents ButBatal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ExpiredDate As DevExpress.XtraEditors.DateEdit
    Friend WithEvents RewardValue As DevExpress.XtraEditors.TextEdit
    Friend WithEvents KebutuhanPoint As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SpesifikasiReward As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents NamaReward As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CekInaktif As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
End Class
