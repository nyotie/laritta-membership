﻿Public Class MasterRewardStamp

    Private Sub MasterRewardStamp_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("DataMasterRewardStamp")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub MasterRewardStamp_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading(Me)

        If Not selmaster_id = 0 Then
            SQLquery = String.Format("SELECT nama_rewardstamp, reward_specification, reward_value, required_stamp, expired_date, Inactive FROM mbsm_rewardstamp WHERE id_rewardstamp={0}", selmaster_id)
            ExDb.ExecQuery(1, SQLquery, xSet, "DataMasterRewardStamp")

            NamaReward.Text = xSet.Tables("DataMasterRewardStamp").Rows(0).Item("nama_rewardstamp")
            SpesifikasiReward.Text = xSet.Tables("DataMasterRewardStamp").Rows(0).Item("reward_specification")
            ExpiredDate.EditValue = xSet.Tables("DataMasterRewardStamp").Rows(0).Item("expired_date")
            KebutuhanStamp.EditValue = xSet.Tables("DataMasterRewardStamp").Rows(0).Item("required_stamp")
            RewardValue.EditValue = xSet.Tables("DataMasterRewardStamp").Rows(0).Item("reward_value")
            CekInaktif.Checked = xSet.Tables("DataMasterRewardStamp").Rows(0).Item("Inactive")
        End If
    End Sub

    Private Sub MasterRewardStamp_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        LoadSprite.CloseLoading()
        NamaReward.Focus()

        If selmaster_id = 0 Then
            CekInaktif.Properties.ReadOnly = True
            CekInaktif.Checked = False
        End If
    End Sub

    Private Sub ButSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSimpan.Click
        If Not beforeSave() Then Return
        Try
            LoadSprite.ShowLoading()
            If selmaster_id = "0" Then
                sp_Query = "INSERT INTO mbsm_rewardstamp(nama_rewardstamp, reward_specification, reward_value, required_stamp, expired_date, createby, createdate) "
                param_Query = String.Format("VALUES('{0}','{1}',{2},{3},'{4}',{5},NOW()) ",
                                            NamaReward.Text, SpesifikasiReward.Text, RewardValue.EditValue, KebutuhanStamp.EditValue, Format(ExpiredDate.EditValue, "yyyy-MM-dd"), staff_id)
            Else
                sp_Query = String.Format("UPDATE mbsm_rewardstamp SET nama_rewardstamp='{0}', reward_specification='{1}', reward_value={2}, required_stamp={3}, expired_date='{4}', inactive={5}, updateBy={6}, updateDate=NOW() ",
                                            NamaReward.Text, SpesifikasiReward.Text, RewardValue.EditValue, KebutuhanStamp.EditValue, Format(ExpiredDate.EditValue, "yyyy-MM-dd"), CekInaktif.Checked, staff_id)
                param_Query = String.Format("WHERE id_rewardstamp={0}", selmaster_id)
            End If

            If MultiServer Then
                ExDb.ExecData(1, sp_Query & param_Query)
            Else
                ExDb.ExecData(1, sp_Query & param_Query)
            End If
            MsgInfo("Data berhasil disimpan, tekan OK untuk melanjutkan.")
            BrowserMasterRewardStamp.isChange = True
        Catch ex As Exception
            MsgErrorDev()
        End Try
        LoadSprite.CloseLoading()
        Close()
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Function beforeSave() As Boolean
        If NamaReward.Text.Length < 3 Or NamaReward.Text.Contains("'") Then
            MsgInfo("Nama reward terlalu pendek atau mengandung tanda petik (')")
            Return False
        End If
        If ExpiredDate.EditValue Is Nothing Then
            MsgInfo("Batas akhir pemberian reward tidak boleh kosong")
            Return False
        End If
        If RewardValue.EditValue < 0 Or KebutuhanStamp.EditValue < 0 Then
            MsgInfo("Stamp atau nilai dari reward tidak boleh kurang dari nol (0)")
            Return False
        End If

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        If NamaReward.Text <> nameCheck Then
            If Not isDoubleData(String.Format("SELECT id_rewardstamp ID FROM mbsm_rewardstamp WHERE nama_rewardstamp='{0}'", NamaReward.Text)) Then
                MsgWarning("Nama reward sudah terdapat dalam daftar!")
                Return False
            End If
        End If

        Return True
    End Function
End Class