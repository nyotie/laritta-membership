﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits DevExpress.XtraBars.Ribbon.RibbonForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
        Me.RibbonControl = New DevExpress.XtraBars.Ribbon.RibbonControl()
        Me.HLogout = New DevExpress.XtraBars.BarButtonItem()
        Me.MCustomer = New DevExpress.XtraBars.BarButtonItem()
        Me.MKota = New DevExpress.XtraBars.BarButtonItem()
        Me.MArea = New DevExpress.XtraBars.BarButtonItem()
        Me.MTipeMembership = New DevExpress.XtraBars.BarButtonItem()
        Me.MPoint = New DevExpress.XtraBars.BarButtonItem()
        Me.MStamp = New DevExpress.XtraBars.BarButtonItem()
        Me.MGift = New DevExpress.XtraBars.BarButtonItem()
        Me.MPromo = New DevExpress.XtraBars.BarButtonItem()
        Me.MRewardPoint = New DevExpress.XtraBars.BarButtonItem()
        Me.MRewardStamp = New DevExpress.XtraBars.BarButtonItem()
        Me.TransCheckMember = New DevExpress.XtraBars.BarButtonItem()
        Me.TransMassBlessings = New DevExpress.XtraBars.BarButtonItem()
        Me.TransLogs = New DevExpress.XtraBars.BarButtonItem()
        Me.TransReport = New DevExpress.XtraBars.BarButtonItem()
        Me.ToolProgPerm = New DevExpress.XtraBars.BarButtonItem()
        Me.ToolRepPerm = New DevExpress.XtraBars.BarButtonItem()
        Me.ToolCabang = New DevExpress.XtraBars.BarButtonItem()
        Me.ToolKoneksi = New DevExpress.XtraBars.BarButtonItem()
        Me.TransVoidNota = New DevExpress.XtraBars.BarButtonItem()
        Me.TransPenggunaanPG = New DevExpress.XtraBars.BarButtonItem()
        Me.TransAuthentication = New DevExpress.XtraBars.BarButtonItem()
        Me.MMembercard = New DevExpress.XtraBars.BarButtonItem()
        Me.ToolRFID = New DevExpress.XtraBars.BarButtonItem()
        Me.TransPenggunaanPS = New DevExpress.XtraBars.BarButtonItem()
        Me.ToolBlockCard = New DevExpress.XtraBars.BarButtonItem()
        Me.TransSettle = New DevExpress.XtraBars.BarButtonItem()
        Me.Icon48 = New DevExpress.Utils.ImageCollection(Me.components)
        Me.PageHome = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup1 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.PageMaster = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup9 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup2 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup7 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.PageTransaksi = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.PageTransTransaksi = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.PageTransPenggunaan = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.PageTransBlessing = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.PageTransLogs = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.PageToolsAndSetting = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup6 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup5 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonStatusBar = New DevExpress.XtraBars.Ribbon.RibbonStatusBar()
        Me.XtraTabbedMdiManager1 = New DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(Me.components)
        Me.RibbonPageAbout = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.ToolAbout = New DevExpress.XtraBars.BarButtonItem()
        CType(Me.RibbonControl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Icon48, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XtraTabbedMdiManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RibbonControl
        '
        Me.RibbonControl.ApplicationButtonText = Nothing
        '
        '
        '
        Me.RibbonControl.ExpandCollapseItem.Id = 0
        Me.RibbonControl.ExpandCollapseItem.Name = ""
        Me.RibbonControl.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.RibbonControl.ExpandCollapseItem, Me.HLogout, Me.MCustomer, Me.MKota, Me.MArea, Me.MTipeMembership, Me.MPoint, Me.MStamp, Me.MGift, Me.MPromo, Me.MRewardPoint, Me.MRewardStamp, Me.TransCheckMember, Me.TransMassBlessings, Me.TransLogs, Me.TransReport, Me.ToolProgPerm, Me.ToolRepPerm, Me.ToolCabang, Me.ToolKoneksi, Me.TransVoidNota, Me.TransPenggunaanPG, Me.TransAuthentication, Me.MMembercard, Me.ToolRFID, Me.TransPenggunaanPS, Me.ToolBlockCard, Me.TransSettle, Me.ToolAbout})
        Me.RibbonControl.LargeImages = Me.Icon48
        Me.RibbonControl.Location = New System.Drawing.Point(0, 0)
        Me.RibbonControl.MaxItemId = 31
        Me.RibbonControl.Name = "RibbonControl"
        Me.RibbonControl.Pages.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPage() {Me.PageHome, Me.PageMaster, Me.PageTransaksi, Me.PageToolsAndSetting})
        Me.RibbonControl.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.MacOffice
        Me.RibbonControl.SelectedPage = Me.PageToolsAndSetting
        Me.RibbonControl.ShowExpandCollapseButton = DevExpress.Utils.DefaultBoolean.[False]
        Me.RibbonControl.ShowToolbarCustomizeItem = False
        Me.RibbonControl.Size = New System.Drawing.Size(788, 133)
        Me.RibbonControl.StatusBar = Me.RibbonStatusBar
        Me.RibbonControl.Toolbar.ShowCustomizeItem = False
        '
        'HLogout
        '
        Me.HLogout.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.HLogout.Appearance.Options.UseFont = True
        Me.HLogout.Caption = "Logout"
        Me.HLogout.Id = 1
        Me.HLogout.LargeImageIndex = 7
        Me.HLogout.Name = "HLogout"
        '
        'MCustomer
        '
        Me.MCustomer.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.MCustomer.Appearance.Options.UseFont = True
        Me.MCustomer.Caption = "Customer"
        Me.MCustomer.Id = 3
        Me.MCustomer.LargeImageIndex = 2
        Me.MCustomer.Name = "MCustomer"
        '
        'MKota
        '
        Me.MKota.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.MKota.Appearance.Options.UseFont = True
        Me.MKota.Caption = "Kota"
        Me.MKota.Id = 4
        Me.MKota.LargeImageIndex = 6
        Me.MKota.Name = "MKota"
        '
        'MArea
        '
        Me.MArea.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.MArea.Appearance.Options.UseFont = True
        Me.MArea.Caption = "Area"
        Me.MArea.Id = 5
        Me.MArea.LargeImageIndex = 0
        Me.MArea.Name = "MArea"
        '
        'MTipeMembership
        '
        Me.MTipeMembership.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.MTipeMembership.Appearance.Options.UseFont = True
        Me.MTipeMembership.Caption = "Tipe Membership"
        Me.MTipeMembership.Id = 6
        Me.MTipeMembership.LargeImageIndex = 16
        Me.MTipeMembership.Name = "MTipeMembership"
        '
        'MPoint
        '
        Me.MPoint.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.MPoint.Appearance.Options.UseFont = True
        Me.MPoint.Caption = "Point"
        Me.MPoint.Id = 7
        Me.MPoint.LargeImageIndex = 10
        Me.MPoint.Name = "MPoint"
        '
        'MStamp
        '
        Me.MStamp.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.MStamp.Appearance.Options.UseFont = True
        Me.MStamp.Caption = "Stamp"
        Me.MStamp.Id = 8
        Me.MStamp.LargeImageIndex = 15
        Me.MStamp.Name = "MStamp"
        '
        'MGift
        '
        Me.MGift.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.MGift.Appearance.Options.UseFont = True
        Me.MGift.Caption = "Gift"
        Me.MGift.Id = 9
        Me.MGift.LargeImageIndex = 4
        Me.MGift.Name = "MGift"
        '
        'MPromo
        '
        Me.MPromo.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.MPromo.Appearance.Options.UseFont = True
        Me.MPromo.Caption = "Promo"
        Me.MPromo.Id = 10
        Me.MPromo.LargeImageIndex = 12
        Me.MPromo.Name = "MPromo"
        '
        'MRewardPoint
        '
        Me.MRewardPoint.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.MRewardPoint.Appearance.Options.UseFont = True
        Me.MRewardPoint.Caption = "Reward Point"
        Me.MRewardPoint.Id = 11
        Me.MRewardPoint.LargeImageIndex = 17
        Me.MRewardPoint.Name = "MRewardPoint"
        '
        'MRewardStamp
        '
        Me.MRewardStamp.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.MRewardStamp.Appearance.Options.UseFont = True
        Me.MRewardStamp.Caption = "Reward Stamp"
        Me.MRewardStamp.Id = 12
        Me.MRewardStamp.LargeImageIndex = 18
        Me.MRewardStamp.Name = "MRewardStamp"
        '
        'TransCheckMember
        '
        Me.TransCheckMember.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.TransCheckMember.Appearance.Options.UseFont = True
        Me.TransCheckMember.Caption = "Check Member"
        Me.TransCheckMember.Id = 13
        Me.TransCheckMember.LargeImageIndex = 1
        Me.TransCheckMember.Name = "TransCheckMember"
        '
        'TransMassBlessings
        '
        Me.TransMassBlessings.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.TransMassBlessings.Appearance.Options.UseFont = True
        Me.TransMassBlessings.Caption = "Mass Blessings"
        Me.TransMassBlessings.Id = 14
        Me.TransMassBlessings.LargeImageIndex = 9
        Me.TransMassBlessings.Name = "TransMassBlessings"
        '
        'TransLogs
        '
        Me.TransLogs.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.TransLogs.Appearance.Options.UseFont = True
        Me.TransLogs.Caption = "Logs"
        Me.TransLogs.Id = 15
        Me.TransLogs.LargeImageIndex = 8
        Me.TransLogs.Name = "TransLogs"
        '
        'TransReport
        '
        Me.TransReport.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.TransReport.Appearance.Options.UseFont = True
        Me.TransReport.Caption = "Report"
        Me.TransReport.Id = 16
        Me.TransReport.LargeImageIndex = 14
        Me.TransReport.Name = "TransReport"
        '
        'ToolProgPerm
        '
        Me.ToolProgPerm.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ToolProgPerm.Appearance.Options.UseFont = True
        Me.ToolProgPerm.Caption = "Program Permissions"
        Me.ToolProgPerm.Id = 17
        Me.ToolProgPerm.LargeImageIndex = 11
        Me.ToolProgPerm.Name = "ToolProgPerm"
        '
        'ToolRepPerm
        '
        Me.ToolRepPerm.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ToolRepPerm.Appearance.Options.UseFont = True
        Me.ToolRepPerm.Caption = "Report Permissions"
        Me.ToolRepPerm.Id = 18
        Me.ToolRepPerm.LargeImageIndex = 13
        Me.ToolRepPerm.Name = "ToolRepPerm"
        '
        'ToolCabang
        '
        Me.ToolCabang.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ToolCabang.Appearance.Options.UseFont = True
        Me.ToolCabang.Caption = "Cabang"
        Me.ToolCabang.Id = 19
        Me.ToolCabang.LargeImageIndex = 3
        Me.ToolCabang.Name = "ToolCabang"
        '
        'ToolKoneksi
        '
        Me.ToolKoneksi.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ToolKoneksi.Appearance.Options.UseFont = True
        Me.ToolKoneksi.Caption = "Koneksi"
        Me.ToolKoneksi.Id = 20
        Me.ToolKoneksi.LargeImageIndex = 5
        Me.ToolKoneksi.Name = "ToolKoneksi"
        '
        'TransVoidNota
        '
        Me.TransVoidNota.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.TransVoidNota.Appearance.Options.UseFont = True
        Me.TransVoidNota.Caption = "Void Nota"
        Me.TransVoidNota.Id = 22
        Me.TransVoidNota.LargeImageIndex = 20
        Me.TransVoidNota.Name = "TransVoidNota"
        '
        'TransPenggunaanPG
        '
        Me.TransPenggunaanPG.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.TransPenggunaanPG.Appearance.Options.UseFont = True
        Me.TransPenggunaanPG.Caption = "Promo dan Gift"
        Me.TransPenggunaanPG.Id = 23
        Me.TransPenggunaanPG.LargeImageIndex = 21
        Me.TransPenggunaanPG.Name = "TransPenggunaanPG"
        '
        'TransAuthentication
        '
        Me.TransAuthentication.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.TransAuthentication.Appearance.Options.UseFont = True
        Me.TransAuthentication.Caption = "Authentication"
        Me.TransAuthentication.Id = 24
        Me.TransAuthentication.LargeImageIndex = 22
        Me.TransAuthentication.Name = "TransAuthentication"
        '
        'MMembercard
        '
        Me.MMembercard.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.MMembercard.Appearance.Options.UseFont = True
        Me.MMembercard.Caption = "Membercard"
        Me.MMembercard.Id = 25
        Me.MMembercard.LargeImageIndex = 24
        Me.MMembercard.Name = "MMembercard"
        '
        'ToolRFID
        '
        Me.ToolRFID.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ToolRFID.Appearance.Options.UseFont = True
        Me.ToolRFID.Caption = "RFID"
        Me.ToolRFID.Id = 26
        Me.ToolRFID.LargeImageIndex = 25
        Me.ToolRFID.Name = "ToolRFID"
        '
        'TransPenggunaanPS
        '
        Me.TransPenggunaanPS.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.TransPenggunaanPS.Appearance.Options.UseFont = True
        Me.TransPenggunaanPS.Caption = "Point dan Stamp"
        Me.TransPenggunaanPS.Id = 27
        Me.TransPenggunaanPS.LargeImageIndex = 26
        Me.TransPenggunaanPS.Name = "TransPenggunaanPS"
        '
        'ToolBlockCard
        '
        Me.ToolBlockCard.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ToolBlockCard.Appearance.Options.UseFont = True
        Me.ToolBlockCard.Caption = "Blocking Card"
        Me.ToolBlockCard.Id = 28
        Me.ToolBlockCard.LargeImageIndex = 23
        Me.ToolBlockCard.Name = "ToolBlockCard"
        '
        'TransSettle
        '
        Me.TransSettle.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.TransSettle.Appearance.Options.UseFont = True
        Me.TransSettle.Caption = "Settle"
        Me.TransSettle.Id = 29
        Me.TransSettle.LargeImageIndex = 27
        Me.TransSettle.Name = "TransSettle"
        '
        'Icon48
        '
        Me.Icon48.ImageSize = New System.Drawing.Size(48, 48)
        Me.Icon48.ImageStream = CType(resources.GetObject("Icon48.ImageStream"), DevExpress.Utils.ImageCollectionStreamer)
        Me.Icon48.Images.SetKeyName(0, "Area.png")
        Me.Icon48.Images.SetKeyName(1, "Check Member.png")
        Me.Icon48.Images.SetKeyName(2, "Customer.png")
        Me.Icon48.Images.SetKeyName(3, "Ganti Cabang.png")
        Me.Icon48.Images.SetKeyName(4, "Gift.png")
        Me.Icon48.Images.SetKeyName(5, "Koneksi.png")
        Me.Icon48.Images.SetKeyName(6, "Kota.png")
        Me.Icon48.Images.SetKeyName(7, "Logout.png")
        Me.Icon48.Images.SetKeyName(8, "Logs.png")
        Me.Icon48.Images.SetKeyName(9, "Mass Blessings.png")
        Me.Icon48.Images.SetKeyName(10, "Point.png")
        Me.Icon48.Images.SetKeyName(11, "Program Permissions.png")
        Me.Icon48.Images.SetKeyName(12, "Promo.png")
        Me.Icon48.Images.SetKeyName(13, "Report Permissions.png")
        Me.Icon48.Images.SetKeyName(14, "Report.png")
        Me.Icon48.Images.SetKeyName(15, "Stamp.png")
        Me.Icon48.Images.SetKeyName(16, "Tipe Membership.png")
        Me.Icon48.Images.SetKeyName(17, "Reward Point.png")
        Me.Icon48.Images.SetKeyName(18, "Reward Stamp.png")
        Me.Icon48.Images.SetKeyName(19, "Register Membership.png")
        Me.Icon48.Images.SetKeyName(20, "Void Nota.png")
        Me.Icon48.Images.SetKeyName(21, "Penggunaan Promo gift.png")
        Me.Icon48.Images.SetKeyName(22, "Authentication.png")
        Me.Icon48.Images.SetKeyName(23, "Block Card.png")
        Me.Icon48.Images.SetKeyName(24, "Membercard.png")
        Me.Icon48.Images.SetKeyName(25, "RFID.png")
        Me.Icon48.Images.SetKeyName(26, "Point Stamp.png")
        Me.Icon48.Images.SetKeyName(27, "Settle.png")
        '
        'PageHome
        '
        Me.PageHome.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup1})
        Me.PageHome.Name = "PageHome"
        Me.PageHome.Text = "Home"
        '
        'RibbonPageGroup1
        '
        Me.RibbonPageGroup1.ItemLinks.Add(Me.HLogout)
        Me.RibbonPageGroup1.Name = "RibbonPageGroup1"
        Me.RibbonPageGroup1.ShowCaptionButton = False
        Me.RibbonPageGroup1.Text = "Home"
        '
        'PageMaster
        '
        Me.PageMaster.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup9, Me.RibbonPageGroup2, Me.RibbonPageGroup7})
        Me.PageMaster.Name = "PageMaster"
        Me.PageMaster.Text = "Master"
        Me.PageMaster.Visible = False
        '
        'RibbonPageGroup9
        '
        Me.RibbonPageGroup9.ItemLinks.Add(Me.MTipeMembership)
        Me.RibbonPageGroup9.ItemLinks.Add(Me.MKota)
        Me.RibbonPageGroup9.ItemLinks.Add(Me.MArea)
        Me.RibbonPageGroup9.ItemLinks.Add(Me.MCustomer)
        Me.RibbonPageGroup9.ItemLinks.Add(Me.MMembercard)
        Me.RibbonPageGroup9.Name = "RibbonPageGroup9"
        Me.RibbonPageGroup9.ShowCaptionButton = False
        Me.RibbonPageGroup9.Text = "Customer"
        '
        'RibbonPageGroup2
        '
        Me.RibbonPageGroup2.ItemLinks.Add(Me.MPoint)
        Me.RibbonPageGroup2.ItemLinks.Add(Me.MStamp)
        Me.RibbonPageGroup2.ItemLinks.Add(Me.MGift)
        Me.RibbonPageGroup2.ItemLinks.Add(Me.MPromo)
        Me.RibbonPageGroup2.Name = "RibbonPageGroup2"
        Me.RibbonPageGroup2.ShowCaptionButton = False
        Me.RibbonPageGroup2.Text = "Benefit"
        '
        'RibbonPageGroup7
        '
        Me.RibbonPageGroup7.ItemLinks.Add(Me.MRewardPoint)
        Me.RibbonPageGroup7.ItemLinks.Add(Me.MRewardStamp)
        Me.RibbonPageGroup7.Name = "RibbonPageGroup7"
        Me.RibbonPageGroup7.ShowCaptionButton = False
        Me.RibbonPageGroup7.Text = "Reward"
        '
        'PageTransaksi
        '
        Me.PageTransaksi.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.PageTransTransaksi, Me.PageTransPenggunaan, Me.PageTransBlessing, Me.PageTransLogs})
        Me.PageTransaksi.Name = "PageTransaksi"
        Me.PageTransaksi.Text = "Transaksi"
        Me.PageTransaksi.Visible = False
        '
        'PageTransTransaksi
        '
        Me.PageTransTransaksi.ItemLinks.Add(Me.TransCheckMember)
        Me.PageTransTransaksi.ItemLinks.Add(Me.TransVoidNota)
        Me.PageTransTransaksi.Name = "PageTransTransaksi"
        Me.PageTransTransaksi.ShowCaptionButton = False
        Me.PageTransTransaksi.Text = "Transaction"
        '
        'PageTransPenggunaan
        '
        Me.PageTransPenggunaan.ItemLinks.Add(Me.TransPenggunaanPG)
        Me.PageTransPenggunaan.ItemLinks.Add(Me.TransPenggunaanPS)
        Me.PageTransPenggunaan.Name = "PageTransPenggunaan"
        Me.PageTransPenggunaan.ShowCaptionButton = False
        Me.PageTransPenggunaan.Text = "Using Benefit"
        '
        'PageTransBlessing
        '
        Me.PageTransBlessing.ItemLinks.Add(Me.TransMassBlessings)
        Me.PageTransBlessing.ItemLinks.Add(Me.TransAuthentication)
        Me.PageTransBlessing.Name = "PageTransBlessing"
        Me.PageTransBlessing.ShowCaptionButton = False
        Me.PageTransBlessing.Text = "Blessing"
        '
        'PageTransLogs
        '
        Me.PageTransLogs.ItemLinks.Add(Me.TransSettle)
        Me.PageTransLogs.ItemLinks.Add(Me.TransLogs)
        Me.PageTransLogs.ItemLinks.Add(Me.TransReport)
        Me.PageTransLogs.Name = "PageTransLogs"
        Me.PageTransLogs.ShowCaptionButton = False
        Me.PageTransLogs.Text = "Main Panel"
        '
        'PageToolsAndSetting
        '
        Me.PageToolsAndSetting.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup6, Me.RibbonPageGroup5, Me.RibbonPageAbout})
        Me.PageToolsAndSetting.Name = "PageToolsAndSetting"
        Me.PageToolsAndSetting.Text = "Tools & Settings"
        Me.PageToolsAndSetting.Visible = False
        '
        'RibbonPageGroup6
        '
        Me.RibbonPageGroup6.ItemLinks.Add(Me.ToolProgPerm)
        Me.RibbonPageGroup6.ItemLinks.Add(Me.ToolRepPerm)
        Me.RibbonPageGroup6.ItemLinks.Add(Me.ToolBlockCard)
        Me.RibbonPageGroup6.Name = "RibbonPageGroup6"
        Me.RibbonPageGroup6.ShowCaptionButton = False
        Me.RibbonPageGroup6.Text = "Security"
        '
        'RibbonPageGroup5
        '
        Me.RibbonPageGroup5.ItemLinks.Add(Me.ToolCabang)
        Me.RibbonPageGroup5.ItemLinks.Add(Me.ToolKoneksi)
        Me.RibbonPageGroup5.ItemLinks.Add(Me.ToolRFID)
        Me.RibbonPageGroup5.Name = "RibbonPageGroup5"
        Me.RibbonPageGroup5.ShowCaptionButton = False
        Me.RibbonPageGroup5.Text = "Settings"
        '
        'RibbonStatusBar
        '
        Me.RibbonStatusBar.Location = New System.Drawing.Point(0, 418)
        Me.RibbonStatusBar.Name = "RibbonStatusBar"
        Me.RibbonStatusBar.Ribbon = Me.RibbonControl
        Me.RibbonStatusBar.Size = New System.Drawing.Size(788, 31)
        '
        'XtraTabbedMdiManager1
        '
        Me.XtraTabbedMdiManager1.MdiParent = Me
        '
        'RibbonPageAbout
        '
        Me.RibbonPageAbout.ItemLinks.Add(Me.ToolAbout)
        Me.RibbonPageAbout.Name = "RibbonPageAbout"
        Me.RibbonPageAbout.ShowCaptionButton = False
        Me.RibbonPageAbout.Text = "About"
        '
        'ToolAbout
        '
        Me.ToolAbout.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ToolAbout.Appearance.Options.UseFont = True
        Me.ToolAbout.Caption = "About"
        Me.ToolAbout.Id = 30
        Me.ToolAbout.Name = "ToolAbout"
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(788, 449)
        Me.Controls.Add(Me.RibbonStatusBar)
        Me.Controls.Add(Me.RibbonControl)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.Name = "MainForm"
        Me.Ribbon = Me.RibbonControl
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.StatusBar = Me.RibbonStatusBar
        Me.Text = "Laritta Membership"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.RibbonControl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Icon48, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XtraTabbedMdiManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents RibbonControl As DevExpress.XtraBars.Ribbon.RibbonControl
    Friend WithEvents PageMaster As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup1 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents RibbonStatusBar As DevExpress.XtraBars.Ribbon.RibbonStatusBar
    Friend WithEvents XtraTabbedMdiManager1 As DevExpress.XtraTabbedMdi.XtraTabbedMdiManager
    Friend WithEvents HLogout As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents MCustomer As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents MKota As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents MArea As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents MTipeMembership As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents MPoint As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents MStamp As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents MGift As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents MPromo As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents MRewardPoint As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents MRewardStamp As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents TransCheckMember As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents TransMassBlessings As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents TransLogs As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents TransReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ToolProgPerm As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ToolRepPerm As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ToolCabang As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ToolKoneksi As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents PageHome As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup2 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents RibbonPageGroup7 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents PageTransaksi As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents PageTransTransaksi As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents PageTransLogs As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents PageToolsAndSetting As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup6 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents RibbonPageGroup5 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents Icon48 As DevExpress.Utils.ImageCollection
    Friend WithEvents RibbonPageGroup9 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents TransVoidNota As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents TransPenggunaanPG As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents PageTransBlessing As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents TransAuthentication As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents MMembercard As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ToolRFID As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents TransPenggunaanPS As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents PageTransPenggunaan As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents ToolBlockCard As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents TransSettle As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ToolAbout As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageAbout As DevExpress.XtraBars.Ribbon.RibbonPageGroup


End Class
