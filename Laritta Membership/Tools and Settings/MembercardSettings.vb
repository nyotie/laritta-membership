﻿Imports Laritta_Membership.RFID_Tools

Public Class MembercardSettings

    Private Sub SettingReaderAndCard_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        MyCard.Disconnect()
    End Sub

    Private Sub SettingReaderAndCard_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        tReaderNameList.Text = DefReaderName
    End Sub

    Private Sub SettingReaderAndCard_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown

    End Sub

    Private Sub ButInit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButInit.Click
        Try
            If Not MyCard.ReaderEstablished Then MyCard.Establish(GlobalReaderContext)
            tReaderNameList.Properties.Items.Clear()
            tReaderNameList.Properties.Items.AddRange(MyCard.ReaderList)

            KeyA.Enabled = True
            KeyB.Enabled = True
        Catch ex As RFID_Exception
            tReaderNameList.SelectedIndex = -1
            tReaderNameList.Properties.Items.Clear()
            KeyA.Enabled = False
            KeyB.Enabled = False
        End Try
    End Sub

    Private Sub ButOK_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButOK.Click
        If tReaderNameList.Text = "" Then
            MsgWarning("Reader has to be defined!")
            Return
        End If

        If tReaderNameList.Properties.Items.Count > 0 Then FileSettingsHelper.WriteSettings(Application.StartupPath & "\setting.ini", "Reader and Card", "ReaderDefaultName", tReaderNameList.Text)
        DefReaderName = tReaderNameList.Text

        If KeyA.Text <> "" Then FileSettingsHelper.WriteSettings(Application.StartupPath & "\setting.ini", "Reader and Card", "KeyA", Encryptor.EncryptData(KeyA.Text))
        If KeyB.Text <> "" Then FileSettingsHelper.WriteSettings(Application.StartupPath & "\setting.ini", "Reader and Card", "KeyB", Encryptor.EncryptData(KeyB.Text))
        If KeyA.Text <> "" And KeyB.Text <> "" Then MyMiFareKey = New MiFareKey(KeyA.Text, KeyB.Text)

        '-----------Begone
        MsgInfo("Setting has been saved!")
        Close()
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub
End Class
