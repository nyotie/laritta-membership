﻿Public Class MembercardBlock
    Dim SearchExists As Boolean

    Private Sub MembercardBlock_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("BlockableLists")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub MembercardBlock_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub MembercardBlock_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown

    End Sub

    Private Sub ButReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButReset.Click
        ClearAll(True)
    End Sub

    Private Sub ButCari_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButCari.Click
        Dim CekCount As Integer = 0

        If CekKode.Checked Then CekCount += 1
        If CekBirthDate.Checked Then CekCount += 1
        If CekNama.Checked Then CekCount += 1
        If CekCount < 2 Then Return

        If Not xSet.Tables("BlockableLists") Is Nothing Then xSet.Tables("BlockableLists").Clear()
        SQLquery = "SELECT a.idCust ID, a.KTP_NIK KTP, a.namaCust Nama, a.kode_member Kode, a.BirthPlace TPL, a.BirthDate TGL, a.Sex, a.Phone PhR, a.PhoneMobile PhM, a.Email " & _
            "FROM mcustomer a WHERE a.xtimes_changecard>0 "
        If CekKode.Checked Then SQLquery &= String.Format("AND a.kode_member=REPLACE('{0}', ' ', '') ", TextKode.EditValue)
        If CekBirthDate.Checked Then SQLquery &= String.Format("AND a.BirthDate='{0}' ", Format(TextTGL.EditValue, "yyyy-MM-dd"))
        If CekNama.Checked Then SQLquery &= String.Format("AND LOWER(a.namaCust)=LOWER('{0}')", TextNama.Text)
        ExDb.ExecQuery(1, SQLquery, xSet, "BlockableLists")

        If xSet.Tables("BlockableLists").Rows.Count > 0 Then
            DataKTP.Text = xSet.Tables("BlockableLists").Rows(0).Item("KTP")
            DataTPL.Text = xSet.Tables("BlockableLists").Rows(0).Item("TPL")
            DataSex.EditValue = xSet.Tables("BlockableLists").Rows(0).Item("Sex")
            DataPhR.Text = xSet.Tables("BlockableLists").Rows(0).Item("PhR")
            DataPhM.Text = xSet.Tables("BlockableLists").Rows(0).Item("PhM")
            DataEmail.Text = xSet.Tables("BlockableLists").Rows(0).Item("Email")

            CekKode.Enabled = False
            CekBirthDate.Enabled = False
            CekNama.Enabled = False
            TextKode.Enabled = False
            TextTGL.Enabled = False
            TextNama.Enabled = False
            SearchExists = True
        Else
            ClearAll(False)
            MsgInfo("Pencarian tidak ditemukan")
            SearchExists = False
        End If
    End Sub

    Private Sub ButSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSimpan.Click
        If SearchExists Then
            If MsgBox("Yakin ingin melakukan pemblokiran kartu?" & vbCrLf & "(Pemblokiran tidak dapat di batalkan!)", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then Return

            Try
                LoadSprite.ShowLoading()
                SQLquery = String.Format("UPDATE mcustomer SET kode_kartuUID='', updateBy={1}, updateDate=NOW() WHERE idCust={0}; ",
                                         xSet.Tables("BlockableLists").Rows(0).Item("ID"), staff_id)
                ExDb.ExecData(1, SQLquery)

                SQLquery = String.Format("INSERT INTO mbst_log(id_cabang, id_customer, trans_id, event_date, event_id, event_targetname, creator) VALUES ({0}, {1}, 0, NOW(), 25, '{2}', {3});",
                                         id_cabang,
                                         xSet.Tables("BlockableLists").Rows(0).Item("ID"),
                                         xSet.Tables("BlockableLists").Rows(0).Item("Nama"),
                                         staff_id)
                ExDb.ExecData(1, SQLquery)

                MsgInfo("Data berhasil disimpan, tekan OK untuk melanjutkan.")
            Catch ex As Exception
                MsgErrorDev()
            End Try
            LoadSprite.CloseLoading()

            ClearAll(True)
        End If
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Sub ClearAll(ByVal Really As Boolean)
        If Really Then
            CekKode.Checked = False
            CekBirthDate.Checked = False
            CekNama.Checked = False
            TextKode.Text = ""
            TextTGL.Text = ""
            TextNama.Text = ""

            CekKode.Enabled = True
            CekBirthDate.Enabled = True
            CekNama.Enabled = True
            TextKode.Enabled = True
            TextTGL.Enabled = True
            TextNama.Enabled = True
        End If

        DataKTP.Text = ""
        DataTPL.Text = ""
        DataSex.Text = ""
        DataPhR.Text = ""
        DataPhM.Text = ""
        DataEmail.Text = ""

        SearchExists = False
    End Sub

    Private Sub CekKode_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CekKode.CheckedChanged
        If CekKode.Checked Then
            TextKode.Enabled = True
        Else
            TextKode.Enabled = False
        End If
    End Sub

    Private Sub CekKTP_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CekBirthDate.CheckedChanged
        If CekBirthDate.Checked Then
            TextTGL.Enabled = True
        Else
            TextTGL.Enabled = False
        End If
    End Sub

    Private Sub CekNama_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CekNama.CheckedChanged
        If CekNama.Checked Then
            TextNama.Enabled = True
        Else
            TextNama.Enabled = False
        End If
    End Sub
End Class