﻿Imports MySql.Data.MySqlClient

Public Class FrmSettingKoneksi
    Dim MySize As Size

    Private Sub FrmSettingKoneksi_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        LocalServer.Focus()
    End Sub

    Private Sub FrmSettingKoneksi_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LocalDB.Items.Add("- Select -")
        LocalDB.SelectedIndex = 0

        OnlineDB.Items.Add("- Select -")
        OnlineDB.SelectedIndex = 0
    End Sub

    Private Sub isMultiserver_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles isMultiserver.CheckedChanged
        PanelOnline.Enabled = isMultiserver.Checked
        PanelOnline.Visible = isMultiserver.Checked

        If isMultiserver.Checked Then
            MySize.Width = 722
        Else
            MySize.Width = 378
        End If
        MySize.Height = 308

        Size = MySize
        CenterToScreen()
    End Sub

    Private Sub LocalPWD_LostFocus(ByVal sender As Object, ByVal e As EventArgs) Handles LocalPWD.LostFocus
        If LocalUID.Text = "" Or LocalPWD.Text = "" Then Return
        LocalDB.Focus()
        LoadSprite.ShowLoading("Mencoba koneksi ke Database", "Testing Koneksi", Me)
        ConnectDatabase("Local Database")
    End Sub

    Private Sub OnlinePWD_LostFocus(ByVal sender As Object, ByVal e As EventArgs) Handles OnlinePWD.LostFocus
        If OnlineUID.Text = "" Or OnlinePWD.Text = "" Then Return
        OnlineDB.Focus()
        LoadSprite.ShowLoading("Mencoba koneksi ke Database", "Testing Koneksi", Me)
        ConnectDatabase("Online Database")
    End Sub

    Private Sub CmdSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CmdSimpan.Click

        If LocalDB.SelectedIndex = 0 Or LocalDB.Text = "" Then Return
        If isMultiserver.Checked Then
            If OnlineDB.SelectedIndex = 0 Or OnlineDB.Text = "" Then Return

            If LocalServer.Text = OnlineServer.Text Then Return
        End If

        FileSettingsHelper.WriteSettings(Application.StartupPath & "\setting.ini", "Application Settings", "Multiserver", isMultiserver.Checked)
        If isMultiserver.Checked Then SettingsWritter("Online Database")
        SettingsWritter("Local Database")

        MsgInfo("Setting berhasil disimpan!")
        Close()
    End Sub

    Private Sub CmdKeluar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CmdKeluar.Click
        Close()
    End Sub

    Private Sub ConnectDatabase(ByVal LineConnection As String)
        Try
            If LineConnection = "Local Database" Then
                If LocalDB.DataSource IsNot Nothing Then LocalDB.DataSource = Nothing
                LocalDB.Items.Clear()

                Using conn As New MySqlConnection() With {.ConnectionString = String.Format("server={0};User Id={1};password={2};Persist Security Info=True;Connect Timeout=5", LocalServer.Text, LocalUID.Text, LocalPWD.Text)}
                    conn.Open()
                    Dim da As New MySqlDataAdapter("SHOW DATABASES;", conn)
                    Using ds As New DataSet()
                        da.Fill(ds, "localdbs")
                        With (LocalDB)
                            .DataSource = ds.Tables("localdbs")
                            .DisplayMember = "Database"
                            .ValueMember = "Database"
                            .SelectedIndex = 0
                        End With
                    End Using
                End Using
            Else
                If OnlineDB.DataSource IsNot Nothing Then OnlineDB.DataSource = Nothing
                OnlineDB.Items.Clear()

                Using conn As New MySqlConnection() With {.ConnectionString = String.Format("server={0};User Id={1};password={2};Persist Security Info=True;Connect Timeout=25", OnlineServer.Text, OnlineUID.Text, OnlinePWD.Text)}
                    conn.Open()
                    Dim da As New MySqlDataAdapter("SHOW DATABASES;", conn)
                    Using ds As New DataSet()
                        da.Fill(ds, "onlinedbs")
                        With (OnlineDB)
                            .DataSource = ds.Tables("onlinedbs")
                            .DisplayMember = "Database"
                            .ValueMember = "Database"
                            .SelectedIndex = 0
                        End With
                    End Using
                End Using

            End If
        Catch err As MySqlException
            MsgWarning("Connection failed!")

            If LineConnection = "Local Database" Then
                LocalDB.Items.Add("- Select -")
                LocalDB.SelectedIndex = 0
            Else
                OnlineDB.Items.Add("- Select -")
                OnlineDB.SelectedIndex = 0
            End If
        End Try

        LoadSprite.CloseLoading()
    End Sub

    Private Sub SettingsWritter(ByVal LineConnection As String)
        If LineConnection = "Online Database" Then
            FileSettingsHelper.WriteSettings(Application.StartupPath & "\setting.ini", LineConnection, "Host", OnlineServer.Text)
            FileSettingsHelper.WriteSettings(Application.StartupPath & "\setting.ini", LineConnection, "User", OnlineUID.Text)
            FileSettingsHelper.WriteSettings(Application.StartupPath & "\setting.ini", LineConnection, "Password", Encryptor.EncryptData(OnlinePWD.Text))
            FileSettingsHelper.WriteSettings(Application.StartupPath & "\setting.ini", LineConnection, "DatabaseName", OnlineDB.Text)

        Else
            FileSettingsHelper.WriteSettings(Application.StartupPath & "\setting.ini", LineConnection, "Host", LocalServer.Text)
            FileSettingsHelper.WriteSettings(Application.StartupPath & "\setting.ini", LineConnection, "User", LocalUID.Text)
            FileSettingsHelper.WriteSettings(Application.StartupPath & "\setting.ini", LineConnection, "Password", Encryptor.EncryptData(LocalPWD.Text))
            FileSettingsHelper.WriteSettings(Application.StartupPath & "\setting.ini", LineConnection, "DatabaseName", LocalDB.Text)

        End If
    End Sub
End Class