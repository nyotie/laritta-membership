﻿Imports DevExpress.Utils
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraGrid.Views.Grid

Public Class ProgPermissions

    Private Sub ProgPermissions_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("DaftarSG")
        xSet.Tables.Remove("DaftarProgPermissions")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub ProgPermissions_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()
        Get_KebutuhanDaftar()
    End Sub

    Private Sub ProgPermissions_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        LoadSprite.CloseLoading()
        LookupSecurityGroup.Focus()
    End Sub

    Private Sub LookupSecurityGroup_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles LookupSecurityGroup.EditValueChanged
        If LookupSecurityGroup.EditValue Is Nothing Then
            If Not xSet.Tables("DaftarProgPermissions") Is Nothing Then xSet.Tables("DaftarProgPermissions").Clear()
            Return
        End If

        Try
            If Not xSet.Tables("DaftarProgPermissions") Is Nothing Then xSet.Tables("DaftarProgPermissions").Clear()
            SQLquery = String.Format("SELECT a.permission_id, a.kat_num, a.permission_num, a.kat_name, a.permission_name, " & _
                        "IFNULL( CASE b.allow_read WHEN 0 THEN 'False' WHEN 1 THEN 'True' END, 'False' ) Baca, " & _
                        "IFNULL( CASE b.allow_new WHEN 0 THEN 'False' WHEN 1 THEN 'True' END, 'False' ) Tambah, " & _
                        "IFNULL( CASE b.allow_edit WHEN 0 THEN 'False' WHEN 1 THEN 'True' END, 'False' ) Edit, " & _
                        "IFNULL( CASE b.allow_void WHEN 0 THEN 'False' WHEN 1 THEN 'True' END, 'False' ) Batal, " & _
                        "IFNULL( CASE b.allow_print WHEN 0 THEN 'False' WHEN 1 THEN 'True' END, 'False' ) Cetak, " & _
                        "a.default_rule FROM mbsm_permissionlists a LEFT JOIN mbsm_permissionuse b ON a.permission_id=b.permission_id AND b.id_sc={0} ORDER BY a.kat_num, a.permission_num", LookupSecurityGroup.EditValue)

            '----------------------------Grid Settings
            If xSet.Tables("DaftarProgPermissions") Is Nothing Then
                'first time!
                ExDb.ExecQuery(1, SQLquery, xSet, "DaftarProgPermissions")
                DaftarProgPermissions.DataSource = xSet.Tables("DaftarProgPermissions").DefaultView
                xSet.Tables("DaftarProgPermissions").DefaultView.Sort = "kat_num, permission_num"

                For Each coll As DataColumn In xSet.Tables("DaftarProgPermissions").Columns
                    ViewProgPermissions.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
                Next

                CheckMe.ValueChecked = "True"
                CheckMe.ValueUnchecked = "False"
                CheckMe.ValueGrayed = "False"

                DaftarProgPermissions.RepositoryItems.Add(CheckMe)

                ViewProgPermissions.Columns("permission_id").Visible = False
                ViewProgPermissions.Columns("kat_num").Visible = False
                ViewProgPermissions.Columns("permission_num").Visible = False
                ViewProgPermissions.Columns("default_rule").Visible = False

                ViewProgPermissions.Columns("Baca").ColumnEdit = CheckMe
                ViewProgPermissions.Columns("Tambah").ColumnEdit = CheckMe
                ViewProgPermissions.Columns("Edit").ColumnEdit = CheckMe
                ViewProgPermissions.Columns("Batal").ColumnEdit = CheckMe
                ViewProgPermissions.Columns("Cetak").ColumnEdit = CheckMe

                ViewProgPermissions.Columns("kat_name").Caption = "Kategori"
                ViewProgPermissions.Columns("permission_name").Caption = "Nama"

                ViewProgPermissions.Columns("permission_id").OptionsColumn.AllowEdit = False
                ViewProgPermissions.Columns("kat_name").OptionsColumn.AllowEdit = False
                ViewProgPermissions.Columns("permission_name").OptionsColumn.AllowEdit = False
                ViewProgPermissions.Columns("default_rule").OptionsColumn.AllowEdit = False

                Dim View As GridView = DaftarProgPermissions.FocusedView
                ViewProgPermissions.SortInfo.ClearAndAddRange(New GridColumnSortInfo() { _
                   New GridColumnSortInfo(View.Columns("kat_name"), DevExpress.Data.ColumnSortOrder.Ascending)
                }, 1)


                ViewProgPermissions.Columns("permission_name").Width = DaftarProgPermissions.Width * 0.4
                ViewProgPermissions.Columns("Baca").Width = DaftarProgPermissions.Width * 0.12
                ViewProgPermissions.Columns("Tambah").Width = DaftarProgPermissions.Width * 0.12
                ViewProgPermissions.Columns("Edit").Width = DaftarProgPermissions.Width * 0.12
                ViewProgPermissions.Columns("Batal").Width = DaftarProgPermissions.Width * 0.12
                ViewProgPermissions.Columns("Cetak").Width = DaftarProgPermissions.Width * 0.12
            Else
                'not a virgin anymore
                ExDb.ExecQuery(1, SQLquery, xSet, "DaftarProgPermissions")
            End If

            ViewProgPermissions.ExpandAllGroups()
        Catch ex As Exception
            MsgErrorDev()
        End Try
    End Sub

    Private Sub ButSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSimpan.Click
        If Not beforeSave() Then Return
        LoadSprite.ShowLoading("Menyimpan Informasi dasar", "Loading", Me)
        Try
            SQLquery = String.Format("DELETE FROM mbsm_permissionuse WHERE id_sc={0}; INSERT INTO mbsm_permissionuse(id_sc, permission_id, allow_read, allow_new, allow_edit, allow_void, allow_print) VALUES", LookupSecurityGroup.EditValue)
            For Each row As DataRow In xSet.Tables("DaftarProgPermissions").Rows
                SQLquery += String.Format("({0}, {1}, {2}, {3}, {4}, {5}, {6}), ",
                                          LookupSecurityGroup.EditValue,
                                          row.Item("permission_id"),
                                          row.Item("Baca"),
                                          row.Item("Tambah"),
                                          row.Item("Edit"),
                                          row.Item("Batal"),
                                          row.Item("Cetak"))
            Next
            SQLquery = Mid(SQLquery, 1, SQLquery.Length - 2) & "; "
            ExDb.ExecData(1, SQLquery)

            MsgInfo("Data berhasil disimpan, tekan OK untuk melanjutkan.")
            LookupSecurityGroup.EditValue = Nothing
        Catch ex As Exception
            MsgErrorDev()
        End Try
        LoadSprite.CloseLoading()
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Sub Get_KebutuhanDaftar()
        '--------------------------------Security Group
        If Not xSet.Tables("DaftarSG") Is Nothing Then xSet.Tables("DaftarSG").Clear()
        SQLquery = "SELECT idSecurityGroup ID, namaSG Nama FROM msecuritygroup where inactive=0 AND idSecurityGroup<>1;"
        ExDb.ExecQuery(1, SQLquery, xSet, "DaftarSG")

        '----------------------------Grid Settings
        LookupSecurityGroup.Properties.DataSource = xSet.Tables("DaftarSG").DefaultView

        LookupSecurityGroup.Properties.NullText = ""
        LookupSecurityGroup.Properties.ValueMember = "ID"
        LookupSecurityGroup.Properties.DisplayMember = "Nama"
        LookupSecurityGroup.Properties.ShowClearButton = False
        LookupSecurityGroup.Properties.PopulateViewColumns()

        LookupSecurityGroup.Properties.PopupFormSize = New Size(400, 300)

        ViewSecurityGroup.Columns("ID").Visible = False

        For Each coll As DataColumn In xSet.Tables("DaftarSG").Columns
            ViewSecurityGroup.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next
    End Sub

    Private Function beforeSave() As Boolean
        If LookupSecurityGroup.EditValue Is Nothing Then Return False
        If ViewProgPermissions.RowCount < 1 Then Return False

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        Return True
    End Function

    Private Sub ViewProgPermissions_RowCellStyle(ByVal sender As Object, ByVal e As RowCellStyleEventArgs) Handles ViewProgPermissions.RowCellStyle
        Dim View As GridView = sender

        Dim cekAllow As String = View.GetRowCellValue(e.RowHandle, "default_rule")
        If e.Column.FieldName = "Baca" Then
            If Not cekAllow.Contains("1") Then e.Appearance.BackColor = Color.Gray
        ElseIf e.Column.FieldName = "Tambah" Then
            If Not cekAllow.Contains("2") Then e.Appearance.BackColor = Color.Gray
        ElseIf e.Column.FieldName = "Edit" Then
            If Not cekAllow.Contains("3") Then e.Appearance.BackColor = Color.Gray
        ElseIf e.Column.FieldName = "Batal" Then
            If Not cekAllow.Contains("4") Then e.Appearance.BackColor = Color.Gray
        ElseIf e.Column.FieldName = "Cetak" Then
            If Not cekAllow.Contains("5") Then e.Appearance.BackColor = Color.Gray
        End If
    End Sub

    Private Sub ViewRewardGift_ShowingEditor(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles ViewProgPermissions.ShowingEditor
        Dim View As GridView = sender

        Dim cekAllow As String = View.GetFocusedRowCellValue("default_rule")
        If View.FocusedColumn.FieldName = "Baca" Then
            If Not cekAllow.Contains("1") Then e.Cancel = True
        ElseIf View.FocusedColumn.FieldName = "Tambah" Then
            If Not cekAllow.Contains("2") Then e.Cancel = True
        ElseIf View.FocusedColumn.FieldName = "Edit" Then
            If Not cekAllow.Contains("3") Then e.Cancel = True
        ElseIf View.FocusedColumn.FieldName = "Batal" Then
            If Not cekAllow.Contains("4") Then e.Cancel = True
        ElseIf View.FocusedColumn.FieldName = "Cetak" Then
            If Not cekAllow.Contains("5") Then e.Cancel = True
        End If
    End Sub
End Class