﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MembercardSettings
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.tReaderNameList = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.ButInit = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.KeyB = New System.Windows.Forms.MaskedTextBox()
        Me.KeyA = New System.Windows.Forms.MaskedTextBox()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.ButBatal = New DevExpress.XtraEditors.SimpleButton()
        Me.ButOK = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.tReaderNameList.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        Me.SuspendLayout()
        '
        'tReaderNameList
        '
        Me.tReaderNameList.Location = New System.Drawing.Point(126, 38)
        Me.tReaderNameList.Name = "tReaderNameList"
        Me.tReaderNameList.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.tReaderNameList.Properties.Appearance.Options.UseFont = True
        Me.tReaderNameList.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tReaderNameList.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.tReaderNameList.Size = New System.Drawing.Size(294, 21)
        Me.tReaderNameList.TabIndex = 0
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl9.Location = New System.Drawing.Point(21, 41)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(74, 14)
        Me.LabelControl9.TabIndex = 34
        Me.LabelControl9.Text = "Reader lists"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl2.Location = New System.Drawing.Point(21, 67)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(35, 14)
        Me.LabelControl2.TabIndex = 34
        Me.LabelControl2.Text = "Key A"
        '
        'ButInit
        '
        Me.ButInit.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButInit.Appearance.Options.UseFont = True
        Me.ButInit.Location = New System.Drawing.Point(426, 38)
        Me.ButInit.Name = "ButInit"
        Me.ButInit.Size = New System.Drawing.Size(69, 21)
        Me.ButInit.TabIndex = 1
        Me.ButInit.Text = "Init"
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.Controls.Add(Me.KeyB)
        Me.GroupControl1.Controls.Add(Me.KeyA)
        Me.GroupControl1.Controls.Add(Me.LabelControl9)
        Me.GroupControl1.Controls.Add(Me.ButInit)
        Me.GroupControl1.Controls.Add(Me.tReaderNameList)
        Me.GroupControl1.Controls.Add(Me.LabelControl4)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(513, 125)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Card Security"
        '
        'KeyB
        '
        Me.KeyB.Location = New System.Drawing.Point(126, 92)
        Me.KeyB.Mask = ">>AA AA AA AA AA AA"
        Me.KeyB.Name = "KeyB"
        Me.KeyB.Size = New System.Drawing.Size(158, 21)
        Me.KeyB.TabIndex = 3
        Me.KeyB.Text = "6E796F746965"
        '
        'KeyA
        '
        Me.KeyA.Location = New System.Drawing.Point(126, 65)
        Me.KeyA.Mask = ">>AA AA AA AA AA AA"
        Me.KeyA.Name = "KeyA"
        Me.KeyA.Size = New System.Drawing.Size(158, 21)
        Me.KeyA.TabIndex = 2
        Me.KeyA.Text = "616C62657274"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl4.Location = New System.Drawing.Point(21, 94)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(35, 14)
        Me.LabelControl4.TabIndex = 34
        Me.LabelControl4.Text = "Key B"
        '
        'ButBatal
        '
        Me.ButBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButBatal.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButBatal.Appearance.Options.UseFont = True
        Me.ButBatal.Image = Global.Laritta_Membership.My.Resources.Resources.back_32
        Me.ButBatal.Location = New System.Drawing.Point(435, 143)
        Me.ButBatal.Name = "ButBatal"
        Me.ButBatal.Size = New System.Drawing.Size(90, 40)
        Me.ButBatal.TabIndex = 2
        Me.ButBatal.Text = "&Batal"
        '
        'ButOK
        '
        Me.ButOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButOK.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButOK.Appearance.Options.UseFont = True
        Me.ButOK.Image = Global.Laritta_Membership.My.Resources.Resources.apply_32
        Me.ButOK.Location = New System.Drawing.Point(339, 143)
        Me.ButOK.Name = "ButOK"
        Me.ButOK.Size = New System.Drawing.Size(90, 40)
        Me.ButOK.TabIndex = 1
        Me.ButOK.Text = "&OK"
        '
        'MembercardSettings
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(537, 195)
        Me.Controls.Add(Me.ButBatal)
        Me.Controls.Add(Me.ButOK)
        Me.Controls.Add(Me.GroupControl1)
        Me.Name = "MembercardSettings"
        CType(Me.tReaderNameList.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tReaderNameList As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ButInit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents ButBatal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButOK As DevExpress.XtraEditors.SimpleButton
    Private WithEvents KeyB As System.Windows.Forms.MaskedTextBox
    Private WithEvents KeyA As System.Windows.Forms.MaskedTextBox
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
End Class
