﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MembercardBlock
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.DataSex = New DevExpress.XtraEditors.RadioGroup()
        Me.DataEmail = New DevExpress.XtraEditors.TextEdit()
        Me.DataPhM = New DevExpress.XtraEditors.TextEdit()
        Me.DataPhR = New DevExpress.XtraEditors.TextEdit()
        Me.DataKTP = New DevExpress.XtraEditors.TextEdit()
        Me.DataTPL = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        Me.TextTGL = New DevExpress.XtraEditors.DateEdit()
        Me.TextNama = New DevExpress.XtraEditors.TextEdit()
        Me.TextKode = New DevExpress.XtraEditors.TextEdit()
        Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl()
        Me.CekNama = New DevExpress.XtraEditors.CheckEdit()
        Me.ButCari = New DevExpress.XtraEditors.SimpleButton()
        Me.CekBirthDate = New DevExpress.XtraEditors.CheckEdit()
        Me.CekKode = New DevExpress.XtraEditors.CheckEdit()
        Me.ButBatal = New DevExpress.XtraEditors.SimpleButton()
        Me.ButSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.ButReset = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.DataSex.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataEmail.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataPhM.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataPhR.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataKTP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataTPL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextTGL.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextTGL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextNama.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextKode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.CekNama.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CekBirthDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CekKode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.Controls.Add(Me.DataSex)
        Me.GroupControl1.Controls.Add(Me.DataEmail)
        Me.GroupControl1.Controls.Add(Me.DataPhM)
        Me.GroupControl1.Controls.Add(Me.DataPhR)
        Me.GroupControl1.Controls.Add(Me.DataKTP)
        Me.GroupControl1.Controls.Add(Me.DataTPL)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Controls.Add(Me.LabelControl4)
        Me.GroupControl1.Controls.Add(Me.LabelControl13)
        Me.GroupControl1.Controls.Add(Me.LabelControl14)
        Me.GroupControl1.Controls.Add(Me.LabelControl22)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 177)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(416, 206)
        Me.GroupControl1.TabIndex = 1
        Me.GroupControl1.Text = "Data Member"
        '
        'DataSex
        '
        Me.DataSex.EditValue = False
        Me.DataSex.Location = New System.Drawing.Point(158, 84)
        Me.DataSex.Name = "DataSex"
        Me.DataSex.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataSex.Properties.Appearance.Options.UseFont = True
        Me.DataSex.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat
        Me.DataSex.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(False, "Pria"), New DevExpress.XtraEditors.Controls.RadioGroupItem(True, "Wanita")})
        Me.DataSex.Properties.ReadOnly = True
        Me.DataSex.Size = New System.Drawing.Size(192, 24)
        Me.DataSex.TabIndex = 8
        Me.DataSex.TabStop = False
        '
        'DataEmail
        '
        Me.DataEmail.Location = New System.Drawing.Point(158, 168)
        Me.DataEmail.Name = "DataEmail"
        Me.DataEmail.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataEmail.Properties.Appearance.Options.UseFont = True
        Me.DataEmail.Properties.Mask.EditMask = "[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}"
        Me.DataEmail.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.DataEmail.Properties.ReadOnly = True
        Me.DataEmail.Size = New System.Drawing.Size(192, 21)
        Me.DataEmail.TabIndex = 11
        Me.DataEmail.TabStop = False
        '
        'DataPhM
        '
        Me.DataPhM.Location = New System.Drawing.Point(158, 141)
        Me.DataPhM.Name = "DataPhM"
        Me.DataPhM.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataPhM.Properties.Appearance.Options.UseFont = True
        Me.DataPhM.Properties.Mask.EditMask = "[0-9,]+"
        Me.DataPhM.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.DataPhM.Properties.ReadOnly = True
        Me.DataPhM.Size = New System.Drawing.Size(192, 21)
        Me.DataPhM.TabIndex = 10
        Me.DataPhM.TabStop = False
        '
        'DataPhR
        '
        Me.DataPhR.Location = New System.Drawing.Point(158, 114)
        Me.DataPhR.Name = "DataPhR"
        Me.DataPhR.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataPhR.Properties.Appearance.Options.UseFont = True
        Me.DataPhR.Properties.Mask.EditMask = "[0-9,]+"
        Me.DataPhR.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.DataPhR.Properties.ReadOnly = True
        Me.DataPhR.Size = New System.Drawing.Size(192, 21)
        Me.DataPhR.TabIndex = 9
        Me.DataPhR.TabStop = False
        '
        'DataKTP
        '
        Me.DataKTP.Location = New System.Drawing.Point(158, 30)
        Me.DataKTP.Name = "DataKTP"
        Me.DataKTP.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataKTP.Properties.Appearance.Options.UseFont = True
        Me.DataKTP.Properties.Mask.EditMask = "[0-9.]+"
        Me.DataKTP.Properties.ReadOnly = True
        Me.DataKTP.Size = New System.Drawing.Size(192, 21)
        Me.DataKTP.TabIndex = 6
        Me.DataKTP.TabStop = False
        '
        'DataTPL
        '
        Me.DataTPL.Location = New System.Drawing.Point(158, 57)
        Me.DataTPL.Name = "DataTPL"
        Me.DataTPL.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataTPL.Properties.Appearance.Options.UseFont = True
        Me.DataTPL.Properties.Mask.EditMask = "[A-Z ]+"
        Me.DataTPL.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.DataTPL.Properties.ReadOnly = True
        Me.DataTPL.Size = New System.Drawing.Size(192, 21)
        Me.DataTPL.TabIndex = 7
        Me.DataTPL.TabStop = False
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl2.Location = New System.Drawing.Point(40, 60)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(78, 14)
        Me.LabelControl2.TabIndex = 1
        Me.LabelControl2.Text = "Tempat lahir"
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl3.Location = New System.Drawing.Point(40, 33)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(44, 14)
        Me.LabelControl3.TabIndex = 0
        Me.LabelControl3.Text = "No KTP"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl4.Location = New System.Drawing.Point(40, 87)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(83, 14)
        Me.LabelControl4.TabIndex = 2
        Me.LabelControl4.Text = "Jenis kelamin"
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl13.Location = New System.Drawing.Point(40, 117)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(70, 14)
        Me.LabelControl13.TabIndex = 3
        Me.LabelControl13.Text = "Telp rumah"
        '
        'LabelControl14
        '
        Me.LabelControl14.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl14.Location = New System.Drawing.Point(40, 144)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(47, 14)
        Me.LabelControl14.TabIndex = 4
        Me.LabelControl14.Text = "Telp HP"
        '
        'LabelControl22
        '
        Me.LabelControl22.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl22.Location = New System.Drawing.Point(40, 171)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(33, 14)
        Me.LabelControl22.TabIndex = 5
        Me.LabelControl22.Text = "Email"
        '
        'TextTGL
        '
        Me.TextTGL.EditValue = Nothing
        Me.TextTGL.Enabled = False
        Me.TextTGL.Location = New System.Drawing.Point(158, 59)
        Me.TextTGL.Name = "TextTGL"
        Me.TextTGL.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextTGL.Properties.Appearance.Options.UseFont = True
        Me.TextTGL.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.TextTGL.Properties.Mask.EditMask = "dd MMMM yyyy"
        Me.TextTGL.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextTGL.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.TextTGL.Size = New System.Drawing.Size(253, 21)
        Me.TextTGL.TabIndex = 3
        '
        'TextNama
        '
        Me.TextNama.Enabled = False
        Me.TextNama.Location = New System.Drawing.Point(158, 86)
        Me.TextNama.Name = "TextNama"
        Me.TextNama.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextNama.Properties.Appearance.Options.UseFont = True
        Me.TextNama.Properties.Mask.EditMask = "[A-Z() .,]+"
        Me.TextNama.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextNama.Size = New System.Drawing.Size(253, 21)
        Me.TextNama.TabIndex = 5
        '
        'TextKode
        '
        Me.TextKode.Location = New System.Drawing.Point(158, 32)
        Me.TextKode.Name = "TextKode"
        Me.TextKode.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.TextKode.Properties.Appearance.Options.UseFont = True
        Me.TextKode.Properties.Mask.EditMask = "[0-9]{8} [0-9]{4}"
        Me.TextKode.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextKode.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextKode.Properties.MaxLength = 50
        Me.TextKode.Size = New System.Drawing.Size(253, 21)
        Me.TextKode.TabIndex = 1
        '
        'GroupControl4
        '
        Me.GroupControl4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl4.AppearanceCaption.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.GroupControl4.AppearanceCaption.Options.UseFont = True
        Me.GroupControl4.Controls.Add(Me.CekNama)
        Me.GroupControl4.Controls.Add(Me.TextTGL)
        Me.GroupControl4.Controls.Add(Me.ButCari)
        Me.GroupControl4.Controls.Add(Me.CekBirthDate)
        Me.GroupControl4.Controls.Add(Me.CekKode)
        Me.GroupControl4.Controls.Add(Me.TextKode)
        Me.GroupControl4.Controls.Add(Me.TextNama)
        Me.GroupControl4.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(416, 159)
        Me.GroupControl4.TabIndex = 0
        Me.GroupControl4.Text = "Pencarian"
        '
        'CekNama
        '
        Me.CekNama.Location = New System.Drawing.Point(18, 86)
        Me.CekNama.Name = "CekNama"
        Me.CekNama.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.CekNama.Properties.Appearance.Options.UseFont = True
        Me.CekNama.Properties.Caption = "Nama lengkap"
        Me.CekNama.Size = New System.Drawing.Size(134, 19)
        Me.CekNama.TabIndex = 4
        '
        'ButCari
        '
        Me.ButCari.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButCari.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButCari.Appearance.Options.UseFont = True
        Me.ButCari.Image = Global.Laritta_Membership.My.Resources.Resources.refresh_32
        Me.ButCari.Location = New System.Drawing.Point(321, 113)
        Me.ButCari.Name = "ButCari"
        Me.ButCari.Size = New System.Drawing.Size(90, 40)
        Me.ButCari.TabIndex = 6
        Me.ButCari.Text = "&Cari"
        '
        'CekBirthDate
        '
        Me.CekBirthDate.Location = New System.Drawing.Point(18, 59)
        Me.CekBirthDate.Name = "CekBirthDate"
        Me.CekBirthDate.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.CekBirthDate.Properties.Appearance.Options.UseFont = True
        Me.CekBirthDate.Properties.Caption = "Tanggal lahir"
        Me.CekBirthDate.Size = New System.Drawing.Size(134, 19)
        Me.CekBirthDate.TabIndex = 2
        '
        'CekKode
        '
        Me.CekKode.EditValue = True
        Me.CekKode.Location = New System.Drawing.Point(18, 32)
        Me.CekKode.Name = "CekKode"
        Me.CekKode.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.CekKode.Properties.Appearance.Options.UseFont = True
        Me.CekKode.Properties.Caption = "Kode Member"
        Me.CekKode.Size = New System.Drawing.Size(134, 19)
        Me.CekKode.TabIndex = 0
        '
        'ButBatal
        '
        Me.ButBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButBatal.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButBatal.Appearance.Options.UseFont = True
        Me.ButBatal.Image = Global.Laritta_Membership.My.Resources.Resources.back_32
        Me.ButBatal.Location = New System.Drawing.Point(338, 389)
        Me.ButBatal.Name = "ButBatal"
        Me.ButBatal.Size = New System.Drawing.Size(90, 40)
        Me.ButBatal.TabIndex = 4
        Me.ButBatal.Text = "&Batal"
        '
        'ButSimpan
        '
        Me.ButSimpan.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButSimpan.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButSimpan.Appearance.Options.UseFont = True
        Me.ButSimpan.Image = Global.Laritta_Membership.My.Resources.Resources.apply_32
        Me.ButSimpan.Location = New System.Drawing.Point(242, 389)
        Me.ButSimpan.Name = "ButSimpan"
        Me.ButSimpan.Size = New System.Drawing.Size(90, 40)
        Me.ButSimpan.TabIndex = 3
        Me.ButSimpan.Text = "&Simpan"
        '
        'ButReset
        '
        Me.ButReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButReset.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButReset.Appearance.Options.UseFont = True
        Me.ButReset.Image = Global.Laritta_Membership.My.Resources.Resources.reset_32
        Me.ButReset.Location = New System.Drawing.Point(12, 389)
        Me.ButReset.Name = "ButReset"
        Me.ButReset.Size = New System.Drawing.Size(90, 40)
        Me.ButReset.TabIndex = 2
        Me.ButReset.Text = "&Reset"
        '
        'MembercardBlock
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(440, 441)
        Me.Controls.Add(Me.ButReset)
        Me.Controls.Add(Me.ButBatal)
        Me.Controls.Add(Me.ButSimpan)
        Me.Controls.Add(Me.GroupControl4)
        Me.Controls.Add(Me.GroupControl1)
        Me.Name = "MembercardBlock"
        Me.Text = "MembercardBlock"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.DataSex.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataEmail.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataPhM.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataPhR.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataKTP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataTPL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextTGL.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextTGL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextNama.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextKode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        CType(Me.CekNama.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CekBirthDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CekKode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents DataKTP As DevExpress.XtraEditors.TextEdit
    Friend WithEvents DataSex As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents TextTGL As DevExpress.XtraEditors.DateEdit
    Friend WithEvents DataEmail As DevExpress.XtraEditors.TextEdit
    Friend WithEvents DataPhM As DevExpress.XtraEditors.TextEdit
    Friend WithEvents DataPhR As DevExpress.XtraEditors.TextEdit
    Friend WithEvents DataTPL As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextNama As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextKode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents ButBatal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButCari As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents CekNama As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CekBirthDate As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CekKode As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ButReset As DevExpress.XtraEditors.SimpleButton
End Class
