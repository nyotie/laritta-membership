﻿Public Class WhatBranchAmI

    Private Sub WhatBranchAmI_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub WhatBranchAmI_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        LoadSprite.CloseLoading()
        DaftarCabang.Focus()
    End Sub

    Private Sub WhatBranchAmI_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()
        Get_DataBranch()

        If Not selmaster_id = 0 Then
            DaftarCabang.EditValue = id_cabang
        Else
            DaftarCabang.EditValue = Nothing
        End If
    End Sub

    Private Sub ButSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSimpan.Click
        If Not beforeSave() Then Return
        Try
            LoadSprite.ShowLoading()
            FileSettingsHelper.WriteSettings(Application.StartupPath & "\setting.ini", "Cabang", "id_cabang", DaftarCabang.EditValue)

            MsgInfo("Data berhasil disimpan, tekan OK untuk melanjutkan.")
        Catch ex As Exception
            MsgErrorDev()
        End Try
        LoadSprite.CloseLoading()
        Close()
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Function beforeSave() As Boolean
        If DaftarCabang.EditValue = Nothing Then
            MsgWarning("Pilih salah satu cabang!")
            Return False
        End If

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        Return True
    End Function

    Private Sub Get_DataBranch()
        If Not xSet.Tables("DataBranch") Is Nothing Then xSet.Tables("DataBranch").Clear()
        SQLquery = "SELECT idCabang ID, isProduksi Produksi, namaCabang Nama, alamatCabang Alamat, telpCabang Telp FROM mcabang WHERE inactive=0 ORDER BY idCabang, namaCabang"
        ExDb.ExecQuery(0, SQLquery, xSet, "DataBranch")
        DaftarCabang.Properties.DataSource = xSet.Tables("DataBranch").DefaultView

        DaftarCabang.Properties.NullText = ""
        DaftarCabang.Properties.ValueMember = "ID"
        DaftarCabang.Properties.DisplayMember = "Nama"
        DaftarCabang.Properties.ShowClearButton = False
        DaftarCabang.Properties.PopulateViewColumns()

        ViewDaftarCabang.Columns("ID").Visible = False
        ViewDaftarCabang.Columns("Produksi").Visible = False

        For Each coll As DataColumn In xSet.Tables("DataBranch").Columns
            ViewDaftarCabang.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Next

        ViewDaftarCabang.BestFitColumns()
    End Sub

End Class