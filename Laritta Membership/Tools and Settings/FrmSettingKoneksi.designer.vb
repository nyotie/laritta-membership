﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSettingKoneksi
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmSettingKoneksi))
        Me.CmdKeluar = New DevExpress.XtraEditors.SimpleButton()
        Me.CmdSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelLocal = New DevExpress.XtraEditors.GroupControl()
        Me.LocalServer = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LocalDB = New System.Windows.Forms.ComboBox()
        Me.LocalPWD = New DevExpress.XtraEditors.TextEdit()
        Me.LocalUID = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.LineShape1 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.isMultiserver = New DevExpress.XtraEditors.CheckEdit()
        Me.PanelOnline = New DevExpress.XtraEditors.GroupControl()
        Me.OnlineServer = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.OnlineDB = New System.Windows.Forms.ComboBox()
        Me.OnlinePWD = New DevExpress.XtraEditors.TextEdit()
        Me.OnlineUID = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.ShapeContainer2 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.LineShape2 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        CType(Me.PanelLocal, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelLocal.SuspendLayout()
        CType(Me.LocalServer.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LocalPWD.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LocalUID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.isMultiserver.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelOnline, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelOnline.SuspendLayout()
        CType(Me.OnlineServer.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.OnlinePWD.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.OnlineUID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CmdKeluar
        '
        Me.CmdKeluar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CmdKeluar.Appearance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CmdKeluar.Appearance.Options.UseFont = True
        Me.CmdKeluar.Image = CType(resources.GetObject("CmdKeluar.Image"), System.Drawing.Image)
        Me.CmdKeluar.Location = New System.Drawing.Point(618, 233)
        Me.CmdKeluar.Name = "CmdKeluar"
        Me.CmdKeluar.Size = New System.Drawing.Size(76, 27)
        Me.CmdKeluar.TabIndex = 4
        Me.CmdKeluar.Text = "Cancel"
        '
        'CmdSimpan
        '
        Me.CmdSimpan.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CmdSimpan.Appearance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CmdSimpan.Appearance.Options.UseFont = True
        Me.CmdSimpan.Image = CType(resources.GetObject("CmdSimpan.Image"), System.Drawing.Image)
        Me.CmdSimpan.Location = New System.Drawing.Point(536, 233)
        Me.CmdSimpan.Name = "CmdSimpan"
        Me.CmdSimpan.Size = New System.Drawing.Size(76, 27)
        Me.CmdSimpan.TabIndex = 3
        Me.CmdSimpan.Text = "Connect"
        '
        'PanelLocal
        '
        Me.PanelLocal.AppearanceCaption.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanelLocal.AppearanceCaption.Options.UseFont = True
        Me.PanelLocal.Controls.Add(Me.LocalServer)
        Me.PanelLocal.Controls.Add(Me.LabelControl2)
        Me.PanelLocal.Controls.Add(Me.LabelControl1)
        Me.PanelLocal.Controls.Add(Me.LabelControl7)
        Me.PanelLocal.Controls.Add(Me.LabelControl3)
        Me.PanelLocal.Controls.Add(Me.LocalDB)
        Me.PanelLocal.Controls.Add(Me.LocalPWD)
        Me.PanelLocal.Controls.Add(Me.LocalUID)
        Me.PanelLocal.Controls.Add(Me.LabelControl6)
        Me.PanelLocal.Controls.Add(Me.ShapeContainer1)
        Me.PanelLocal.Location = New System.Drawing.Point(12, 37)
        Me.PanelLocal.Name = "PanelLocal"
        Me.PanelLocal.Size = New System.Drawing.Size(338, 186)
        Me.PanelLocal.TabIndex = 1
        Me.PanelLocal.Text = "Local Connection"
        '
        'LocalServer
        '
        Me.LocalServer.Location = New System.Drawing.Point(61, 34)
        Me.LocalServer.Name = "LocalServer"
        Me.LocalServer.Size = New System.Drawing.Size(269, 20)
        Me.LocalServer.TabIndex = 2
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl2.Location = New System.Drawing.Point(61, 97)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(53, 15)
        Me.LabelControl2.TabIndex = 4
        Me.LabelControl2.Text = "Username"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Location = New System.Drawing.Point(61, 153)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(50, 15)
        Me.LabelControl1.TabIndex = 8
        Me.LabelControl1.Text = "Database"
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl7.Location = New System.Drawing.Point(61, 124)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(52, 15)
        Me.LabelControl7.TabIndex = 6
        Me.LabelControl7.Text = "Password"
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl3.Location = New System.Drawing.Point(11, 76)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(86, 15)
        Me.LabelControl3.TabIndex = 3
        Me.LabelControl3.Text = "Connect Using :"
        '
        'LocalDB
        '
        Me.LocalDB.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LocalDB.FormattingEnabled = True
        Me.LocalDB.Location = New System.Drawing.Point(130, 149)
        Me.LocalDB.Name = "LocalDB"
        Me.LocalDB.Size = New System.Drawing.Size(193, 23)
        Me.LocalDB.TabIndex = 9
        '
        'LocalPWD
        '
        Me.LocalPWD.Location = New System.Drawing.Point(130, 121)
        Me.LocalPWD.Name = "LocalPWD"
        Me.LocalPWD.Properties.Appearance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LocalPWD.Properties.Appearance.Options.UseFont = True
        Me.LocalPWD.Properties.MaxLength = 50
        Me.LocalPWD.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.LocalPWD.Size = New System.Drawing.Size(132, 21)
        Me.LocalPWD.TabIndex = 7
        '
        'LocalUID
        '
        Me.LocalUID.Location = New System.Drawing.Point(130, 94)
        Me.LocalUID.Name = "LocalUID"
        Me.LocalUID.Properties.Appearance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LocalUID.Properties.Appearance.Options.UseFont = True
        Me.LocalUID.Properties.MaxLength = 50
        Me.LocalUID.Size = New System.Drawing.Size(132, 21)
        Me.LocalUID.TabIndex = 5
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl6.Location = New System.Drawing.Point(11, 36)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(34, 15)
        Me.LabelControl6.TabIndex = 1
        Me.LabelControl6.Text = "Server"
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(2, 23)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape1})
        Me.ShapeContainer1.Size = New System.Drawing.Size(334, 161)
        Me.ShapeContainer1.TabIndex = 0
        Me.ShapeContainer1.TabStop = False
        '
        'LineShape1
        '
        Me.LineShape1.BorderColor = System.Drawing.SystemColors.ButtonShadow
        Me.LineShape1.Name = "LineShape1"
        Me.LineShape1.X1 = 9
        Me.LineShape1.X2 = 327
        Me.LineShape1.Y1 = 41
        Me.LineShape1.Y2 = 41
        '
        'isMultiserver
        '
        Me.isMultiserver.EditValue = True
        Me.isMultiserver.Location = New System.Drawing.Point(12, 12)
        Me.isMultiserver.Name = "isMultiserver"
        Me.isMultiserver.Properties.Caption = "MultiServer"
        Me.isMultiserver.Size = New System.Drawing.Size(75, 19)
        Me.isMultiserver.TabIndex = 0
        '
        'PanelOnline
        '
        Me.PanelOnline.AppearanceCaption.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanelOnline.AppearanceCaption.Options.UseFont = True
        Me.PanelOnline.Controls.Add(Me.OnlineServer)
        Me.PanelOnline.Controls.Add(Me.LabelControl4)
        Me.PanelOnline.Controls.Add(Me.OnlineDB)
        Me.PanelOnline.Controls.Add(Me.OnlinePWD)
        Me.PanelOnline.Controls.Add(Me.OnlineUID)
        Me.PanelOnline.Controls.Add(Me.LabelControl12)
        Me.PanelOnline.Controls.Add(Me.LabelControl14)
        Me.PanelOnline.Controls.Add(Me.LabelControl16)
        Me.PanelOnline.Controls.Add(Me.LabelControl18)
        Me.PanelOnline.Controls.Add(Me.ShapeContainer2)
        Me.PanelOnline.Location = New System.Drawing.Point(356, 37)
        Me.PanelOnline.Name = "PanelOnline"
        Me.PanelOnline.Size = New System.Drawing.Size(338, 186)
        Me.PanelOnline.TabIndex = 2
        Me.PanelOnline.Text = "Online Connection"
        '
        'OnlineServer
        '
        Me.OnlineServer.Location = New System.Drawing.Point(54, 34)
        Me.OnlineServer.Name = "OnlineServer"
        Me.OnlineServer.Size = New System.Drawing.Size(276, 20)
        Me.OnlineServer.TabIndex = 2
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl4.Location = New System.Drawing.Point(11, 76)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(86, 15)
        Me.LabelControl4.TabIndex = 3
        Me.LabelControl4.Text = "Connect Using :"
        '
        'OnlineDB
        '
        Me.OnlineDB.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OnlineDB.FormattingEnabled = True
        Me.OnlineDB.Location = New System.Drawing.Point(124, 148)
        Me.OnlineDB.Name = "OnlineDB"
        Me.OnlineDB.Size = New System.Drawing.Size(193, 23)
        Me.OnlineDB.TabIndex = 9
        '
        'OnlinePWD
        '
        Me.OnlinePWD.Location = New System.Drawing.Point(124, 121)
        Me.OnlinePWD.Name = "OnlinePWD"
        Me.OnlinePWD.Properties.Appearance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OnlinePWD.Properties.Appearance.Options.UseFont = True
        Me.OnlinePWD.Properties.MaxLength = 50
        Me.OnlinePWD.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.OnlinePWD.Size = New System.Drawing.Size(132, 21)
        Me.OnlinePWD.TabIndex = 7
        '
        'OnlineUID
        '
        Me.OnlineUID.Location = New System.Drawing.Point(124, 94)
        Me.OnlineUID.Name = "OnlineUID"
        Me.OnlineUID.Properties.Appearance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OnlineUID.Properties.Appearance.Options.UseFont = True
        Me.OnlineUID.Properties.MaxLength = 50
        Me.OnlineUID.Size = New System.Drawing.Size(132, 21)
        Me.OnlineUID.TabIndex = 5
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl12.Location = New System.Drawing.Point(54, 124)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(52, 15)
        Me.LabelControl12.TabIndex = 6
        Me.LabelControl12.Text = "Password"
        '
        'LabelControl14
        '
        Me.LabelControl14.Appearance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl14.Location = New System.Drawing.Point(54, 97)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(53, 15)
        Me.LabelControl14.TabIndex = 4
        Me.LabelControl14.Text = "Username"
        '
        'LabelControl16
        '
        Me.LabelControl16.Appearance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl16.Location = New System.Drawing.Point(54, 152)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(50, 15)
        Me.LabelControl16.TabIndex = 8
        Me.LabelControl16.Text = "Database"
        '
        'LabelControl18
        '
        Me.LabelControl18.Appearance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl18.Location = New System.Drawing.Point(11, 36)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(34, 15)
        Me.LabelControl18.TabIndex = 1
        Me.LabelControl18.Text = "Server"
        '
        'ShapeContainer2
        '
        Me.ShapeContainer2.Location = New System.Drawing.Point(2, 23)
        Me.ShapeContainer2.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer2.Name = "ShapeContainer2"
        Me.ShapeContainer2.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape2})
        Me.ShapeContainer2.Size = New System.Drawing.Size(334, 161)
        Me.ShapeContainer2.TabIndex = 0
        Me.ShapeContainer2.TabStop = False
        '
        'LineShape2
        '
        Me.LineShape2.BorderColor = System.Drawing.SystemColors.ButtonShadow
        Me.LineShape2.Name = "LineShape1"
        Me.LineShape2.X1 = 9
        Me.LineShape2.X2 = 327
        Me.LineShape2.Y1 = 41
        Me.LineShape2.Y2 = 41
        '
        'FrmSettingKoneksi
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(706, 269)
        Me.ControlBox = False
        Me.Controls.Add(Me.isMultiserver)
        Me.Controls.Add(Me.CmdKeluar)
        Me.Controls.Add(Me.CmdSimpan)
        Me.Controls.Add(Me.PanelOnline)
        Me.Controls.Add(Me.PanelLocal)
        Me.Name = "FrmSettingKoneksi"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Database"
        CType(Me.PanelLocal, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelLocal.ResumeLayout(False)
        Me.PanelLocal.PerformLayout()
        CType(Me.LocalServer.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LocalPWD.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LocalUID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.isMultiserver.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelOnline, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelOnline.ResumeLayout(False)
        Me.PanelOnline.PerformLayout()
        CType(Me.OnlineServer.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.OnlinePWD.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.OnlineUID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents CmdKeluar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents CmdSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelLocal As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LocalPWD As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LocalUID As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LocalDB As System.Windows.Forms.ComboBox
    Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents LineShape1 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents isMultiserver As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents PanelOnline As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents OnlineDB As System.Windows.Forms.ComboBox
    Friend WithEvents OnlinePWD As DevExpress.XtraEditors.TextEdit
    Friend WithEvents OnlineUID As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ShapeContainer2 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents LineShape2 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LocalServer As DevExpress.XtraEditors.TextEdit
    Friend WithEvents OnlineServer As DevExpress.XtraEditors.TextEdit
End Class
