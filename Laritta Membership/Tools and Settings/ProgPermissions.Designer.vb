﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ProgPermissions
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LookupSecurityGroup = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.ViewSecurityGroup = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.DaftarProgPermissions = New DevExpress.XtraGrid.GridControl()
        Me.ViewProgPermissions = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.CheckMe = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.ButBatal = New DevExpress.XtraEditors.SimpleButton()
        Me.ButSimpan = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.LookupSecurityGroup.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewSecurityGroup, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DaftarProgPermissions, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewProgPermissions, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckMe, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        Me.SuspendLayout()
        '
        'LookupSecurityGroup
        '
        Me.LookupSecurityGroup.Location = New System.Drawing.Point(117, 32)
        Me.LookupSecurityGroup.Name = "LookupSecurityGroup"
        Me.LookupSecurityGroup.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LookupSecurityGroup.Properties.Appearance.Options.UseFont = True
        Me.LookupSecurityGroup.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.LookupSecurityGroup.Properties.NullText = ""
        Me.LookupSecurityGroup.Properties.View = Me.ViewSecurityGroup
        Me.LookupSecurityGroup.Size = New System.Drawing.Size(239, 21)
        Me.LookupSecurityGroup.TabIndex = 1
        '
        'ViewSecurityGroup
        '
        Me.ViewSecurityGroup.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.ViewSecurityGroup.Name = "ViewSecurityGroup"
        Me.ViewSecurityGroup.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.ViewSecurityGroup.OptionsView.ShowGroupPanel = False
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl2.Location = New System.Drawing.Point(19, 35)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(92, 14)
        Me.LabelControl2.TabIndex = 0
        Me.LabelControl2.Text = "Security Group"
        '
        'DaftarProgPermissions
        '
        Me.DaftarProgPermissions.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DaftarProgPermissions.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.DaftarProgPermissions.Location = New System.Drawing.Point(5, 59)
        Me.DaftarProgPermissions.MainView = Me.ViewProgPermissions
        Me.DaftarProgPermissions.Name = "DaftarProgPermissions"
        Me.DaftarProgPermissions.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.CheckMe})
        Me.DaftarProgPermissions.Size = New System.Drawing.Size(758, 319)
        Me.DaftarProgPermissions.TabIndex = 2
        Me.DaftarProgPermissions.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.ViewProgPermissions})
        '
        'ViewProgPermissions
        '
        Me.ViewProgPermissions.Appearance.HeaderPanel.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ViewProgPermissions.Appearance.HeaderPanel.Options.UseFont = True
        Me.ViewProgPermissions.Appearance.Row.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ViewProgPermissions.Appearance.Row.Options.UseFont = True
        Me.ViewProgPermissions.ColumnPanelRowHeight = 40
        Me.ViewProgPermissions.GridControl = Me.DaftarProgPermissions
        Me.ViewProgPermissions.Name = "ViewProgPermissions"
        Me.ViewProgPermissions.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewProgPermissions.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewProgPermissions.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewProgPermissions.OptionsCustomization.AllowColumnMoving = False
        Me.ViewProgPermissions.OptionsCustomization.AllowColumnResizing = False
        Me.ViewProgPermissions.OptionsCustomization.AllowFilter = False
        Me.ViewProgPermissions.OptionsCustomization.AllowGroup = False
        Me.ViewProgPermissions.OptionsCustomization.AllowQuickHideColumns = False
        Me.ViewProgPermissions.OptionsCustomization.AllowSort = False
        Me.ViewProgPermissions.OptionsView.ShowGroupPanel = False
        Me.ViewProgPermissions.RowHeight = 30
        '
        'CheckMe
        '
        Me.CheckMe.AutoHeight = False
        Me.CheckMe.Name = "CheckMe"
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.DaftarProgPermissions)
        Me.GroupControl1.Controls.Add(Me.LookupSecurityGroup)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(768, 383)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Permission Settings"
        '
        'ButBatal
        '
        Me.ButBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButBatal.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButBatal.Appearance.Options.UseFont = True
        Me.ButBatal.Image = Global.Laritta_Membership.My.Resources.Resources.back_32
        Me.ButBatal.Location = New System.Drawing.Point(690, 401)
        Me.ButBatal.Name = "ButBatal"
        Me.ButBatal.Size = New System.Drawing.Size(90, 40)
        Me.ButBatal.TabIndex = 2
        Me.ButBatal.Text = "&Batal"
        '
        'ButSimpan
        '
        Me.ButSimpan.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButSimpan.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButSimpan.Appearance.Options.UseFont = True
        Me.ButSimpan.Image = Global.Laritta_Membership.My.Resources.Resources.apply_32
        Me.ButSimpan.Location = New System.Drawing.Point(594, 401)
        Me.ButSimpan.Name = "ButSimpan"
        Me.ButSimpan.Size = New System.Drawing.Size(90, 40)
        Me.ButSimpan.TabIndex = 1
        Me.ButSimpan.Text = "&Simpan"
        '
        'ProgPermissions
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(792, 453)
        Me.Controls.Add(Me.ButBatal)
        Me.Controls.Add(Me.ButSimpan)
        Me.Controls.Add(Me.GroupControl1)
        Me.Name = "ProgPermissions"
        Me.Text = "ProgPermissions"
        CType(Me.LookupSecurityGroup.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewSecurityGroup, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DaftarProgPermissions, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewProgPermissions, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckMe, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LookupSecurityGroup As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents ViewSecurityGroup As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DaftarProgPermissions As DevExpress.XtraGrid.GridControl
    Friend WithEvents ViewProgPermissions As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents CheckMe As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents ButBatal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButSimpan As DevExpress.XtraEditors.SimpleButton
End Class
