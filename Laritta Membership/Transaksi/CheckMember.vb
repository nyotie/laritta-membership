﻿Imports DevExpress.Utils
Imports System.ComponentModel
Imports Laritta_Membership.RFID_Tools

Public Class CheckMember
    Private ReadOnly bw As BackgroundWorker = New BackgroundWorker()

    Public CurrentMember As Integer
    Public CurrentKodeKartuMember As String

    Dim MSCount As Integer
    Dim ReaderIsBusy As Boolean
    Dim CardUID As String

    Private Sub CheckMember_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("ThisMemberData")
        xSet.Tables.Remove("ThisMemberPromo")
        xSet.Tables.Remove("ThisMemberGift")

        If bw.IsBusy Then bw.CancelAsync()
        bw.Dispose()

        ReaderStarter()
        MyACR.ClearLCD()
        MyACR.Backlight(False)
        MyACR.Disconnect()
        MyCard.Disconnect()

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub CheckMember_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        bw.WorkerSupportsCancellation = True
        bw.WorkerReportsProgress = True
        AddHandler bw.DoWork, AddressOf bw_DoWork
        AddHandler bw.ProgressChanged, AddressOf bw_ProgressChanged
        AddHandler bw.RunWorkerCompleted, AddressOf bw_RunWorkerCompleted

        Try
            If Not KeySetupCheck() Then
                CardProgressStatus.Caption = "Membercard settings hasn't defined!"
                Return
            End If

            ReaderStarter()
            MyACR.Backlight(True)
            MyACR.DisplayLCD(1, "    Welcome")
            MyACR.DisplayLCD(2, " Laritta Bakery")
            MyACR.Disconnect()

            bw.RunWorkerAsync()
        Catch ex As Exception
            CardProgressStatus.Caption = "Reader not found!"
        End Try
    End Sub

    Private Sub CheckMember_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        ButUpgradeMember.Enabled = xSet.Tables("User_ProgPermisisons").Select("permission_id=15")(0).Item("allow_read")
        ButPemberianPointStamp.Enabled = xSet.Tables("User_ProgPermisisons").Select("permission_id=16")(0).Item("allow_read")
        'ButPenukaranPointStamp.Enabled = xSet.Tables("User_ProgPermisisons").Select("permission_id=17")(0).Item("allow_read")
        'ButPenukaranPromoGift.Enabled = xSet.Tables("User_ProgPermisisons").Select("permission_id=18")(0).Item("allow_read")
        ButBlessing.Enabled = xSet.Tables("User_ProgPermisisons").Select("permission_id=19")(0).Item("allow_read")
        ButVoidPoint.Enabled = xSet.Tables("User_ProgPermisisons").Select("permission_id=20")(0).Item("allow_read")
        ButVoidGift.Enabled = xSet.Tables("User_ProgPermisisons").Select("permission_id=21")(0).Item("allow_read")
    End Sub

    Private Sub KodeMember_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles KodeMember.EditValueChanged
        If KodeMember.EditValue.ToString.Length < 11 Then Return

        If KodeMember.EditValue.ToString.Length = 13 And CurrentMember = 0 Then
            Try
                If Not xSet.Tables("ThisMemberData") Is Nothing Then xSet.Tables("ThisMemberData").Clear()
                SQLquery = String.Format("SELECT a.idCust, b.namaKategoriCust, a.id_tipemembership, c.nama_tipemembership, c.level_tipemembership, namaCust, BirthDate, homeAddress, d.nama_kota, Phone, PhoneMobile, Pekerjaan, kode_kartu, kode_kartuUID, " & _
                                                 "emoney, points, stamp, sum_trans_outlet, sum_trans_order, sum_buy_outlet, sum_buy_order, a.notes " & _
                                                 "FROM mcustomer a INNER JOIN mkategoricust b ON a.idKategoriCust=b.idKategoriCust INNER JOIN mbsm_tipemembership c ON a.id_tipemembership=c.id_tipemembership INNER JOIN mbsm_lockota d ON a.id_kota=d.id_kota " & _
                                                 "WHERE a.kode_member='{0}'", KodeMember.Text.Replace(" ", ""))
                ExDb.ExecQuery(1, SQLquery, xSet, "ThisMemberData")

                If xSet.Tables("ThisMemberData").Rows.Count < 1 Then Return

                XtraTabControl1.SelectedTabPageIndex = 0
                If Not xSet.Tables("ThisMemberPromo") Is Nothing Then xSet.Tables.Remove("ThisMemberPromo")
                If Not xSet.Tables("ThisMemberGift") Is Nothing Then xSet.Tables.Remove("ThisMemberGift")

                CurrentMember = xSet.Tables("ThisMemberData").Rows(0).Item("idCust")
                CurrentKodeKartuMember = xSet.Tables("ThisMemberData").Rows(0).Item("kode_kartu")

                TipeMember.EditValue = xSet.Tables("ThisMemberData").Rows(0).Item("nama_tipemembership")
                KategoriMember.EditValue = xSet.Tables("ThisMemberData").Rows(0).Item("namaKategoriCust")

                CustName.EditValue = xSet.Tables("ThisMemberData").Rows(0).Item("namaCust")
                CustBday.EditValue = xSet.Tables("ThisMemberData").Rows(0).Item("BirthDate")
                CustAddress.EditValue = xSet.Tables("ThisMemberData").Rows(0).Item("homeAddress")
                CustCity.EditValue = xSet.Tables("ThisMemberData").Rows(0).Item("nama_kota")
                CustPhoneHome.EditValue = xSet.Tables("ThisMemberData").Rows(0).Item("Phone")
                CustPhoneMobile.EditValue = xSet.Tables("ThisMemberData").Rows(0).Item("PhoneMobile")
                CustJob.EditValue = xSet.Tables("ThisMemberData").Rows(0).Item("Pekerjaan")
                CustNotes.EditValue = xSet.Tables("ThisMemberData").Rows(0).Item("notes")

                MemberPoint.EditValue = xSet.Tables("ThisMemberData").Rows(0).Item("points")
                MemberStamp.EditValue = xSet.Tables("ThisMemberData").Rows(0).Item("stamp")
                MemberEmoney.EditValue = xSet.Tables("ThisMemberData").Rows(0).Item("emoney")
                MemberJmlTransOrder.EditValue = xSet.Tables("ThisMemberData").Rows(0).Item("sum_trans_order")
                MemberJmlTransOutlet.EditValue = xSet.Tables("ThisMemberData").Rows(0).Item("sum_trans_outlet")
                MemberTotalTransOrder.EditValue = xSet.Tables("ThisMemberData").Rows(0).Item("sum_buy_order")
                MemberTotalTransOutlet.EditValue = xSet.Tables("ThisMemberData").Rows(0).Item("sum_buy_outlet")
                MemberJmlTrans.EditValue = MemberJmlTransOrder.EditValue + MemberJmlTransOutlet.EditValue
                MemberTotalTrans.EditValue = MemberTotalTransOrder.EditValue + MemberTotalTransOutlet.EditValue

                If ReaderIsBusy Then
                    ButPenukaranPointStamp.Enabled = IIf(xSet.Tables("User_ProgPermisisons").Select("permission_id=17")(0).Item("allow_read"), True, False)
                    ButPenukaranPromoGift.Enabled = IIf(xSet.Tables("User_ProgPermisisons").Select("permission_id=18")(0).Item("allow_read"), True, False)

                    If CardUID <> xSet.Tables("ThisMemberData").Rows(0).Item("kode_kartuUID") Then
                        MsgWarning("Kartu ini sudah tidak berlaku lagi!")
                        Throw New Exception
                    End If
                Else
                    ReaderIsBusy = True
                    ButPenukaranPointStamp.Enabled = False
                    ButPenukaranPromoGift.Enabled = False
                End If

                'show data @LCD
                ReaderStarter()
                If MyACR.CardConnected Then
                    MyACR.ClearLCD()
                    MyACR.DisplayLCD(1, CustName.EditValue)
                    MyACR.DisplayLCD(2, "Point:" & Format(MemberPoint.EditValue, "n0"))
                    MyACR.Disconnect()
                Else
                    CardProgressStatus.Caption = "Reader not found"
                End If
            Catch ex As Exception
                ClearAll()
            End Try
        ElseIf KodeMember.EditValue.ToString.Length = 12 And CurrentMember <> 0 Then
            ClearAll()
        End If
    End Sub

    Private Sub XtraTabControl1_SelectedPageChanged(ByVal sender As Object, ByVal e As DevExpress.XtraTab.TabPageChangedEventArgs) Handles XtraTabControl1.SelectedPageChanged
        If CurrentMember = 0 Then Return
        If XtraTabControl1.SelectedTabPageIndex = 2 And xSet.Tables("ThisMemberGift") Is Nothing Then
            'Gift
            LoadSprite.ShowLoading("Wrapping the gift", "Loading", Me)

            SQLquery = String.Format("SELECT a.id_gift ID, b.nama_gift Nama, get_date TglStart, a.expired_date Expired, a.availability Tersedia, a.use_count Terpakai, b.syarat_none Syarat, b.exc_at_outlet Outlet, b.exc_at_order 'Order' " & _
                        "FROM mbsm_membergifts a INNER JOIN mbsm_gift b ON a.id_gift=b.id_gift WHERE a.Inactive=0 AND a.availability > a.use_count AND a.expired_date>=CURDATE() AND a.id_customer={0}; ", CurrentMember)
            ExDb.ExecQuery(1, SQLquery, xSet, "ThisMemberGift")
            GridGiftMember.DataSource = xSet.Tables("ThisMemberGift").DefaultView
            ViewGiftMember.Columns("ID").Visible = False
            ViewGiftMember.Columns("TglStart").DisplayFormat.FormatType = FormatType.DateTime
            ViewGiftMember.Columns("TglStart").DisplayFormat.FormatString = "dd MMM yyyy"
            ViewGiftMember.Columns("Expired").DisplayFormat.FormatType = FormatType.DateTime
            ViewGiftMember.Columns("Expired").DisplayFormat.FormatString = "dd MMM yyyy"

            ViewGiftMember.OptionsView.ColumnAutoWidth = False

            ViewGiftMember.Columns("Nama").Width = 150
            ViewGiftMember.Columns("TglStart").Width = 80
            ViewGiftMember.Columns("Expired").Width = 80
            ViewGiftMember.Columns("Tersedia").Width = 60
            ViewGiftMember.Columns("Terpakai").Width = 60
            ViewGiftMember.Columns("Syarat").Width = 50
            ViewGiftMember.Columns("Outlet").Width = 50
            ViewGiftMember.Columns("Order").Width = 50
            LoadSprite.CloseLoading()
        ElseIf XtraTabControl1.SelectedTabPageIndex = 3 And xSet.Tables("ThisMemberPromo") Is Nothing Then
            'Promo
            LoadSprite.ShowLoading("Preparing surprises", "Loading", Me)
            SQLquery = String.Format("SELECT a.id_promo ID, a.nama_promo Nama, a.syaratperiode_start TglStart, a.syaratperiode_end Expired, b.nama_syaratrepetisi Repetisi, a.ismultiplied Kelipatan, a.syarat_none Syarat, a.exc_at_outlet Outlet, a.exc_at_order 'Order' " & _
                        "FROM mbsm_promo a INNER JOIN mbsm_syaratrepetisi b ON a.repetisi_promo=b.id_syaratrepetisi WHERE a.Inactive=0 AND a.id_tipemembership={0} AND a.syaratperiode_start<=CURDATE() AND a.syaratperiode_end>=CURDATE(); ", xSet.Tables("ThisMemberData").Rows(0).Item("id_tipemembership"))
            ExDb.ExecQuery(1, SQLquery, xSet, "ThisMemberPromo")
            GridPromoMember.DataSource = xSet.Tables("ThisMemberPromo").DefaultView
            ViewPromoMember.Columns("ID").Visible = False
            ViewPromoMember.Columns("TglStart").DisplayFormat.FormatType = FormatType.DateTime
            ViewPromoMember.Columns("TglStart").DisplayFormat.FormatString = "dd MMM yyyy"
            ViewPromoMember.Columns("Expired").DisplayFormat.FormatType = FormatType.DateTime
            ViewPromoMember.Columns("Expired").DisplayFormat.FormatString = "dd MMM yyyy"

            ViewPromoMember.OptionsView.ColumnAutoWidth = False

            ViewPromoMember.Columns("Nama").Width = 150
            ViewPromoMember.Columns("TglStart").Width = 80
            ViewPromoMember.Columns("Expired").Width = 80
            ViewPromoMember.Columns("Repetisi").Width = 80
            ViewPromoMember.Columns("Kelipatan").Width = 70
            ViewPromoMember.Columns("Syarat").Width = 50
            ViewPromoMember.Columns("Outlet").Width = 50
            ViewPromoMember.Columns("Order").Width = 50
            LoadSprite.CloseLoading()
        End If
    End Sub

    Private Sub ButUpgradeMember_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButUpgradeMember.Click
        If CurrentMember = 0 Then Return

        ShowModule(UpgradeMember, "Upgrade Member")
        UpgradeMember = Nothing
    End Sub

    Private Sub ButPemberianPointStamp_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButPemberianPointStamp.Click
        If CurrentMember = 0 Then Return

        ShowModule(CheckTransaksiOutlet, "Cek Transaksi")
        CheckTransaksiOutlet = Nothing
    End Sub

    Private Sub ButPenukaranPointStamp_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButPenukaranPointStamp.Click
        If CurrentMember = 0 Then Return

        ShowModule(PenukaranPointStamp, "Penukaran Point & Stamp")
        PenukaranPointStamp = Nothing
    End Sub

    Private Sub ButPenukaranPromoGift_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButPenukaranPromoGift.Click
        If CurrentMember = 0 Then Return

        ShowModule(PenukaranGiftPromo, "Penukaran Gift/Promo")
        PenukaranGiftPromo = Nothing
    End Sub

    Private Sub ButBlessing_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBlessing.Click
        If CurrentMember = 0 Then Return

        ShowModule(Blessing, "Blessing")
        Blessing = Nothing
    End Sub

    Private Sub ButVoidPoint_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButVoidPoint.Click
        If CurrentMember = 0 Then Return

        ShowModule(VoidPenukaranPoint, "Void Penukaran Point")
        VoidPenukaranPoint = Nothing
    End Sub

    Private Sub ButVoidGift_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButVoidGift.Click
        If CurrentMember = 0 Then Return

        ShowModule(VoidGift, "Void Gift")
        VoidGift = Nothing
    End Sub

    Private Sub ButClear_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButClear.Click
        If CurrentMember = 0 Then
            KodeMember.EditValue = ""
        Else
            ClearAll()
        End If
    End Sub

    Private Sub ButClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButClose.Click
        Close()
    End Sub

#Region "Asynchronous Background Worker"

    Private Sub bw_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs)
        On Error Resume Next
        MSCount = 10

        Do While CardProgressBar.EditValue <> 100
            Dim worker As BackgroundWorker = CType(sender, BackgroundWorker)
            If bw.CancellationPending Or ReaderIsBusy Then
                e.Cancel = True
                Exit Do
            End If

            System.Threading.Thread.Sleep(100)
            bw.ReportProgress(0)

            If MSCount < 10 Then MSCount += 1

            'check if the reader available
            If Not MyCard.ReaderEstablished Then MyCard.Establish(GlobalReaderContext)
            If Not MyCard.ReaderEstablished Then
                SetLabelText_ThreadSafe(CardProgressStatus, "Reader not found")             'Cant found any reader
                MSCount = 0
                Continue Do
            End If
            bw.ReportProgress(1)
            SetLabelText_ThreadSafe(CardProgressStatus, "Waiting..")                        'Successfully connected to reader

            'check is any available card
            MyCard.Connect(GlobalReaderContext, DefReaderName)
            If Not MyCard.CardConnected Then
                Continue Do
            End If
            'the shit is coming
            bw.ReportProgress(30)
            SetLabelText_ThreadSafe(CardProgressStatus, "Authenticating..")                 'Successfully connected to card

            'try to authenticate card
            MyCard.LoadKeys(MiFareKey.KeyType.TypeA, MyMiFareKey.KeyA)
            If Not MyCard.ReturnCode = 0 Then
                'shit happen
                SetLabelText_ThreadSafe(CardProgressStatus, "Load failed..")                'Failed to load keys
                MSCount = 0
                Continue Do
            Else
                bw.ReportProgress(60)
                MyCard.Authentication(4, MiFareKey.KeyType.TypeA)                          'auth block no.4 where the key stored
                If Not MyCard.ReturnCode = 0 Then
                    'shit!
                    SetLabelText_ThreadSafe(CardProgressStatus, "Authenticate failed..")    'Failed to authenticate
                    MSCount = 0
                    Continue Do
                End If
            End If
            bw.ReportProgress(75)

            'get card uid
            CardUID = MyCard.GetUID
            If CardUID = "" Then
                SetLabelText_ThreadSafe(CardProgressStatus, "Card not recognized!")         'IMPOSIBRO!
                MSCount = 0
                Return
            End If
            bw.ReportProgress(85)
            SetLabelText_ThreadSafe(CardProgressStatus, "Reading..")                        'Trying to read

            'try to get kodemember
            MyCard.BlockRead(4, 16)                                                        'read block no.4 where the key stored
            If Not MyCard.ReturnCode = 0 Then
                'holy shit!
                SetLabelText_ThreadSafe(CardProgressStatus, "Read failed..")                'Failed to read
                MSCount = 0
                Continue Do
            Else
                bw.ReportProgress(100)
                SetLabelText_ThreadSafe(CardProgressStatus, "Success..")                    'Godspeed..
                Return
            End If
        Loop
    End Sub

    Private Sub bw_ProgressChanged(ByVal sender As Object, ByVal e As ProgressChangedEventArgs)
        CardProgressBar.EditValue = e.ProgressPercentage
    End Sub

    Private Sub bw_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs)
        If e.Cancelled = True Then
            'cancel
            CardProgressStatus.Caption = "Canceled.." 'User trespass, WTF!?
        ElseIf e.Error IsNot Nothing Then
            'error
            CardProgressStatus.Caption = "Error.." 'OMFG?!
        Else
            'done
            ReaderIsBusy = True

            Dim TempKodeMember As String = ""
            For i = 0 To 16
                If MyCard.RecvBuff(i) = &H0 Then Continue For
                TempKodeMember = TempKodeMember + Convert.ToChar(MyCard.RecvBuff(i))
            Next
            MyCard.Disconnect()
            KodeMember.EditValue = Trim(TempKodeMember) 'Godspeed..
        End If
    End Sub
#End Region

#Region "Crossthreading"

    ' The delegate
    Delegate Sub SetLabelText_Delegate(ByVal [TargetObject] As DevExpress.XtraBars.BarStaticItem, ByVal [text] As String)

    ' The delegates subroutine.
    Private Sub SetLabelText_ThreadSafe(ByVal [TargetObject] As DevExpress.XtraBars.BarStaticItem, ByVal [text] As String)
        ' InvokeRequired required compares the thread ID of the calling thread to the thread ID of the creating thread.
        ' If these threads are different, it returns true.
        If MSCount >= 10 Or [text] = "Success.." Then
            If InvokeRequired Then
                Dim MyDelegate As New SetLabelText_Delegate(AddressOf SetLabelText_ThreadSafe)
                Invoke(MyDelegate, New Object() {[TargetObject], [text]})
            Else
                [TargetObject].Caption = [text]
            End If
        End If
    End Sub
#End Region

    Private Sub ClearAll()
        KodeMember.EditValue = ""

        CurrentMember = 0
        CurrentKodeKartuMember = ""

        TipeMember.EditValue = ""
        KategoriMember.EditValue = ""

        CustName.EditValue = ""
        CustBday.EditValue = ""
        CustAddress.EditValue = ""
        CustCity.EditValue = ""
        CustPhoneHome.EditValue = ""
        CustPhoneMobile.EditValue = ""
        CustJob.EditValue = ""

        MemberPoint.EditValue = 0
        MemberStamp.EditValue = 0
        MemberEmoney.EditValue = 0
        MemberJmlTrans.EditValue = 0
        MemberTotalTrans.EditValue = 0
        MemberJmlTransOrder.EditValue = 0
        MemberJmlTransOutlet.EditValue = 0
        MemberTotalTransOrder.EditValue = 0
        MemberTotalTransOutlet.EditValue = 0

        ButPenukaranPointStamp.Enabled = False
        ButPenukaranPromoGift.Enabled = False

        If Not xSet.Tables("ThisMemberPromo") Is Nothing Then xSet.Tables.Remove("ThisMemberPromo")
        If Not xSet.Tables("ThisMemberGift") Is Nothing Then xSet.Tables.Remove("ThisMemberGift")

        If KeySetupCheck() Then
            ReaderStarter()
            If MyACR.CardConnected Then
                MyACR.ClearLCD()
                MyACR.DisplayLCD(1, "    Welcome")
                MyACR.DisplayLCD(2, " Laritta Bakery")
                MyACR.Disconnect()
            End If
            If ReaderIsBusy Then
                CardUID = ""
                CardProgressBar.EditValue = 0
                ReaderIsBusy = False
                CardProgressStatus.Caption = "Waiting.."
                If bw.IsBusy Then bw.CancelAsync()
                bw.RunWorkerAsync()
            End If
        End If
    End Sub

    Private Function KeySetupCheck() As Boolean
        If DefReaderName = "" Then
            Return False
        End If
        If IsNothing(MyMiFareKey.KeyA) Then
            Return False
        End If

        Return True
    End Function

    Private Sub ReaderStarter()
        Try
            If Not MyACR.ReaderEstablished Then
                MyACR.Establish(GlobalReaderContext)
            End If
            MyACR.ReaderName = DefReaderName
            MyACR.EasyConnection()
        Catch ex As RFID_Exception
            CardProgressStatus.Caption = "Error.." 'OMFG?!
        End Try
    End Sub

End Class