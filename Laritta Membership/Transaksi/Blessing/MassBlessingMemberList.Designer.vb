﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MassBlessingMemberList
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.CheckAllMember = New DevExpress.XtraEditors.CheckEdit()
        Me.DaftarJoinedMember = New DevExpress.XtraGrid.GridControl()
        Me.ViewJoinedMember = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.CheckedMember = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.ViewDetailTransOutlet = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.PickedBlessing = New DevExpress.XtraEditors.TextEdit()
        Me.ButOK = New DevExpress.XtraEditors.SimpleButton()
        Me.ButSelectBlessing = New DevExpress.XtraEditors.SimpleButton()
        Me.ButClose = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.CheckAllMember.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DaftarJoinedMember, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewJoinedMember, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckedMember, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewDetailTransOutlet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.PickedBlessing.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.Controls.Add(Me.CheckAllMember)
        Me.GroupControl1.Controls.Add(Me.DaftarJoinedMember)
        Me.GroupControl1.Location = New System.Drawing.Point(16, 13)
        Me.GroupControl1.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(661, 355)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Joined members"
        '
        'CheckAllMember
        '
        Me.CheckAllMember.Location = New System.Drawing.Point(5, 26)
        Me.CheckAllMember.Name = "CheckAllMember"
        Me.CheckAllMember.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.CheckAllMember.Properties.Appearance.Options.UseFont = True
        Me.CheckAllMember.Properties.Caption = "Check All"
        Me.CheckAllMember.Size = New System.Drawing.Size(102, 19)
        Me.CheckAllMember.TabIndex = 1
        '
        'DaftarJoinedMember
        '
        Me.DaftarJoinedMember.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DaftarJoinedMember.Location = New System.Drawing.Point(5, 51)
        Me.DaftarJoinedMember.MainView = Me.ViewJoinedMember
        Me.DaftarJoinedMember.Name = "DaftarJoinedMember"
        Me.DaftarJoinedMember.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.CheckedMember})
        Me.DaftarJoinedMember.Size = New System.Drawing.Size(651, 294)
        Me.DaftarJoinedMember.TabIndex = 0
        Me.DaftarJoinedMember.TabStop = False
        Me.DaftarJoinedMember.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.ViewJoinedMember, Me.ViewDetailTransOutlet})
        '
        'ViewJoinedMember
        '
        Me.ViewJoinedMember.Appearance.HeaderPanel.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ViewJoinedMember.Appearance.HeaderPanel.Options.UseFont = True
        Me.ViewJoinedMember.Appearance.Row.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ViewJoinedMember.Appearance.Row.Options.UseFont = True
        Me.ViewJoinedMember.ColumnPanelRowHeight = 40
        Me.ViewJoinedMember.GridControl = Me.DaftarJoinedMember
        Me.ViewJoinedMember.Name = "ViewJoinedMember"
        Me.ViewJoinedMember.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewJoinedMember.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewJoinedMember.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewJoinedMember.OptionsCustomization.AllowColumnMoving = False
        Me.ViewJoinedMember.OptionsCustomization.AllowFilter = False
        Me.ViewJoinedMember.OptionsCustomization.AllowGroup = False
        Me.ViewJoinedMember.OptionsCustomization.AllowQuickHideColumns = False
        Me.ViewJoinedMember.OptionsView.ShowAutoFilterRow = True
        Me.ViewJoinedMember.OptionsView.ShowFooter = True
        Me.ViewJoinedMember.OptionsView.ShowGroupPanel = False
        Me.ViewJoinedMember.RowHeight = 30
        '
        'CheckedMember
        '
        Me.CheckedMember.AutoHeight = False
        Me.CheckedMember.Name = "CheckedMember"
        '
        'ViewDetailTransOutlet
        '
        Me.ViewDetailTransOutlet.Appearance.HeaderPanel.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ViewDetailTransOutlet.Appearance.HeaderPanel.Options.UseFont = True
        Me.ViewDetailTransOutlet.Appearance.Row.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ViewDetailTransOutlet.Appearance.Row.Options.UseFont = True
        Me.ViewDetailTransOutlet.GridControl = Me.DaftarJoinedMember
        Me.ViewDetailTransOutlet.Name = "ViewDetailTransOutlet"
        Me.ViewDetailTransOutlet.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewDetailTransOutlet.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewDetailTransOutlet.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewDetailTransOutlet.OptionsBehavior.Editable = False
        Me.ViewDetailTransOutlet.OptionsCustomization.AllowColumnMoving = False
        Me.ViewDetailTransOutlet.OptionsCustomization.AllowFilter = False
        Me.ViewDetailTransOutlet.OptionsCustomization.AllowGroup = False
        Me.ViewDetailTransOutlet.OptionsCustomization.AllowQuickHideColumns = False
        Me.ViewDetailTransOutlet.OptionsCustomization.AllowSort = False
        Me.ViewDetailTransOutlet.OptionsView.ShowGroupPanel = False
        '
        'PanelControl1
        '
        Me.PanelControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Controls.Add(Me.PickedBlessing)
        Me.PanelControl1.Controls.Add(Me.ButOK)
        Me.PanelControl1.Controls.Add(Me.ButSelectBlessing)
        Me.PanelControl1.Controls.Add(Me.ButClose)
        Me.PanelControl1.Location = New System.Drawing.Point(16, 374)
        Me.PanelControl1.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(661, 56)
        Me.PanelControl1.TabIndex = 1
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl1.Location = New System.Drawing.Point(114, 11)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(101, 14)
        Me.LabelControl1.TabIndex = 1
        Me.LabelControl1.Text = "Blessing dipilih :"
        '
        'PickedBlessing
        '
        Me.PickedBlessing.Location = New System.Drawing.Point(114, 30)
        Me.PickedBlessing.Name = "PickedBlessing"
        Me.PickedBlessing.Properties.ReadOnly = True
        Me.PickedBlessing.Size = New System.Drawing.Size(228, 20)
        Me.PickedBlessing.TabIndex = 2
        Me.PickedBlessing.TabStop = False
        '
        'ButOK
        '
        Me.ButOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButOK.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButOK.Appearance.Options.UseFont = True
        Me.ButOK.Image = Global.Laritta_Membership.My.Resources.Resources.apply_32
        Me.ButOK.Location = New System.Drawing.Point(450, 5)
        Me.ButOK.Name = "ButOK"
        Me.ButOK.Size = New System.Drawing.Size(100, 45)
        Me.ButOK.TabIndex = 3
        Me.ButOK.Text = "&Simpan"
        '
        'ButSelectBlessing
        '
        Me.ButSelectBlessing.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButSelectBlessing.Appearance.Options.UseFont = True
        Me.ButSelectBlessing.Image = Global.Laritta_Membership.My.Resources.Resources.blessing_32
        Me.ButSelectBlessing.Location = New System.Drawing.Point(8, 5)
        Me.ButSelectBlessing.Name = "ButSelectBlessing"
        Me.ButSelectBlessing.Size = New System.Drawing.Size(100, 45)
        Me.ButSelectBlessing.TabIndex = 0
        Me.ButSelectBlessing.Text = "&Blessing"
        '
        'ButClose
        '
        Me.ButClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButClose.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButClose.Appearance.Options.UseFont = True
        Me.ButClose.Image = Global.Laritta_Membership.My.Resources.Resources.close_32
        Me.ButClose.Location = New System.Drawing.Point(556, 5)
        Me.ButClose.Name = "ButClose"
        Me.ButClose.Size = New System.Drawing.Size(100, 45)
        Me.ButClose.TabIndex = 4
        Me.ButClose.Text = "&Tutup"
        '
        'MassBlessingMemberList
        '
        Me.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(693, 443)
        Me.Controls.Add(Me.PanelControl1)
        Me.Controls.Add(Me.GroupControl1)
        Me.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Name = "MassBlessingMemberList"
        Me.Text = "MassBlessingMemberList"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.CheckAllMember.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DaftarJoinedMember, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewJoinedMember, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckedMember, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewDetailTransOutlet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.PickedBlessing.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents DaftarJoinedMember As DevExpress.XtraGrid.GridControl
    Friend WithEvents ViewJoinedMember As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents CheckedMember As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents ViewDetailTransOutlet As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents ButSelectBlessing As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButClose As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PickedBlessing As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ButOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents CheckAllMember As DevExpress.XtraEditors.CheckEdit
End Class
