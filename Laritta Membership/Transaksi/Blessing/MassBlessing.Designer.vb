﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MassBlessing
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ButBatal = New DevExpress.XtraEditors.SimpleButton()
        Me.ButOK = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.BlessingGift = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.ViewBlessingGift = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.TipeBlessings = New DevExpress.XtraEditors.RadioGroup()
        Me.BlessingPoint = New DevExpress.XtraEditors.TextEdit()
        Me.BlessingUpgrade = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.ViewBlessingUpgrade = New DevExpress.XtraGrid.Views.Grid.GridView()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.BlessingGift.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewBlessingGift, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TipeBlessings.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BlessingPoint.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BlessingUpgrade.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewBlessingUpgrade, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ButBatal
        '
        Me.ButBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButBatal.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButBatal.Appearance.Options.UseFont = True
        Me.ButBatal.Image = Global.Laritta_Membership.My.Resources.Resources.back_32
        Me.ButBatal.Location = New System.Drawing.Point(271, 125)
        Me.ButBatal.Name = "ButBatal"
        Me.ButBatal.Size = New System.Drawing.Size(90, 40)
        Me.ButBatal.TabIndex = 2
        Me.ButBatal.Text = "&Batal"
        '
        'ButOK
        '
        Me.ButOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButOK.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButOK.Appearance.Options.UseFont = True
        Me.ButOK.Image = Global.Laritta_Membership.My.Resources.Resources.apply_32
        Me.ButOK.Location = New System.Drawing.Point(175, 125)
        Me.ButOK.Name = "ButOK"
        Me.ButOK.Size = New System.Drawing.Size(90, 40)
        Me.ButOK.TabIndex = 1
        Me.ButOK.Text = "&OK"
        '
        'PanelControl1
        '
        Me.PanelControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelControl1.Controls.Add(Me.BlessingGift)
        Me.PanelControl1.Controls.Add(Me.TipeBlessings)
        Me.PanelControl1.Controls.Add(Me.BlessingPoint)
        Me.PanelControl1.Controls.Add(Me.BlessingUpgrade)
        Me.PanelControl1.Location = New System.Drawing.Point(12, 12)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(349, 107)
        Me.PanelControl1.TabIndex = 0
        '
        'BlessingGift
        '
        Me.BlessingGift.Enabled = False
        Me.BlessingGift.Location = New System.Drawing.Point(89, 69)
        Me.BlessingGift.Name = "BlessingGift"
        Me.BlessingGift.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.BlessingGift.Properties.Appearance.Options.UseFont = True
        Me.BlessingGift.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.BlessingGift.Properties.NullText = ""
        Me.BlessingGift.Properties.View = Me.ViewBlessingGift
        Me.BlessingGift.Size = New System.Drawing.Size(243, 21)
        Me.BlessingGift.TabIndex = 3
        '
        'ViewBlessingGift
        '
        Me.ViewBlessingGift.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.ViewBlessingGift.Name = "ViewBlessingGift"
        Me.ViewBlessingGift.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.ViewBlessingGift.OptionsView.ShowGroupPanel = False
        '
        'TipeBlessings
        '
        Me.TipeBlessings.EditValue = 4
        Me.TipeBlessings.Location = New System.Drawing.Point(5, 5)
        Me.TipeBlessings.Name = "TipeBlessings"
        Me.TipeBlessings.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.TipeBlessings.Properties.Appearance.Options.UseBackColor = True
        Me.TipeBlessings.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.TipeBlessings.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(4, "Upgrade"), New DevExpress.XtraEditors.Controls.RadioGroupItem(5, "Point"), New DevExpress.XtraEditors.Controls.RadioGroupItem(6, "Gift")})
        Me.TipeBlessings.Size = New System.Drawing.Size(78, 96)
        Me.TipeBlessings.TabIndex = 0
        '
        'BlessingPoint
        '
        Me.BlessingPoint.Enabled = False
        Me.BlessingPoint.Location = New System.Drawing.Point(89, 42)
        Me.BlessingPoint.Name = "BlessingPoint"
        Me.BlessingPoint.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.BlessingPoint.Properties.Appearance.Options.UseFont = True
        Me.BlessingPoint.Properties.Appearance.Options.UseTextOptions = True
        Me.BlessingPoint.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.BlessingPoint.Properties.Mask.EditMask = "n2"
        Me.BlessingPoint.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.BlessingPoint.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.BlessingPoint.Properties.NullText = "0.00"
        Me.BlessingPoint.Size = New System.Drawing.Size(118, 21)
        Me.BlessingPoint.TabIndex = 2
        '
        'BlessingUpgrade
        '
        Me.BlessingUpgrade.Location = New System.Drawing.Point(89, 15)
        Me.BlessingUpgrade.Name = "BlessingUpgrade"
        Me.BlessingUpgrade.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.BlessingUpgrade.Properties.Appearance.Options.UseFont = True
        Me.BlessingUpgrade.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.BlessingUpgrade.Properties.NullText = ""
        Me.BlessingUpgrade.Properties.View = Me.ViewBlessingUpgrade
        Me.BlessingUpgrade.Size = New System.Drawing.Size(243, 21)
        Me.BlessingUpgrade.TabIndex = 1
        '
        'ViewBlessingUpgrade
        '
        Me.ViewBlessingUpgrade.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.ViewBlessingUpgrade.Name = "ViewBlessingUpgrade"
        Me.ViewBlessingUpgrade.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.ViewBlessingUpgrade.OptionsView.ShowGroupPanel = False
        '
        'MassBlessing
        '
        Me.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(373, 177)
        Me.Controls.Add(Me.PanelControl1)
        Me.Controls.Add(Me.ButBatal)
        Me.Controls.Add(Me.ButOK)
        Me.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Name = "MassBlessing"
        Me.Text = "MassBlessing"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.BlessingGift.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewBlessingGift, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TipeBlessings.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BlessingPoint.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BlessingUpgrade.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewBlessingUpgrade, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ButBatal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents BlessingUpgrade As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents ViewBlessingUpgrade As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents BlessingPoint As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TipeBlessings As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents BlessingGift As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents ViewBlessingGift As DevExpress.XtraGrid.Views.Grid.GridView
End Class
