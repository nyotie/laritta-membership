﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Blessing
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ButBatal = New DevExpress.XtraEditors.SimpleButton()
        Me.ButOK = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.DaftarBlessableGift = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.ViewBlessableGift = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.TipeBlessings = New DevExpress.XtraEditors.RadioGroup()
        Me.BlessingPoint = New DevExpress.XtraEditors.TextEdit()
        Me.DaftarBlessingUpgrade = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.ViewBlessingUpgrade = New DevExpress.XtraGrid.Views.Grid.GridView()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.DaftarBlessableGift.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewBlessableGift, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TipeBlessings.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BlessingPoint.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DaftarBlessingUpgrade.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewBlessingUpgrade, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ButBatal
        '
        Me.ButBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButBatal.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButBatal.Appearance.Options.UseFont = True
        Me.ButBatal.Image = Global.Laritta_Membership.My.Resources.Resources.back_32
        Me.ButBatal.Location = New System.Drawing.Point(271, 125)
        Me.ButBatal.Name = "ButBatal"
        Me.ButBatal.Size = New System.Drawing.Size(90, 40)
        Me.ButBatal.TabIndex = 2
        Me.ButBatal.Text = "&Batal"
        '
        'ButOK
        '
        Me.ButOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButOK.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButOK.Appearance.Options.UseFont = True
        Me.ButOK.Image = Global.Laritta_Membership.My.Resources.Resources.apply_32
        Me.ButOK.Location = New System.Drawing.Point(175, 125)
        Me.ButOK.Name = "ButOK"
        Me.ButOK.Size = New System.Drawing.Size(90, 40)
        Me.ButOK.TabIndex = 1
        Me.ButOK.Text = "&OK"
        '
        'PanelControl1
        '
        Me.PanelControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelControl1.Controls.Add(Me.DaftarBlessableGift)
        Me.PanelControl1.Controls.Add(Me.TipeBlessings)
        Me.PanelControl1.Controls.Add(Me.BlessingPoint)
        Me.PanelControl1.Controls.Add(Me.DaftarBlessingUpgrade)
        Me.PanelControl1.Location = New System.Drawing.Point(12, 12)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(349, 107)
        Me.PanelControl1.TabIndex = 0
        '
        'DaftarBlessableGift
        '
        Me.DaftarBlessableGift.Enabled = False
        Me.DaftarBlessableGift.Location = New System.Drawing.Point(89, 69)
        Me.DaftarBlessableGift.Name = "DaftarBlessableGift"
        Me.DaftarBlessableGift.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.DaftarBlessableGift.Properties.Appearance.Options.UseFont = True
        Me.DaftarBlessableGift.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DaftarBlessableGift.Properties.NullText = ""
        Me.DaftarBlessableGift.Properties.View = Me.ViewBlessableGift
        Me.DaftarBlessableGift.Size = New System.Drawing.Size(243, 21)
        Me.DaftarBlessableGift.TabIndex = 3
        '
        'ViewBlessableGift
        '
        Me.ViewBlessableGift.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.ViewBlessableGift.Name = "ViewBlessableGift"
        Me.ViewBlessableGift.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.ViewBlessableGift.OptionsView.ShowGroupPanel = False
        '
        'TipeBlessings
        '
        Me.TipeBlessings.EditValue = 4
        Me.TipeBlessings.Location = New System.Drawing.Point(5, 5)
        Me.TipeBlessings.Name = "TipeBlessings"
        Me.TipeBlessings.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.TipeBlessings.Properties.Appearance.Options.UseBackColor = True
        Me.TipeBlessings.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.TipeBlessings.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(4, "Upgrade"), New DevExpress.XtraEditors.Controls.RadioGroupItem(5, "Point"), New DevExpress.XtraEditors.Controls.RadioGroupItem(6, "Gift")})
        Me.TipeBlessings.Size = New System.Drawing.Size(78, 96)
        Me.TipeBlessings.TabIndex = 0
        '
        'BlessingPoint
        '
        Me.BlessingPoint.Enabled = False
        Me.BlessingPoint.Location = New System.Drawing.Point(89, 42)
        Me.BlessingPoint.Name = "BlessingPoint"
        Me.BlessingPoint.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.BlessingPoint.Properties.Appearance.Options.UseFont = True
        Me.BlessingPoint.Properties.Appearance.Options.UseTextOptions = True
        Me.BlessingPoint.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.BlessingPoint.Properties.Mask.EditMask = "n2"
        Me.BlessingPoint.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.BlessingPoint.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.BlessingPoint.Properties.NullText = "0.00"
        Me.BlessingPoint.Size = New System.Drawing.Size(118, 21)
        Me.BlessingPoint.TabIndex = 2
        '
        'DaftarBlessingUpgrade
        '
        Me.DaftarBlessingUpgrade.Location = New System.Drawing.Point(89, 15)
        Me.DaftarBlessingUpgrade.Name = "DaftarBlessingUpgrade"
        Me.DaftarBlessingUpgrade.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.DaftarBlessingUpgrade.Properties.Appearance.Options.UseFont = True
        Me.DaftarBlessingUpgrade.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DaftarBlessingUpgrade.Properties.NullText = ""
        Me.DaftarBlessingUpgrade.Properties.View = Me.ViewBlessingUpgrade
        Me.DaftarBlessingUpgrade.Size = New System.Drawing.Size(243, 21)
        Me.DaftarBlessingUpgrade.TabIndex = 1
        '
        'ViewBlessingUpgrade
        '
        Me.ViewBlessingUpgrade.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.ViewBlessingUpgrade.Name = "ViewBlessingUpgrade"
        Me.ViewBlessingUpgrade.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.ViewBlessingUpgrade.OptionsView.ShowGroupPanel = False
        '
        'Blessing
        '
        Me.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(373, 177)
        Me.Controls.Add(Me.PanelControl1)
        Me.Controls.Add(Me.ButBatal)
        Me.Controls.Add(Me.ButOK)
        Me.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Name = "Blessing"
        Me.Text = "Blessing"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.DaftarBlessableGift.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewBlessableGift, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TipeBlessings.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BlessingPoint.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DaftarBlessingUpgrade.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewBlessingUpgrade, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ButBatal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents DaftarBlessingUpgrade As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents ViewBlessingUpgrade As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents BlessingPoint As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TipeBlessings As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents DaftarBlessableGift As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents ViewBlessableGift As DevExpress.XtraGrid.Views.Grid.GridView
End Class
