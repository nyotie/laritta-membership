﻿Imports DevExpress.Utils

Public Class BlessingAuth

    Private Sub BlessingAuth_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("DaftarUnauth")
        xSet.Tables.Remove("DaftarBlessings")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub BlessingAuth_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()
        Get_KebutuhanDaftar()
    End Sub

    Private Sub BlessingAuth_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        DateCreateStart.EditValue = DateAdd(DateInterval.Day, -1, Today.Date)
        DateCreateEnd.EditValue = Today.Date
        LoadSprite.CloseLoading()
    End Sub

    Private Sub BlessPageTab_SelectedPageChanged(ByVal sender As Object, ByVal e As DevExpress.XtraTab.TabPageChangedEventArgs) Handles BlessPageTab.SelectedPageChanged
        If e.Page.Name = "PageBlessLists" Then
            ButAuthorize.Enabled = False
            ButVoid.Enabled = False
            ButRefreshUnbless.Enabled = False
        Else
            ButAuthorize.Enabled = True
            ButVoid.Enabled = True
            ButRefreshUnbless.Enabled = True
        End If
    End Sub

    Private Sub CheckAll_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CheckAll.CheckedChanged
        If xSet.Tables("DaftarUnauth") Is Nothing Then Return
        If CheckAll.Checked = True Then
            For Each row As DataRow In xSet.Tables("DaftarUnauth").Select("pilihan=0")
                row.Item("pilihan") = True
            Next
        Else
            For Each row As DataRow In xSet.Tables("DaftarUnauth").Rows
                row.Item("pilihan") = False
            Next
        End If
    End Sub

    Private Sub RadioGroup1_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles RadioGroup1.SelectedIndexChanged
        If RadioGroup1.SelectedIndex = 0 Then
            'Create date
            DateCreateStart.Enabled = True
            DateCreateEnd.Enabled = True
            DateAuthStart.Enabled = False
            DateAuthEnd.Enabled = False
        Else
            'Auth date
            DateCreateStart.Enabled = False
            DateCreateEnd.Enabled = False
            DateAuthStart.Enabled = True
            DateAuthEnd.Enabled = True
        End If
    End Sub

    Private Sub butRefresh_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butRefreshBless.Click
        If RadioGroup1.SelectedIndex = 0 Then
            If DateCreateStart.EditValue Is Nothing Or DateCreateEnd.EditValue Is Nothing Then Return

            SQLquery = String.Format("SELECT d.namaCabang Cabang, b.namaCust Customer, CASE tipe_blessings WHEN 0 THEN 'Upgrade' WHEN 1 THEN 'Point' ELSE 'Gift' END Jenis, " & _
                        "e.event_targetname Isi, c.Nama Pembuat, f.Nama 'Auth by', g.namaCabang 'Auth at', a.auth_date 'Auth date', a.isVoid Void FROM mbst_blessings a " & _
                        "INNER JOIN mcustomer b ON a.id_customer=b.idCust INNER JOIN mstaff c ON a.createby=c.idStaff INNER JOIN mcabang d ON a.id_cabang=d.idCabang " & _
                        "INNER JOIN mbst_log e ON a.id_blessings=e.trans_id AND e.event_id IN (4,5,6) LEFT JOIN mstaff f ON a.auth_by=f.idStaff LEFT JOIN mcabang g ON a.auth_at=g.idCabang " & _
                        "WHERE DATE(a.createdate) BETWEEN '{0}' AND '{1}'", Format(DateCreateStart.EditValue, "yyyy-MM-dd"), Format(DateCreateEnd.EditValue, "yyyy-MM-dd"))
        Else
            If DateAuthStart.EditValue Is Nothing Or DateAuthEnd.EditValue Is Nothing Then Return

            SQLquery = String.Format("SELECT d.namaCabang Cabang, b.namaCust Customer, CASE tipe_blessings WHEN 0 THEN 'Upgrade' WHEN 1 THEN 'Point' ELSE 'Gift' END Jenis, " & _
                        "e.event_targetname Isi, c.Nama Pembuat, f.Nama 'Auth by', g.namaCabang 'Auth at', a.auth_date 'Auth date', a.isVoid Void FROM mbst_blessings a " & _
                        "INNER JOIN mcustomer b ON a.id_customer=b.idCust INNER JOIN mstaff c ON a.createby=c.idStaff INNER JOIN mcabang d ON a.id_cabang=d.idCabang " & _
                        "INNER JOIN mbst_log e ON a.id_blessings=e.trans_id AND e.event_id IN (4,5,6) LEFT JOIN mstaff f ON a.auth_by=f.idStaff LEFT JOIN mcabang g ON a.auth_at=g.idCabang " & _
                        "WHERE DATE(a.auth_date) BETWEEN '{0}' AND '{1}'", Format(DateCreateStart.EditValue, "yyyy-MM-dd"), Format(DateCreateEnd.EditValue, "yyyy-MM-dd"))
        End If

        If Not xSet.Tables("DaftarBlessings") Is Nothing Then
            xSet.Tables("DaftarBlessings").Clear()
            ExDb.ExecQuery(1, SQLquery, xSet, "DaftarBlessings")
        Else
            ExDb.ExecQuery(1, SQLquery, xSet, "DaftarBlessings")
            '----------------------------Grid Settings
            DaftarBlessings.DataSource = xSet.Tables("DaftarBlessings").DefaultView

            For Each coll As DataColumn In xSet.Tables("DaftarBlessings").Columns
                ViewBlessings.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
            Next

            ViewBlessings.Columns("Auth date").DisplayFormat.FormatType = FormatType.DateTime
            ViewBlessings.Columns("Auth date").DisplayFormat.FormatString = "dd MMM yyyy, HH:mm"

            'ViewBlessings.OptionsView.ColumnAutoWidth = False

            ViewBlessings.Columns("Cabang").Width = 100
            ViewBlessings.Columns("Customer").Width = 200
            ViewBlessings.Columns("Jenis").Width = 100
            ViewBlessings.Columns("Isi").Width = 250
            ViewBlessings.Columns("Pembuat").Width = 100
            ViewBlessings.Columns("Auth by").Width = 100
            ViewBlessings.Columns("Auth at").Width = 100
            ViewBlessings.Columns("Auth date").Width = 120
            ViewBlessings.Columns("Void").Width = 60
        End If
    End Sub

    Private Sub ButSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButAuthorize.Click
        If Not beforeSave() Then Return
        Try
            LoadSprite.ShowLoading("Menyimpan authentikasi", "Loading", Me)
            SQLquery = String.Format("UPDATE mbst_blessings SET auth_at={0}, auth_by={1}, auth_date=NOW() WHERE id_blessings IN (", id_cabang, staff_id)
            For Each row As DataRow In xSet.Tables("DaftarUnauth").Select("pilihan=1")
                SQLquery += row.Item("ID") & ", "
            Next
            SQLquery = Mid(SQLquery, 1, SQLquery.Length - 2) & "); "
            ExDb.ExecData(1, SQLquery)

            MsgInfo("Data berhasil disimpan, tekan OK untuk melanjutkan.")
        Catch ex As Exception
            MsgErrorDev()
        End Try
        LoadSprite.CloseLoading()
        Get_KebutuhanDaftar()
    End Sub

    Private Sub ButVoid_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButVoid.Click
        If Not beforeSave() Then Return
        Try
            LoadSprite.ShowLoading("Melakukan update pada data", "Loading", Me)
            SQLquery = String.Format("UPDATE mbst_blessings SET isVoid=1, updateby={0}, updatedate=NOW() WHERE id_blessings IN (", staff_id)
            For Each row As DataRow In xSet.Tables("DaftarUnauth").Select("pilihan=1")
                SQLquery += row.Item("ID") & ", "
            Next
            SQLquery = Mid(SQLquery, 1, SQLquery.Length - 2) & "); "
            ExDb.ExecData(1, SQLquery)

            MsgInfo("Data berhasil di-void, tekan OK untuk melanjutkan.")
        Catch ex As Exception
            MsgErrorDev()
        End Try
        LoadSprite.CloseLoading()
        Get_KebutuhanDaftar()
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Sub Get_KebutuhanDaftar()
        '--------------------------------Daftar Unauthorized Blessing yet
        If Not xSet.Tables("DaftarUnauth") Is Nothing Then xSet.Tables("DaftarUnauth").Clear()
        SQLquery = "SELECT a.id_blessings ID, isVoid pilihan, d.namaCabang Cabang, b.namaCust Customer, CASE tipe_blessings WHEN 0 THEN 'Upgrade' WHEN 1 THEN 'Point' ELSE 'Gift' END Jenis, " & _
                    "e.event_targetname Isi, c.Nama 'User', a.createdate Tanggal FROM mbst_blessings a INNER JOIN mcustomer b ON a.id_customer=b.idCust INNER JOIN mstaff c ON a.createby=c.idStaff " & _
                    "INNER JOIN mcabang d ON a.id_cabang=d.idCabang INNER JOIN mbst_log e ON a.id_blessings=e.trans_id AND e.event_id IN (4,5,6) " & _
                    "WHERE a.isVoid = 0 AND a.auth_by IS NULL;"

        If xSet.Tables("DaftarUnauth") Is Nothing Then
            ExDb.ExecQuery(1, SQLquery, xSet, "DaftarUnauth")

            '----------------------------Grid Settings
            DaftarUnauth.DataSource = xSet.Tables("DaftarUnauth").DefaultView

            For Each coll As DataColumn In xSet.Tables("DaftarUnauth").Columns
                ViewUnauth.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
            Next

            ViewUnauth.Columns("ID").Visible = False

            ViewUnauth.Columns("pilihan").Caption = "..."
            ViewUnauth.Columns("Cabang").OptionsColumn.AllowEdit = False
            ViewUnauth.Columns("Customer").OptionsColumn.AllowEdit = False
            ViewUnauth.Columns("Jenis").OptionsColumn.AllowEdit = False
            ViewUnauth.Columns("Isi").OptionsColumn.AllowEdit = False
            ViewUnauth.Columns("User").OptionsColumn.AllowEdit = False

            ViewUnauth.Columns("Tanggal").DisplayFormat.FormatType = FormatType.DateTime
            ViewUnauth.Columns("Tanggal").DisplayFormat.FormatString = "dd MMM yyyy, HH:mm"

            'ViewUnauth.OptionsView.ColumnAutoWidth = False

            ViewUnauth.Columns("pilihan").Width = 60
            ViewUnauth.Columns("Cabang").Width = 100
            ViewUnauth.Columns("Customer").Width = 150
            ViewUnauth.Columns("Jenis").Width = 100
            ViewUnauth.Columns("Isi").Width = 250
            'ViewUnauth.Columns("User").Width = 150
        Else
            ExDb.ExecQuery(1, SQLquery, xSet, "DaftarUnauth")
        End If
    End Sub

    Private Function beforeSave() As Boolean
        If xSet.Tables("DaftarUnauth").Select("pilihan=1").Length < 1 Then Return False

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        Return True
    End Function
End Class