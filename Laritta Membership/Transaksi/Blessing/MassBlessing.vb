﻿Imports DevExpress.Utils

Public Class MassBlessing

    Private Sub MassBlessing_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub MassBlessing_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading(Me)
        Get_KebutuhanDaftar()
    End Sub

    Private Sub MassBlessing_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        LoadSprite.CloseLoading()
    End Sub

    Private Sub TipeBlessings_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TipeBlessings.SelectedIndexChanged
        Select Case TipeBlessings.SelectedIndex
            Case 0
                'upgrade
                BlessingUpgrade.Enabled = True
                BlessingPoint.Enabled = False
                BlessingGift.Enabled = False
            Case 1
                'point
                BlessingUpgrade.Enabled = False
                BlessingPoint.Enabled = True
                BlessingGift.Enabled = False
            Case 2
                'gift
                BlessingUpgrade.Enabled = False
                BlessingPoint.Enabled = False
                BlessingGift.Enabled = True
        End Select
    End Sub

    Private Sub ButOK_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButOK.Click
        If Not beforeSave() Then Return
        Try
            LoadSprite.ShowLoading(Me)

            Select Case TipeBlessings.SelectedIndex
                'idcbg, idcust, bless, num, name, staff
                Case 0
                    'Upgrade
                    MassBlessingMemberList.pick_blessid = 1
                    MassBlessingMemberList.pick_blessvalue = BlessingUpgrade.EditValue
                    MassBlessingMemberList.pick_blesstext = BlessingUpgrade.Text
                Case 1
                    'Point
                    MassBlessingMemberList.pick_blessid = 2
                    MassBlessingMemberList.pick_blessvalue = BlessingPoint.EditValue
                    MassBlessingMemberList.pick_blesstext = IIf(BlessingPoint.EditValue > 0, "+" & BlessingPoint.Text & " point", BlessingPoint.Text & " point")
                Case 2
                    'Gift
                    MassBlessingMemberList.pick_blessid = 3
                    MassBlessingMemberList.pick_blessvalue = BlessingGift.EditValue
                    MassBlessingMemberList.pick_blesstext = BlessingGift.Text
            End Select
        Catch ex As Exception
            MsgErrorDev()
        End Try
        LoadSprite.CloseLoading()
        Close()
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Sub Get_KebutuhanDaftar()
        If xSet.Tables("MBDaftarTipeMembership") Is Nothing Then
            '---------------------------------------------------Tipe Membership
            SQLquery = "SELECT id_tipemembership ID, nama_tipemembership Nama, level_tipemembership Level, syarat_krt Syarat, harga_krt Harga FROM mbsm_tipemembership WHERE Inactive=0;"
            ExDb.ExecQuery(1, SQLquery, xSet, "MBDaftarTipeMembership")
            BlessingUpgrade.Properties.DataSource = xSet.Tables("MBDaftarTipeMembership").DefaultView

            BlessingUpgrade.Properties.NullText = ""
            BlessingUpgrade.Properties.ValueMember = "ID"
            BlessingUpgrade.Properties.DisplayMember = "Nama"
            BlessingUpgrade.Properties.ShowClearButton = False
            BlessingUpgrade.Properties.PopulateViewColumns()

            ViewBlessingUpgrade.Columns("ID").Visible = False
            ViewBlessingUpgrade.Columns("Syarat").DisplayFormat.FormatType = FormatType.Numeric
            ViewBlessingUpgrade.Columns("Syarat").DisplayFormat.FormatString = "Rp{0:n0}"
            ViewBlessingUpgrade.Columns("Harga").DisplayFormat.FormatType = FormatType.Numeric
            ViewBlessingUpgrade.Columns("Harga").DisplayFormat.FormatString = "Rp{0:n0}"

            For Each coll As DataColumn In xSet.Tables("MBDaftarTipeMembership").Columns
                ViewBlessingUpgrade.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
            Next
        End If

        If xSet.Tables("MBDaftarGift") Is Nothing Then
            '---------------------------------------------------Gift
            SQLquery = "SELECT a.id_gift ID, a.nama_gift Nama, a.expired_day, a.expired_date, CASE WHEN a.expired_day=0 THEN DATE_FORMAT(expired_date, '%d %b %Y') " & _
                        "ELSE CONCAT(a.expired_day, ' hari') END Expiring, a.repetisi_gift Repetisi, a.reward_value Nilai, a.syarat_none Syarat " & _
                        "FROM mbsm_gift a WHERE a.Inactive=0 AND IFNULL(a.expired_date, CURRENT_DATE()+1)>CURRENT_DATE();"
            ExDb.ExecQuery(1, SQLquery, xSet, "MBDaftarGift")
            BlessingGift.Properties.DataSource = xSet.Tables("MBDaftarGift").DefaultView

            BlessingGift.Properties.NullText = ""
            BlessingGift.Properties.ValueMember = "ID"
            BlessingGift.Properties.DisplayMember = "Nama"
            BlessingGift.Properties.ShowClearButton = False
            BlessingGift.Properties.PopulateViewColumns()

            ViewBlessingGift.Columns("ID").Visible = False
            ViewBlessingGift.Columns("expired_day").Visible = False
            ViewBlessingGift.Columns("expired_date").Visible = False
            ViewBlessingGift.Columns("Nilai").DisplayFormat.FormatType = FormatType.Numeric
            ViewBlessingGift.Columns("Nilai").DisplayFormat.FormatString = "Rp{0:n0}"

            ViewBlessingGift.Columns("Nama").Width = 250
            ViewBlessingGift.Columns("Expiring").Width = 150
            ViewBlessingGift.Columns("Repetisi").Width = 100
            ViewBlessingGift.Columns("Nilai").Width = 150
            ViewBlessingGift.Columns("Syarat").Width = 80

            For Each coll As DataColumn In xSet.Tables("MBDaftarGift").Columns
                ViewBlessingGift.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
            Next
        End If
    End Sub

    Private Function beforeSave() As Boolean
        Select Case TipeBlessings.SelectedIndex
            Case 0
                If BlessingUpgrade.EditValue Is Nothing Then Return False
            Case 1
                If BlessingPoint.EditValue = 0 Or BlessingPoint.EditValue Is Nothing Then Return False
            Case 2
                If BlessingGift.EditValue Is Nothing Then Return False
        End Select

        Return True
    End Function
End Class