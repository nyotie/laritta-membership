﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BlessingAuth
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ButBatal = New DevExpress.XtraEditors.SimpleButton()
        Me.ButAuthorize = New DevExpress.XtraEditors.SimpleButton()
        Me.BlessPageTab = New DevExpress.XtraTab.XtraTabControl()
        Me.PageUnauth = New DevExpress.XtraTab.XtraTabPage()
        Me.CheckAll = New DevExpress.XtraEditors.CheckEdit()
        Me.DaftarUnauth = New DevExpress.XtraGrid.GridControl()
        Me.ViewUnauth = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.PageBlessLists = New DevExpress.XtraTab.XtraTabPage()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.RadioGroup1 = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.butRefreshBless = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.DateCreateStart = New DevExpress.XtraEditors.DateEdit()
        Me.DateCreateEnd = New DevExpress.XtraEditors.DateEdit()
        Me.DateAuthStart = New DevExpress.XtraEditors.DateEdit()
        Me.DateAuthEnd = New DevExpress.XtraEditors.DateEdit()
        Me.DaftarBlessings = New DevExpress.XtraGrid.GridControl()
        Me.ViewBlessings = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.RepositoryItemSearchLookUpEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemSearchLookUpEdit()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.RepositoryItemCheckEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.ButVoid = New DevExpress.XtraEditors.SimpleButton()
        Me.ButRefreshUnbless = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.BlessPageTab, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BlessPageTab.SuspendLayout()
        Me.PageUnauth.SuspendLayout()
        CType(Me.CheckAll.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DaftarUnauth, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewUnauth, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PageBlessLists.SuspendLayout()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateCreateStart.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateCreateStart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateCreateEnd.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateCreateEnd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateAuthStart.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateAuthStart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateAuthEnd.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateAuthEnd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DaftarBlessings, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewBlessings, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemSearchLookUpEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ButBatal
        '
        Me.ButBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButBatal.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButBatal.Appearance.Options.UseFont = True
        Me.ButBatal.Image = Global.Laritta_Membership.My.Resources.Resources.back_32
        Me.ButBatal.Location = New System.Drawing.Point(707, 409)
        Me.ButBatal.Name = "ButBatal"
        Me.ButBatal.Size = New System.Drawing.Size(90, 40)
        Me.ButBatal.TabIndex = 4
        Me.ButBatal.Text = "&Batal"
        '
        'ButAuthorize
        '
        Me.ButAuthorize.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButAuthorize.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButAuthorize.Appearance.Options.UseFont = True
        Me.ButAuthorize.Image = Global.Laritta_Membership.My.Resources.Resources.apply_32
        Me.ButAuthorize.Location = New System.Drawing.Point(485, 409)
        Me.ButAuthorize.Name = "ButAuthorize"
        Me.ButAuthorize.Size = New System.Drawing.Size(120, 40)
        Me.ButAuthorize.TabIndex = 2
        Me.ButAuthorize.Text = "&Authorize"
        '
        'BlessPageTab
        '
        Me.BlessPageTab.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BlessPageTab.AppearancePage.Header.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.BlessPageTab.AppearancePage.Header.Options.UseFont = True
        Me.BlessPageTab.Location = New System.Drawing.Point(12, 12)
        Me.BlessPageTab.Name = "BlessPageTab"
        Me.BlessPageTab.SelectedTabPage = Me.PageUnauth
        Me.BlessPageTab.Size = New System.Drawing.Size(785, 391)
        Me.BlessPageTab.TabIndex = 0
        Me.BlessPageTab.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.PageUnauth, Me.PageBlessLists})
        '
        'PageUnauth
        '
        Me.PageUnauth.Controls.Add(Me.CheckAll)
        Me.PageUnauth.Controls.Add(Me.DaftarUnauth)
        Me.PageUnauth.Image = Global.Laritta_Membership.My.Resources.Resources.unauth_blessing_32
        Me.PageUnauth.Name = "PageUnauth"
        Me.PageUnauth.Size = New System.Drawing.Size(779, 346)
        Me.PageUnauth.Text = "Unauthorized Blessings"
        '
        'CheckAll
        '
        Me.CheckAll.Location = New System.Drawing.Point(3, 3)
        Me.CheckAll.Name = "CheckAll"
        Me.CheckAll.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.CheckAll.Properties.Appearance.Options.UseFont = True
        Me.CheckAll.Properties.Caption = "Check All"
        Me.CheckAll.Size = New System.Drawing.Size(210, 19)
        Me.CheckAll.TabIndex = 0
        '
        'DaftarUnauth
        '
        Me.DaftarUnauth.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DaftarUnauth.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.DaftarUnauth.Location = New System.Drawing.Point(3, 28)
        Me.DaftarUnauth.MainView = Me.ViewUnauth
        Me.DaftarUnauth.Name = "DaftarUnauth"
        Me.DaftarUnauth.Size = New System.Drawing.Size(773, 315)
        Me.DaftarUnauth.TabIndex = 1
        Me.DaftarUnauth.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.ViewUnauth})
        '
        'ViewUnauth
        '
        Me.ViewUnauth.Appearance.HeaderPanel.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ViewUnauth.Appearance.HeaderPanel.Options.UseFont = True
        Me.ViewUnauth.Appearance.Row.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ViewUnauth.Appearance.Row.Options.UseFont = True
        Me.ViewUnauth.ColumnPanelRowHeight = 40
        Me.ViewUnauth.GridControl = Me.DaftarUnauth
        Me.ViewUnauth.Name = "ViewUnauth"
        Me.ViewUnauth.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewUnauth.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewUnauth.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewUnauth.OptionsCustomization.AllowColumnMoving = False
        Me.ViewUnauth.OptionsCustomization.AllowColumnResizing = False
        Me.ViewUnauth.OptionsCustomization.AllowFilter = False
        Me.ViewUnauth.OptionsCustomization.AllowGroup = False
        Me.ViewUnauth.OptionsCustomization.AllowQuickHideColumns = False
        Me.ViewUnauth.OptionsView.ShowGroupPanel = False
        Me.ViewUnauth.RowHeight = 30
        '
        'PageBlessLists
        '
        Me.PageBlessLists.Controls.Add(Me.PanelControl1)
        Me.PageBlessLists.Controls.Add(Me.DaftarBlessings)
        Me.PageBlessLists.Image = Global.Laritta_Membership.My.Resources.Resources.blessing_32
        Me.PageBlessLists.Name = "PageBlessLists"
        Me.PageBlessLists.Size = New System.Drawing.Size(779, 346)
        Me.PageBlessLists.Text = "Blessing Lists"
        '
        'PanelControl1
        '
        Me.PanelControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelControl1.Controls.Add(Me.RadioGroup1)
        Me.PanelControl1.Controls.Add(Me.LabelControl4)
        Me.PanelControl1.Controls.Add(Me.butRefreshBless)
        Me.PanelControl1.Controls.Add(Me.LabelControl2)
        Me.PanelControl1.Controls.Add(Me.DateCreateStart)
        Me.PanelControl1.Controls.Add(Me.DateCreateEnd)
        Me.PanelControl1.Controls.Add(Me.DateAuthStart)
        Me.PanelControl1.Controls.Add(Me.DateAuthEnd)
        Me.PanelControl1.Location = New System.Drawing.Point(3, 3)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(773, 61)
        Me.PanelControl1.TabIndex = 12
        '
        'RadioGroup1
        '
        Me.RadioGroup1.Location = New System.Drawing.Point(24, 0)
        Me.RadioGroup1.Name = "RadioGroup1"
        Me.RadioGroup1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.RadioGroup1.Properties.Appearance.Options.UseBackColor = True
        Me.RadioGroup1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.RadioGroup1.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Tanggal blessing"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Tanggal authorize")})
        Me.RadioGroup1.Size = New System.Drawing.Size(121, 58)
        Me.RadioGroup1.TabIndex = 0
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl4.Location = New System.Drawing.Point(288, 35)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(5, 14)
        Me.LabelControl4.TabIndex = 11
        Me.LabelControl4.Text = "-"
        '
        'butRefreshBless
        '
        Me.butRefreshBless.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butRefreshBless.Appearance.Options.UseFont = True
        Me.butRefreshBless.Image = Global.Laritta_Membership.My.Resources.Resources.refresh_32
        Me.butRefreshBless.Location = New System.Drawing.Point(437, 5)
        Me.butRefreshBless.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.butRefreshBless.Name = "butRefreshBless"
        Me.butRefreshBless.Size = New System.Drawing.Size(99, 47)
        Me.butRefreshBless.TabIndex = 5
        Me.butRefreshBless.Text = "Refresh"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl2.Location = New System.Drawing.Point(288, 9)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(5, 14)
        Me.LabelControl2.TabIndex = 11
        Me.LabelControl2.Text = "-"
        '
        'DateCreateStart
        '
        Me.DateCreateStart.EditValue = Nothing
        Me.DateCreateStart.Location = New System.Drawing.Point(151, 6)
        Me.DateCreateStart.Name = "DateCreateStart"
        Me.DateCreateStart.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.DateCreateStart.Properties.Appearance.Options.UseFont = True
        Me.DateCreateStart.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateCreateStart.Properties.Mask.EditMask = "dd MMM yyyy"
        Me.DateCreateStart.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.DateCreateStart.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateCreateStart.Size = New System.Drawing.Size(131, 21)
        Me.DateCreateStart.TabIndex = 1
        '
        'DateCreateEnd
        '
        Me.DateCreateEnd.EditValue = Nothing
        Me.DateCreateEnd.Location = New System.Drawing.Point(299, 6)
        Me.DateCreateEnd.Name = "DateCreateEnd"
        Me.DateCreateEnd.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.DateCreateEnd.Properties.Appearance.Options.UseFont = True
        Me.DateCreateEnd.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateCreateEnd.Properties.Mask.EditMask = "dd MMM yyyy"
        Me.DateCreateEnd.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.DateCreateEnd.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateCreateEnd.Size = New System.Drawing.Size(131, 21)
        Me.DateCreateEnd.TabIndex = 2
        '
        'DateAuthStart
        '
        Me.DateAuthStart.EditValue = Nothing
        Me.DateAuthStart.Enabled = False
        Me.DateAuthStart.Location = New System.Drawing.Point(151, 32)
        Me.DateAuthStart.Name = "DateAuthStart"
        Me.DateAuthStart.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.DateAuthStart.Properties.Appearance.Options.UseFont = True
        Me.DateAuthStart.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateAuthStart.Properties.Mask.EditMask = "dd MMM yyyy"
        Me.DateAuthStart.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.DateAuthStart.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateAuthStart.Size = New System.Drawing.Size(131, 21)
        Me.DateAuthStart.TabIndex = 3
        '
        'DateAuthEnd
        '
        Me.DateAuthEnd.EditValue = Nothing
        Me.DateAuthEnd.Enabled = False
        Me.DateAuthEnd.Location = New System.Drawing.Point(299, 32)
        Me.DateAuthEnd.Name = "DateAuthEnd"
        Me.DateAuthEnd.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.DateAuthEnd.Properties.Appearance.Options.UseFont = True
        Me.DateAuthEnd.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateAuthEnd.Properties.Mask.EditMask = "dd MMM yyyy"
        Me.DateAuthEnd.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.DateAuthEnd.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateAuthEnd.Size = New System.Drawing.Size(131, 21)
        Me.DateAuthEnd.TabIndex = 4
        '
        'DaftarBlessings
        '
        Me.DaftarBlessings.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DaftarBlessings.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.DaftarBlessings.Location = New System.Drawing.Point(3, 70)
        Me.DaftarBlessings.MainView = Me.ViewBlessings
        Me.DaftarBlessings.Name = "DaftarBlessings"
        Me.DaftarBlessings.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemSearchLookUpEdit1, Me.RepositoryItemCheckEdit1})
        Me.DaftarBlessings.Size = New System.Drawing.Size(773, 273)
        Me.DaftarBlessings.TabIndex = 0
        Me.DaftarBlessings.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.ViewBlessings})
        '
        'ViewBlessings
        '
        Me.ViewBlessings.Appearance.HeaderPanel.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ViewBlessings.Appearance.HeaderPanel.Options.UseFont = True
        Me.ViewBlessings.Appearance.Row.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ViewBlessings.Appearance.Row.Options.UseFont = True
        Me.ViewBlessings.ColumnPanelRowHeight = 40
        Me.ViewBlessings.GridControl = Me.DaftarBlessings
        Me.ViewBlessings.Name = "ViewBlessings"
        Me.ViewBlessings.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewBlessings.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewBlessings.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewBlessings.OptionsBehavior.Editable = False
        Me.ViewBlessings.OptionsCustomization.AllowColumnMoving = False
        Me.ViewBlessings.OptionsCustomization.AllowColumnResizing = False
        Me.ViewBlessings.OptionsCustomization.AllowFilter = False
        Me.ViewBlessings.OptionsCustomization.AllowGroup = False
        Me.ViewBlessings.OptionsCustomization.AllowQuickHideColumns = False
        Me.ViewBlessings.OptionsView.ShowGroupPanel = False
        Me.ViewBlessings.RowHeight = 30
        '
        'RepositoryItemSearchLookUpEdit1
        '
        Me.RepositoryItemSearchLookUpEdit1.AutoHeight = False
        Me.RepositoryItemSearchLookUpEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemSearchLookUpEdit1.Name = "RepositoryItemSearchLookUpEdit1"
        Me.RepositoryItemSearchLookUpEdit1.View = Me.GridView2
        '
        'GridView2
        '
        Me.GridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView2.OptionsView.ShowGroupPanel = False
        '
        'RepositoryItemCheckEdit1
        '
        Me.RepositoryItemCheckEdit1.AutoHeight = False
        Me.RepositoryItemCheckEdit1.Name = "RepositoryItemCheckEdit1"
        '
        'ButVoid
        '
        Me.ButVoid.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButVoid.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButVoid.Appearance.Options.UseFont = True
        Me.ButVoid.Image = Global.Laritta_Membership.My.Resources.Resources.void_32
        Me.ButVoid.Location = New System.Drawing.Point(611, 409)
        Me.ButVoid.Name = "ButVoid"
        Me.ButVoid.Size = New System.Drawing.Size(90, 40)
        Me.ButVoid.TabIndex = 3
        Me.ButVoid.Text = "&Void"
        '
        'ButRefreshUnbless
        '
        Me.ButRefreshUnbless.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButRefreshUnbless.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButRefreshUnbless.Appearance.Options.UseFont = True
        Me.ButRefreshUnbless.Image = Global.Laritta_Membership.My.Resources.Resources.refresh_32
        Me.ButRefreshUnbless.Location = New System.Drawing.Point(12, 409)
        Me.ButRefreshUnbless.Name = "ButRefreshUnbless"
        Me.ButRefreshUnbless.Size = New System.Drawing.Size(120, 40)
        Me.ButRefreshUnbless.TabIndex = 1
        Me.ButRefreshUnbless.Text = "&Refresh"
        '
        'BlessingAuth
        '
        Me.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(809, 461)
        Me.Controls.Add(Me.BlessPageTab)
        Me.Controls.Add(Me.ButBatal)
        Me.Controls.Add(Me.ButVoid)
        Me.Controls.Add(Me.ButRefreshUnbless)
        Me.Controls.Add(Me.ButAuthorize)
        Me.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Name = "BlessingAuth"
        Me.Text = "BlessingAuth"
        CType(Me.BlessPageTab, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BlessPageTab.ResumeLayout(False)
        Me.PageUnauth.ResumeLayout(False)
        CType(Me.CheckAll.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DaftarUnauth, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewUnauth, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PageBlessLists.ResumeLayout(False)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateCreateStart.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateCreateStart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateCreateEnd.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateCreateEnd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateAuthStart.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateAuthStart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateAuthEnd.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateAuthEnd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DaftarBlessings, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewBlessings, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemSearchLookUpEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ButBatal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButAuthorize As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents BlessPageTab As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents PageUnauth As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents PageBlessLists As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents DaftarUnauth As DevExpress.XtraGrid.GridControl
    Friend WithEvents ViewUnauth As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents DaftarBlessings As DevExpress.XtraGrid.GridControl
    Friend WithEvents ViewBlessings As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RepositoryItemSearchLookUpEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemSearchLookUpEdit
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RepositoryItemCheckEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents butRefreshBless As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateCreateEnd As DevExpress.XtraEditors.DateEdit
    Friend WithEvents DateCreateStart As DevExpress.XtraEditors.DateEdit
    Friend WithEvents CheckAll As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ButVoid As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateAuthStart As DevExpress.XtraEditors.DateEdit
    Friend WithEvents DateAuthEnd As DevExpress.XtraEditors.DateEdit
    Friend WithEvents RadioGroup1 As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents ButRefreshUnbless As DevExpress.XtraEditors.SimpleButton
End Class
