﻿Imports DevExpress.Utils

Public Class MassBlessingMemberList
    Public pick_blessid As Integer '1=upgrade, 2=point, 3=gift
    Public pick_blessvalue As Decimal
    Public pick_blesstext As String

    Private Sub MassBlessingMemberList_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("JoinedMemberLists")
        xSet.Tables.Remove("MBDaftarGift")
        xSet.Tables.Remove("MBDaftarTipeMembership")
        MassBlessing = Nothing

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub MassBlessingMemberList_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()
        Get_KebutuhanDaftar()
       
    End Sub

    Private Sub MassBlessingMemberList_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown

        LoadSprite.CloseLoading()
    End Sub

    Private Sub CheckAllMember_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CheckAllMember.CheckedChanged
        If CheckAllMember.EditValue Then
            For Each row As DataRow In xSet.Tables("JoinedMemberLists").Rows
                row.Item("ChoosenOnes") = True
            Next
        Else
            For Each row As DataRow In xSet.Tables("JoinedMemberLists").Rows
                row.Item("ChoosenOnes") = False
            Next
        End If
    End Sub

    Private Sub ButSelectBlessing_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSelectBlessing.Click
        ShowModule(MassBlessing, "Mass Blessing")

        PickedBlessing.Text = IIf(pick_blessid = 0, "", pick_blesstext)
        If pick_blessid <> 0 Then
            Select Case pick_blessid
                Case 1
                    PickedBlessing.Text = "Upgrade : " & pick_blesstext
                Case 2
                    PickedBlessing.Text = "Point : " & pick_blesstext
                Case 3
                    PickedBlessing.Text = "Gift : " & pick_blesstext
            End Select
        Else
            PickedBlessing.Text = ""
        End If
    End Sub

    Private Sub ButOK_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButOK.Click
        If Not beforeSave() Then Return
        Try
            LoadSprite.ShowLoading(Me)

            SQLquery = ""
            For Each drow As DataRow In xSet.Tables("JoinedMemberLists").Select("ChoosenOnes=1")
                'if tipe member = picked -> skip
                'if member alr hav the gift -> skip
                Select Case pick_blessid
                    Case 1
                        If drow.Item("id_tipemembership") = pick_blessvalue Then Continue For
                        SQLquery = String.Format("CALL Blessing({0}, {1}, {2}, {3}, '{4}', {5});",
                                            id_cabang,
                                            drow.Item("ID"),
                                            pick_blessid - 1,
                                            pick_blessvalue,
                                            pick_blesstext,
                                            staff_id)
                    Case 2
                        SQLquery = String.Format("CALL Blessing({0}, {1}, {2}, {3}, '{4}', {5});",
                                            id_cabang,
                                            drow.Item("ID"),
                                            pick_blessid - 1,
                                            pick_blessvalue,
                                            pick_blesstext,
                                            staff_id)
                    Case 3
                        SQLquery = String.Format("CALL Blessing({0}, {1}, {2}, {3}, '{4}', {5});",
                                            id_cabang,
                                            drow.Item("ID"),
                                            pick_blessid - 1,
                                            pick_blessvalue,
                                            pick_blesstext,
                                            staff_id)
                End Select
                ExDb.ExecData(1, SQLquery)
            Next

            MsgInfo("Data berhasil disimpan, tekan OK untuk melanjutkan.")
        Catch ex As Exception
            MsgErrorDev()
        End Try
        LoadSprite.CloseLoading()
        Get_KebutuhanDaftar()
    End Sub

    Private Sub ButClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButClose.Click
        Close()
    End Sub

    Private Sub Get_KebutuhanDaftar()
        If Not xSet.Tables("JoinedMemberLists") Is Nothing Then xSet.Tables("JoinedMemberLists").Clear()
        SQLquery = "SELECT a.Inactive ChoosenOnes, idCust ID, a.id_tipemembership, d.nama_tipemembership Tipe, d.level_tipemembership Level, namaCust Nama, BirthDate Bday, CASE WHEN sex=0 THEN 'Pria' ELSE 'Wanita' END Sex, homeAddress Alamat, b.nama_kota Kota, c.nama_area 'Area', Phone Telp, PhoneMobile HP, Religion Agama, StatusKawin Menikah, Email, joinmembership_date JoinDate, Points, Stamp, Blacklist " & _
                    "FROM mcustomer a INNER JOIN mbsm_lockota b ON a.id_kota=b.id_kota LEFT JOIN mbsm_locarea c ON a.id_area=c.id_area INNER JOIN mbsm_tipemembership d ON a.id_tipemembership=d.id_tipemembership WHERE a.id_tipemembership IS NOT NULL AND a.inactive=0;"

        If xSet.Tables("JoinedMemberLists") Is Nothing Then
            ExDb.ExecQuery(1, SQLquery, xSet, "JoinedMemberLists")
            DaftarJoinedMember.DataSource = xSet.Tables("JoinedMemberLists").DefaultView

            DaftarJoinedMember.RepositoryItems.Add(CheckedMember)

            ViewJoinedMember.Columns("ChoosenOnes").ColumnEdit = CheckedMember
            ViewJoinedMember.Columns("ChoosenOnes").Caption = "..."
            ViewJoinedMember.Columns("ID").Visible = False
            ViewJoinedMember.Columns("id_tipemembership").Visible = False

            For Each coll As DataColumn In xSet.Tables("JoinedMemberLists").Columns
                If Not coll.Caption = "ChoosenOnes" Then ViewJoinedMember.Columns(coll.ColumnName).OptionsColumn.AllowEdit = False
                ViewJoinedMember.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
            Next

            ViewJoinedMember.Columns("Bday").DisplayFormat.FormatType = FormatType.DateTime
            ViewJoinedMember.Columns("Bday").DisplayFormat.FormatString = "dd MMM yyyy"
            ViewJoinedMember.Columns("JoinDate").DisplayFormat.FormatType = FormatType.DateTime
            ViewJoinedMember.Columns("JoinDate").DisplayFormat.FormatString = "dd MMM yyyy"
            ViewJoinedMember.Columns("Points").DisplayFormat.FormatType = FormatType.Numeric
            ViewJoinedMember.Columns("Points").DisplayFormat.FormatString = "{0:n2}"

            ViewJoinedMember.Columns("ChoosenOnes").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            ViewJoinedMember.Columns("ChoosenOnes").SummaryItem.DisplayFormat = "Jumlah:{0:n0}"

            ViewJoinedMember.OptionsView.ColumnAutoWidth = False

            ViewJoinedMember.Columns("ChoosenOnes").Width = 60
            ViewJoinedMember.Columns("Tipe").Width = 100
            ViewJoinedMember.Columns("Level").Width = 60
            ViewJoinedMember.Columns("Nama").Width = 150
            ViewJoinedMember.Columns("Bday").Width = 120
            ViewJoinedMember.Columns("Sex").Width = 60
            ViewJoinedMember.Columns("Alamat").Width = 225
            ViewJoinedMember.Columns("Kota").Width = 120
            ViewJoinedMember.Columns("Area").Width = 120
            ViewJoinedMember.Columns("Telp").Width = 120
            ViewJoinedMember.Columns("HP").Width = 120
            ViewJoinedMember.Columns("Agama").Width = 100
            ViewJoinedMember.Columns("Menikah").Width = 70
            ViewJoinedMember.Columns("Email").Width = 120
            ViewJoinedMember.Columns("JoinDate").Width = 120
            ViewJoinedMember.Columns("Points").Width = 100
            ViewJoinedMember.Columns("Stamp").Width = 70
            ViewJoinedMember.Columns("Blacklist").Width = 60
        Else
            ExDb.ExecQuery(1, SQLquery, xSet, "JoinedMemberLists")
        End If
    End Sub

    Private Function beforeSave() As Boolean
        If pick_blessid = 0 Then Return False
        If xSet.Tables("JoinedMemberLists").Select("ChoosenOnes=1").Length < 1 Then Return False

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then Return False

        Return True
    End Function
End Class