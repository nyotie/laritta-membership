﻿Imports DevExpress.Utils

Public Class Blessing

    Private Sub Blessing_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("BlessableGifts")
        xSet.Tables.Remove("DaftarTipeMembership")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub Blessing_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading(Me)
        Get_KebutuhanDaftar()
    End Sub

    Private Sub Blessing_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        LoadSprite.CloseLoading()
    End Sub

    Private Sub TipeBlessings_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TipeBlessings.SelectedIndexChanged
        Select Case TipeBlessings.SelectedIndex
            Case 0
                'upgrade
                DaftarBlessingUpgrade.Enabled = True
                BlessingPoint.Enabled = False
                DaftarBlessableGift.Enabled = False
            Case 1
                'point
                DaftarBlessingUpgrade.Enabled = False
                BlessingPoint.Enabled = True
                DaftarBlessableGift.Enabled = False
            Case 2
                'gift
                DaftarBlessingUpgrade.Enabled = False
                BlessingPoint.Enabled = False
                DaftarBlessableGift.Enabled = True
        End Select
    End Sub

    Private Sub ButOK_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButOK.Click
        If Not beforeSave() Then Return
        Try
            LoadSprite.ShowLoading(Me)

            Select Case TipeBlessings.SelectedIndex
                'idcbg, idcust, bless, num, name, staff
                Case 0
                    SQLquery = String.Format("CALL Blessing({0}, {1}, {2}, {3}, '{4}', {5});",
                                             id_cabang,
                                             CheckMember.CurrentMember,
                                             TipeBlessings.SelectedIndex,
                                             DaftarBlessingUpgrade.EditValue,
                                             ViewBlessingUpgrade.GetFocusedRowCellValue("Nama"),
                                             staff_id)
                Case 1
                    SQLquery = String.Format("CALL Blessing({0}, {1}, {2}, {3}, '{4} point', {5});",
                                             id_cabang,
                                             CheckMember.CurrentMember,
                                             TipeBlessings.SelectedIndex,
                                             BlessingPoint.EditValue,
                                             IIf(BlessingPoint.EditValue > 0, "+", "") & BlessingPoint.Text,
                                             staff_id)
                Case 2
                    SQLquery = String.Format("CALL Blessing({0}, {1}, {2}, {3}, '{4}', {5});",
                                             id_cabang,
                                             CheckMember.CurrentMember,
                                             TipeBlessings.SelectedIndex,
                                             DaftarBlessableGift.EditValue,
                                             ViewBlessableGift.GetFocusedRowCellValue("Nama"),
                                             staff_id)
            End Select
            ExDb.ExecData(1, SQLquery)

            MsgInfo("Data berhasil disimpan, tekan OK untuk melanjutkan.")
        Catch ex As Exception
            MsgErrorDev()
        End Try
        LoadSprite.CloseLoading()
        Close()
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Sub Get_KebutuhanDaftar()
        '---------------------------------------------------Tipe Membership
        SQLquery = String.Format("SELECT id_tipemembership ID, nama_tipemembership Nama, level_tipemembership Level, syarat_krt Syarat, harga_krt Harga FROM mbsm_tipemembership a WHERE a.Inactive=0 AND id_tipemembership<>{0} AND level_tipemembership>={1};",
                                xSet.Tables("ThisMemberData").Rows(0).Item("id_tipemembership"), xSet.Tables("ThisMemberData").Rows(0).Item("level_tipemembership"))
        ExDb.ExecQuery(1, SQLquery, xSet, "DaftarTipeMembership")
        DaftarBlessingUpgrade.Properties.DataSource = xSet.Tables("DaftarTipeMembership").DefaultView

        DaftarBlessingUpgrade.Properties.NullText = ""
        DaftarBlessingUpgrade.Properties.ValueMember = "ID"
        DaftarBlessingUpgrade.Properties.DisplayMember = "Nama"
        DaftarBlessingUpgrade.Properties.ShowClearButton = False
        DaftarBlessingUpgrade.Properties.PopulateViewColumns()

        ViewBlessingUpgrade.Columns("ID").Visible = False
        ViewBlessingUpgrade.Columns("Syarat").DisplayFormat.FormatType = FormatType.Numeric
        ViewBlessingUpgrade.Columns("Syarat").DisplayFormat.FormatString = "Rp{0:n0}"
        ViewBlessingUpgrade.Columns("Harga").DisplayFormat.FormatType = FormatType.Numeric
        ViewBlessingUpgrade.Columns("Harga").DisplayFormat.FormatString = "Rp{0:n0}"

        For Each coll As DataColumn In xSet.Tables("DaftarTipeMembership").Columns
            ViewBlessingUpgrade.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        '---------------------------------------------------Gift
        SQLquery = String.Format("SELECT a.id_gift ID, b.nama_gift Nama, CASE WHEN b.expired_day=0 THEN DATE_FORMAT(expired_date, '%d %b %Y') " & _
                                "ELSE CONCAT(b.expired_day, ' hari') END Expiring, b.repetisi_gift Repetisi, b.reward_value Nilai, b.syarat_none Syarat " & _
                                "FROM mbsm_tipemembershipgift a INNER JOIN mbsm_gift b ON a.id_gift=b.id_gift AND a.id_tipemembership={0} WHERE b.Inactive=0 " & _
                                "AND IFNULL(b.expired_date, CURRENT_DATE()+1)>CURRENT_DATE() AND a.id_gift NOT IN (SELECT x.id_gift FROM mbsm_membergifts x WHERE x.id_customer={1} AND x.Inactive=0);",
                                xSet.Tables("ThisMemberData").Rows(0).Item("id_tipemembership"), CheckMember.CurrentMember)
        ExDb.ExecQuery(1, SQLquery, xSet, "BlessableGifts")
        DaftarBlessableGift.Properties.DataSource = xSet.Tables("BlessableGifts").DefaultView

        DaftarBlessableGift.Properties.NullText = ""
        DaftarBlessableGift.Properties.ValueMember = "ID"
        DaftarBlessableGift.Properties.DisplayMember = "Nama"
        DaftarBlessableGift.Properties.ShowClearButton = False
        DaftarBlessableGift.Properties.PopulateViewColumns()

        ViewBlessableGift.Columns("ID").Visible = False
        ViewBlessableGift.Columns("Nilai").DisplayFormat.FormatType = FormatType.Numeric
        ViewBlessableGift.Columns("Nilai").DisplayFormat.FormatString = "Rp{0:n0}"

        ViewBlessableGift.Columns("Nama").Width = 250
        ViewBlessableGift.Columns("Expiring").Width = 150
        ViewBlessableGift.Columns("Repetisi").Width = 100
        ViewBlessableGift.Columns("Nilai").Width = 150
        ViewBlessableGift.Columns("Syarat").Width = 80

        For Each coll As DataColumn In xSet.Tables("BlessableGifts").Columns
            ViewBlessableGift.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next
    End Sub

    Private Function beforeSave() As Boolean
        Select Case TipeBlessings.SelectedIndex
            Case 0
                If DaftarBlessingUpgrade.EditValue Is Nothing Then Return False
            Case 1
                If BlessingPoint.EditValue = 0 Or BlessingPoint.EditValue Is Nothing Then Return False
            Case 2
                If DaftarBlessableGift.EditValue Is Nothing Then Return False
        End Select

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then Return False

        Return True
    End Function
End Class