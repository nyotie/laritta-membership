﻿Imports DevExpress.Utils

Public Class Settlement

    Private Sub Settlement_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("UncollectedRewards")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub Settlement_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()
        Get_UncollectedReward(UncollectedOnly.Checked)
    End Sub

    Private Sub Settlement_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        ButPrint.Enabled = xSet.Tables("User_ProgPermisisons").Select("permission_id=35")(0).Item("allow_print")
        LoadSprite.CloseLoading()
    End Sub

    Private Sub ButPrint_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButPrint.Click
        If ViewUncollectedRewards.RowCount < 1 Then Return

        Using printDPesanan As New PrintOut_UncollectedRewards
            Using tool As DevExpress.XtraReports.UI.ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(printDPesanan)
                tool.ShowRibbonPreviewDialog()
            End Using
        End Using
    End Sub

    Private Sub butRefresh_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butRefresh.Click
        Get_UncollectedReward(UncollectedOnly.Checked)
    End Sub

    Private Sub butClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butClose.Click
        Close()
    End Sub

    Private Sub Get_UncollectedReward(ByVal ShowUncollected As Boolean)
        '---------------------------Event Logs
        If Not xSet.Tables("UncollectedRewards") Is Nothing Then xSet.Tables("UncollectedRewards").Clear()
        SQLquery = String.Format("CALL GetUncollectedReward (NOW(),{0});", id_cabang)
        ExDb.ExecQuery(1, SQLquery, xSet, "UncollectedRewards")

        '----------------------------Grid Settings
        UncollectedRewards.DataSource = xSet.Tables("UncollectedRewards").DefaultView
        xSet.Tables("UncollectedRewards").DefaultView.RowFilter = IIf(ShowUncollected, "Redeem='False'", "")

        For Each coll As DataColumn In xSet.Tables("UncollectedRewards").Columns
            ViewUncollectedRewards.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        'UncollectedRewards.RepositoryItems.Add(Redeemed)

        'ViewUncollectedRewards.Columns("Kode").Visible = False
        ViewUncollectedRewards.Columns("Redeem").ColumnEdit = Redeemed

        ViewUncollectedRewards.Columns("Tanggal").DisplayFormat.FormatType = FormatType.DateTime
        ViewUncollectedRewards.Columns("Tanggal").DisplayFormat.FormatString = "dd MMM yyyy, HH:mm"
        ViewUncollectedRewards.Columns("TypeReward").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count
        ViewUncollectedRewards.Columns("TypeReward").SummaryItem.DisplayFormat = "Jumlah:{0:n0}"
        ViewUncollectedRewards.BestFitColumns()
    End Sub
End Class