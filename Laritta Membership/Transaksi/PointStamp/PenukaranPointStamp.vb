﻿Imports DevExpress.Utils

Public Class PenukaranPointStamp

    Private Sub PenukaranPointStamp_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("DaftarPoint")
        xSet.Tables.Remove("DaftarStamp")
        xSet.Tables.Remove("HasilInsertPointStamp")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub PenukaranPointStamp_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()
        Get_KebutuhanDaftar()
    End Sub

    Private Sub PenukaranPointStamp_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        JumlahPoint.EditValue = xSet.Tables("ThisMemberData").Rows(0).Item("points")
        JumlahStamp.EditValue = xSet.Tables("ThisMemberData").Rows(0).Item("stamp")
        DaftarRewardPoint.Focus()

        LoadSprite.CloseLoading()
    End Sub

    Private Sub DaftarRewardPoint_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DaftarRewardPoint.EditValueChanged
        If DaftarRewardPoint.Enabled = False Then Return

        KebutuhanPoint.EditValue = ViewRewardPoint.GetFocusedRowCellValue("Syarat")
        SisaPoint.EditValue = JumlahPoint.EditValue - KebutuhanPoint.EditValue
    End Sub

    Private Sub DaftarRewardStamp_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DaftarRewardStamp.EditValueChanged
        If DaftarRewardStamp.Enabled = False Then Return

        KebutuhanStamp.EditValue = ViewRewardStamp.GetFocusedRowCellValue("Syarat")
        SisaStamp.EditValue = JumlahStamp.EditValue - KebutuhanStamp.EditValue
    End Sub

    Private Sub JmlPenggunaanPoint_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles JmlPenggunaanPoint.EditValueChanged
        If JmlPenggunaanPoint.EditValue <= 0 Then
            SisaPoint.EditValue = JumlahPoint.EditValue
        Else
            SisaPoint.EditValue = JumlahPoint.EditValue - (JmlPenggunaanPoint.EditValue * KebutuhanPoint.EditValue)
        End If
    End Sub

    Private Sub JmlPenggunaanStamp_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles JmlPenggunaanStamp.EditValueChanged
        If JmlPenggunaanPoint.EditValue <= 0 Then
            SisaStamp.EditValue = JumlahStamp.EditValue
        Else
            SisaStamp.EditValue = JumlahStamp.EditValue - (JmlPenggunaanStamp.EditValue * KebutuhanStamp.EditValue)
        End If
    End Sub

    Private Sub ButOK_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButOK.Click
        If Not beforeSave() Then Return
        Try
            LoadSprite.ShowLoading()

            If XtraTabControl1.SelectedTabPageIndex = 0 Then
                'point
                SQLquery = String.Format("CALL InsertExchangePoints_M({0},{1},{2},{3},{4},{5},{6})",
                                         id_cabang,
                                         CheckMember.CurrentMember,
                                         DaftarRewardPoint.EditValue,
                                         KebutuhanPoint.EditValue * JmlPenggunaanPoint.EditValue * -1,
                                         ViewRewardPoint.GetFocusedRowCellValue("reward_value") * JmlPenggunaanPoint.EditValue,
                                         staff_id, JmlPenggunaanPoint.EditValue)
            Else
                'stamp
                SQLquery = String.Format("CALL InsertExchangeStamps_M({0},{1},{2},{3},{4},{5},{6})",
                                         id_cabang,
                                         CheckMember.CurrentMember,
                                         DaftarRewardStamp.EditValue,
                                         KebutuhanStamp.EditValue * JmlPenggunaanStamp.EditValue * -1,
                                         ViewRewardStamp.GetFocusedRowCellValue("reward_value") * JmlPenggunaanStamp.EditValue,
                                         staff_id, JmlPenggunaanStamp.EditValue)
            End If
            ExDb.ExecQuery(1, SQLquery, xSet, "HasilInsertPointStamp")

            MsgInfo("Data berhasil disimpan, tekan OK untuk melanjutkan.")
            LoadSprite.CloseLoading()
        Catch ex As Exception
            LoadSprite.CloseLoading()
            MsgErrorDev()
            Return
        End Try

        Using NotaPenukaranPointDanStamp As New PrintOut_PenukaranPointDanStamp
            Using tool As DevExpress.XtraReports.UI.ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(NotaPenukaranPointDanStamp)
                tool.ShowRibbonPreviewDialog()
            End Using
        End Using
        Close()
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Sub Get_KebutuhanDaftar()
        '--------------------------------Daftar Point
        SQLquery = "SELECT id_rewardpoint ID, nama_rewardpoint Nama, reward_specification Spek, reward_value, required_point Syarat FROM mbsm_rewardpoint WHERE Inactive=0 AND expired_date>=CURDATE();"
        ExDb.ExecQuery(1, SQLquery, xSet, "DaftarPoint")
        DaftarRewardPoint.Properties.DataSource = xSet.Tables("DaftarPoint").DefaultView

        DaftarRewardPoint.Properties.NullText = ""
        DaftarRewardPoint.Properties.ValueMember = "ID"
        DaftarRewardPoint.Properties.DisplayMember = "Nama"
        DaftarRewardPoint.Properties.ShowClearButton = False
        DaftarRewardPoint.Properties.PopulateViewColumns()

        DaftarRewardPoint.Properties.PopupFormSize = New Size(400, 300)
        ViewRewardPoint.RowHeight = 45

        ViewRewardPoint.Columns("ID").Visible = False
        ViewRewardPoint.Columns("reward_value").Visible = False
        ViewRewardPoint.Columns("Syarat").DisplayFormat.FormatType = FormatType.Numeric
        ViewRewardPoint.Columns("Syarat").DisplayFormat.FormatString = "{0:n0}"

        For Each coll As DataColumn In xSet.Tables("DaftarPoint").Columns
            ViewRewardPoint.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        '--------------------------------Daftar Stamp
        SQLquery = "SELECT id_rewardstamp ID, nama_rewardstamp Nama, reward_specification Spek, reward_value, required_stamp Syarat FROM mbsm_rewardstamp WHERE Inactive=0 AND expired_date>=CURDATE();"
        ExDb.ExecQuery(1, SQLquery, xSet, "DaftarStamp")
        DaftarRewardStamp.Properties.DataSource = xSet.Tables("DaftarStamp").DefaultView

        DaftarRewardStamp.Properties.NullText = ""
        DaftarRewardStamp.Properties.ValueMember = "ID"
        DaftarRewardStamp.Properties.DisplayMember = "Nama"
        DaftarRewardStamp.Properties.ShowClearButton = False
        DaftarRewardStamp.Properties.PopulateViewColumns()

        DaftarRewardStamp.Properties.PopupFormSize = New Size(400, 300)
        ViewRewardStamp.RowHeight = 45

        ViewRewardStamp.Columns("ID").Visible = False
        ViewRewardStamp.Columns("reward_value").Visible = False

        For Each coll As DataColumn In xSet.Tables("DaftarStamp").Columns
            ViewRewardStamp.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next
    End Sub

    Private Function beforeSave() As Boolean
        If XtraTabControl1.SelectedTabPageIndex = 0 Then
            'point
            If DaftarRewardPoint.EditValue Is Nothing Then
                MsgWarning("Point reward belum dipilih!")
                DaftarRewardPoint.Focus()
                Return False
            End If

            If SisaPoint.EditValue < 0 Then
                MsgWarning("Point tidak cukup!")
                DaftarRewardPoint.Focus()
                Return False
            End If
        Else
            'stamp
            If DaftarRewardStamp.EditValue Is Nothing Then
                MsgWarning("Stamp reward belum dipilih!")
                DaftarRewardStamp.Focus()
                Return False
            End If

            If SisaStamp.EditValue < 0 Then
                MsgWarning("Stamp tidak cukup!")
                DaftarRewardStamp.Focus()
                Return False
            End If

            If JmlPenggunaanStamp.EditValue < 0 Or JmlPenggunaanStamp.EditValue = Nothing Then
                MsgWarning("Inputkan jumlah kelipatan penggunaan stamp!")
                JmlPenggunaanStamp.Focus()
                Return False
            End If
        End If

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        Return True
    End Function
End Class