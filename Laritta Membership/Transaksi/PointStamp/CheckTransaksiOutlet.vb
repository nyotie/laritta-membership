﻿Imports DevExpress.Utils

Public Class CheckTransaksiOutlet
    Dim CurrentTrans As Integer

    Private Sub CheckTransaksiOutlet_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("ThisNota")
        xSet.Tables.Remove("This_mtpos")
        xSet.Tables.Remove("This_dtpos")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub CheckTransaksiOutlet_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub CheckTransaksiOutlet_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown

    End Sub

    Private Sub NoNota_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles NoNota.EditValueChanged
        If NoNota.EditValue.ToString.Length < 11 Then Return

        If NoNota.EditValue.ToString.Length = 15 And CurrentTrans = 0 Then
            Dim Tahun As String = ""
            Dim Bulan As String = ""
            Dim Nomer As Integer = 0

            Tahun = "20" & Mid(NoNota.Text, 4, 2)
            Bulan = Mid(NoNota.Text, 7, 2)
            Nomer = Microsoft.VisualBasic.Right(NoNota.Text, 6)

            '----------------------------------------------CHECK NOTA SUDAH TERDAFTAR/BELUM
            If Not xSet.Tables("ThisNota") Is Nothing Then xSet.Tables("ThisNota").Clear()
            SQLquery = String.Format("SELECT b.namaCust, a.tgl_input FROM mbst_posm a INNER JOIN mcustomer b ON a.idCust=b.idCust WHERE a.idCabang={0} AND nomor_nota='{1}';", id_cabang, NoNota.Text)
            ExDb.ExecQuery(1, SQLquery, xSet, "ThisNota") 'TRY AT ONLINE, IF NOT MULTISERVER IT WILL AUTOMATICALLY GO FOR LOCAL
            If xSet.Tables("ThisNota").Rows.Count > 0 Then
                MsgWarning(String.Format("Nota ini sudah digunakan tanggal [{0}] oleh [{1}]", Format(xSet.Tables("ThisNota").Rows(0).Item("tgl_input"), "dd MMMM yyyy HH:mm"), xSet.Tables("ThisNota").Rows(0).Item("namaCust")))
                Return
            End If

            '----------------------------------------------IF BELUM TERDAFTAR, CHECK STATUS NOTA VOID/TDK
            If Not xSet.Tables("This_mtpos") Is Nothing Then xSet.Tables("This_mtpos").Clear()
            SQLquery = String.Format("SELECT idOrder ID, totalQTY totalqtt, disc Diskon, subTotal-disc TotalTrans, createDate TglTrans, isVoid FROM mtpos WHERE YEAR(createdate) ='{0}' AND MONTH(createdate) ='{1}' AND numorder ={2} AND isVoid = 0 ", Tahun, Bulan, Nomer)
            ExDb.ExecQuery(0, SQLquery, xSet, "This_mtpos")

            If xSet.Tables("This_mtpos").Rows.Count > 0 Then
                If xSet.Tables("This_mtpos").Rows(0).Item("isVoid") = 1 Then
                    MsgWarning("No nota telah tervoid!")
                    Return
                Else
                    CurrentTrans = xSet.Tables("This_mtpos").Rows(0).Item("ID")
                    TransDiskon.EditValue = xSet.Tables("This_mtpos").Rows(0).Item("Diskon")
                    TransGrandTotal.EditValue = xSet.Tables("This_mtpos").Rows(0).Item("TotalTrans")
                End If
            Else
                MsgWarning("Nomor nota tidak ditemukan!")
                Return
            End If

            '----------------------------------------------IF NOT VOID, GET DETAIL TRANSAKSI
            If Not xSet.Tables("This_dtpos") Is Nothing Then xSet.Tables("This_dtpos").Clear()
            SQLquery = String.Format("SELECT a.idBrg ID, b.namaBrg Nama, a.quantity Jml, a.price Hrg, a.disc Disc, a.totalPrice Total FROM dtpos a INNER JOIN mbarang b ON a.idBrg=b.idBrg AND a.status=1 WHERE idOrder={0};", CurrentTrans)
            ExDb.ExecQuery(0, SQLquery, xSet, "This_dtpos")

            DaftarTransaksiOutlet.DataSource = xSet.Tables("This_dtpos").DefaultView

            ViewTransaksiOutlet.Columns("ID").Visible = False

            ViewTransaksiOutlet.Columns("Hrg").DisplayFormat.FormatType = FormatType.Numeric
            ViewTransaksiOutlet.Columns("Hrg").DisplayFormat.FormatString = "Rp{0:n0}"
            ViewTransaksiOutlet.Columns("Disc").DisplayFormat.FormatType = FormatType.Numeric
            ViewTransaksiOutlet.Columns("Disc").DisplayFormat.FormatString = "Rp{0:n0}"
            ViewTransaksiOutlet.Columns("Total").DisplayFormat.FormatType = FormatType.Numeric
            ViewTransaksiOutlet.Columns("Total").DisplayFormat.FormatString = "Rp{0:n0}"

            ViewTransaksiOutlet.Columns("Nama").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count
            ViewTransaksiOutlet.Columns("Nama").SummaryItem.DisplayFormat = "Jumlah:{0:n0}"
            ViewTransaksiOutlet.Columns("Total").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            ViewTransaksiOutlet.Columns("Total").SummaryItem.DisplayFormat = "Subtotal:{0:n0}"

            For Each coll As DataColumn In xSet.Tables("This_dtpos").Columns
                ViewTransaksiOutlet.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
            Next

            ViewTransaksiOutlet.Columns("Nama").Width = 250
            ViewTransaksiOutlet.Columns("Jml").Width = 60
            ViewTransaksiOutlet.Columns("Hrg").Width = 100
            ViewTransaksiOutlet.Columns("Disc").Width = 80
            ViewTransaksiOutlet.Columns("Total").Width = 120

        ElseIf NoNota.EditValue.ToString.Length = 12 And CurrentTrans <> 0 Then
            ClearAll()
        End If
    End Sub

    Private Sub ButClear_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButClear.Click
        ClearAll()
    End Sub

    Private Sub ButOK_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButOK.Click
        If Not beforeSave() Then Return
        Try
            LoadSprite.ShowLoading("Menyimpan transaksi", "Loading", Me)
            'idcust, idcbg, idpos, tglpos, nonota, totalqtt, totalrp, pelaku, isidt
            SQLquery = String.Format("CALL InsertTransOutlet ({0}, {1}, {2}, '{3}', '{4}', {5}, {6}, {7}, '",
                                     CheckMember.CurrentMember,
                                     id_cabang,
                                     CurrentTrans,
                                     Format(xSet.Tables("This_mtpos").Rows(0).Item("TglTrans"), "yyyy-MM-dd HH:mm:ss"),
                                     NoNota.Text,
                                     xSet.Tables("This_mtpos").Rows(0).Item("totalqtt"),
                                     xSet.Tables("This_mtpos").Rows(0).Item("TotalTrans"),
                                     staff_id)
            For Each drow As DataRow In xSet.Tables("This_dtpos").Rows
                SQLquery += String.Format("({0}, {1}, {2}, {3}, {4}, {5}, {6}), ", CurrentTrans, id_cabang, drow.Item("ID"), drow.Item("Jml"), drow.Item("Hrg"), drow.Item("Disc"), drow.Item("Total"))
            Next
            SQLquery = Mid(SQLquery, 1, SQLquery.Length - 2) & "'); "
            ExDb.ExecData(1, SQLquery)

            MsgInfo("Data berhasil disimpan, tekan OK untuk melanjutkan.")
        Catch ex As Exception
            MsgErrorDev()
        End Try
        LoadSprite.CloseLoading()
        Close()
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Sub ClearAll()
        CurrentTrans = 0

        NoNota.EditValue = ""
        TransDiskon.EditValue = 0
        TransGrandTotal.EditValue = 0

        If Not xSet.Tables("ThisNota") Is Nothing Then xSet.Tables("ThisNota").Clear()
        If Not xSet.Tables("This_mtpos") Is Nothing Then xSet.Tables("This_mtpos").Clear()
        If Not xSet.Tables("This_dtpos") Is Nothing Then xSet.Tables("This_dtpos").Clear()
    End Sub

    Private Function beforeSave() As Boolean
        If NoNota.Text = "" Or CurrentTrans = 0 Then Return False

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        Return True
    End Function
End Class