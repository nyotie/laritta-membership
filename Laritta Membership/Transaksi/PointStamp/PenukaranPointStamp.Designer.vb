﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PenukaranPointStamp
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
        Me.PagePoint = New DevExpress.XtraTab.XtraTabPage()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.JmlPenggunaanPoint = New DevExpress.XtraEditors.TextEdit()
        Me.DaftarRewardPoint = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.ViewRewardPoint = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.SisaPoint = New DevExpress.XtraEditors.TextEdit()
        Me.KebutuhanPoint = New DevExpress.XtraEditors.TextEdit()
        Me.JumlahPoint = New DevExpress.XtraEditors.TextEdit()
        Me.PageStamp = New DevExpress.XtraTab.XtraTabPage()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.SisaStamp = New DevExpress.XtraEditors.TextEdit()
        Me.JmlPenggunaanStamp = New DevExpress.XtraEditors.TextEdit()
        Me.KebutuhanStamp = New DevExpress.XtraEditors.TextEdit()
        Me.DaftarRewardStamp = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.ViewRewardStamp = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.JumlahStamp = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.ButBatal = New DevExpress.XtraEditors.SimpleButton()
        Me.ButOK = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.PagePoint.SuspendLayout()
        CType(Me.JmlPenggunaanPoint.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DaftarRewardPoint.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewRewardPoint, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SisaPoint.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.KebutuhanPoint.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.JumlahPoint.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PageStamp.SuspendLayout()
        CType(Me.SisaStamp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.JmlPenggunaanStamp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.KebutuhanStamp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DaftarRewardStamp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewRewardStamp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.JumlahStamp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.XtraTabControl1.AppearancePage.Header.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.XtraTabControl1.AppearancePage.Header.Options.UseFont = True
        Me.XtraTabControl1.Location = New System.Drawing.Point(12, 12)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.PagePoint
        Me.XtraTabControl1.Size = New System.Drawing.Size(363, 194)
        Me.XtraTabControl1.TabIndex = 0
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.PagePoint, Me.PageStamp})
        '
        'PagePoint
        '
        Me.PagePoint.Controls.Add(Me.LabelControl8)
        Me.PagePoint.Controls.Add(Me.JmlPenggunaanPoint)
        Me.PagePoint.Controls.Add(Me.DaftarRewardPoint)
        Me.PagePoint.Controls.Add(Me.LabelControl5)
        Me.PagePoint.Controls.Add(Me.LabelControl3)
        Me.PagePoint.Controls.Add(Me.LabelControl2)
        Me.PagePoint.Controls.Add(Me.LabelControl11)
        Me.PagePoint.Controls.Add(Me.SisaPoint)
        Me.PagePoint.Controls.Add(Me.KebutuhanPoint)
        Me.PagePoint.Controls.Add(Me.JumlahPoint)
        Me.PagePoint.Image = Global.Laritta_Membership.My.Resources.Resources.point_32
        Me.PagePoint.Name = "PagePoint"
        Me.PagePoint.Size = New System.Drawing.Size(357, 149)
        Me.PagePoint.Text = "Point"
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl8.Location = New System.Drawing.Point(234, 82)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl8.TabIndex = 8
        Me.LabelControl8.Text = "x"
        '
        'JmlPenggunaanPoint
        '
        Me.JmlPenggunaanPoint.EditValue = "1"
        Me.JmlPenggunaanPoint.Location = New System.Drawing.Point(247, 79)
        Me.JmlPenggunaanPoint.Name = "JmlPenggunaanPoint"
        Me.JmlPenggunaanPoint.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.JmlPenggunaanPoint.Properties.Appearance.Options.UseFont = True
        Me.JmlPenggunaanPoint.Properties.Appearance.Options.UseTextOptions = True
        Me.JmlPenggunaanPoint.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.JmlPenggunaanPoint.Properties.Mask.EditMask = "n0"
        Me.JmlPenggunaanPoint.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.JmlPenggunaanPoint.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.JmlPenggunaanPoint.Properties.NullText = "1"
        Me.JmlPenggunaanPoint.Size = New System.Drawing.Size(41, 21)
        Me.JmlPenggunaanPoint.TabIndex = 3
        '
        'DaftarRewardPoint
        '
        Me.DaftarRewardPoint.Location = New System.Drawing.Point(143, 47)
        Me.DaftarRewardPoint.Name = "DaftarRewardPoint"
        Me.DaftarRewardPoint.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DaftarRewardPoint.Properties.Appearance.Options.UseFont = True
        Me.DaftarRewardPoint.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DaftarRewardPoint.Properties.NullText = ""
        Me.DaftarRewardPoint.Properties.View = Me.ViewRewardPoint
        Me.DaftarRewardPoint.Size = New System.Drawing.Size(192, 26)
        Me.DaftarRewardPoint.TabIndex = 1
        '
        'ViewRewardPoint
        '
        Me.ViewRewardPoint.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.ViewRewardPoint.Name = "ViewRewardPoint"
        Me.ViewRewardPoint.OptionsCustomization.AllowColumnMoving = False
        Me.ViewRewardPoint.OptionsCustomization.AllowColumnResizing = False
        Me.ViewRewardPoint.OptionsCustomization.AllowFilter = False
        Me.ViewRewardPoint.OptionsCustomization.AllowGroup = False
        Me.ViewRewardPoint.OptionsCustomization.AllowQuickHideColumns = False
        Me.ViewRewardPoint.OptionsCustomization.AllowSort = False
        Me.ViewRewardPoint.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.ViewRewardPoint.OptionsView.ShowGroupPanel = False
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl5.Location = New System.Drawing.Point(18, 54)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(81, 14)
        Me.LabelControl5.TabIndex = 2
        Me.LabelControl5.Text = "Point reward"
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl3.Location = New System.Drawing.Point(18, 109)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(62, 14)
        Me.LabelControl3.TabIndex = 6
        Me.LabelControl3.Text = "Sisa point"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl2.Location = New System.Drawing.Point(18, 82)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(105, 14)
        Me.LabelControl2.TabIndex = 4
        Me.LabelControl2.Text = "Kebutuhan point"
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl11.Location = New System.Drawing.Point(18, 23)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(77, 14)
        Me.LabelControl11.TabIndex = 0
        Me.LabelControl11.Text = "Point dimiliki"
        '
        'SisaPoint
        '
        Me.SisaPoint.Location = New System.Drawing.Point(143, 106)
        Me.SisaPoint.Name = "SisaPoint"
        Me.SisaPoint.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.SisaPoint.Properties.Appearance.Options.UseFont = True
        Me.SisaPoint.Properties.Appearance.Options.UseTextOptions = True
        Me.SisaPoint.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.SisaPoint.Properties.Mask.EditMask = "n2"
        Me.SisaPoint.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.SisaPoint.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.SisaPoint.Properties.NullText = "0.00"
        Me.SisaPoint.Properties.ReadOnly = True
        Me.SisaPoint.Size = New System.Drawing.Size(145, 21)
        Me.SisaPoint.TabIndex = 4
        Me.SisaPoint.TabStop = False
        '
        'KebutuhanPoint
        '
        Me.KebutuhanPoint.Location = New System.Drawing.Point(143, 79)
        Me.KebutuhanPoint.Name = "KebutuhanPoint"
        Me.KebutuhanPoint.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.KebutuhanPoint.Properties.Appearance.Options.UseFont = True
        Me.KebutuhanPoint.Properties.Appearance.Options.UseTextOptions = True
        Me.KebutuhanPoint.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.KebutuhanPoint.Properties.Mask.EditMask = "n2"
        Me.KebutuhanPoint.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.KebutuhanPoint.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.KebutuhanPoint.Properties.NullText = "0.00"
        Me.KebutuhanPoint.Properties.ReadOnly = True
        Me.KebutuhanPoint.Size = New System.Drawing.Size(85, 21)
        Me.KebutuhanPoint.TabIndex = 2
        Me.KebutuhanPoint.TabStop = False
        '
        'JumlahPoint
        '
        Me.JumlahPoint.Location = New System.Drawing.Point(143, 20)
        Me.JumlahPoint.Name = "JumlahPoint"
        Me.JumlahPoint.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.JumlahPoint.Properties.Appearance.Options.UseFont = True
        Me.JumlahPoint.Properties.Appearance.Options.UseTextOptions = True
        Me.JumlahPoint.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.JumlahPoint.Properties.Mask.EditMask = "n2"
        Me.JumlahPoint.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.JumlahPoint.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.JumlahPoint.Properties.NullText = "0.00"
        Me.JumlahPoint.Properties.ReadOnly = True
        Me.JumlahPoint.Size = New System.Drawing.Size(145, 21)
        Me.JumlahPoint.TabIndex = 0
        Me.JumlahPoint.TabStop = False
        '
        'PageStamp
        '
        Me.PageStamp.Controls.Add(Me.LabelControl4)
        Me.PageStamp.Controls.Add(Me.LabelControl7)
        Me.PageStamp.Controls.Add(Me.LabelControl6)
        Me.PageStamp.Controls.Add(Me.SisaStamp)
        Me.PageStamp.Controls.Add(Me.JmlPenggunaanStamp)
        Me.PageStamp.Controls.Add(Me.KebutuhanStamp)
        Me.PageStamp.Controls.Add(Me.DaftarRewardStamp)
        Me.PageStamp.Controls.Add(Me.LabelControl1)
        Me.PageStamp.Controls.Add(Me.JumlahStamp)
        Me.PageStamp.Controls.Add(Me.LabelControl12)
        Me.PageStamp.Image = Global.Laritta_Membership.My.Resources.Resources.stamp_32
        Me.PageStamp.Name = "PageStamp"
        Me.PageStamp.Size = New System.Drawing.Size(357, 149)
        Me.PageStamp.Text = "Stamp"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl4.Location = New System.Drawing.Point(18, 109)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(69, 14)
        Me.LabelControl4.TabIndex = 7
        Me.LabelControl4.Text = "Sisa stamp"
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl7.Location = New System.Drawing.Point(234, 82)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl7.TabIndex = 4
        Me.LabelControl7.Text = "x"
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl6.Location = New System.Drawing.Point(18, 82)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(112, 14)
        Me.LabelControl6.TabIndex = 4
        Me.LabelControl6.Text = "Kebutuhan stamp"
        '
        'SisaStamp
        '
        Me.SisaStamp.Location = New System.Drawing.Point(143, 106)
        Me.SisaStamp.Name = "SisaStamp"
        Me.SisaStamp.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.SisaStamp.Properties.Appearance.Options.UseFont = True
        Me.SisaStamp.Properties.Appearance.Options.UseTextOptions = True
        Me.SisaStamp.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.SisaStamp.Properties.Mask.EditMask = "n0"
        Me.SisaStamp.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.SisaStamp.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.SisaStamp.Properties.NullText = "0"
        Me.SisaStamp.Properties.ReadOnly = True
        Me.SisaStamp.Size = New System.Drawing.Size(145, 21)
        Me.SisaStamp.TabIndex = 8
        Me.SisaStamp.TabStop = False
        '
        'JmlPenggunaanStamp
        '
        Me.JmlPenggunaanStamp.EditValue = "1"
        Me.JmlPenggunaanStamp.Location = New System.Drawing.Point(247, 79)
        Me.JmlPenggunaanStamp.Name = "JmlPenggunaanStamp"
        Me.JmlPenggunaanStamp.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.JmlPenggunaanStamp.Properties.Appearance.Options.UseFont = True
        Me.JmlPenggunaanStamp.Properties.Appearance.Options.UseTextOptions = True
        Me.JmlPenggunaanStamp.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.JmlPenggunaanStamp.Properties.Mask.EditMask = "n0"
        Me.JmlPenggunaanStamp.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.JmlPenggunaanStamp.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.JmlPenggunaanStamp.Properties.NullText = "1"
        Me.JmlPenggunaanStamp.Size = New System.Drawing.Size(41, 21)
        Me.JmlPenggunaanStamp.TabIndex = 6
        '
        'KebutuhanStamp
        '
        Me.KebutuhanStamp.Location = New System.Drawing.Point(143, 79)
        Me.KebutuhanStamp.Name = "KebutuhanStamp"
        Me.KebutuhanStamp.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.KebutuhanStamp.Properties.Appearance.Options.UseFont = True
        Me.KebutuhanStamp.Properties.Appearance.Options.UseTextOptions = True
        Me.KebutuhanStamp.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.KebutuhanStamp.Properties.Mask.EditMask = "n0"
        Me.KebutuhanStamp.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.KebutuhanStamp.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.KebutuhanStamp.Properties.NullText = "0"
        Me.KebutuhanStamp.Properties.ReadOnly = True
        Me.KebutuhanStamp.Size = New System.Drawing.Size(85, 21)
        Me.KebutuhanStamp.TabIndex = 5
        Me.KebutuhanStamp.TabStop = False
        '
        'DaftarRewardStamp
        '
        Me.DaftarRewardStamp.Location = New System.Drawing.Point(143, 47)
        Me.DaftarRewardStamp.Name = "DaftarRewardStamp"
        Me.DaftarRewardStamp.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DaftarRewardStamp.Properties.Appearance.Options.UseFont = True
        Me.DaftarRewardStamp.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DaftarRewardStamp.Properties.NullText = ""
        Me.DaftarRewardStamp.Properties.View = Me.ViewRewardStamp
        Me.DaftarRewardStamp.Size = New System.Drawing.Size(192, 26)
        Me.DaftarRewardStamp.TabIndex = 3
        '
        'ViewRewardStamp
        '
        Me.ViewRewardStamp.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.ViewRewardStamp.Name = "ViewRewardStamp"
        Me.ViewRewardStamp.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.ViewRewardStamp.OptionsView.ShowGroupPanel = False
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl1.Location = New System.Drawing.Point(18, 54)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(89, 14)
        Me.LabelControl1.TabIndex = 2
        Me.LabelControl1.Text = "Stamp reward"
        '
        'JumlahStamp
        '
        Me.JumlahStamp.Location = New System.Drawing.Point(143, 20)
        Me.JumlahStamp.Name = "JumlahStamp"
        Me.JumlahStamp.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.JumlahStamp.Properties.Appearance.Options.UseFont = True
        Me.JumlahStamp.Properties.Appearance.Options.UseTextOptions = True
        Me.JumlahStamp.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.JumlahStamp.Properties.Mask.EditMask = "n0"
        Me.JumlahStamp.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.JumlahStamp.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.JumlahStamp.Properties.NullText = "0"
        Me.JumlahStamp.Properties.ReadOnly = True
        Me.JumlahStamp.Size = New System.Drawing.Size(145, 21)
        Me.JumlahStamp.TabIndex = 1
        Me.JumlahStamp.TabStop = False
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl12.Location = New System.Drawing.Point(18, 23)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(85, 14)
        Me.LabelControl12.TabIndex = 0
        Me.LabelControl12.Text = "Stamp dimiliki"
        '
        'ButBatal
        '
        Me.ButBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButBatal.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButBatal.Appearance.Options.UseFont = True
        Me.ButBatal.Image = Global.Laritta_Membership.My.Resources.Resources.back_32
        Me.ButBatal.Location = New System.Drawing.Point(285, 212)
        Me.ButBatal.Name = "ButBatal"
        Me.ButBatal.Size = New System.Drawing.Size(90, 40)
        Me.ButBatal.TabIndex = 2
        Me.ButBatal.Text = "&Batal"
        '
        'ButOK
        '
        Me.ButOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButOK.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButOK.Appearance.Options.UseFont = True
        Me.ButOK.Image = Global.Laritta_Membership.My.Resources.Resources.apply_32
        Me.ButOK.Location = New System.Drawing.Point(189, 212)
        Me.ButOK.Name = "ButOK"
        Me.ButOK.Size = New System.Drawing.Size(90, 40)
        Me.ButOK.TabIndex = 1
        Me.ButOK.Text = "&OK"
        '
        'PenukaranPointStamp
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(387, 264)
        Me.Controls.Add(Me.ButBatal)
        Me.Controls.Add(Me.ButOK)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.Name = "PenukaranPointStamp"
        Me.Text = "PenukaranPointStamp"
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.PagePoint.ResumeLayout(False)
        Me.PagePoint.PerformLayout()
        CType(Me.JmlPenggunaanPoint.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DaftarRewardPoint.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewRewardPoint, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SisaPoint.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.KebutuhanPoint.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.JumlahPoint.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PageStamp.ResumeLayout(False)
        Me.PageStamp.PerformLayout()
        CType(Me.SisaStamp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.JmlPenggunaanStamp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.KebutuhanStamp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DaftarRewardStamp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewRewardStamp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.JumlahStamp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents PagePoint As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents PageStamp As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents ButBatal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents JumlahPoint As DevExpress.XtraEditors.TextEdit
    Friend WithEvents JumlahStamp As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DaftarRewardPoint As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents ViewRewardPoint As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DaftarRewardStamp As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents ViewRewardStamp As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SisaPoint As DevExpress.XtraEditors.TextEdit
    Friend WithEvents KebutuhanPoint As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SisaStamp As DevExpress.XtraEditors.TextEdit
    Friend WithEvents KebutuhanStamp As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents JmlPenggunaanPoint As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents JmlPenggunaanStamp As DevExpress.XtraEditors.TextEdit
End Class
