﻿Public Class PenggunaanPointStamp
    Dim CodeExists As Boolean

    Private Sub PenggunaanPointStamp_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("GetDataExcPS")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub PenggunaanPointStamp_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading(Me)
    End Sub

    Private Sub PenggunaanPointStamp_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        LoadSprite.CloseLoading()
    End Sub

    Private Sub KodePenukaran_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles KodePenukaran.EditValueChanged
        Try
            If KodePenukaran.EditValue.ToString.Length = 13 Then
                If Not xSet.Tables("GetDataExcPS") Is Nothing Then xSet.Tables("GetDataExcPS").Clear()

                If Mid(KodePenukaran.Text, 1, 3) = "PT/" Then
                    SQLquery = String.Format("SELECT id_exchangepoint ID, redeemed used, b.namaCabang cabang, a.nama_rewardpoint reward_name, jml_rewardpoint reward_qtt, a.spec_rewardpoint reward_spec, point_taken reward_costs, a.createdate " & _
                                             "FROM mbst_exchangepoint a INNER JOIN mcabang b ON a.id_cabang=b.idCabang WHERE a.isVoid=0 AND a.kode_exchangepoint='{0}';", KodePenukaran.Text)
                ElseIf Mid(KodePenukaran.Text, 1, 3) = "ST/" Then
                    SQLquery = String.Format("SELECT id_exchangestamp ID, redeemed used, b.namaCabang cabang, a.nama_rewardstamp reward_name, jml_rewardstamp reward_qtt, a.spec_rewardstamp reward_spec, stamp_taken reward_costs, a.createdate " & _
                                             "FROM mbst_exchangestamp a INNER JOIN mcabang b ON a.id_cabang=b.idCabang WHERE a.isVoid=0 AND a.kode_exchangestamp='{0}';", KodePenukaran.Text)
                End If
                ExDb.ExecQuery(1, SQLquery, xSet, "GetDataExcPS")

                If xSet.Tables("GetDataExcPS").Rows.Count < 1 Then
                    MsgWarning("Kode tidak ditemukan")
                ElseIf xSet.Tables("GetDataExcPS").Rows(0).Item("used") = 1 Then
                    MsgWarning("Kode sudah digunakan!")
                Else
                    RewardType.Text = IIf(Mid(KodePenukaran.Text, 1, 3) = "PT/", "Point", "Stamp")
                    RewardName.Text = xSet.Tables("GetDataExcPS").Rows(0).Item("reward_name")
                    RewardSpec.Text = xSet.Tables("GetDataExcPS").Rows(0).Item("reward_spec")
                    RewardQtt.Text = xSet.Tables("GetDataExcPS").Rows(0).Item("reward_qtt")
                    ExchangeDate.Text = xSet.Tables("GetDataExcPS").Rows(0).Item("createdate")
                    ExchangeBranch.Text = xSet.Tables("GetDataExcPS").Rows(0).Item("cabang")
                    RewardCosts.EditValue = xSet.Tables("GetDataExcPS").Rows(0).Item("reward_costs")

                    CodeExists = True
                End If
            Else
                Throw New Exception
            End If
        Catch ex As Exception
            If RewardName.Text <> "" Then ClearAll()
        End Try
    End Sub

    Private Sub ButClear_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButClear.Click
        KodePenukaran.Text = ""
        ClearAll()
    End Sub

    Private Sub ButOK_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButOK.Click
        If Not beforeSave() Then Return
        Try
            LoadSprite.ShowLoading()

            SQLquery = String.Format("CALL InsertUsePointStamp({0}, {1}, {2}, '{3}', {4}); ",
                                     id_cabang,
                                     IIf(RewardType.Text = "Point", 0, 1),
                                     xSet.Tables("GetDataExcPS").Rows(0).Item("ID"),
                                     KodePenukaran.EditValue,
                                     staff_id)
            ExDb.ExecData(1, SQLquery)

            MsgInfo("Data berhasil disimpan, tekan OK untuk melanjutkan.")
        Catch ex As Exception
            MsgErrorDev()
        End Try
        LoadSprite.CloseLoading()
        Close()
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Sub ClearAll()
        RewardType.Text = ""
        RewardName.Text = ""
        RewardSpec.Text = ""
        ExchangeDate.Text = ""
        ExchangeBranch.Text = ""
        RewardCosts.EditValue = 0

        CodeExists = False
    End Sub

    Private Function beforeSave() As Boolean
        If CodeExists = False Then
            Return False
        End If

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        Return True
    End Function
End Class