﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PenggunaanPointStamp
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.KodePenukaran = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.ButClear = New DevExpress.XtraEditors.SimpleButton()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.RewardSpec = New DevExpress.XtraEditors.MemoEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.RewardCosts = New DevExpress.XtraEditors.TextEdit()
        Me.ExchangeBranch = New DevExpress.XtraEditors.TextEdit()
        Me.RewardQtt = New DevExpress.XtraEditors.TextEdit()
        Me.ExchangeDate = New DevExpress.XtraEditors.TextEdit()
        Me.RewardName = New DevExpress.XtraEditors.TextEdit()
        Me.RewardType = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.ButBatal = New DevExpress.XtraEditors.SimpleButton()
        Me.ButOK = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.KodePenukaran.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.RewardSpec.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RewardCosts.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ExchangeBranch.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RewardQtt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ExchangeDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RewardName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RewardType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelControl1.Controls.Add(Me.KodePenukaran)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Controls.Add(Me.ButClear)
        Me.PanelControl1.Location = New System.Drawing.Point(12, 12)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(417, 39)
        Me.PanelControl1.TabIndex = 0
        '
        'KodePenukaran
        '
        Me.KodePenukaran.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.KodePenukaran.EditValue = ""
        Me.KodePenukaran.Location = New System.Drawing.Point(86, 5)
        Me.KodePenukaran.Name = "KodePenukaran"
        Me.KodePenukaran.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.KodePenukaran.Properties.Appearance.Options.UseFont = True
        Me.KodePenukaran.Properties.Mask.EditMask = "[PT|ST]{2}/[0-9]{2}/[0-9]{2}/[0-9]{4}"
        Me.KodePenukaran.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.KodePenukaran.Properties.ValidateOnEnterKey = True
        Me.KodePenukaran.Size = New System.Drawing.Size(238, 29)
        Me.KodePenukaran.TabIndex = 1
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Location = New System.Drawing.Point(19, 8)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(61, 23)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Kode :"
        '
        'ButClear
        '
        Me.ButClear.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButClear.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButClear.Appearance.Options.UseFont = True
        Me.ButClear.Location = New System.Drawing.Point(330, 5)
        Me.ButClear.Name = "ButClear"
        Me.ButClear.Size = New System.Drawing.Size(82, 29)
        Me.ButClear.TabIndex = 2
        Me.ButClear.Text = "&Clear"
        '
        'GridView1
        '
        Me.GridView1.Name = "GridView1"
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.Controls.Add(Me.RewardSpec)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Controls.Add(Me.LabelControl7)
        Me.GroupControl1.Controls.Add(Me.LabelControl6)
        Me.GroupControl1.Controls.Add(Me.RewardCosts)
        Me.GroupControl1.Controls.Add(Me.ExchangeBranch)
        Me.GroupControl1.Controls.Add(Me.RewardQtt)
        Me.GroupControl1.Controls.Add(Me.ExchangeDate)
        Me.GroupControl1.Controls.Add(Me.RewardName)
        Me.GroupControl1.Controls.Add(Me.RewardType)
        Me.GroupControl1.Controls.Add(Me.LabelControl4)
        Me.GroupControl1.Controls.Add(Me.LabelControl8)
        Me.GroupControl1.Controls.Add(Me.LabelControl5)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 57)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(417, 272)
        Me.GroupControl1.TabIndex = 1
        Me.GroupControl1.Text = "Benefit"
        '
        'RewardSpec
        '
        Me.RewardSpec.Location = New System.Drawing.Point(164, 93)
        Me.RewardSpec.Name = "RewardSpec"
        Me.RewardSpec.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.RewardSpec.Properties.Appearance.Options.UseFont = True
        Me.RewardSpec.Properties.ReadOnly = True
        Me.RewardSpec.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.RewardSpec.Size = New System.Drawing.Size(235, 58)
        Me.RewardSpec.TabIndex = 5
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl2.Location = New System.Drawing.Point(24, 241)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(34, 14)
        Me.LabelControl2.TabIndex = 12
        Me.LabelControl2.Text = "Biaya"
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl3.Location = New System.Drawing.Point(24, 214)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(121, 14)
        Me.LabelControl3.TabIndex = 10
        Me.LabelControl3.Text = "Cabang penukaran"
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl7.Location = New System.Drawing.Point(24, 160)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(43, 14)
        Me.LabelControl7.TabIndex = 6
        Me.LabelControl7.Text = "Jumlah"
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl6.Location = New System.Drawing.Point(24, 187)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(122, 14)
        Me.LabelControl6.TabIndex = 8
        Me.LabelControl6.Text = "Tanggal penukaran"
        '
        'RewardCosts
        '
        Me.RewardCosts.EditValue = "0"
        Me.RewardCosts.Location = New System.Drawing.Point(164, 238)
        Me.RewardCosts.Name = "RewardCosts"
        Me.RewardCosts.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.RewardCosts.Properties.Appearance.Options.UseFont = True
        Me.RewardCosts.Properties.Mask.EditMask = "n2"
        Me.RewardCosts.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.RewardCosts.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.RewardCosts.Properties.ReadOnly = True
        Me.RewardCosts.Size = New System.Drawing.Size(135, 21)
        Me.RewardCosts.TabIndex = 13
        Me.RewardCosts.TabStop = False
        '
        'ExchangeBranch
        '
        Me.ExchangeBranch.Location = New System.Drawing.Point(164, 211)
        Me.ExchangeBranch.Name = "ExchangeBranch"
        Me.ExchangeBranch.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ExchangeBranch.Properties.Appearance.Options.UseFont = True
        Me.ExchangeBranch.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.ExchangeBranch.Properties.ReadOnly = True
        Me.ExchangeBranch.Size = New System.Drawing.Size(185, 21)
        Me.ExchangeBranch.TabIndex = 11
        Me.ExchangeBranch.TabStop = False
        '
        'RewardQtt
        '
        Me.RewardQtt.EditValue = "0"
        Me.RewardQtt.Location = New System.Drawing.Point(164, 157)
        Me.RewardQtt.Name = "RewardQtt"
        Me.RewardQtt.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.RewardQtt.Properties.Appearance.Options.UseFont = True
        Me.RewardQtt.Properties.Mask.EditMask = "n0"
        Me.RewardQtt.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.RewardQtt.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.RewardQtt.Properties.ReadOnly = True
        Me.RewardQtt.Size = New System.Drawing.Size(57, 21)
        Me.RewardQtt.TabIndex = 7
        Me.RewardQtt.TabStop = False
        '
        'ExchangeDate
        '
        Me.ExchangeDate.Location = New System.Drawing.Point(164, 184)
        Me.ExchangeDate.Name = "ExchangeDate"
        Me.ExchangeDate.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ExchangeDate.Properties.Appearance.Options.UseFont = True
        Me.ExchangeDate.Properties.Mask.EditMask = "dd MMMM yyyy, HH:mm"
        Me.ExchangeDate.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.ExchangeDate.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.ExchangeDate.Properties.ReadOnly = True
        Me.ExchangeDate.Size = New System.Drawing.Size(185, 21)
        Me.ExchangeDate.TabIndex = 9
        Me.ExchangeDate.TabStop = False
        '
        'RewardName
        '
        Me.RewardName.Location = New System.Drawing.Point(164, 66)
        Me.RewardName.Name = "RewardName"
        Me.RewardName.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.RewardName.Properties.Appearance.Options.UseFont = True
        Me.RewardName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.RewardName.Properties.ReadOnly = True
        Me.RewardName.Size = New System.Drawing.Size(235, 21)
        Me.RewardName.TabIndex = 3
        Me.RewardName.TabStop = False
        '
        'RewardType
        '
        Me.RewardType.Location = New System.Drawing.Point(164, 39)
        Me.RewardType.Name = "RewardType"
        Me.RewardType.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.RewardType.Properties.Appearance.Options.UseFont = True
        Me.RewardType.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.RewardType.Properties.ReadOnly = True
        Me.RewardType.Size = New System.Drawing.Size(135, 21)
        Me.RewardType.TabIndex = 1
        Me.RewardType.TabStop = False
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl4.Location = New System.Drawing.Point(24, 95)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(115, 14)
        Me.LabelControl4.TabIndex = 4
        Me.LabelControl4.Text = "Spesifikasi reward"
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl8.Location = New System.Drawing.Point(24, 69)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(48, 14)
        Me.LabelControl8.TabIndex = 2
        Me.LabelControl8.Text = "Reward"
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl5.Location = New System.Drawing.Point(24, 42)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(80, 14)
        Me.LabelControl5.TabIndex = 0
        Me.LabelControl5.Text = "Jenis reward"
        '
        'ButBatal
        '
        Me.ButBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButBatal.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButBatal.Appearance.Options.UseFont = True
        Me.ButBatal.Image = Global.Laritta_Membership.My.Resources.Resources.back_32
        Me.ButBatal.Location = New System.Drawing.Point(339, 335)
        Me.ButBatal.Name = "ButBatal"
        Me.ButBatal.Size = New System.Drawing.Size(90, 40)
        Me.ButBatal.TabIndex = 3
        Me.ButBatal.Text = "&Batal"
        '
        'ButOK
        '
        Me.ButOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButOK.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButOK.Appearance.Options.UseFont = True
        Me.ButOK.Image = Global.Laritta_Membership.My.Resources.Resources.apply_32
        Me.ButOK.Location = New System.Drawing.Point(243, 335)
        Me.ButOK.Name = "ButOK"
        Me.ButOK.Size = New System.Drawing.Size(90, 40)
        Me.ButOK.TabIndex = 2
        Me.ButOK.Text = "&OK"
        '
        'PenggunaanPointStamp
        '
        Me.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(441, 387)
        Me.Controls.Add(Me.ButBatal)
        Me.Controls.Add(Me.ButOK)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.PanelControl1)
        Me.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Name = "PenggunaanPointStamp"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.KodePenukaran.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.RewardSpec.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RewardCosts.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ExchangeBranch.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RewardQtt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ExchangeDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RewardName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RewardType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ButBatal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents KodePenukaran As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ButClear As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ExchangeDate As DevExpress.XtraEditors.TextEdit
    Friend WithEvents RewardName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents RewardType As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents RewardCosts As DevExpress.XtraEditors.TextEdit
    Friend WithEvents RewardSpec As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ExchangeBranch As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents RewardQtt As DevExpress.XtraEditors.TextEdit
End Class
