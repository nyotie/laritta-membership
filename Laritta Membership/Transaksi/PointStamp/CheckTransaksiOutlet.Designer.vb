﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CheckTransaksiOutlet
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.NoNota = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.ButClear = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.TransGrandTotal = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.TransDiskon = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.DaftarTransaksiOutlet = New DevExpress.XtraGrid.GridControl()
        Me.ViewTransaksiOutlet = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.ButBatal = New DevExpress.XtraEditors.SimpleButton()
        Me.ButOK = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.NoNota.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.TransGrandTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TransDiskon.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DaftarTransaksiOutlet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewTransaksiOutlet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelControl1.Controls.Add(Me.NoNota)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Controls.Add(Me.ButClear)
        Me.PanelControl1.Location = New System.Drawing.Point(12, 12)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(560, 39)
        Me.PanelControl1.TabIndex = 1
        '
        'NoNota
        '
        Me.NoNota.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.NoNota.EditValue = ""
        Me.NoNota.Location = New System.Drawing.Point(123, 5)
        Me.NoNota.Name = "NoNota"
        Me.NoNota.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NoNota.Properties.Appearance.Options.UseFont = True
        Me.NoNota.Properties.Mask.EditMask = "SO/[0-9]{2}/[0-9]{2}/[0-9]{6}"
        Me.NoNota.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.NoNota.Size = New System.Drawing.Size(344, 29)
        Me.NoNota.TabIndex = 1
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Location = New System.Drawing.Point(19, 8)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(98, 23)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "No. Nota :"
        '
        'ButClear
        '
        Me.ButClear.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButClear.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButClear.Appearance.Options.UseFont = True
        Me.ButClear.Location = New System.Drawing.Point(473, 5)
        Me.ButClear.Name = "ButClear"
        Me.ButClear.Size = New System.Drawing.Size(82, 29)
        Me.ButClear.TabIndex = 2
        Me.ButClear.Text = "&Clear"
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.Controls.Add(Me.TransGrandTotal)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Controls.Add(Me.LabelControl6)
        Me.GroupControl1.Controls.Add(Me.TransDiskon)
        Me.GroupControl1.Controls.Add(Me.LabelControl4)
        Me.GroupControl1.Controls.Add(Me.LabelControl7)
        Me.GroupControl1.Controls.Add(Me.DaftarTransaksiOutlet)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 57)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(560, 365)
        Me.GroupControl1.TabIndex = 3
        Me.GroupControl1.Text = "Isi transaksi"
        '
        'TransGrandTotal
        '
        Me.TransGrandTotal.EditValue = 0
        Me.TransGrandTotal.Location = New System.Drawing.Point(441, 339)
        Me.TransGrandTotal.Name = "TransGrandTotal"
        Me.TransGrandTotal.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TransGrandTotal.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.TransGrandTotal.Properties.Appearance.Options.UseFont = True
        Me.TransGrandTotal.Properties.Appearance.Options.UseTextOptions = True
        Me.TransGrandTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.TransGrandTotal.Properties.Mask.EditMask = "n0"
        Me.TransGrandTotal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TransGrandTotal.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TransGrandTotal.Properties.NullText = "0"
        Me.TransGrandTotal.Size = New System.Drawing.Size(114, 21)
        Me.TransGrandTotal.TabIndex = 99
        Me.TransGrandTotal.TabStop = False
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl3.Location = New System.Drawing.Point(337, 315)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(45, 14)
        Me.LabelControl3.TabIndex = 11
        Me.LabelControl3.Text = "Diskon"
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl6.Location = New System.Drawing.Point(419, 315)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(16, 14)
        Me.LabelControl6.TabIndex = 10
        Me.LabelControl6.Text = "Rp"
        '
        'TransDiskon
        '
        Me.TransDiskon.EditValue = 0
        Me.TransDiskon.Location = New System.Drawing.Point(441, 312)
        Me.TransDiskon.Name = "TransDiskon"
        Me.TransDiskon.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TransDiskon.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.TransDiskon.Properties.Appearance.Options.UseFont = True
        Me.TransDiskon.Properties.Appearance.Options.UseTextOptions = True
        Me.TransDiskon.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.TransDiskon.Properties.Mask.EditMask = "n0"
        Me.TransDiskon.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TransDiskon.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TransDiskon.Properties.NullText = "0"
        Me.TransDiskon.Size = New System.Drawing.Size(114, 21)
        Me.TransDiskon.TabIndex = 99
        Me.TransDiskon.TabStop = False
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl4.Location = New System.Drawing.Point(337, 342)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(74, 14)
        Me.LabelControl4.TabIndex = 9
        Me.LabelControl4.Text = "Grand total"
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl7.Location = New System.Drawing.Point(419, 342)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(16, 14)
        Me.LabelControl7.TabIndex = 8
        Me.LabelControl7.Text = "Rp"
        '
        'DaftarTransaksiOutlet
        '
        Me.DaftarTransaksiOutlet.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DaftarTransaksiOutlet.Location = New System.Drawing.Point(5, 26)
        Me.DaftarTransaksiOutlet.MainView = Me.ViewTransaksiOutlet
        Me.DaftarTransaksiOutlet.Name = "DaftarTransaksiOutlet"
        Me.DaftarTransaksiOutlet.Size = New System.Drawing.Size(550, 280)
        Me.DaftarTransaksiOutlet.TabIndex = 99
        Me.DaftarTransaksiOutlet.TabStop = False
        Me.DaftarTransaksiOutlet.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.ViewTransaksiOutlet})
        '
        'ViewTransaksiOutlet
        '
        Me.ViewTransaksiOutlet.Appearance.HeaderPanel.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ViewTransaksiOutlet.Appearance.HeaderPanel.Options.UseFont = True
        Me.ViewTransaksiOutlet.Appearance.Row.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ViewTransaksiOutlet.Appearance.Row.Options.UseFont = True
        Me.ViewTransaksiOutlet.ColumnPanelRowHeight = 40
        Me.ViewTransaksiOutlet.GridControl = Me.DaftarTransaksiOutlet
        Me.ViewTransaksiOutlet.Name = "ViewTransaksiOutlet"
        Me.ViewTransaksiOutlet.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewTransaksiOutlet.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewTransaksiOutlet.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewTransaksiOutlet.OptionsBehavior.Editable = False
        Me.ViewTransaksiOutlet.OptionsCustomization.AllowColumnMoving = False
        Me.ViewTransaksiOutlet.OptionsCustomization.AllowColumnResizing = False
        Me.ViewTransaksiOutlet.OptionsCustomization.AllowFilter = False
        Me.ViewTransaksiOutlet.OptionsCustomization.AllowGroup = False
        Me.ViewTransaksiOutlet.OptionsCustomization.AllowQuickHideColumns = False
        Me.ViewTransaksiOutlet.OptionsCustomization.AllowSort = False
        Me.ViewTransaksiOutlet.OptionsView.ShowFooter = True
        Me.ViewTransaksiOutlet.OptionsView.ShowGroupPanel = False
        Me.ViewTransaksiOutlet.RowHeight = 30
        '
        'ButBatal
        '
        Me.ButBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButBatal.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButBatal.Appearance.Options.UseFont = True
        Me.ButBatal.Image = Global.Laritta_Membership.My.Resources.Resources.back_32
        Me.ButBatal.Location = New System.Drawing.Point(482, 433)
        Me.ButBatal.Name = "ButBatal"
        Me.ButBatal.Size = New System.Drawing.Size(90, 40)
        Me.ButBatal.TabIndex = 4
        Me.ButBatal.Text = "&Batal"
        '
        'ButOK
        '
        Me.ButOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButOK.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButOK.Appearance.Options.UseFont = True
        Me.ButOK.Image = Global.Laritta_Membership.My.Resources.Resources.apply_32
        Me.ButOK.Location = New System.Drawing.Point(386, 433)
        Me.ButOK.Name = "ButOK"
        Me.ButOK.Size = New System.Drawing.Size(90, 40)
        Me.ButOK.TabIndex = 3
        Me.ButOK.Text = "&OK"
        '
        'CheckTransaksiOutlet
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(584, 485)
        Me.Controls.Add(Me.ButBatal)
        Me.Controls.Add(Me.ButOK)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.PanelControl1)
        Me.Name = "CheckTransaksiOutlet"
        Me.Text = "CheckTransaksiOutlet"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.NoNota.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.TransGrandTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TransDiskon.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DaftarTransaksiOutlet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewTransaksiOutlet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents NoNota As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ButClear As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents ButBatal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents DaftarTransaksiOutlet As DevExpress.XtraGrid.GridControl
    Friend WithEvents ViewTransaksiOutlet As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents TransGrandTotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TransDiskon As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
End Class
