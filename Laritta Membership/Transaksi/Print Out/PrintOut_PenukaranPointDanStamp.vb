﻿Public Class PrintOut_PenukaranPointDanStamp

    Private Sub PrintOut_PenukaranPointDanStamp_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles Me.BeforePrint
        DataCabang.Text = String.Format("{1}{0}Telp : {2}", vbCrLf,
                                        xSet.Tables("DataBranch").Select("ID=" & id_cabang)(0).Item("Alamat"),
                                        xSet.Tables("DataBranch").Select("ID=" & id_cabang)(0).Item("Telp"))
        TglDanJamPrint.Text = Format(Now, "dd-MM-yyyy HH:mm")

        Dim jenis As String = IIf(Mid(xSet.Tables("HasilInsertPointStamp").Rows(0).Item("kode"), 1, 2) = "PT", "Point", "Stamp")

        JenisBenefit.Text = jenis
        KodeBenefit.Text = xSet.Tables("HasilInsertPointStamp").Rows(0).Item("kode")
        RewardName.Text = xSet.Tables("HasilInsertPointStamp").Rows(0).Item("nama")
        RewardQtt.Text = "x" & xSet.Tables("HasilInsertPointStamp").Rows(0).Item("Jml")
        RewardCosts.Text = String.Format("{0} {1}", xSet.Tables("HasilInsertPointStamp").Rows(0).Item("biaya"), IIf(jenis = "Point", "pt", "st"))

    End Sub
End Class