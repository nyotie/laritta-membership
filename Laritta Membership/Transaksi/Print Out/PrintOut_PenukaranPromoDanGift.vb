﻿Public Class PrintOut_PenukaranPromoDanGift

    Private Sub PrintOut_PenukaranPromoDanGift_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles Me.BeforePrint
        DataCabang.Text = String.Format("{1}{0}Telp : {2}", vbCrLf,
                                        xSet.Tables("DataBranch").Select("ID=" & id_cabang)(0).Item("Alamat"),
                                        xSet.Tables("DataBranch").Select("ID=" & id_cabang)(0).Item("Telp"))
        TglDanJamPrint.Text = Format(Now, "dd-MM-yyyy HH:mm")

        JenisBenefit.Text = IIf(Mid(xSet.Tables("HasilInsertGiftPromo").Rows(0).Item("kode"), 1, 2) = "PR", "Promo", "Gift")
        KodeBenefit.Text = xSet.Tables("HasilInsertGiftPromo").Rows(0).Item("kode")
        ExpiredDates.Text = Format(xSet.Tables("HasilInsertGiftPromo").Rows(0).Item("expired_date"), "dd MMM yyyy")
        BenefitDanRepetisi.Text = String.Format("{0} x{1}", xSet.Tables("HasilInsertGiftPromo").Rows(0).Item("nama"), xSet.Tables("HasilInsertGiftPromo").Rows(0).Item("repetisi"))

        If xSet.Tables("HasilInsertGiftPromo").Rows(0).Item("use_at_outlet") = True Then
            If xSet.Tables("HasilInsertGiftPromo").Rows(0).Item("use_at_order") = True Then
                BerlakuDi.Text = "Retail dan Pesanan"
            Else
                BerlakuDi.Text = "Retail"
            End If
        Else
            BerlakuDi.Text = "Pesanan"
        End If
    End Sub
End Class