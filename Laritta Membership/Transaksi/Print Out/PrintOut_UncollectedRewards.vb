﻿Public Class PrintOut_UncollectedRewards

    Private Sub PrintOut_UncollectedRewards_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles Me.BeforePrint
        NamaCabang.Text = "Laritta - " & xSet.Tables("DataBranch").Select("ID=" & id_cabang)(0).Item("Nama").ToString

        Set_Laporan1.Data_UncollectedRewards.Clear()
        Set_Laporan1.Data_UncollectedRewards.Merge(xSet.Tables("UncollectedRewards"))
        Set_Laporan1.Data_UncollectedRewards.DefaultView.Sort = "Redeem ASC"
    End Sub
End Class