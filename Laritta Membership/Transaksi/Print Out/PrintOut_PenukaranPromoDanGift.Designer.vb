﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class PrintOut_PenukaranPromoDanGift
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PrintOut_PenukaranPromoDanGift))
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.BerlakuDi = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
        Me.JenisBenefit = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.ExpiredDates = New DevExpress.XtraReports.UI.XRLabel()
        Me.KodeBenefit = New DevExpress.XtraReports.UI.XRLabel()
        Me.BenefitDanRepetisi = New DevExpress.XtraReports.UI.XRLabel()
        Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
        Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
        Me.TglDanJamPrint = New DevExpress.XtraReports.UI.XRLabel()
        Me.DataCabang = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrPictureBox1 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
        Me.XrLine2 = New DevExpress.XtraReports.UI.XRLine()
        Me.LabelFooter = New DevExpress.XtraReports.UI.XRLabel()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.BerlakuDi, Me.XrLabel6, Me.XrLabel1, Me.XrLabel8, Me.XrLabel7, Me.JenisBenefit, Me.XrLabel5, Me.XrLabel4, Me.XrLabel3, Me.XrLabel2, Me.ExpiredDates, Me.KodeBenefit, Me.BenefitDanRepetisi})
        Me.Detail.Dpi = 254.0!
        Me.Detail.HeightF = 422.0!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'BerlakuDi
        '
        Me.BerlakuDi.Dpi = 254.0!
        Me.BerlakuDi.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.BerlakuDi.LocationFloat = New DevExpress.Utils.PointFloat(251.3542!, 175.26!)
        Me.BerlakuDi.Name = "BerlakuDi"
        Me.BerlakuDi.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.BerlakuDi.SizeF = New System.Drawing.SizeF(376.6458!, 58.41995!)
        Me.BerlakuDi.StylePriority.UseFont = False
        Me.BerlakuDi.StylePriority.UseTextAlignment = False
        Me.BerlakuDi.Text = "Berlaku Di"
        Me.BerlakuDi.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel6
        '
        Me.XrLabel6.Dpi = 254.0!
        Me.XrLabel6.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(224.8958!, 175.26!)
        Me.XrLabel6.Name = "XrLabel6"
        Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel6.SizeF = New System.Drawing.SizeF(26.45836!, 58.42001!)
        Me.XrLabel6.StylePriority.UseFont = False
        Me.XrLabel6.StylePriority.UseTextAlignment = False
        Me.XrLabel6.Text = ":"
        Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel1
        '
        Me.XrLabel1.Dpi = 254.0!
        Me.XrLabel1.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(25.00001!, 175.26!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(199.8958!, 58.42!)
        Me.XrLabel1.StylePriority.UseFont = False
        Me.XrLabel1.StylePriority.UseTextAlignment = False
        Me.XrLabel1.Text = "Valid at"
        Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel8
        '
        Me.XrLabel8.Dpi = 254.0!
        Me.XrLabel8.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(224.8958!, 0.0!)
        Me.XrLabel8.Name = "XrLabel8"
        Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel8.SizeF = New System.Drawing.SizeF(26.45836!, 58.41999!)
        Me.XrLabel8.StylePriority.UseFont = False
        Me.XrLabel8.StylePriority.UseTextAlignment = False
        Me.XrLabel8.Text = ":"
        Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel7
        '
        Me.XrLabel7.Dpi = 254.0!
        Me.XrLabel7.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(25.00001!, 0.0!)
        Me.XrLabel7.Name = "XrLabel7"
        Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel7.SizeF = New System.Drawing.SizeF(199.8958!, 58.42!)
        Me.XrLabel7.StylePriority.UseFont = False
        Me.XrLabel7.StylePriority.UseTextAlignment = False
        Me.XrLabel7.Text = "Type"
        Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'JenisBenefit
        '
        Me.JenisBenefit.Dpi = 254.0!
        Me.JenisBenefit.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.JenisBenefit.LocationFloat = New DevExpress.Utils.PointFloat(251.3542!, 0.0!)
        Me.JenisBenefit.Name = "JenisBenefit"
        Me.JenisBenefit.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.JenisBenefit.SizeF = New System.Drawing.SizeF(376.6458!, 58.42001!)
        Me.JenisBenefit.StylePriority.UseFont = False
        Me.JenisBenefit.StylePriority.UseTextAlignment = False
        Me.JenisBenefit.Text = "Jenis Benefit"
        Me.JenisBenefit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel5
        '
        Me.XrLabel5.Dpi = 254.0!
        Me.XrLabel5.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(224.8958!, 116.84!)
        Me.XrLabel5.Name = "XrLabel5"
        Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel5.SizeF = New System.Drawing.SizeF(26.45836!, 58.42001!)
        Me.XrLabel5.StylePriority.UseFont = False
        Me.XrLabel5.StylePriority.UseTextAlignment = False
        Me.XrLabel5.Text = ":"
        Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel4
        '
        Me.XrLabel4.Dpi = 254.0!
        Me.XrLabel4.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(224.8958!, 58.41999!)
        Me.XrLabel4.Name = "XrLabel4"
        Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel4.SizeF = New System.Drawing.SizeF(26.45836!, 58.41999!)
        Me.XrLabel4.StylePriority.UseFont = False
        Me.XrLabel4.StylePriority.UseTextAlignment = False
        Me.XrLabel4.Text = ":"
        Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel3
        '
        Me.XrLabel3.Dpi = 254.0!
        Me.XrLabel3.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(25.00001!, 116.84!)
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel3.SizeF = New System.Drawing.SizeF(199.8958!, 58.42!)
        Me.XrLabel3.StylePriority.UseFont = False
        Me.XrLabel3.StylePriority.UseTextAlignment = False
        Me.XrLabel3.Text = "Valid until"
        Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel2
        '
        Me.XrLabel2.Dpi = 254.0!
        Me.XrLabel2.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(25.00001!, 58.41996!)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(199.8958!, 58.42!)
        Me.XrLabel2.StylePriority.UseFont = False
        Me.XrLabel2.StylePriority.UseTextAlignment = False
        Me.XrLabel2.Text = "Code"
        Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'ExpiredDates
        '
        Me.ExpiredDates.Dpi = 254.0!
        Me.ExpiredDates.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.ExpiredDates.LocationFloat = New DevExpress.Utils.PointFloat(251.3542!, 116.84!)
        Me.ExpiredDates.Name = "ExpiredDates"
        Me.ExpiredDates.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.ExpiredDates.SizeF = New System.Drawing.SizeF(376.6458!, 58.41997!)
        Me.ExpiredDates.StylePriority.UseFont = False
        Me.ExpiredDates.StylePriority.UseTextAlignment = False
        Me.ExpiredDates.Text = "Expired Date"
        Me.ExpiredDates.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'KodeBenefit
        '
        Me.KodeBenefit.Dpi = 254.0!
        Me.KodeBenefit.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.KodeBenefit.LocationFloat = New DevExpress.Utils.PointFloat(251.3542!, 58.41996!)
        Me.KodeBenefit.Name = "KodeBenefit"
        Me.KodeBenefit.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.KodeBenefit.SizeF = New System.Drawing.SizeF(376.6458!, 58.42!)
        Me.KodeBenefit.StylePriority.UseFont = False
        Me.KodeBenefit.StylePriority.UseTextAlignment = False
        Me.KodeBenefit.Text = "Kode Benefit"
        Me.KodeBenefit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'BenefitDanRepetisi
        '
        Me.BenefitDanRepetisi.Dpi = 254.0!
        Me.BenefitDanRepetisi.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BenefitDanRepetisi.LocationFloat = New DevExpress.Utils.PointFloat(25.00001!, 233.68!)
        Me.BenefitDanRepetisi.Name = "BenefitDanRepetisi"
        Me.BenefitDanRepetisi.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 11, 11, 254.0!)
        Me.BenefitDanRepetisi.SizeF = New System.Drawing.SizeF(602.9999!, 188.0659!)
        Me.BenefitDanRepetisi.StylePriority.UseFont = False
        Me.BenefitDanRepetisi.StylePriority.UsePadding = False
        Me.BenefitDanRepetisi.StylePriority.UseTextAlignment = False
        Me.BenefitDanRepetisi.Text = "Benefit & Repetisi"
        Me.BenefitDanRepetisi.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'TopMargin
        '
        Me.TopMargin.Dpi = 254.0!
        Me.TopMargin.HeightF = 64.0!
        Me.TopMargin.Name = "TopMargin"
        Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254.0!)
        Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'BottomMargin
        '
        Me.BottomMargin.Dpi = 254.0!
        Me.BottomMargin.HeightF = 64.0!
        Me.BottomMargin.Name = "BottomMargin"
        Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254.0!)
        Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'ReportHeader
        '
        Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine1, Me.TglDanJamPrint, Me.DataCabang, Me.XrPictureBox1})
        Me.ReportHeader.Dpi = 254.0!
        Me.ReportHeader.HeightF = 380.0!
        Me.ReportHeader.Name = "ReportHeader"
        '
        'XrLine1
        '
        Me.XrLine1.Dpi = 254.0!
        Me.XrLine1.LineWidth = 5
        Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(1.999939!, 365.125!)
        Me.XrLine1.Name = "XrLine1"
        Me.XrLine1.SizeF = New System.Drawing.SizeF(648.0001!, 13.22919!)
        '
        'TglDanJamPrint
        '
        Me.TglDanJamPrint.Dpi = 254.0!
        Me.TglDanJamPrint.Font = New System.Drawing.Font("Verdana", 10.0!)
        Me.TglDanJamPrint.LocationFloat = New DevExpress.Utils.PointFloat(139.3541!, 306.705!)
        Me.TglDanJamPrint.Name = "TglDanJamPrint"
        Me.TglDanJamPrint.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.TglDanJamPrint.SizeF = New System.Drawing.SizeF(510.6459!, 58.42!)
        Me.TglDanJamPrint.StylePriority.UseFont = False
        Me.TglDanJamPrint.StylePriority.UseTextAlignment = False
        Me.TglDanJamPrint.Text = "Tanggal; waktu"
        Me.TglDanJamPrint.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'DataCabang
        '
        Me.DataCabang.Dpi = 254.0!
        Me.DataCabang.Font = New System.Drawing.Font("Verdana", 10.0!)
        Me.DataCabang.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 164.2534!)
        Me.DataCabang.Multiline = True
        Me.DataCabang.Name = "DataCabang"
        Me.DataCabang.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.DataCabang.SizeF = New System.Drawing.SizeF(648.0001!, 124.5658!)
        Me.DataCabang.StylePriority.UseFont = False
        Me.DataCabang.StylePriority.UseTextAlignment = False
        Me.DataCabang.Text = "Alamat" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Telp"
        Me.DataCabang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrPictureBox1
        '
        Me.XrPictureBox1.Dpi = 254.0!
        Me.XrPictureBox1.Image = CType(resources.GetObject("XrPictureBox1.Image"), System.Drawing.Image)
        Me.XrPictureBox1.LocationFloat = New DevExpress.Utils.PointFloat(139.3541!, 2.645833!)
        Me.XrPictureBox1.Name = "XrPictureBox1"
        Me.XrPictureBox1.SizeF = New System.Drawing.SizeF(362.2675!, 161.6075!)
        Me.XrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.Squeeze
        '
        'ReportFooter
        '
        Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine2, Me.LabelFooter})
        Me.ReportFooter.Dpi = 254.0!
        Me.ReportFooter.HeightF = 170.0!
        Me.ReportFooter.Name = "ReportFooter"
        '
        'XrLine2
        '
        Me.XrLine2.Dpi = 254.0!
        Me.XrLine2.LineWidth = 5
        Me.XrLine2.LocationFloat = New DevExpress.Utils.PointFloat(1.999938!, 0.0!)
        Me.XrLine2.Name = "XrLine2"
        Me.XrLine2.SizeF = New System.Drawing.SizeF(648.0001!, 13.22919!)
        '
        'LabelFooter
        '
        Me.LabelFooter.Dpi = 254.0!
        Me.LabelFooter.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelFooter.LocationFloat = New DevExpress.Utils.PointFloat(1.999938!, 13.22917!)
        Me.LabelFooter.Multiline = True
        Me.LabelFooter.Name = "LabelFooter"
        Me.LabelFooter.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.LabelFooter.SizeF = New System.Drawing.SizeF(648.0001!, 156.3158!)
        Me.LabelFooter.StylePriority.UseFont = False
        Me.LabelFooter.StylePriority.UseTextAlignment = False
        Me.LabelFooter.Text = "Mohon membawa bukti print ini pada saat melakukan penukaran" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Terima kasih" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "www.la" & _
            "rittabakery.com"
        Me.LabelFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'PrintOut_PenukaranPromoDanGift
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.ReportHeader, Me.ReportFooter})
        Me.Dpi = 254.0!
        Me.Margins = New System.Drawing.Printing.Margins(64, 64, 64, 64)
        Me.PageHeight = 1125
        Me.PageWidth = 781
        Me.PaperKind = System.Drawing.Printing.PaperKind.Custom
        Me.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter
        Me.ShowPrintMarginsWarning = False
        Me.SnapGridSize = 31.75!
        Me.Version = "11.1"
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents JenisBenefit As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents ExpiredDates As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents KodeBenefit As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
    Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents TglDanJamPrint As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents DataCabang As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPictureBox1 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
    Friend WithEvents LabelFooter As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents BenefitDanRepetisi As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine2 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents BerlakuDi As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
End Class
