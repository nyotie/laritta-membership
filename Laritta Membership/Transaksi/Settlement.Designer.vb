﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Settlement
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.UncollectedRewards = New DevExpress.XtraGrid.GridControl()
        Me.ViewUncollectedRewards = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.Redeemed = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.UncollectedOnly = New DevExpress.XtraEditors.CheckEdit()
        Me.butRefresh = New DevExpress.XtraEditors.SimpleButton()
        Me.butClose = New DevExpress.XtraEditors.SimpleButton()
        Me.ButPrint = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.UncollectedRewards, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewUncollectedRewards, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Redeemed, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UncollectedOnly.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'UncollectedRewards
        '
        Me.UncollectedRewards.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.UncollectedRewards.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.UncollectedRewards.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.UncollectedRewards.Location = New System.Drawing.Point(13, 39)
        Me.UncollectedRewards.MainView = Me.ViewUncollectedRewards
        Me.UncollectedRewards.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.UncollectedRewards.Name = "UncollectedRewards"
        Me.UncollectedRewards.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.Redeemed})
        Me.UncollectedRewards.Size = New System.Drawing.Size(777, 322)
        Me.UncollectedRewards.TabIndex = 1
        Me.UncollectedRewards.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.ViewUncollectedRewards})
        '
        'ViewUncollectedRewards
        '
        Me.ViewUncollectedRewards.Appearance.HeaderPanel.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ViewUncollectedRewards.Appearance.HeaderPanel.Options.UseFont = True
        Me.ViewUncollectedRewards.Appearance.Row.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ViewUncollectedRewards.Appearance.Row.Options.UseFont = True
        Me.ViewUncollectedRewards.ColumnPanelRowHeight = 40
        Me.ViewUncollectedRewards.GridControl = Me.UncollectedRewards
        Me.ViewUncollectedRewards.Name = "ViewUncollectedRewards"
        Me.ViewUncollectedRewards.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewUncollectedRewards.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewUncollectedRewards.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewUncollectedRewards.OptionsBehavior.Editable = False
        Me.ViewUncollectedRewards.OptionsCustomization.AllowColumnMoving = False
        Me.ViewUncollectedRewards.OptionsCustomization.AllowColumnResizing = False
        Me.ViewUncollectedRewards.OptionsCustomization.AllowFilter = False
        Me.ViewUncollectedRewards.OptionsCustomization.AllowGroup = False
        Me.ViewUncollectedRewards.OptionsCustomization.AllowQuickHideColumns = False
        Me.ViewUncollectedRewards.OptionsView.ShowAutoFilterRow = True
        Me.ViewUncollectedRewards.OptionsView.ShowFooter = True
        Me.ViewUncollectedRewards.OptionsView.ShowGroupPanel = False
        Me.ViewUncollectedRewards.RowHeight = 30
        '
        'Redeemed
        '
        Me.Redeemed.AutoHeight = False
        Me.Redeemed.Name = "Redeemed"
        Me.Redeemed.ValueChecked = "True"
        Me.Redeemed.ValueGrayed = "False"
        Me.Redeemed.ValueUnchecked = "False"
        '
        'UncollectedOnly
        '
        Me.UncollectedOnly.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.UncollectedOnly.EditValue = True
        Me.UncollectedOnly.Location = New System.Drawing.Point(529, 12)
        Me.UncollectedOnly.Name = "UncollectedOnly"
        Me.UncollectedOnly.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.UncollectedOnly.Properties.Appearance.Options.UseFont = True
        Me.UncollectedOnly.Properties.Caption = "Tampilkan hanya yang belum di ambil"
        Me.UncollectedOnly.Size = New System.Drawing.Size(262, 19)
        Me.UncollectedOnly.TabIndex = 0
        '
        'butRefresh
        '
        Me.butRefresh.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.butRefresh.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butRefresh.Appearance.Options.UseFont = True
        Me.butRefresh.Image = Global.Laritta_Membership.My.Resources.Resources.refresh_32
        Me.butRefresh.Location = New System.Drawing.Point(582, 367)
        Me.butRefresh.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.butRefresh.Name = "butRefresh"
        Me.butRefresh.Size = New System.Drawing.Size(100, 42)
        Me.butRefresh.TabIndex = 2
        Me.butRefresh.Text = "Refresh"
        '
        'butClose
        '
        Me.butClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.butClose.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butClose.Appearance.Options.UseFont = True
        Me.butClose.Image = Global.Laritta_Membership.My.Resources.Resources.close_32
        Me.butClose.Location = New System.Drawing.Point(690, 367)
        Me.butClose.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.butClose.Name = "butClose"
        Me.butClose.Size = New System.Drawing.Size(100, 42)
        Me.butClose.TabIndex = 4
        Me.butClose.Text = "&1 Tutup"
        '
        'ButPrint
        '
        Me.ButPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButPrint.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButPrint.Appearance.Options.UseFont = True
        Me.ButPrint.Image = Global.Laritta_Membership.My.Resources.Resources.print_32
        Me.ButPrint.Location = New System.Drawing.Point(13, 367)
        Me.ButPrint.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.ButPrint.Name = "ButPrint"
        Me.ButPrint.Size = New System.Drawing.Size(100, 42)
        Me.ButPrint.TabIndex = 3
        Me.ButPrint.Text = "Print"
        '
        'Settlement
        '
        Me.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(803, 421)
        Me.Controls.Add(Me.ButPrint)
        Me.Controls.Add(Me.butRefresh)
        Me.Controls.Add(Me.butClose)
        Me.Controls.Add(Me.UncollectedRewards)
        Me.Controls.Add(Me.UncollectedOnly)
        Me.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Name = "Settlement"
        Me.Text = "Settlement"
        CType(Me.UncollectedRewards, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewUncollectedRewards, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Redeemed, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UncollectedOnly.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents UncollectedRewards As DevExpress.XtraGrid.GridControl
    Friend WithEvents ViewUncollectedRewards As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents UncollectedOnly As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents butRefresh As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butClose As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButPrint As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Redeemed As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
End Class
