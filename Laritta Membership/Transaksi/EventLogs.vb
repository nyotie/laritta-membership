﻿Imports DevExpress.Utils

Public Class EventLogs

    Private Sub EventLogs_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("DaftarEventLists")
        xSet.Tables.Remove("DaftarEventLogs")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub EventLogs_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()
        StartDate.EditValue = DateAdd(DateInterval.Day, -1, Today.Date)
        EndDate.EditValue = Now
        Get_EventLists()
        Get_EventLogs()
    End Sub

    Private Sub EventLogs_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        LoadSprite.CloseLoading()
        StartDate.Focus()
    End Sub

    Private Sub CekPeriode_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CekPeriode.CheckedChanged
        WindOfChange()
    End Sub

    Private Sub CekEvent_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CekEvent.CheckedChanged
        WindOfChange()
    End Sub

    Private Sub CekMember_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CekMember.CheckedChanged
        WindOfChange()
    End Sub

    Private Sub butRefresh_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butRefresh.Click
        If CekEvent.Checked And DaftarEventList.EditValue Is Nothing Then Return
        If CekMember.Checked And (NamaMember.Text = "" Or NamaMember.Text.Contains("'")) Then Return
        If Not CekPeriode.Checked And Not CekEvent.Checked And Not CekMember.Checked Then Return

        Get_EventLogs()
    End Sub

    Private Sub ButPrint_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButPrint.Click
        ViewEventLogs.ShowRibbonPrintPreview()
    End Sub

    Private Sub ButReset_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButReset.Click
        StartDate.EditValue = DateAdd(DateInterval.Day, -1, Today.Date)
        EndDate.EditValue = Now
        CekPeriode.Checked = True
        CekEvent.Checked = False
        CekMember.Checked = False
        parameterevent.SelectedIndex = 0
        parametermember.SelectedIndex = 0
        DaftarEventList.EditValue = Nothing
        NamaMember.Text = ""
    End Sub

    Private Sub butClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butClose.Click
        Close()
    End Sub

    Private Sub WindOfChange()
        If CekPeriode.Checked Then
            StartDate.Enabled = True
            EndDate.Enabled = True
        Else
            StartDate.Enabled = False
            EndDate.Enabled = False
        End If

        If CekEvent.Checked Then
            DaftarEventList.Enabled = True
        Else
            DaftarEventList.Enabled = False
        End If

        If CekMember.Checked Then
            NamaMember.Enabled = True
        Else
            NamaMember.Enabled = False
        End If

        If CekPeriode.Checked And CekEvent.Checked Then
            parameterevent.Enabled = True
        Else
            parameterevent.Enabled = False
        End If

        If (CekPeriode.Checked Or CekEvent.Checked) And CekMember.Checked Then
            parametermember.Enabled = True
        Else
            parametermember.Enabled = False
        End If

    End Sub

    Private Sub Get_EventLists()
        '---------------------------Event Lists
        SQLquery = "SELECT event_id ID, event_name 'Event' FROM mbsm_eventlist;"
        ExDb.ExecQuery(1, SQLquery, xSet, "DaftarEventLists")
        DaftarEventList.Properties.DataSource = xSet.Tables("DaftarEventLists").DefaultView

        DaftarEventList.Properties.NullText = ""
        DaftarEventList.Properties.ValueMember = "ID"
        DaftarEventList.Properties.DisplayMember = "Event"
        DaftarEventList.Properties.ShowClearButton = False
        DaftarEventList.Properties.PopulateViewColumns()

        DaftarEventList.Properties.PopupFormSize = New Size(280, 400)
        ViewEventList.RowHeight = 40

        ViewEventList.Columns("ID").Visible = False

        For Each coll As DataColumn In xSet.Tables("DaftarEventLists").Columns
            ViewEventList.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next
    End Sub

    Private Sub Get_EventLogs()
        '---------------------------Event Logs
        If Not xSet.Tables("DaftarEventLogs") Is Nothing Then xSet.Tables("DaftarEventLogs").Clear()
        SQLquery = "SELECT a.id_cabang, d.namaCabang Cabang, a.id_customer, c.namaCust Member, a.event_id, b.event_name 'Event', a.event_targetname 'Event info', a.event_date Tanggal, a.event_value Rupiah, a.event_point 'Point', a.event_stamp Stamp, e.Nama 'User' FROM mbst_log a INNER JOIN mbsm_eventlist b ON a.event_id=b.event_id INNER JOIN mcustomer c ON a.id_customer=c.idCust INNER JOIN mcabang d ON a.id_cabang=d.idCabang INNER JOIN mstaff e ON a.creator=e.idStaff WHERE "
        If CekPeriode.Checked Then SQLquery += String.Format("(DATE_FORMAT(a.event_date,'%Y-%m-%d %H:%i') BETWEEN '{0}' AND '{1}') ", Format(StartDate.EditValue, "yyyy-MM-dd HH:mm"), Format(EndDate.EditValue, "yyyy-MM-dd HH:mm"))
        If CekEvent.Checked Then SQLquery += String.Format("{0} a.event_id={1} ", IIf(parameterevent.Enabled, parameterevent.SelectedItem, ""), DaftarEventList.EditValue)
        If CekMember.Checked Then SQLquery += String.Format("{0} c.namaCust LIKE '%{1}%' ", IIf(parametermember.Enabled, parametermember.SelectedItem, ""), NamaMember.Text)
        SQLquery += "ORDER BY a.event_date"
        ExDb.ExecQuery(1, SQLquery, xSet, "DaftarEventLogs")

        '----------------------------Grid Settings
        GridEventLogs.DataSource = xSet.Tables("DaftarEventLogs").DefaultView

        For Each coll As DataColumn In xSet.Tables("DaftarEventLogs").Columns
            ViewEventLogs.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next
        ViewEventLogs.Columns("id_cabang").Visible = False
        ViewEventLogs.Columns("id_customer").Visible = False
        ViewEventLogs.Columns("event_id").Visible = False
        ViewEventLogs.Columns("Tanggal").DisplayFormat.FormatType = FormatType.DateTime
        ViewEventLogs.Columns("Tanggal").DisplayFormat.FormatString = "dd MMM yyyy, HH:mm"
        ViewEventLogs.Columns("Rupiah").DisplayFormat.FormatType = FormatType.Numeric
        ViewEventLogs.Columns("Rupiah").DisplayFormat.FormatString = "{0:n2}"
        ViewEventLogs.Columns("Point").DisplayFormat.FormatType = FormatType.Numeric
        ViewEventLogs.Columns("Point").DisplayFormat.FormatString = "{0:n2}"
        ViewEventLogs.Columns("Stamp").DisplayFormat.FormatType = FormatType.Numeric
        ViewEventLogs.Columns("Stamp").DisplayFormat.FormatString = "{0:n0}"
        ViewEventLogs.Columns("Cabang").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count
        ViewEventLogs.Columns("Cabang").SummaryItem.DisplayFormat = "Jumlah:{0:n0}"
        ViewEventLogs.BestFitColumns()
    End Sub
End Class