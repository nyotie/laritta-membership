﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EventLogs
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GridEventLogs = New DevExpress.XtraGrid.GridControl()
        Me.ViewEventLogs = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.ButPrint = New DevExpress.XtraEditors.SimpleButton()
        Me.butClose = New DevExpress.XtraEditors.SimpleButton()
        Me.butRefresh = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.CekMember = New DevExpress.XtraEditors.CheckEdit()
        Me.CekEvent = New DevExpress.XtraEditors.CheckEdit()
        Me.CekPeriode = New DevExpress.XtraEditors.CheckEdit()
        Me.parametermember = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.parameterevent = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.NamaMember = New DevExpress.XtraEditors.TextEdit()
        Me.DaftarEventList = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.ViewEventList = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.ButReset = New DevExpress.XtraEditors.SimpleButton()
        Me.EndDate = New DevExpress.XtraEditors.DateEdit()
        Me.StartDate = New DevExpress.XtraEditors.DateEdit()
        CType(Me.GridEventLogs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewEventLogs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.CekMember.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CekEvent.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CekPeriode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.parametermember.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.parameterevent.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NamaMember.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DaftarEventList.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewEventList, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EndDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EndDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StartDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StartDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GridEventLogs
        '
        Me.GridEventLogs.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridEventLogs.Location = New System.Drawing.Point(12, 118)
        Me.GridEventLogs.MainView = Me.ViewEventLogs
        Me.GridEventLogs.Name = "GridEventLogs"
        Me.GridEventLogs.Size = New System.Drawing.Size(828, 311)
        Me.GridEventLogs.TabIndex = 1
        Me.GridEventLogs.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.ViewEventLogs})
        '
        'ViewEventLogs
        '
        Me.ViewEventLogs.Appearance.HeaderPanel.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ViewEventLogs.Appearance.HeaderPanel.Options.UseFont = True
        Me.ViewEventLogs.Appearance.Row.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ViewEventLogs.Appearance.Row.Options.UseFont = True
        Me.ViewEventLogs.ColumnPanelRowHeight = 40
        Me.ViewEventLogs.GridControl = Me.GridEventLogs
        Me.ViewEventLogs.Name = "ViewEventLogs"
        Me.ViewEventLogs.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewEventLogs.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewEventLogs.OptionsBehavior.Editable = False
        Me.ViewEventLogs.OptionsCustomization.AllowQuickHideColumns = False
        Me.ViewEventLogs.OptionsView.EnableAppearanceEvenRow = True
        Me.ViewEventLogs.OptionsView.ShowAutoFilterRow = True
        Me.ViewEventLogs.OptionsView.ShowFooter = True
        Me.ViewEventLogs.OptionsView.ShowGroupPanel = False
        Me.ViewEventLogs.RowHeight = 30
        '
        'ButPrint
        '
        Me.ButPrint.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButPrint.Appearance.Options.UseFont = True
        Me.ButPrint.Image = Global.Laritta_Membership.My.Resources.Resources.print_32
        Me.ButPrint.Location = New System.Drawing.Point(571, 53)
        Me.ButPrint.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.ButPrint.Name = "ButPrint"
        Me.ButPrint.Size = New System.Drawing.Size(106, 42)
        Me.ButPrint.TabIndex = 11
        Me.ButPrint.Text = "&Print"
        '
        'butClose
        '
        Me.butClose.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butClose.Appearance.Options.UseFont = True
        Me.butClose.Image = Global.Laritta_Membership.My.Resources.Resources.close_32
        Me.butClose.Location = New System.Drawing.Point(685, 53)
        Me.butClose.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.butClose.Name = "butClose"
        Me.butClose.Size = New System.Drawing.Size(106, 42)
        Me.butClose.TabIndex = 12
        Me.butClose.Text = "&1 Tutup"
        '
        'butRefresh
        '
        Me.butRefresh.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butRefresh.Appearance.Options.UseFont = True
        Me.butRefresh.Image = Global.Laritta_Membership.My.Resources.Resources.refresh_32
        Me.butRefresh.Location = New System.Drawing.Point(571, 5)
        Me.butRefresh.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.butRefresh.Name = "butRefresh"
        Me.butRefresh.Size = New System.Drawing.Size(106, 42)
        Me.butRefresh.TabIndex = 9
        Me.butRefresh.Text = "&Refresh"
        '
        'PanelControl2
        '
        Me.PanelControl2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelControl2.Controls.Add(Me.CekMember)
        Me.PanelControl2.Controls.Add(Me.CekEvent)
        Me.PanelControl2.Controls.Add(Me.CekPeriode)
        Me.PanelControl2.Controls.Add(Me.parametermember)
        Me.PanelControl2.Controls.Add(Me.parameterevent)
        Me.PanelControl2.Controls.Add(Me.NamaMember)
        Me.PanelControl2.Controls.Add(Me.DaftarEventList)
        Me.PanelControl2.Controls.Add(Me.ButPrint)
        Me.PanelControl2.Controls.Add(Me.LabelControl2)
        Me.PanelControl2.Controls.Add(Me.ButReset)
        Me.PanelControl2.Controls.Add(Me.butRefresh)
        Me.PanelControl2.Controls.Add(Me.butClose)
        Me.PanelControl2.Controls.Add(Me.EndDate)
        Me.PanelControl2.Controls.Add(Me.StartDate)
        Me.PanelControl2.Location = New System.Drawing.Point(12, 11)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(828, 101)
        Me.PanelControl2.TabIndex = 0
        '
        'CekMember
        '
        Me.CekMember.Location = New System.Drawing.Point(5, 65)
        Me.CekMember.Name = "CekMember"
        Me.CekMember.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.CekMember.Properties.Appearance.Options.UseFont = True
        Me.CekMember.Properties.Caption = "Nama member"
        Me.CekMember.Size = New System.Drawing.Size(131, 23)
        Me.CekMember.TabIndex = 6
        '
        'CekEvent
        '
        Me.CekEvent.Location = New System.Drawing.Point(5, 35)
        Me.CekEvent.Name = "CekEvent"
        Me.CekEvent.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.CekEvent.Properties.Appearance.Options.UseFont = True
        Me.CekEvent.Properties.Caption = "Event"
        Me.CekEvent.Size = New System.Drawing.Size(131, 23)
        Me.CekEvent.TabIndex = 3
        '
        'CekPeriode
        '
        Me.CekPeriode.EditValue = True
        Me.CekPeriode.Location = New System.Drawing.Point(5, 5)
        Me.CekPeriode.Name = "CekPeriode"
        Me.CekPeriode.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.CekPeriode.Properties.Appearance.Options.UseFont = True
        Me.CekPeriode.Properties.Caption = "Periode"
        Me.CekPeriode.Size = New System.Drawing.Size(131, 23)
        Me.CekPeriode.TabIndex = 0
        '
        'parametermember
        '
        Me.parametermember.EditValue = "AND"
        Me.parametermember.Enabled = False
        Me.parametermember.Location = New System.Drawing.Point(142, 65)
        Me.parametermember.Name = "parametermember"
        Me.parametermember.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.parametermember.Properties.Appearance.Options.UseFont = True
        Me.parametermember.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.parametermember.Properties.Items.AddRange(New Object() {"AND", "OR"})
        Me.parametermember.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.parametermember.Size = New System.Drawing.Size(56, 24)
        Me.parametermember.TabIndex = 7
        '
        'parameterevent
        '
        Me.parameterevent.EditValue = "AND"
        Me.parameterevent.Enabled = False
        Me.parameterevent.Location = New System.Drawing.Point(142, 35)
        Me.parameterevent.Name = "parameterevent"
        Me.parameterevent.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.parameterevent.Properties.Appearance.Options.UseFont = True
        Me.parameterevent.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.parameterevent.Properties.Items.AddRange(New Object() {"AND", "OR"})
        Me.parameterevent.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.parameterevent.Size = New System.Drawing.Size(56, 24)
        Me.parameterevent.TabIndex = 4
        '
        'NamaMember
        '
        Me.NamaMember.Enabled = False
        Me.NamaMember.Location = New System.Drawing.Point(204, 65)
        Me.NamaMember.Name = "NamaMember"
        Me.NamaMember.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NamaMember.Properties.Appearance.Options.UseFont = True
        Me.NamaMember.Size = New System.Drawing.Size(192, 25)
        Me.NamaMember.TabIndex = 8
        '
        'DaftarEventList
        '
        Me.DaftarEventList.Enabled = False
        Me.DaftarEventList.Location = New System.Drawing.Point(204, 35)
        Me.DaftarEventList.Name = "DaftarEventList"
        Me.DaftarEventList.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.DaftarEventList.Properties.Appearance.Options.UseFont = True
        Me.DaftarEventList.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DaftarEventList.Properties.NullText = ""
        Me.DaftarEventList.Properties.View = Me.ViewEventList
        Me.DaftarEventList.Size = New System.Drawing.Size(192, 24)
        Me.DaftarEventList.TabIndex = 5
        '
        'ViewEventList
        '
        Me.ViewEventList.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.ViewEventList.Name = "ViewEventList"
        Me.ViewEventList.OptionsCustomization.AllowColumnMoving = False
        Me.ViewEventList.OptionsCustomization.AllowColumnResizing = False
        Me.ViewEventList.OptionsCustomization.AllowFilter = False
        Me.ViewEventList.OptionsCustomization.AllowGroup = False
        Me.ViewEventList.OptionsCustomization.AllowQuickHideColumns = False
        Me.ViewEventList.OptionsCustomization.AllowSort = False
        Me.ViewEventList.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.ViewEventList.OptionsView.ShowGroupPanel = False
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl2.Location = New System.Drawing.Point(340, 8)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(5, 18)
        Me.LabelControl2.TabIndex = 1
        Me.LabelControl2.Text = "-"
        '
        'ButReset
        '
        Me.ButReset.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButReset.Appearance.Options.UseFont = True
        Me.ButReset.Image = Global.Laritta_Membership.My.Resources.Resources.reset_32
        Me.ButReset.Location = New System.Drawing.Point(685, 5)
        Me.ButReset.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.ButReset.Name = "ButReset"
        Me.ButReset.Size = New System.Drawing.Size(106, 42)
        Me.ButReset.TabIndex = 10
        Me.ButReset.Text = "R&eset"
        '
        'EndDate
        '
        Me.EndDate.EditValue = Nothing
        Me.EndDate.Location = New System.Drawing.Point(351, 5)
        Me.EndDate.Name = "EndDate"
        Me.EndDate.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.EndDate.Properties.Appearance.Options.UseFont = True
        Me.EndDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.EndDate.Properties.Mask.EditMask = "dd MMM yyyy, HH:mm"
        Me.EndDate.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.EndDate.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.EndDate.Size = New System.Drawing.Size(192, 24)
        Me.EndDate.TabIndex = 2
        '
        'StartDate
        '
        Me.StartDate.EditValue = Nothing
        Me.StartDate.Location = New System.Drawing.Point(142, 5)
        Me.StartDate.Name = "StartDate"
        Me.StartDate.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.StartDate.Properties.Appearance.Options.UseFont = True
        Me.StartDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.StartDate.Properties.Mask.EditMask = "dd MMM yyyy, HH:mm"
        Me.StartDate.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.StartDate.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.StartDate.Size = New System.Drawing.Size(192, 24)
        Me.StartDate.TabIndex = 1
        '
        'EventLogs
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(852, 441)
        Me.Controls.Add(Me.PanelControl2)
        Me.Controls.Add(Me.GridEventLogs)
        Me.Name = "EventLogs"
        Me.Text = "EventLogs"
        CType(Me.GridEventLogs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewEventLogs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        CType(Me.CekMember.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CekEvent.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CekPeriode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.parametermember.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.parameterevent.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NamaMember.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DaftarEventList.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewEventList, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EndDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EndDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StartDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StartDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GridEventLogs As DevExpress.XtraGrid.GridControl
    Friend WithEvents ViewEventLogs As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents ButPrint As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butClose As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butRefresh As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents EndDate As DevExpress.XtraEditors.DateEdit
    Friend WithEvents StartDate As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents NamaMember As DevExpress.XtraEditors.TextEdit
    Friend WithEvents DaftarEventList As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents ViewEventList As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents ButReset As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents CekMember As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CekEvent As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CekPeriode As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents parametermember As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents parameterevent As DevExpress.XtraEditors.ComboBoxEdit
End Class
