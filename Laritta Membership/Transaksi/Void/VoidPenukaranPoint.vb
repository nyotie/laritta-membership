﻿Imports DevExpress.Utils

Public Class VoidPenukaranPoint

    Private Sub VoidPenukaranPoint_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("DaftarExchangePoint")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub VoidPenukaranPoint_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()
        Get_KebutuhanDaftar()
    End Sub

    Private Sub VoidPenukaranPoint_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown

        LoadSprite.CloseLoading()
    End Sub

    Private Sub ButOK_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButOK.Click
        Try
            If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then Return

            LoadSprite.ShowLoading()
            If ViewExchangePoint.FocusedRowHandle < 0 Then Return
            Dim TargetID As Integer = ViewExchangePoint.GetFocusedRowCellValue("ID")
            If TargetID <= 0 Then Return

            SQLquery = String.Format("UPDATE mbst_exchangepoint SET updateby={1}, updatedate=NOW(), isVoid=1 WHERE id_exchangepoint={0}", TargetID, staff_id)
            ExDb.ExecData(1, SQLquery)

            MsgInfo("Data berhasil disimpan, tekan OK untuk melanjutkan.")
        Catch ex As Exception
            MsgErrorDev()
        End Try
        LoadSprite.CloseLoading()
        Close()
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Sub Get_KebutuhanDaftar()
        '--------------------------------Daftar Exchange Point
        SQLquery = String.Format("SELECT id_exchangepoint ID, b.namaCabang Cabang, a.createdate Tanggal, a.nama_rewardpoint Reward, a.jml_rewardpoint Jml, a.point_taken, a.reward_value FROM mbst_exchangepoint a INNER JOIN mcabang b ON a.id_cabang=b.idCabang WHERE a.isVoid=0 AND a.redeemed=0 AND a.id_customer={0} AND DATEDIFF(CURDATE(), DATE(a.createdate))<=7;", CheckMember.CurrentMember)
        ExDb.ExecQuery(1, SQLquery, xSet, "DaftarExchangePoint")
        DaftarExchangePoint.DataSource = xSet.Tables("DaftarExchangePoint").DefaultView

        ViewExchangePoint.Columns("ID").Visible = False
        ViewExchangePoint.Columns("point_taken").Visible = False
        ViewExchangePoint.Columns("reward_value").Visible = False
        ViewExchangePoint.Columns("Tanggal").DisplayFormat.FormatType = FormatType.DateTime
        ViewExchangePoint.Columns("Tanggal").DisplayFormat.FormatString = "dd MMM yyyy HH:mm"

        ViewExchangePoint.Columns("Cabang").Width = 80
        ViewExchangePoint.Columns("Tanggal").Width = 100
    End Sub
End Class