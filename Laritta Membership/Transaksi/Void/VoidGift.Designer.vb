﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class VoidGift
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DaftarGiftDimiliki = New DevExpress.XtraGrid.GridControl()
        Me.ViewGiftDimiliki = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.ButBatal = New DevExpress.XtraEditors.SimpleButton()
        Me.ButOK = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.DaftarGiftDimiliki, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewGiftDimiliki, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        Me.SuspendLayout()
        '
        'DaftarGiftDimiliki
        '
        Me.DaftarGiftDimiliki.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DaftarGiftDimiliki.Location = New System.Drawing.Point(5, 26)
        Me.DaftarGiftDimiliki.MainView = Me.ViewGiftDimiliki
        Me.DaftarGiftDimiliki.Name = "DaftarGiftDimiliki"
        Me.DaftarGiftDimiliki.Size = New System.Drawing.Size(450, 410)
        Me.DaftarGiftDimiliki.TabIndex = 2
        Me.DaftarGiftDimiliki.TabStop = False
        Me.DaftarGiftDimiliki.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.ViewGiftDimiliki})
        '
        'ViewGiftDimiliki
        '
        Me.ViewGiftDimiliki.Appearance.HeaderPanel.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ViewGiftDimiliki.Appearance.HeaderPanel.Options.UseFont = True
        Me.ViewGiftDimiliki.Appearance.Row.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ViewGiftDimiliki.Appearance.Row.Options.UseFont = True
        Me.ViewGiftDimiliki.ColumnPanelRowHeight = 40
        Me.ViewGiftDimiliki.GridControl = Me.DaftarGiftDimiliki
        Me.ViewGiftDimiliki.Name = "ViewGiftDimiliki"
        Me.ViewGiftDimiliki.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewGiftDimiliki.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewGiftDimiliki.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewGiftDimiliki.OptionsBehavior.Editable = False
        Me.ViewGiftDimiliki.OptionsCustomization.AllowColumnMoving = False
        Me.ViewGiftDimiliki.OptionsCustomization.AllowColumnResizing = False
        Me.ViewGiftDimiliki.OptionsCustomization.AllowFilter = False
        Me.ViewGiftDimiliki.OptionsCustomization.AllowGroup = False
        Me.ViewGiftDimiliki.OptionsCustomization.AllowQuickHideColumns = False
        Me.ViewGiftDimiliki.OptionsCustomization.AllowSort = False
        Me.ViewGiftDimiliki.OptionsView.ShowFooter = True
        Me.ViewGiftDimiliki.OptionsView.ShowGroupPanel = False
        Me.ViewGiftDimiliki.RowHeight = 30
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.Controls.Add(Me.DaftarGiftDimiliki)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(460, 441)
        Me.GroupControl1.TabIndex = 8
        Me.GroupControl1.Text = "Daftar Gift dimiliki"
        '
        'ButBatal
        '
        Me.ButBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButBatal.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButBatal.Appearance.Options.UseFont = True
        Me.ButBatal.Image = Global.Laritta_Membership.My.Resources.Resources.back_32
        Me.ButBatal.Location = New System.Drawing.Point(382, 459)
        Me.ButBatal.Name = "ButBatal"
        Me.ButBatal.Size = New System.Drawing.Size(90, 40)
        Me.ButBatal.TabIndex = 7
        Me.ButBatal.Text = "&Batal"
        '
        'ButOK
        '
        Me.ButOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButOK.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButOK.Appearance.Options.UseFont = True
        Me.ButOK.Image = Global.Laritta_Membership.My.Resources.Resources.apply_32
        Me.ButOK.Location = New System.Drawing.Point(286, 459)
        Me.ButOK.Name = "ButOK"
        Me.ButOK.Size = New System.Drawing.Size(90, 40)
        Me.ButOK.TabIndex = 6
        Me.ButOK.Text = "&OK"
        '
        'VoidGift
        '
        Me.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(484, 511)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.ButBatal)
        Me.Controls.Add(Me.ButOK)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "VoidGift"
        Me.Text = "VoidGift"
        CType(Me.DaftarGiftDimiliki, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewGiftDimiliki, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DaftarGiftDimiliki As DevExpress.XtraGrid.GridControl
    Friend WithEvents ViewGiftDimiliki As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents ButBatal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButOK As DevExpress.XtraEditors.SimpleButton
End Class
