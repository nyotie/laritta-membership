﻿Imports DevExpress.Utils

Public Class VoidGift

    Private Sub VoidGift_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub VoidGift_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()
        Get_KebutuhanDaftar()
        AddHandler DaftarGiftDimiliki.DoubleClick, AddressOf ButOK_Click
    End Sub

    Private Sub VoidGift_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown

        LoadSprite.CloseLoading()
    End Sub

    Private Sub ButOK_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButOK.Click
        If ViewGiftDimiliki.FocusedRowHandle < 0 Then Return
        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then Return

        Try
            LoadSprite.ShowLoading()
            SQLquery = String.Format("UPDATE mbsm_membergifts SET Inactive=1  WHERE id_customer={0} AND id_gift={1} AND Inactive=0;" & _
                                     "INSERT INTO mbst_log	(id_cabang, id_customer, trans_id, event_date, event_id, event_targetname, creator) " & _
                                     "VALUES({3}, {0}, 0, NOW(), 16, '{2}', {4});", CheckMember.CurrentMember, ViewGiftDimiliki.GetFocusedRowCellValue("ID"),
                                     ViewGiftDimiliki.GetFocusedRowCellValue("Nama"), id_cabang, staff_id)
            ExDb.ExecData(1, SQLquery)

            MsgInfo("Data berhasil disimpan, tekan OK untuk melanjutkan.")
        Catch ex As Exception
            MsgErrorDev()
        End Try
        LoadSprite.CloseLoading()
        Close()
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Sub Get_KebutuhanDaftar()
        '--------------------------------Daftar Gift dimiliki
        If Not xSet.Tables("ThisMemberGift") Is Nothing Then xSet.Tables("ThisMemberGift").Clear()
        SQLquery = String.Format("SELECT a.id_gift ID, b.nama_gift Nama, get_date TglStart, a.expired_date Expired, a.availability Tersedia, a.use_count Terpakai, b.syarat_none Syarat, b.exc_at_outlet Outlet, b.exc_at_order 'Order' " & _
                        "FROM mbsm_membergifts a INNER JOIN mbsm_gift b ON a.id_gift=b.id_gift WHERE a.inactive=0 AND a.availability > a.use_count AND a.expired_date>=CURDATE() AND a.id_customer={0}; ", CheckMember.CurrentMember)
        ExDb.ExecQuery(1, SQLquery, xSet, "ThisMemberGift")
        DaftarGiftDimiliki.DataSource = xSet.Tables("ThisMemberGift").DefaultView

        ViewGiftDimiliki.Columns("ID").Visible = False
        ViewGiftDimiliki.Columns("Tersedia").Visible = False
        ViewGiftDimiliki.Columns("Terpakai").Visible = False
        ViewGiftDimiliki.Columns("Syarat").Visible = False
        ViewGiftDimiliki.Columns("Outlet").Visible = False
        ViewGiftDimiliki.Columns("Order").Visible = False

        ViewGiftDimiliki.Columns("TglStart").DisplayFormat.FormatType = FormatType.DateTime
        ViewGiftDimiliki.Columns("TglStart").DisplayFormat.FormatString = "dd MMM yyyy"
        ViewGiftDimiliki.Columns("Expired").DisplayFormat.FormatType = FormatType.DateTime
        ViewGiftDimiliki.Columns("Expired").DisplayFormat.FormatString = "dd MMM yyyy"

        ViewGiftDimiliki.Columns("Nama").Width = 150
        ViewGiftDimiliki.Columns("TglStart").Width = 80
    End Sub
End Class