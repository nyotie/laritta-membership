﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class VoidPenukaranPoint
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DaftarExchangePoint = New DevExpress.XtraGrid.GridControl()
        Me.ViewExchangePoint = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.ButBatal = New DevExpress.XtraEditors.SimpleButton()
        Me.ButOK = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        CType(Me.DaftarExchangePoint, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewExchangePoint, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        Me.SuspendLayout()
        '
        'DaftarExchangePoint
        '
        Me.DaftarExchangePoint.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DaftarExchangePoint.Location = New System.Drawing.Point(5, 26)
        Me.DaftarExchangePoint.MainView = Me.ViewExchangePoint
        Me.DaftarExchangePoint.Name = "DaftarExchangePoint"
        Me.DaftarExchangePoint.Size = New System.Drawing.Size(527, 410)
        Me.DaftarExchangePoint.TabIndex = 2
        Me.DaftarExchangePoint.TabStop = False
        Me.DaftarExchangePoint.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.ViewExchangePoint})
        '
        'ViewExchangePoint
        '
        Me.ViewExchangePoint.Appearance.HeaderPanel.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ViewExchangePoint.Appearance.HeaderPanel.Options.UseFont = True
        Me.ViewExchangePoint.Appearance.Row.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ViewExchangePoint.Appearance.Row.Options.UseFont = True
        Me.ViewExchangePoint.ColumnPanelRowHeight = 40
        Me.ViewExchangePoint.GridControl = Me.DaftarExchangePoint
        Me.ViewExchangePoint.Name = "ViewExchangePoint"
        Me.ViewExchangePoint.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewExchangePoint.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewExchangePoint.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewExchangePoint.OptionsBehavior.Editable = False
        Me.ViewExchangePoint.OptionsCustomization.AllowColumnMoving = False
        Me.ViewExchangePoint.OptionsCustomization.AllowColumnResizing = False
        Me.ViewExchangePoint.OptionsCustomization.AllowFilter = False
        Me.ViewExchangePoint.OptionsCustomization.AllowGroup = False
        Me.ViewExchangePoint.OptionsCustomization.AllowQuickHideColumns = False
        Me.ViewExchangePoint.OptionsCustomization.AllowSort = False
        Me.ViewExchangePoint.OptionsView.ShowFooter = True
        Me.ViewExchangePoint.OptionsView.ShowGroupPanel = False
        Me.ViewExchangePoint.RowHeight = 30
        '
        'ButBatal
        '
        Me.ButBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButBatal.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButBatal.Appearance.Options.UseFont = True
        Me.ButBatal.Image = Global.Laritta_Membership.My.Resources.Resources.back_32
        Me.ButBatal.Location = New System.Drawing.Point(459, 459)
        Me.ButBatal.Name = "ButBatal"
        Me.ButBatal.Size = New System.Drawing.Size(90, 40)
        Me.ButBatal.TabIndex = 4
        Me.ButBatal.Text = "&Batal"
        '
        'ButOK
        '
        Me.ButOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButOK.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButOK.Appearance.Options.UseFont = True
        Me.ButOK.Image = Global.Laritta_Membership.My.Resources.Resources.apply_32
        Me.ButOK.Location = New System.Drawing.Point(363, 459)
        Me.ButOK.Name = "ButOK"
        Me.ButOK.Size = New System.Drawing.Size(90, 40)
        Me.ButOK.TabIndex = 3
        Me.ButOK.Text = "&OK"
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.Controls.Add(Me.DaftarExchangePoint)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(537, 441)
        Me.GroupControl1.TabIndex = 5
        Me.GroupControl1.Text = "Daftar Penukaran"
        '
        'VoidPenukaranPoint
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(561, 511)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.ButBatal)
        Me.Controls.Add(Me.ButOK)
        Me.Name = "VoidPenukaranPoint"
        CType(Me.DaftarExchangePoint, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewExchangePoint, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ButBatal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents DaftarExchangePoint As DevExpress.XtraGrid.GridControl
    Friend WithEvents ViewExchangePoint As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
End Class
