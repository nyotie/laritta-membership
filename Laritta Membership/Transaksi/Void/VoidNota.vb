﻿Imports DevExpress.Utils

Public Class VoidNota
    Dim CurrentTrans As Integer

    Private Sub VoidNota_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("This_mtpos")
        xSet.Tables.Remove("This_dtpos")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub VoidNota_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub VoidNota_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown

    End Sub

    Private Sub NoNota_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles NoNota.EditValueChanged
        If NoNota.EditValue.ToString.Length < 11 Then Return

        If NoNota.EditValue.ToString.Length = 15 And CurrentTrans = 0 Then
            Dim Tahun As String = ""
            Dim Bulan As String = ""
            Dim Nomer As Integer = 0

            Tahun = "20" & Mid(NoNota.Text, 4, 2)
            Bulan = Mid(NoNota.Text, 7, 2)
            Nomer = Microsoft.VisualBasic.Right(NoNota.Text, 6)

            '----------------------------------------------CHECK NOTA SUDAH TERDAFTAR/BELUM
            If Not xSet.Tables("This_mtpos") Is Nothing Then xSet.Tables("This_mtpos").Clear()
            SQLquery = String.Format("SELECT id_pos ID, idCust, total_price, point_gained, stamp_gained, isVoid FROM mbst_posm a WHERE a.idCabang={0} AND a.nomor_nota='{1}';", id_cabang, NoNota.Text)
            ExDb.ExecQuery(1, SQLquery, xSet, "This_mtpos")
            If xSet.Tables("This_mtpos").Rows.Count < 1 Then
                MsgWarning("Nomor nota tidak ditemukan")
                Return
            Else
                If xSet.Tables("This_mtpos").Rows(0).Item("isVoid") = 1 Then
                    MsgWarning("No nota telah tervoid!")
                    Return
                Else
                    CurrentTrans = xSet.Tables("This_mtpos").Rows(0).Item("ID")
                    JmlPoint.EditValue = xSet.Tables("This_mtpos").Rows(0).Item("point_gained") * -1
                    JmlStamp.EditValue = xSet.Tables("This_mtpos").Rows(0).Item("stamp_gained") * -1
                    TransGrandTotal.EditValue = xSet.Tables("This_mtpos").Rows(0).Item("total_price")
                End If
            End If

            '----------------------------------------------IF NOT VOID, GET DETAIL TRANSAKSI
            If Not xSet.Tables("This_dtpos") Is Nothing Then xSet.Tables("This_dtpos").Clear()
            SQLquery = String.Format("SELECT a.idBrg ID, b.namaBrg Nama, a.quantity Jml, a.price Hrg, a.disc Disc, a.totalPrice Total FROM mbst_posd a INNER JOIN mbarang b ON a.idBrg=b.idBrg WHERE a.id_pos={1} AND a.idCabang={0};", id_cabang, CurrentTrans)
            ExDb.ExecQuery(1, SQLquery, xSet, "This_dtpos")

            DaftarTransaksiOutlet.DataSource = xSet.Tables("This_dtpos").DefaultView

            ViewTransaksiOutlet.Columns("ID").Visible = False

            ViewTransaksiOutlet.Columns("Hrg").DisplayFormat.FormatType = FormatType.Numeric
            ViewTransaksiOutlet.Columns("Hrg").DisplayFormat.FormatString = "Rp{0:n0}"
            ViewTransaksiOutlet.Columns("Disc").DisplayFormat.FormatType = FormatType.Numeric
            ViewTransaksiOutlet.Columns("Disc").DisplayFormat.FormatString = "Rp{0:n0}"
            ViewTransaksiOutlet.Columns("Total").DisplayFormat.FormatType = FormatType.Numeric
            ViewTransaksiOutlet.Columns("Total").DisplayFormat.FormatString = "Rp{0:n0}"

            ViewTransaksiOutlet.Columns("Nama").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count
            ViewTransaksiOutlet.Columns("Nama").SummaryItem.DisplayFormat = "Jumlah:{0:n0}"
            ViewTransaksiOutlet.Columns("Total").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            ViewTransaksiOutlet.Columns("Total").SummaryItem.DisplayFormat = "Subtotal:{0:n0}"

            For Each coll As DataColumn In xSet.Tables("This_dtpos").Columns
                ViewTransaksiOutlet.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
            Next

            ViewTransaksiOutlet.Columns("Nama").Width = 250
            ViewTransaksiOutlet.Columns("Jml").Width = 60
            ViewTransaksiOutlet.Columns("Hrg").Width = 100
            ViewTransaksiOutlet.Columns("Disc").Width = 80
            ViewTransaksiOutlet.Columns("Total").Width = 120

        ElseIf NoNota.EditValue.ToString.Length = 12 And CurrentTrans <> 0 Then
            ClearAll()
        End If
    End Sub

    Private Sub ButClear_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButClear.Click
        ClearAll()
    End Sub

    Private Sub ButOK_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButOK.Click
        If Not beforeSave() Then Return
        Try
            LoadSprite.ShowLoading("Chrushing the cookies", "Loading", Me)
            'idcust, idcbg, idpos, tglpos, nonota, totalqtt, totalrp, pelaku, isidt
            SQLquery = String.Format("UPDATE mbst_posm SET isVoid=1 WHERE idCabang={0} AND id_pos={1} AND nomor_nota='{2}'; " & _
                                     "UPDATE mbst_getpointandstamp SET isVoid=1, updateby={4} WHERE asal_transaksi=0 AND id_cabang={0} AND id_transaksi={1} AND id_customer={3};",
                                     id_cabang,
                                     CurrentTrans,
                                     NoNota.Text,
                                     xSet.Tables("This_mtpos").Rows(0).Item("idCust"),
                                     staff_id)
            ExDb.ExecData(1, SQLquery)

            MsgInfo("Nota berhasil di void, tekan OK untuk melanjutkan.")
        Catch ex As Exception
            MsgErrorDev()
        End Try
        LoadSprite.CloseLoading()
        Close()
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Sub ClearAll()
        CurrentTrans = 0

        NoNota.EditValue = ""
        TransGrandTotal.EditValue = 0

        If Not xSet.Tables("This_mtpos") Is Nothing Then xSet.Tables("This_mtpos").Clear()
        If Not xSet.Tables("This_dtpos") Is Nothing Then xSet.Tables("This_dtpos").Clear()
    End Sub

    Private Function beforeSave() As Boolean
        If NoNota.Text = "" Or CurrentTrans = 0 Then Return False

        If MsgBox("Yakin melakukan void?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        Return True
    End Function
End Class