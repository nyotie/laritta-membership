﻿Imports DevExpress.Utils

Public Class UpgradeMember

    Private Sub UpgradeMember_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("DataMasterCustomer")
        xSet.Tables.Remove("DaftarTipeMembership")
        xSet.Tables.Remove("GetNewKodeMember")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub UpgradeMember_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()

        SQLquery = String.Format("SELECT a.id_tipemembership, d.nama_tipemembership, d.level_tipemembership, IFNULL(SUM(b.NilaiTotalPesanan),0) TotalBeli, IFNULL(SUM(b.NilaiTotalPembayaran),0) TotalBayar, COUNT(a.idCust) xBeli " & _
                                 "FROM mcustomer a INNER JOIN set_pesanan b ON a.idCust=b.idCust INNER JOIN mtpesanan c ON b.idPesanan=c.idPesanan INNER JOIN mbsm_tipemembership d ON a.id_tipemembership=d.id_tipemembership " & _
                                 "WHERE c.Void=0 AND a.idCust={0}", CheckMember.CurrentMember)
        ExDb.ExecQuery(1, SQLquery, xSet, "DataMasterCustomer")

        TipeMembershipNow.Text = xSet.Tables("DataMasterCustomer").Rows(0).Item("nama_tipemembership")
        LevelNow.EditValue = xSet.Tables("DataMasterCustomer").Rows(0).Item("level_tipemembership")
        xTrans.EditValue = xSet.Tables("DataMasterCustomer").Rows(0).Item("xBeli")
        TotalBeli.EditValue = xSet.Tables("DataMasterCustomer").Rows(0).Item("TotalBeli")
        TotalBayar.EditValue = xSet.Tables("DataMasterCustomer").Rows(0).Item("TotalBayar")

        Get_KebutuhanDaftar()
    End Sub

    Private Sub UpgradeMember_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        LoadSprite.CloseLoading()
        DaftarTipeMembership.Focus()
    End Sub

    Private Sub DaftarTipeMembership_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DaftarTipeMembership.EditValueChanged
        If DaftarTipeMembership.EditValue IsNot Nothing Then
            SyaratKartu.EditValue = ViewTipeMembership.GetFocusedRowCellValue("Syarat")
            HargaKartu.EditValue = ViewTipeMembership.GetFocusedRowCellValue("Harga")

            If TotalBayar.EditValue >= SyaratKartu.EditValue Then
                ButUpgrade.Enabled = True
            Else
                ButUpgrade.Enabled = False
            End If
        Else
            SyaratKartu.EditValue = 0
            HargaKartu.EditValue = 0

            ButUpgrade.Enabled = False
        End If
        SyaratKartu.EditValue = ViewTipeMembership.GetFocusedRowCellValue("Syarat")
        HargaKartu.EditValue = ViewTipeMembership.GetFocusedRowCellValue("Harga")
    End Sub

    Private Sub ButUpgrade_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButUpgrade.Click
        Try
            If DaftarTipeMembership.EditValue Is Nothing Then Return
            If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then Return
            UpdateMasterCustomer()

            MsgInfo("Data berhasil disimpan, tekan OK untuk melanjutkan.")
            BrowserMasterCustomer.isChange = True

        Catch ex As Exception
            MsgErrorDev()
        End Try
        LoadSprite.CloseLoading()
        Close()
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Sub Get_KebutuhanDaftar()
        SQLquery = String.Format("SELECT id_tipemembership ID, nama_tipemembership Nama, level_tipemembership Level, syarat_krt Syarat, harga_krt Harga FROM mbsm_tipemembership a WHERE a.Inactive=0 AND id_tipemembership<>{0} AND level_tipemembership>={1};",
                                 xSet.Tables("DataMasterCustomer").Rows(0).Item("id_tipemembership"), xSet.Tables("DataMasterCustomer").Rows(0).Item("level_tipemembership"))
        ExDb.ExecQuery(1, SQLquery, xSet, "DaftarTipeMembership")
        DaftarTipeMembership.Properties.DataSource = xSet.Tables("DaftarTipeMembership").DefaultView

        DaftarTipeMembership.Properties.NullText = ""
        DaftarTipeMembership.Properties.ValueMember = "ID"
        DaftarTipeMembership.Properties.DisplayMember = "Nama"
        DaftarTipeMembership.Properties.ShowClearButton = False
        DaftarTipeMembership.Properties.PopulateViewColumns()

        ViewTipeMembership.Columns("ID").Visible = False
        ViewTipeMembership.Columns("Syarat").DisplayFormat.FormatType = FormatType.Numeric
        ViewTipeMembership.Columns("Syarat").DisplayFormat.FormatString = "Rp{0:n0}"
        ViewTipeMembership.Columns("Harga").DisplayFormat.FormatType = FormatType.Numeric
        ViewTipeMembership.Columns("Harga").DisplayFormat.FormatString = "Rp{0:n0}"

        For Each coll As DataColumn In xSet.Tables("DaftarTipeMembership").Columns
            ViewTipeMembership.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next
    End Sub

    Private Sub UpdateMasterCustomer()
        'sumber : 1-upgrade | 2-buy
        LoadSprite.ShowLoading("Baking cookies", "Register Member", Me)
        SQLquery = String.Format("CALL UpgradeMember({0}, {1}, {2}, {3}, {4});",
                                 CheckMember.CurrentMember, DaftarTipeMembership.EditValue, id_cabang, HargaKartu.EditValue * -1, staff_id)
        ExDb.ExecData(1, SQLquery)
        LoadSprite.CloseLoading()
    End Sub
End Class