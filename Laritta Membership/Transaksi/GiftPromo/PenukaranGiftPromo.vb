﻿Imports DevExpress.Utils

Public Class PenukaranGiftPromo
    Dim BenefitRow, TransRow As DataRow
    Dim TotalChoosenValue As Integer

    Private Sub UseGiftPromo_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("GetAllItemLists")
        xSet.Tables.Remove("GetAllCategoriLists")
        xSet.Tables.Remove("ThisMemberGifts")
        xSet.Tables.Remove("ThisMemberPromos")

        xSet.Relations.Remove("MasterDetailOrder")
        xSet.Tables("AvailableTransOrder_D").Constraints.Clear()
        xSet.Tables.Remove("AvailableTransOrder_D")
        xSet.Tables.Remove("AvailableTransOrder_M")

        xSet.Relations.Remove("MasterDetailOutlet")
        xSet.Tables("AvailableTransOutlet_D").Constraints.Clear()
        xSet.Tables.Remove("AvailableTransOutlet_D")
        xSet.Tables.Remove("AvailableTransOutlet_M")

        xSet.Tables.Remove("HasilInsertGiftPromo")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub UseGiftPromo_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()
        GetBenefitLists()
        GetCategoryAndItemLists()
    End Sub

    Private Sub UseGiftPromo_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown

        LoadSprite.CloseLoading()
    End Sub

    Private Sub ViewPromoAvailable_DoubleClick(ByVal sender As Object, ByVal e As EventArgs) Handles ViewThisMemberPromo.DoubleClick
        If ViewThisMemberPromo.RowCount < 1 Or ViewThisMemberPromo.FocusedRowHandle < 0 Then Return

        Try
            BenefitRow = ViewThisMemberPromo.GetFocusedDataRow
            ChooseBenefit("Promo")
        Catch ex As Exception
            MsgErrorDev()
        End Try
    End Sub

    Private Sub ViewGiftAvailable_DoubleClick(ByVal sender As Object, ByVal e As EventArgs) Handles ViewThisMemberGift.DoubleClick
        If ViewThisMemberGift.RowCount < 1 Or ViewThisMemberGift.FocusedRowHandle < 0 Then Return

        Try
            BenefitRow = ViewThisMemberGift.GetFocusedDataRow
            ChooseBenefit("Gift")
        Catch ex As Exception
            MsgErrorDev()
        End Try
    End Sub

    Private Sub ButNext_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButNext.Click
        If TabControlBenefit.SelectedTabPageIndex = 0 Then
            '---Promo
            If ViewThisMemberPromo.RowCount < 1 Or ViewThisMemberPromo.FocusedRowHandle < 0 Then Return

            Try
                BenefitRow = ViewThisMemberPromo.GetFocusedDataRow
                ChooseBenefit("Promo")
            Catch ex As Exception
                MsgErrorDev()
            End Try
        Else
            '---Gift
            If ViewThisMemberGift.RowCount < 1 Or ViewThisMemberGift.FocusedRowHandle < 0 Then Return

            Try
                BenefitRow = ViewThisMemberGift.GetFocusedDataRow
                ChooseBenefit("Gift")
            Catch ex As Exception
                MsgErrorDev()
            End Try
        End If
    End Sub

    Private Sub ButBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBack.Click
        NowWhat("Back")
    End Sub

    Private Sub ButOK_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButOK.Click
        'save di 2 tempat berbeda (promo & gift)
        Try
            If TotalChoosenValue < 1 Or RewardTimes.EditValue < 1 Then Return
            If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then Return

            LoadSprite.ShowLoading("Menyimpan transaksi", "Loading", Me)
            If TabControlBenefit.SelectedTabPageIndex = 0 Then
                'Promo
                SQLquery = String.Format("CALL InsertExchangePromo_M ({0}, {1}, {2}, {3}, {4}, '{5}', {6}, {7}, {8}, {9}, '",
                                     id_cabang,
                                     xSet.Tables("ThisMemberData").Rows(0).Item("id_tipemembership"),
                                     CheckMember.CurrentMember,
                                     BenefitRow.Item("ID"),
                                     IIf(BenefitRow.Item("reward_jenis") = "disc", 1, RewardTimes.EditValue),
                                     Format(use_expired.EditValue, "yyyy-MM-dd"),
                                     BenefitRow.Item("use_at_outlet"),
                                     BenefitRow.Item("use_at_order"),
                                     IIf(BenefitRow.Item("reward_jenis") = "disc", RewardTimes.EditValue, 1),
                                     staff_id)
            Else
                'Gift
                SQLquery = String.Format("CALL InsertExchangeGift_M ({0}, {1}, {2}, {3}, '{4}', {5}, {6}, {7}, '",
                                     id_cabang,
                                     xSet.Tables("ThisMemberData").Rows(0).Item("id_tipemembership"),
                                     CheckMember.CurrentMember,
                                     BenefitRow.Item("ID"),
                                     Format(use_expired.EditValue, "yyyy-MM-dd"),
                                     BenefitRow.Item("use_at_outlet"),
                                     BenefitRow.Item("use_at_order"),
                                     staff_id)
            End If

            'insert transaksi dari outlet yang di centang
            If PageDaftarTransaksiOutlet.PageEnabled = True Then
                For Each drow As DataRow In xSet.Tables("AvailableTransOutlet_M").Select("ChoosenOnes=1")
                    SQLquery += String.Format("(xyz, 0, {0}, {1}), ", drow.Item("ID"), id_cabang)
                Next
            End If
            'insert transaksi dari order yang di centang
            If PageDaftarTransaksiOrder.PageEnabled = True Then
                For Each drow As DataRow In xSet.Tables("AvailableTransOrder_M").Select("ChoosenOnes=1")
                    SQLquery += String.Format("(xyz, 1, {0}, {1}), ", drow.Item("ID"), id_cabang)
                Next
            End If

            SQLquery = IIf(DenganSyarat.Checked, Mid(SQLquery, 1, SQLquery.Length - 2) & "'); ", SQLquery & "');")
            ExDb.ExecQuery(1, SQLquery, xSet, "HasilInsertGiftPromo")

            MsgInfo("Data berhasil disimpan, tekan OK untuk melanjutkan proses print.")
            LoadSprite.CloseLoading()

            MsgInfo(String.Format("Print nota sebanyak : {0} kali", xSet.Tables("HasilInsertGiftPromo").Rows(0).Item("jml_penggunaan_max")))
            Using NotaPenukaranPromoDanGift As New PrintOut_PenukaranPromoDanGift
                'If xSet.Tables("HasilInsertGiftPromo").Rows(0).Item("jml_penggunaan_max") > 1 Then
                '    For i As Integer = 1 To xSet.Tables("HasilInsertGiftPromo").Rows(0).Item("jml_penggunaan_max")
                '        MsgInfo("Tekan OK untuk melanjutkan proses print (" & i & ")")
                '        NotaPenukaranPromoDanGift.Print()
                '    Next
                'Else
                '    NotaPenukaranPromoDanGift.Print()
                'End If
                Using tool As DevExpress.XtraReports.UI.ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(NotaPenukaranPromoDanGift)
                    tool.ShowRibbonPreviewDialog()
                End Using
            End Using
        Catch ex As Exception
            MsgErrorDev()
        End Try
        Close()
    End Sub

    Private Sub CloseThisForm(ByVal sender As Object, ByVal e As EventArgs) Handles ButClose1.Click, ButClose2.Click
        Close()
    End Sub

    Private Sub NowWhat(ByVal Gonna As String)
        If Gonna = "Next" Then
            Step1.Visible = False
            Step1.Enabled = False

            Step2.Visible = True
            Step2.Enabled = True
        ElseIf Gonna = "Back" Then
            '----URUTANNYA PENTING! Clear Detail dulu baru Master karena ada nya Relation between datatables
            If Not xSet.Tables("AvailableTransOutlet_D") Is Nothing Then xSet.Tables("AvailableTransOutlet_D").Clear()
            If Not xSet.Tables("AvailableTransOutlet_M") Is Nothing Then xSet.Tables("AvailableTransOutlet_M").Clear()
            If Not xSet.Tables("AvailableTransOrder_D") Is Nothing Then xSet.Tables("AvailableTransOrder_D").Clear()
            If Not xSet.Tables("AvailableTransOrder_M") Is Nothing Then xSet.Tables("AvailableTransOrder_M").Clear()

            TotalChoosenValue = 0

            Step2.Visible = False
            Step2.Enabled = False

            Step1.Visible = True
            Step1.Enabled = True
        End If
    End Sub

#Region "Data master barang, kategori dan daftar benefit dari si member"
    Private Sub GetCategoryAndItemLists()
        SQLquery = "SELECT idBrg, namaBrg, price, Inactive FROM mbarang;"
        ExDb.ExecQuery(1, SQLquery, xSet, "GetAllItemLists")
        SQLquery = "SELECT idKategori2, namaKategori2, Inactive FROM mkategoribrg2;"
        ExDb.ExecQuery(1, SQLquery, xSet, "GetAllCategoriLists")
    End Sub

    Private Sub GetBenefitLists()
        Try
            '-----------Gift
            SQLquery = String.Format("SELECT a.id_gift ID, b.nama_gift Nama, b.gift_explanation Keterangan, a.get_date start_date, a.expired_date end_date, a.availability-a.use_count Repetisi, " & _
                        "b.syarat_none, b.syarat_tipe, c.tipe_syaratunlock, b.syarat_operator, b.syarat_tipevalue, IFNULL(c.syarat_tabelwhere,'') syarat_tabelwhere, b.syarat_wherevalue, " & _
                        "b.reward_jenis, b.reward_gift Reward, b.reward_value, b.reward_max, b.exc_at_outlet, b.exc_at_order, 0 AS ismultiplied, " & _
                        "b.use_at_outlet, b.use_at_order, b.use_expired " & _
                        "FROM mbsm_membergifts a " & _
                        "INNER JOIN mbsm_gift b ON a.id_gift=b.id_gift " & _
                        "LEFT JOIN mbsm_syaratunlock c ON b.syarat_tipe=c.id_syaratunlock " & _
                        "WHERE a.use_count<=a.availability AND a.expired_date>=CURDATE() AND a.id_customer={0} AND a.Inactive=0 HAVING Repetisi>0;", CheckMember.CurrentMember)
            ExDb.ExecQuery(1, SQLquery, xSet, "ThisMemberGifts")

            ThisMemberGift.DataSource = xSet.Tables("ThisMemberGifts").DefaultView

            ViewThisMemberGift.Columns("ID").Visible = False
            ViewThisMemberGift.Columns("start_date").Visible = False
            ViewThisMemberGift.Columns("reward_jenis").Visible = False
            ViewThisMemberGift.Columns("reward_value").Visible = False
            ViewThisMemberGift.Columns("reward_max").Visible = False

            ViewThisMemberGift.Columns("syarat_none").Visible = False
            ViewThisMemberGift.Columns("syarat_tipe").Visible = False
            ViewThisMemberGift.Columns("tipe_syaratunlock").Visible = False
            ViewThisMemberGift.Columns("syarat_operator").Visible = False
            ViewThisMemberGift.Columns("syarat_tipevalue").Visible = False
            ViewThisMemberGift.Columns("syarat_tabelwhere").Visible = False
            ViewThisMemberGift.Columns("syarat_wherevalue").Visible = False

            ViewThisMemberGift.Columns("exc_at_outlet").Visible = False
            ViewThisMemberGift.Columns("exc_at_order").Visible = False
            ViewThisMemberGift.Columns("ismultiplied").Visible = False
            ViewThisMemberGift.Columns("use_at_outlet").Visible = False
            ViewThisMemberGift.Columns("use_at_order").Visible = False
            ViewThisMemberGift.Columns("use_expired").Visible = False

            ViewThisMemberGift.Columns("end_date").Caption = "Expired"
            ViewThisMemberGift.Columns("end_date").DisplayFormat.FormatType = FormatType.DateTime
            ViewThisMemberGift.Columns("end_date").DisplayFormat.FormatString = "dd MMM yyyy"

            ViewThisMemberGift.Columns("Keterangan").ColumnEdit = RepoMemo
            ViewThisMemberGift.Columns("Reward").ColumnEdit = RepoMemo
            ViewThisMemberGift.Columns("Nama").Width = ThisMemberGift.Width * 0.2
            ViewThisMemberGift.Columns("Keterangan").Width = ThisMemberGift.Width * 0.4
            ViewThisMemberGift.Columns("end_date").Width = ThisMemberGift.Width * 0.125
            ViewThisMemberGift.Columns("Repetisi").Width = ThisMemberGift.Width * 0.125

            '-----------Promo
            SQLquery = String.Format("SELECT a.id_promo ID, a.nama_promo Nama, a.promo_explanation Keterangan, a.syaratperiode_start start_date, a.syaratperiode_end end_date, a.repetisi_promo, b.nama_syaratrepetisi Repetisi, DATE_ADD(DATE(MAX(c.createdate)),INTERVAL b.day_count+1 DAY) free_date, " & _
                        "a.syarat_none, a.syarat_tipe, d.tipe_syaratunlock, a.syarat_operator, a.syarat_tipevalue, IFNULL(d.syarat_tabelwhere,'') syarat_tabelwhere, a.syarat_wherevalue, " & _
                        "a.reward_jenis, a.reward_promo Reward, a.reward_value, a.reward_max, a.exc_at_outlet, a.exc_at_order, a.ismultiplied, a.use_at_outlet, a.use_at_order, a.use_expired " & _
                        "FROM mbsm_promo a " & _
                        "INNER JOIN mbsm_syaratrepetisi b ON a.repetisi_promo=b.id_syaratrepetisi " & _
                        "LEFT JOIN mbst_exchangepromo c ON a.id_promo=c.id_promo AND c.id_customer={0} " & _
                        "LEFT JOIN mbsm_syaratunlock d ON a.syarat_tipe=d.id_syaratunlock " & _
                        "WHERE (a.Inactive = 0 And a.syaratperiode_start <= CURDATE() And a.syaratperiode_end >= CURDATE() And a.id_tipemembership = {1}) " & _
                        "GROUP BY a.id_promo; ", CheckMember.CurrentMember, xSet.Tables("ThisMemberData").Rows(0).Item("id_tipemembership"))
            ExDb.ExecQuery(1, SQLquery, xSet, "ThisMemberPromos")

            ThisMemberPromo.DataSource = xSet.Tables("ThisMemberPromos").DefaultView

            ViewThisMemberPromo.Columns("ID").Visible = False
            ViewThisMemberPromo.Columns("start_date").Visible = False
            ViewThisMemberPromo.Columns("repetisi_promo").Visible = False
            ViewThisMemberPromo.Columns("free_date").Visible = False
            ViewThisMemberPromo.Columns("reward_jenis").Visible = False
            ViewThisMemberPromo.Columns("reward_value").Visible = False
            ViewThisMemberPromo.Columns("reward_max").Visible = False

            ViewThisMemberPromo.Columns("syarat_none").Visible = False
            ViewThisMemberPromo.Columns("syarat_tipe").Visible = False
            ViewThisMemberPromo.Columns("tipe_syaratunlock").Visible = False
            ViewThisMemberPromo.Columns("syarat_operator").Visible = False
            ViewThisMemberPromo.Columns("syarat_tipevalue").Visible = False
            ViewThisMemberPromo.Columns("syarat_tabelwhere").Visible = False
            ViewThisMemberPromo.Columns("syarat_wherevalue").Visible = False

            ViewThisMemberPromo.Columns("exc_at_outlet").Visible = False
            ViewThisMemberPromo.Columns("exc_at_order").Visible = False
            ViewThisMemberPromo.Columns("ismultiplied").Visible = False
            ViewThisMemberPromo.Columns("use_at_outlet").Visible = False
            ViewThisMemberPromo.Columns("use_at_order").Visible = False
            ViewThisMemberPromo.Columns("use_expired").Visible = False

            ViewThisMemberPromo.Columns("end_date").Caption = "Expired"
            ViewThisMemberPromo.Columns("end_date").DisplayFormat.FormatType = FormatType.DateTime
            ViewThisMemberPromo.Columns("end_date").DisplayFormat.FormatString = "dd MMM yyyy"

            ViewThisMemberPromo.Columns("Keterangan").ColumnEdit = RepoMemo
            ViewThisMemberPromo.Columns("Reward").ColumnEdit = RepoMemo
            ViewThisMemberPromo.Columns("Nama").Width = ThisMemberPromo.Width * 0.2
            ViewThisMemberPromo.Columns("Keterangan").Width = ThisMemberPromo.Width * 0.4
            ViewThisMemberPromo.Columns("end_date").Width = ThisMemberPromo.Width * 0.125
            ViewThisMemberPromo.Columns("Repetisi").Width = ThisMemberPromo.Width * 0.125
        Catch ex As Exception
            MsgErrorDev()
        End Try
    End Sub
#End Region

#Region "Check benefit yang di pilih oleh user"
    Private Function AreYouAllowed(ByVal idRepetisiBenefit As Integer, ByVal free_date As Date) As Boolean
        Select Case idRepetisiBenefit
            Case 0 'harian
                If free_date = Today.Date Then Return True
            Case 1 'senin
                If Today.Date.DayOfWeek = DayOfWeek.Monday And Today.Date >= free_date Then Return True
            Case 2 'selasa
                If Today.Date.DayOfWeek = DayOfWeek.Tuesday And Today.Date >= free_date Then Return True
            Case 3 'rabu
                If Today.Date.DayOfWeek = DayOfWeek.Wednesday And Today.Date >= free_date Then Return True
            Case 4 'kamis
                If Today.Date.DayOfWeek = DayOfWeek.Thursday And Today.Date >= free_date Then Return True
            Case 5 'jumat
                If Today.Date.DayOfWeek = DayOfWeek.Friday And Today.Date >= free_date Then Return True
            Case 6 'sabtu
                If Today.Date.DayOfWeek = DayOfWeek.Saturday And Today.Date >= free_date Then Return True
            Case 7 'minggu
                If Today.Date.DayOfWeek = DayOfWeek.Sunday And Today.Date >= free_date Then Return True
            Case 8 'mingguan
                If My.Application.Culture.Calendar.GetWeekOfYear(free_date, Globalization.CalendarWeekRule.FirstDay, DayOfWeek.Monday) = My.Application.Culture.Calendar.GetWeekOfYear(Today.Date, Globalization.CalendarWeekRule.FirstDay, DayOfWeek.Monday) Then Return True
            Case 9 'bulanan
                If free_date = Today.Date Then Return True
            Case 10 'tahunan
                If free_date.Year = Today.Year Then Return True
            Case 11 'transaksi
                Return True
        End Select

        Return False
    End Function

    Private Sub ChooseBenefit(ByVal ItIs As String)
        If ItIs = "Promo" Then
            Dim tgls As Date = Today.Date
            If Not IsDBNull(BenefitRow.Item("free_date")) Or (BenefitRow.Item("repetisi_promo") >= 1 And BenefitRow.Item("repetisi_promo") <= 7) Then
                If Not IsDBNull(BenefitRow.Item("free_date")) Then tgls = BenefitRow.Item("free_date")

                If Not AreYouAllowed(BenefitRow.Item("repetisi_promo"), tgls) Then
                    MsgInfo("Promo masih belum dapat digunakan.")
                    Return
                End If
            End If
        End If

            Dim IsPernah As Boolean = True

            '--------------------------------------------------------------------------------Berlaku di Outlet
            If BenefitRow.Item("exc_at_outlet") = True Then
                PageDaftarTransaksiOutlet.PageEnabled = True

                '-----Get Master Transaksi Outlet
                If Not xSet.Tables("AvailableTransOutlet_M") Is Nothing Then xSet.Tables("AvailableTransOutlet_M").Clear() Else IsPernah = False
                SQLquery = String.Format("SELECT a.isVoid ChoosenOnes, a.id_pos ID, a.idCabang, a.tgl_pos Tanggal, a.nomor_nota NoNota, a.total_price TotalBeli FROM mbst_posm a " & _
                            "WHERE a.isVoid=0 AND a.{0}=0 AND a.idCust={1} AND (DATE(a.tgl_pos) BETWEEN '{2}' AND '{3}');",
                            IIf(ItIs = "Promo", "use_promo", "use_gift"),
                            CheckMember.CurrentMember,
                            Format(BenefitRow.Item("start_date"), "yyyy-MM-dd"),
                            Format(BenefitRow.Item("end_date"), "yyyy-MM-dd"))
                ExDb.ExecQuery(1, SQLquery, xSet, "AvailableTransOutlet_M")

                '-----Get Detail Transaksi Outlet
                If Not xSet.Tables("AvailableTransOutlet_D") Is Nothing Then xSet.Tables("AvailableTransOutlet_D").Clear()
                SQLquery = String.Format("SELECT a.id_pos ID, a.idCabang, b.idBrg, c.namaBrg Nama, c.idKategori2, d.namaKategori2 Kategori, b.quantity Jml, b.price Hrg, b.totalPrice Total " & _
                            "FROM mbst_posm a INNER JOIN mbst_posd b ON a.id_pos=b.id_pos INNER JOIN mbarang c ON b.idBrg=c.idBrg INNER JOIN mkategoribrg2 d ON c.idKategori2=d.idKategori2 " & _
                            "WHERE a.isVoid=0 AND a.{0}=0 AND a.idCust={1} AND (DATE(a.tgl_pos) BETWEEN '{2}' AND '{3}');",
                            IIf(ItIs = "Promo", "use_promo", "use_gift"),
                            CheckMember.CurrentMember,
                            Format(BenefitRow.Item("start_date"), "yyyy-MM-dd"),
                            Format(BenefitRow.Item("end_date"), "yyyy-MM-dd"))
                ExDb.ExecQuery(1, SQLquery, xSet, "AvailableTransOutlet_D")

                If Not IsPernah Then
                    Dim keyColumn As DataColumn = xSet.Tables("AvailableTransOutlet_M").Columns("ID")
                    Dim foreignKeyColumn As DataColumn = xSet.Tables("AvailableTransOutlet_D").Columns("ID")
                    xSet.Relations.Add("MasterDetailOutlet", keyColumn, foreignKeyColumn)
                    ThisMemberOutlet.DataSource = xSet.Tables("AvailableTransOutlet_M")
                    ThisMemberOutlet.ForceInitialize()
                    ThisMemberOutlet.LevelTree.Nodes.Add("MasterDetailOutlet", ViewDetailTransOutlet)
                    ViewDetailTransOutlet.PopulateColumns(xSet.Tables("AvailableTransOutlet_D"))
                    ViewDetailTransOutlet.ViewCaption = "Daftar isi"

                    ViewMasterTransOutlet.Columns("ID").Visible = False
                    ViewMasterTransOutlet.Columns("idCabang").Visible = False
                    ViewDetailTransOutlet.Columns("ID").Visible = False
                    ViewDetailTransOutlet.Columns("idCabang").Visible = False
                    ViewDetailTransOutlet.Columns("idBrg").Visible = False
                    ViewDetailTransOutlet.Columns("idKategori2").Visible = False

                    ThisMemberOutlet.RepositoryItems.Add(OutletChecker)

                    ViewMasterTransOutlet.Columns("ChoosenOnes").ColumnEdit = OutletChecker
                    ViewMasterTransOutlet.Columns("ChoosenOnes").Caption = "..."
                    ViewMasterTransOutlet.Columns("ID").OptionsColumn.AllowEdit = False
                    ViewMasterTransOutlet.Columns("idCabang").OptionsColumn.AllowEdit = False
                    ViewMasterTransOutlet.Columns("NoNota").OptionsColumn.AllowEdit = False
                    ViewMasterTransOutlet.Columns("Tanggal").OptionsColumn.AllowEdit = False
                    ViewMasterTransOutlet.Columns("TotalBeli").OptionsColumn.AllowEdit = False

                    ViewMasterTransOutlet.Columns("Tanggal").DisplayFormat.FormatType = FormatType.DateTime
                    ViewMasterTransOutlet.Columns("Tanggal").DisplayFormat.FormatString = "dd MMM yyyy"
                    ViewMasterTransOutlet.Columns("TotalBeli").DisplayFormat.FormatType = FormatType.Numeric
                    ViewMasterTransOutlet.Columns("TotalBeli").DisplayFormat.FormatString = "Rp{0:n0}"
                    ViewDetailTransOutlet.Columns("Hrg").DisplayFormat.FormatType = FormatType.Numeric
                    ViewDetailTransOutlet.Columns("Hrg").DisplayFormat.FormatString = "Rp{0:n0}"
                    ViewDetailTransOutlet.Columns("Total").DisplayFormat.FormatType = FormatType.Numeric
                    ViewDetailTransOutlet.Columns("Total").DisplayFormat.FormatString = "Rp{0:n0}"

                    ViewMasterTransOutlet.Columns("ChoosenOnes").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
                    ViewMasterTransOutlet.Columns("ChoosenOnes").SummaryItem.DisplayFormat = "Jumlah:{0:n0}"
                    ViewMasterTransOutlet.Columns("TotalBeli").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
                    ViewMasterTransOutlet.Columns("TotalBeli").SummaryItem.DisplayFormat = "Subtotal:{0:n0}"

                    ViewMasterTransOutlet.Columns("ChoosenOnes").Width = ThisMemberOutlet.Width * 0.2
                    ViewMasterTransOutlet.Columns("Tanggal").Width = ThisMemberOutlet.Width * 0.225
                    ViewMasterTransOutlet.Columns("NoNota").Width = ThisMemberOutlet.Width * 0.225
                    ViewMasterTransOutlet.Columns("TotalBeli").Width = ThisMemberOutlet.Width * 0.35

                    ViewDetailTransOutlet.Columns("Nama").Width = ThisMemberOutlet.Width * 0.3
                    ViewDetailTransOutlet.Columns("Kategori").Width = ThisMemberOutlet.Width * 0.2
                    ViewDetailTransOutlet.Columns("Jml").Width = ThisMemberOutlet.Width * 0.1
                    ViewDetailTransOutlet.Columns("Hrg").Width = ThisMemberOutlet.Width * 0.2
                    ViewDetailTransOutlet.Columns("Total").Width = ThisMemberOutlet.Width * 0.2

                    For Each coll As DataColumn In xSet.Tables("AvailableTransOutlet_M").Columns
                        ViewMasterTransOutlet.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
                    Next
                    For Each coll As DataColumn In xSet.Tables("AvailableTransOutlet_D").Columns
                        ViewDetailTransOutlet.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
                    Next
                End If
            Else
                PageDaftarTransaksiOutlet.PageEnabled = False
            End If

            '--------------------------------------------------------------------------------Berlaku di Order
            IsPernah = True
            If BenefitRow.Item("exc_at_order") = True Then
                PageDaftarTransaksiOrder.PageEnabled = True

                '-----Get Master Transaksi Order
                If Not xSet.Tables("AvailableTransOrder_M") Is Nothing Then xSet.Tables("AvailableTransOrder_M").Clear() Else IsPernah = False
                SQLquery = String.Format("SELECT a.Void ChoosenOnes, a.idPesanan ID, a.idCabang, a.tglPesan Tanggal, a.noPesan NoNota, b.NilaiTotalPesanan TotalBeli " & _
                            "FROM mtpesanan a INNER JOIN set_pesanan b ON a.idPesanan=b.idPesanan " & _
                            "WHERE a.statusLunas=1 AND a.Void=0 AND a.{0}=0 AND a.idCust={1} AND (DATE(a.tglPesan) BETWEEN '{2}' AND '{3}');",
                            IIf(ItIs = "Promo", "use_promo", "use_gift"),
                            CheckMember.CurrentMember,
                            Format(BenefitRow.Item("start_date"), "yyyy-MM-dd"),
                            Format(BenefitRow.Item("end_date"), "yyyy-MM-dd"))
                ExDb.ExecQuery(1, SQLquery, xSet, "AvailableTransOrder_M")

                '-----Get Detail Transaksi Order
                If Not xSet.Tables("AvailableTransOrder_D") Is Nothing Then xSet.Tables("AvailableTransOrder_D").Clear()
                SQLquery = String.Format("CALL GetIsiPesanan({0}, '{1}', '{2}', '{3}');",
                            CheckMember.CurrentMember,
                            ItIs,
                            Format(BenefitRow.Item("start_date"), "yyyy-MM-dd"),
                            Format(BenefitRow.Item("end_date"), "yyyy-MM-dd"))
                ExDb.ExecQuery(1, SQLquery, xSet, "AvailableTransOrder_D")

                If Not IsPernah Then
                    Dim keyColumn As DataColumn = xSet.Tables("AvailableTransOrder_M").Columns("ID")
                    Dim foreignKeyColumn As DataColumn = xSet.Tables("AvailableTransOrder_D").Columns("ID")
                    xSet.Relations.Add("MasterDetailOrder", keyColumn, foreignKeyColumn)
                    ThisMemberOrder.DataSource = xSet.Tables("AvailableTransOrder_M")
                    ThisMemberOrder.ForceInitialize()
                    ThisMemberOrder.LevelTree.Nodes.Add("MasterDetailOrder", ViewDetailTransOrder)
                    ViewDetailTransOrder.PopulateColumns(xSet.Tables("AvailableTransOrder_D"))
                    ViewDetailTransOrder.ViewCaption = "Daftar isi"

                    ViewMasterTransOrder.Columns("ID").Visible = False
                    ViewMasterTransOrder.Columns("idCabang").Visible = False
                    ViewDetailTransOrder.Columns("ID").Visible = False
                    ViewDetailTransOrder.Columns("idCabang").Visible = False
                    ViewDetailTransOrder.Columns("idBrg").Visible = False
                    ViewDetailTransOrder.Columns("idKategori2").Visible = False

                    ThisMemberOrder.RepositoryItems.Add(OrderChecker)

                    ViewMasterTransOrder.Columns("ChoosenOnes").ColumnEdit = OrderChecker
                    ViewMasterTransOrder.Columns("ChoosenOnes").Caption = "..."
                    ViewMasterTransOrder.Columns("ID").OptionsColumn.AllowEdit = False
                    ViewMasterTransOrder.Columns("idCabang").OptionsColumn.AllowEdit = False
                    ViewMasterTransOrder.Columns("NoNota").OptionsColumn.AllowEdit = False
                    ViewMasterTransOrder.Columns("Tanggal").OptionsColumn.AllowEdit = False
                    ViewMasterTransOrder.Columns("TotalBeli").OptionsColumn.AllowEdit = False

                    ViewMasterTransOrder.Columns("Tanggal").DisplayFormat.FormatType = FormatType.DateTime
                    ViewMasterTransOrder.Columns("Tanggal").DisplayFormat.FormatString = "dd MMM yyyy"
                    ViewMasterTransOrder.Columns("TotalBeli").DisplayFormat.FormatType = FormatType.Numeric
                    ViewMasterTransOrder.Columns("TotalBeli").DisplayFormat.FormatString = "Rp{0:n0}"
                    ViewDetailTransOrder.Columns("Hrg").DisplayFormat.FormatType = FormatType.Numeric
                    ViewDetailTransOrder.Columns("Hrg").DisplayFormat.FormatString = "Rp{0:n0}"
                    ViewDetailTransOrder.Columns("Total").DisplayFormat.FormatType = FormatType.Numeric
                    ViewDetailTransOrder.Columns("Total").DisplayFormat.FormatString = "Rp{0:n0}"

                    ViewMasterTransOrder.Columns("ChoosenOnes").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
                    ViewMasterTransOrder.Columns("ChoosenOnes").SummaryItem.DisplayFormat = "Jumlah:{0:n0}"
                    ViewMasterTransOrder.Columns("TotalBeli").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
                    ViewMasterTransOrder.Columns("TotalBeli").SummaryItem.DisplayFormat = "Subtotal:{0:n0}"

                    ViewMasterTransOrder.Columns("ChoosenOnes").Width = ThisMemberOrder.Width * 0.2
                    ViewMasterTransOrder.Columns("Tanggal").Width = ThisMemberOrder.Width * 0.225
                    ViewMasterTransOrder.Columns("NoNota").Width = ThisMemberOrder.Width * 0.225
                    ViewMasterTransOrder.Columns("TotalBeli").Width = ThisMemberOrder.Width * 0.35

                    ViewDetailTransOrder.Columns("Nama").Width = ThisMemberOrder.Width * 0.3
                    ViewDetailTransOrder.Columns("Kategori").Width = ThisMemberOrder.Width * 0.2
                    ViewDetailTransOrder.Columns("Jml").Width = ThisMemberOrder.Width * 0.1
                    ViewDetailTransOrder.Columns("Hrg").Width = ThisMemberOrder.Width * 0.2
                    ViewDetailTransOrder.Columns("Total").Width = ThisMemberOrder.Width * 0.2

                    For Each coll As DataColumn In xSet.Tables("AvailableTransOrder_M").Columns
                        ViewMasterTransOrder.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
                    Next
                    For Each coll As DataColumn In xSet.Tables("AvailableTransOrder_D").Columns
                        ViewDetailTransOrder.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
                    Next
                End If
            Else
                PageDaftarTransaksiOrder.PageEnabled = False
            End If

            '-----tampilkan syarat dan reward dari benefit yang di pilih
            ShowWhatPicked()
            '-----siap semua, open next page
            NowWhat("Next")
    End Sub

    Private Sub ShowWhatPicked()
        DenganSyarat.Checked = IIf(BenefitRow.Item("syarat_none"), False, True)

        If Not DenganSyarat.Checked Then
            TransactionPages.Enabled = False

            SyaratUnlockName.Text = ""
            SyaratUnlockOperator.Text = ""
            SyaratUnlockValues.EditValue = ""

            TotalChoosenValue = 1
            RewardTimes.EditValue = 1
            TransGrandTotal.EditValue = 0
        Else
            TransactionPages.Enabled = True

            If BenefitRow.Item("syarat_tabelwhere") = "" Then
                SyaratUnlockName.Text = BenefitRow.Item("tipe_syaratunlock")
                SyaratUnlockOperator.Text = BenefitRow.Item("syarat_operator")
                SyaratUnlockValues.Text = BenefitRow.Item("syarat_tipevalue")
            ElseIf BenefitRow.Item("syarat_tabelwhere") = "mkategoribrg2" Then
                SyaratUnlockName.Text = Replace(BenefitRow.Item("tipe_syaratunlock"), "tertentu", xSet.Tables("GetAllCategoriLists").Select("idKategori2=" & BenefitRow.Item("syarat_wherevalue"))(0).Item("namaKategori2"))
                SyaratUnlockOperator.Text = BenefitRow.Item("syarat_operator")
                SyaratUnlockValues.Text = BenefitRow.Item("syarat_tipevalue")
            ElseIf BenefitRow.Item("syarat_tabelwhere") = "mbarang" Then
                SyaratUnlockName.Text = Replace(BenefitRow.Item("tipe_syaratunlock"), "tertentu", xSet.Tables("GetAllItemLists").Select("idBrg=" & BenefitRow.Item("syarat_wherevalue"))(0).Item("namaBrg"))
                SyaratUnlockOperator.Text = BenefitRow.Item("syarat_operator")
                SyaratUnlockValues.Text = BenefitRow.Item("syarat_tipevalue")
            End If

            TotalChoosenValue = 0
            RewardTimes.EditValue = 0
            TransGrandTotal.EditValue = 0
        End If

        RewardName.Text = BenefitRow.Item("Reward")
        CekMultiplied.Checked = BenefitRow.Item("ismultiplied")

        use_expired.EditValue = DateAdd(DateInterval.Day, BenefitRow.Item("use_expired"), Today.Date)
        use_outlet.Checked = BenefitRow.Item("use_at_outlet")
        use_order.Checked = BenefitRow.Item("use_at_order")
    End Sub
#End Region

#Region "Check trans yang di pilih user dengan syarat"
    Private Sub OrderChecker_EditValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ChangingEventArgs) Handles OrderChecker.EditValueChanging
        TransRow = ViewMasterTransOrder.GetFocusedDataRow
        'ubah jadi true/false?
        'call prosedur untuk tentukan syarat unlock yang aktif apa?
        If e.NewValue = True And e.OldValue = False Then
            CekSesuaiSyarat("Order", "Plus")
        ElseIf e.NewValue = False And e.OldValue = True Then
            CekSesuaiSyarat("Order", "Minus")
        End If
    End Sub

    Private Sub OutletChecker_EditValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ChangingEventArgs) Handles OutletChecker.EditValueChanging
        TransRow = ViewMasterTransOutlet.GetFocusedDataRow
        'ubah jadi true/false?
        'call prosedur untuk tentukan syarat unlock yang aktif apa?
        If e.NewValue = True And e.OldValue = False Then
            CekSesuaiSyarat("Outlet", "Plus")
        ElseIf e.NewValue = False And e.OldValue = True Then
            CekSesuaiSyarat("Outlet", "Minus")
        End If
    End Sub

    Private Sub CekSesuaiSyarat(ByVal JenisTrans As String, ByVal Fungsi As String)
        Dim ReturnValues As Integer
        If BenefitRow.Item("syarat_tipe") = 1 Then
            '---syarat jml item 
            ReturnValues = Syarat1(JenisTrans)

        ElseIf BenefitRow.Item("syarat_tipe") = 2 Then
            '---syarat jml nota 
            ReturnValues = Syarat2(JenisTrans)

        ElseIf BenefitRow.Item("syarat_tipe") = 3 Or BenefitRow.Item("syarat_tipe") = 6 Then
            '---syarat jml / nominal kat ttt 
            ReturnValues = Syarat3(JenisTrans, BenefitRow.Item("syarat_tipe"))

        ElseIf BenefitRow.Item("syarat_tipe") = 4 Or BenefitRow.Item("syarat_tipe") = 7 Then
            '---syarat jml / nominal item ttt 
            ReturnValues = Syarat4(JenisTrans, BenefitRow.Item("syarat_tipe"))

        ElseIf BenefitRow.Item("syarat_tipe") = 5 Then
            '---syarat nominal trans 
            ReturnValues = Syarat5(JenisTrans)

        End If

        If Fungsi = "Plus" Then TotalChoosenValue += ReturnValues Else TotalChoosenValue -= ReturnValues
        TransGrandTotal.EditValue = TotalChoosenValue
    End Sub

    Private Function Syarat1(ByVal Lokasi As String) As Integer
        '---syarat jml item = SUM quantity On selected rows in Detail Table
        Try
            If Lokasi = "Outlet" Then
                Return CType(xSet.Tables("AvailableTransOutlet_D").Compute("SUM(Jml)", String.Format("ID={0} AND idCabang={1}", TransRow.Item("ID"), TransRow.Item("idCabang"))), Integer)
            Else
                Return CType(xSet.Tables("AvailableTransOrder_D").Compute("SUM(Jml)", String.Format("ID={0} AND idCabang={1}", TransRow.Item("ID"), TransRow.Item("idCabang"))), Integer)
            End If
        Catch ex As Exception
            Return 0
        End Try
    End Function

    Private Function Syarat2(ByVal Lokasi As String) As Integer
        '---syarat jml nota = Count checked_row ON selected rows in Master Table 
        Try
            Return 1

            'If Lokasi = "Outlet" Then
            '    Return CType(xSet.Tables("AvailableTransOutlet_M").Compute("COUNT(ID)", "ChoosenOnes=1"), Integer)
            'Else
            '    Return CType(xSet.Tables("AvailableTransOrder_M").Compute("COUNT(ID)", "ChoosenOnes=1"), Integer)
            'End If
        Catch ex As Exception
            Return 0
        End Try
    End Function

    Private Function Syarat3(ByVal Lokasi As String, ByVal WhatToSum As Integer) As Integer
        '---syarat jml / nominal kat ttt  = SUM qtt / price for certain category On selected rows in Detail Table
        Try
            If Lokasi = "Outlet" Then
                Return CType(xSet.Tables("AvailableTransOutlet_D").Compute(IIf(WhatToSum = 3, "SUM(Jml)", "SUM(Total)"), String.Format("ID={0} AND idCabang={1} AND idKategori2={2}", TransRow.Item("ID"), TransRow.Item("idCabang"), BenefitRow.Item("syarat_wherevalue"))), Integer)
            Else
                Return CType(xSet.Tables("AvailableTransOrder_D").Compute(IIf(WhatToSum = 3, "SUM(Jml)", "SUM(Total)"), String.Format("ID={0} AND idCabang={1} AND idKategori2={2}", TransRow.Item("ID"), TransRow.Item("idCabang"), BenefitRow.Item("syarat_wherevalue"))), Integer)
            End If
        Catch ex As Exception
            Return 0
        End Try
    End Function

    Private Function Syarat4(ByVal Lokasi As String, ByVal WhatToSum As Integer) As Integer
        '---syarat jml / nominal item ttt  = SUM qtt / price for certain item On selected rows in Detail Table
        Try
            If Lokasi = "Outlet" Then
                Return CType(xSet.Tables("AvailableTransOutlet_D").Compute(IIf(WhatToSum = 4, "SUM(Jml)", "SUM(Total)"), String.Format("ID={0} AND idCabang={1} AND idBrg={2}", TransRow.Item("ID"), TransRow.Item("idCabang"), BenefitRow.Item("syarat_wherevalue"))), Integer)
            Else
                Return CType(xSet.Tables("AvailableTransOrder_D").Compute(IIf(WhatToSum = 4, "SUM(Jml)", "SUM(Total)"), String.Format("ID={0} AND idCabang={1} AND idBrg={2}", TransRow.Item("ID"), TransRow.Item("idCabang"), BenefitRow.Item("syarat_wherevalue"))), Integer)
            End If
        Catch ex As Exception
            Return 0
        End Try
    End Function

    Private Function Syarat5(ByVal Lokasi As String) As Integer
        '---syarat nominal trans = Sum total_transaction ON selected rows in Master Table 
        Try
            If Lokasi = "Outlet" Then
                Return TransRow.Item("TotalBeli") 'CType(xSet.Tables("AvailableTransOutlet_M").Compute("SUM(TotalBeli)", "ChoosenOnes=1"), Integer)
            Else
                Return TransRow.Item("TotalBeli") 'CType(xSet.Tables("AvailableTransOrder_M").Compute("SUM(TotalBeli)", "ChoosenOnes=1"), Integer)
            End If
        Catch ex As Exception
            Return 0 'CType(xSet.Tables("AvailableTransOutlet_M").Compute("SUM(TotalBeli)", "ID=" & TransRow.Item("ID")), Integer)
        End Try
    End Function
#End Region

#Region "Check reward bila memenuhi syarat"
    Private Sub TransGrandTotal_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TransGrandTotal.EditValueChanged
        On Error Resume Next
        Dim DapatBerapa As Integer

        'cek dulu dengan bandingkan hasil (operator) syarat
        If EvalProgress(BenefitRow.Item("syarat_operator"), BenefitRow.Item("syarat_tipevalue")) Then
            'sampe sini minimal pasti 1 karena sudah memenuhi syarat
            'selanjutnya cek ini multiply ngga? if yes, reward=hasil/syarat. if no reward=1
            If BenefitRow.Item("ismultiplied") Then
                DapatBerapa = TotalChoosenValue \ BenefitRow.Item("syarat_tipevalue")

                If BenefitRow.Item("reward_max") > 0 And DapatBerapa > BenefitRow.Item("reward_max") Then
                    DapatBerapa = BenefitRow.Item("reward_max")
                End If
            Else
                DapatBerapa = 1
            End If
        Else
            DapatBerapa = 0
        End If

        RewardTimes.EditValue = DapatBerapa
    End Sub

    Private Function EvalProgress(ByVal syarat_operator As String, ByVal syarat_tipevalue As Integer) As Boolean
        If syarat_operator = "=" Then
            If TotalChoosenValue = syarat_tipevalue Then Return True
        ElseIf syarat_operator = ">" Then
            If TotalChoosenValue > syarat_tipevalue Then Return True
        ElseIf syarat_operator = ">=" Then
            If TotalChoosenValue >= syarat_tipevalue Then Return True
        End If
        Return False
    End Function
#End Region
End Class