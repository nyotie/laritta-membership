﻿Public Class PenggunaanGiftPromo

    Private Sub PenggunaanGiftPromo_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("GetDataExcGP")
        xSet.Tables.Remove("GetDataTransaksiOrder")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub PenggunaanGiftPromo_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading(Me)
        Get_KebutuhanDaftar()
    End Sub

    Private Sub PenggunaanGiftPromo_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        LoadSprite.CloseLoading()
    End Sub

    Private Sub KodePenukaran_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles KodePenukaran.EditValueChanged
        Try
            If KodePenukaran.EditValue.ToString.Length < 13 And BenefitJenis.Text <> "" Then
                Throw New Exception
            ElseIf KodePenukaran.Text.Length = 13 Then
                If Not xSet.Tables("GetDataExcGP") Is Nothing Then xSet.Tables("GetDataExcGP").Clear()

                If Mid(KodePenukaran.Text, 1, 3) = "PR/" Then
                    SQLquery = String.Format("SELECT x.id_exchangepromo ID, y.nama_promo Benefit, CASE x.reward_jenis WHEN 'brg' THEN 'Barang' ELSE 'Diskon' END reward_jenis, x.reward_promo Reward, " & _
                                "x.reward_disc, x.reward_value, x.reward_multiplied, x.use_at_outlet, x.use_at_order, x.expired_date, x.jml_penggunaan_max - x.jml_penggunaan sisa_penggunaan, x.createdate " & _
                                "FROM mbst_exchangepromo x INNER JOIN mbsm_promo y ON x.id_promo=y.id_promo WHERE x.kode_exchangepromo='{0}';", KodePenukaran.Text)
                ElseIf Mid(KodePenukaran.Text, 1, 3) = "GF/" Then
                    SQLquery = String.Format("SELECT x.id_exchangegift ID, y.nama_gift Benefit, CASE x.reward_jenis WHEN 'brg' THEN 'Barang' ELSE 'Diskon' END reward_jenis, x.reward_gift Reward, " & _
                                "x.reward_disc, x.reward_value, x.reward_multiplied, x.use_at_outlet, x.use_at_order, x.expired_date, x.jml_penggunaan_max - x.jml_penggunaan sisa_penggunaan, x.createdate " & _
                                "FROM mbst_exchangegift x INNER JOIN mbsm_gift y ON x.id_gift=y.id_gift WHERE x.kode_exchangegift='{0}';", KodePenukaran.Text)
                End If
                ExDb.ExecQuery(1, SQLquery, xSet, "GetDataExcGP")

                If xSet.Tables("GetDataExcGP").Rows.Count < 1 Then
                    MsgWarning("Kode tidak ditemukan")
                ElseIf Now.Date > xSet.Tables("GetDataExcGP").Rows(0).Item("expired_date") Then
                    MsgWarning("Kode ini telah habis masa berlakunya!")
                ElseIf xSet.Tables("GetDataExcGP").Rows(0).Item("sisa_penggunaan") = 0 Then
                    MsgWarning("Kode ini sudah digunakan!")
                Else
                    BenefitJenis.Text = IIf(Mid(KodePenukaran.Text, 1, 3) = "PR/", "Promo", "Gift")
                    BenefitNama.Text = xSet.Tables("GetDataExcGP").Rows(0).Item("Benefit")
                    RewardJenis.Text = xSet.Tables("GetDataExcGP").Rows(0).Item("reward_jenis")
                    RewardNama.Text = xSet.Tables("GetDataExcGP").Rows(0).Item("Reward")
                    RewardJumlah.Text = xSet.Tables("GetDataExcGP").Rows(0).Item("reward_multiplied")

                    '----------if benefit = [barang -> tanpa transaksi | diskon -> dengan transaksi]
                    If RewardJenis.Text = "Barang" Then
                        CabangTransaksi.Enabled = False
                        TransaksiJenis.Enabled = False
                        TransaksiNilai.Enabled = False
                    Else
                        CabangTransaksi.Enabled = True
                        TransaksiJenis.Enabled = True
                        TransaksiNilai.Enabled = True

                        TransaksiJenis.Properties.Items(0).Enabled = xSet.Tables("GetDataExcGP").Rows(0).Item("use_at_outlet")
                        TransaksiJenis.Properties.Items(1).Enabled = xSet.Tables("GetDataExcGP").Rows(0).Item("use_at_order")
                        If TransaksiJenis.Properties.Items(0).Enabled = True Then
                            TransaksiJenis.SelectedIndex = 0
                            TransaksiNoOutlet.Enabled = True
                        Else
                            TransaksiJenis.SelectedIndex = 1
                            TransaksiNoOrder.Enabled = True
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            ClearAll()
        End Try
    End Sub

    Private Sub ButClear_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButClear.Click
        KodePenukaran.Text = ""
        ClearAll()
    End Sub

    Private Sub TransaksiJenis_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TransaksiJenis.SelectedIndexChanged
        Select Case TransaksiJenis.SelectedIndex
            Case 0
                'Outlet
                TransaksiNoOrder.Text = ""
                TransaksiNoOrder.Enabled = False
                TransaksiNoOutlet.Enabled = True

                TransaksiNilai.Properties.ReadOnly = False
                TransaksiNilai.EditValue = 0
            Case 1
                'Order
                TransaksiNoOutlet.Text = ""
                TransaksiNoOutlet.Enabled = False
                TransaksiNoOrder.Enabled = True

                TransaksiNilai.Properties.ReadOnly = True
                TransaksiNilai.EditValue = 0
        End Select
    End Sub

    Private Sub TransaksiNoOrder_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TransaksiNoOrder.EditValueChanged
        Try
            If BenefitNama.Text = "" Or CabangTransaksi.EditValue Is Nothing Or TransaksiNoOrder.EditValue.ToString.Length < 14 Then Throw New Exception
            If Not xSet.Tables("GetDataTransaksiOrder") Is Nothing Then xSet.Tables("GetDataTransaksiOrder").Clear()
            SQLquery = String.Format("SELECT a.idPesanan, a.tglPesan, b.NilaiTotalPesanan FROM mtpesanan a INNER JOIN set_pesanan b ON a.idPesanan=b.idPesanan " & _
                        "WHERE a.Void=0 AND a.idCabang={0} AND YEAR(a.tglPesan)={1} AND MONTH(a.tglPesan)={2} AND noPesan={3}",
                        CabangTransaksi.EditValue,
                        "20" & Mid(TransaksiNoOrder.Text, 5, 2),
                        Mid(TransaksiNoOrder.Text, 8, 2),
                        Microsoft.VisualBasic.Right(TransaksiNoOrder.Text, 4))
            ExDb.ExecQuery(1, SQLquery, xSet, "GetDataTransaksiOrder")

            If xSet.Tables("GetDataTransaksiOrder").Rows(0).Item("tglPesan") < xSet.Tables("GetDataExcGP").Rows(0).Item("createdate") Then
                MsgInfo("Transaksi tidak dapat digunakan karena dilakukan sebelum proses penukaran benefit.")
            Else
                TransaksiNilai.EditValue = xSet.Tables("GetDataTransaksiOrder").Rows(0).Item("NilaiTotalPesanan")
            End If
        Catch ex As Exception
            TransaksiNilai.EditValue = 0
        End Try
    End Sub

    Private Sub ButOK_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButOK.Click
        If Not beforeSave() Then Return
        Try
            LoadSprite.ShowLoading()

            Dim x, y, z As Integer
            x = xSet.Tables("GetDataExcGP").Rows(0).Item("reward_disc")
            y = xSet.Tables("GetDataExcGP").Rows(0).Item("reward_value")
            z = xSet.Tables("GetDataExcGP").Rows(0).Item("reward_multiplied")

            SQLquery = String.Format("CALL InsertUseGiftPromo ({0}, {1}, {2}, '{3}', {4}, {5}, {6}, {7}, {8})",
                                     id_cabang,
                                     IIf(BenefitJenis.Text = "Promo", 0, 1),
                                     xSet.Tables("GetDataExcGP").Rows(0).Item("ID"),
                                     KodePenukaran.EditValue,
                                     HitungNilai("TransCabang", x, y, z),
                                     HitungNilai("TransKode", x, y, z),
                                     HitungNilai("TransNilai", x, y, z),
                                     HitungNilai("TransFinalValue", x, y, z),
                                     staff_id)
            ExDb.ExecData(1, SQLquery)

            MsgInfo("Data berhasil disimpan, tekan OK untuk melanjutkan.")
        Catch ex As Exception
            MsgErrorDev()
        End Try
        LoadSprite.CloseLoading()
        Close()
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Sub ClearAll()
        BenefitJenis.Text = ""
        BenefitNama.Text = ""
        RewardJenis.Text = ""
        RewardNama.Text = ""
        RewardJumlah.Text = ""

        CabangTransaksi.EditValue = Nothing
        CabangTransaksi.Enabled = False

        TransaksiJenis.Properties.Items(0).Enabled = True
        TransaksiJenis.Properties.Items(1).Enabled = True
        TransaksiJenis.Enabled = False

        TransaksiNoOutlet.Text = ""
        TransaksiNoOutlet.Enabled = False

        TransaksiNoOrder.Text = ""
        TransaksiNoOrder.Enabled = False

        TransaksiNilai.Text = ""
        TransaksiNilai.Enabled = False
    End Sub

    Private Sub Get_KebutuhanDaftar()
        CabangTransaksi.Properties.DataSource = xSet.Tables("DataBranch").DefaultView

        CabangTransaksi.Properties.NullText = ""
        CabangTransaksi.Properties.ValueMember = "ID"
        CabangTransaksi.Properties.DisplayMember = "Nama"
        CabangTransaksi.Properties.ShowClearButton = False
        CabangTransaksi.Properties.PopulateViewColumns()

        ViewCabangTransaksi.Columns("ID").Visible = False
        ViewCabangTransaksi.Columns("Produksi").Visible = False

        ViewCabangTransaksi.Columns("Nama").Width = 70
        ViewCabangTransaksi.Columns("Alamat").Width = 150
    End Sub

    Private Function HitungNilai(ByVal WhatDoYouWant As String, ByVal TypeDiskon As Integer, ByVal CalonFinalValue As Integer, ByVal Kelipatan As Integer)
        If RewardJenis.Text = "Barang" Then
            If WhatDoYouWant = "TransCabang" Or WhatDoYouWant = "TransKode" Or WhatDoYouWant = "TransNilai" Then
                Return "NULL"
            Else
                'TransFinalValue
                Return CalonFinalValue * Kelipatan
            End If
        ElseIf RewardJenis.Text = "Diskon" Then
            Select Case WhatDoYouWant
                Case "TransCabang"
                    Return CabangTransaksi.EditValue
                Case "TransKode"
                    If TransaksiJenis.SelectedIndex = 0 Then
                        'Outlet
                        Return String.Format("'{0}'", TransaksiNoOutlet.Text)
                    Else
                        'Order
                        Return String.Format("'{0}'", TransaksiNoOrder.Text)
                    End If
                Case "TransNilai"
                    Return TransaksiNilai.EditValue
                Case "TransFinalValue"
                    'cara hitung value dari benefit. if [brg / disc -> benefit_val | %disc -> transaksi * x%]
                    If TypeDiskon = 0 Then
                        Return CalonFinalValue
                    Else
                        Return (CalonFinalValue / 100) * TransaksiNilai.EditValue
                    End If
            End Select
        End If
        Return 0
    End Function

    Private Function beforeSave() As Boolean
        If RewardJenis.Text = "Diskon" Then
            If CabangTransaksi.EditValue Is Nothing Then Return False
            If TransaksiJenis.SelectedIndex = 0 And (TransaksiNoOutlet.Text.Length = 0 Or TransaksiNilai.EditValue <= 0) Then
                MsgInfo("Data transaksi Outlet belum lengkap!")
                Return False
            End If
            If TransaksiJenis.SelectedIndex = 1 And (TransaksiNoOrder.Text.Length < 14 Or TransaksiNilai.EditValue <= 0) Then
                MsgInfo("Data transaksi Order belum lengkap!")
                Return False
            End If
        ElseIf RewardJenis.Text = "" Then
            Return False
        End If

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        Return True
    End Function
End Class