﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PenukaranGiftPromo
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim GridLevelNode1 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim GridLevelNode2 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Me.ViewDetailTransOutlet = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.ThisMemberOutlet = New DevExpress.XtraGrid.GridControl()
        Me.ViewMasterTransOutlet = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.OutletChecker = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.ViewDetailTransOrder = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.ThisMemberOrder = New DevExpress.XtraGrid.GridControl()
        Me.ViewMasterTransOrder = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.Step1 = New DevExpress.XtraEditors.GroupControl()
        Me.ButClose1 = New DevExpress.XtraEditors.SimpleButton()
        Me.ButNext = New DevExpress.XtraEditors.SimpleButton()
        Me.TabControlBenefit = New DevExpress.XtraTab.XtraTabControl()
        Me.PagePromo = New DevExpress.XtraTab.XtraTabPage()
        Me.ThisMemberPromo = New DevExpress.XtraGrid.GridControl()
        Me.ViewThisMemberPromo = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.RepoMemo = New DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit()
        Me.PageGift = New DevExpress.XtraTab.XtraTabPage()
        Me.ThisMemberGift = New DevExpress.XtraGrid.GridControl()
        Me.ViewThisMemberGift = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.Step2 = New DevExpress.XtraEditors.GroupControl()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.DenganSyarat = New DevExpress.XtraEditors.CheckEdit()
        Me.RewardTimes = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.CekMultiplied = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.use_order = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.use_outlet = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.SyaratUnlockValues = New DevExpress.XtraEditors.TextEdit()
        Me.TransGrandTotal = New DevExpress.XtraEditors.TextEdit()
        Me.SyaratUnlockName = New DevExpress.XtraEditors.TextEdit()
        Me.SyaratUnlockOperator = New DevExpress.XtraEditors.TextEdit()
        Me.use_expired = New DevExpress.XtraEditors.TextEdit()
        Me.RewardName = New DevExpress.XtraEditors.TextEdit()
        Me.ShapeContainer2 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.LineShape1 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.LineShape2 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.ButBack = New DevExpress.XtraEditors.SimpleButton()
        Me.ButOK = New DevExpress.XtraEditors.SimpleButton()
        Me.ButClose2 = New DevExpress.XtraEditors.SimpleButton()
        Me.TransactionPages = New DevExpress.XtraTab.XtraTabControl()
        Me.PageDaftarTransaksiOutlet = New DevExpress.XtraTab.XtraTabPage()
        Me.PageDaftarTransaksiOrder = New DevExpress.XtraTab.XtraTabPage()
        Me.OrderChecker = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        CType(Me.ViewDetailTransOutlet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ThisMemberOutlet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewMasterTransOutlet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.OutletChecker, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewDetailTransOrder, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ThisMemberOrder, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewMasterTransOrder, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Step1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Step1.SuspendLayout()
        CType(Me.TabControlBenefit, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControlBenefit.SuspendLayout()
        Me.PagePromo.SuspendLayout()
        CType(Me.ThisMemberPromo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewThisMemberPromo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepoMemo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PageGift.SuspendLayout()
        CType(Me.ThisMemberGift, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewThisMemberGift, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Step2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Step2.SuspendLayout()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.DenganSyarat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RewardTimes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CekMultiplied.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.use_order.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.use_outlet.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SyaratUnlockValues.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TransGrandTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SyaratUnlockName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SyaratUnlockOperator.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.use_expired.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RewardName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TransactionPages, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TransactionPages.SuspendLayout()
        Me.PageDaftarTransaksiOutlet.SuspendLayout()
        Me.PageDaftarTransaksiOrder.SuspendLayout()
        CType(Me.OrderChecker, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ViewDetailTransOutlet
        '
        Me.ViewDetailTransOutlet.Appearance.HeaderPanel.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ViewDetailTransOutlet.Appearance.HeaderPanel.Options.UseFont = True
        Me.ViewDetailTransOutlet.Appearance.Row.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ViewDetailTransOutlet.Appearance.Row.Options.UseFont = True
        Me.ViewDetailTransOutlet.GridControl = Me.ThisMemberOutlet
        Me.ViewDetailTransOutlet.Name = "ViewDetailTransOutlet"
        Me.ViewDetailTransOutlet.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewDetailTransOutlet.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewDetailTransOutlet.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewDetailTransOutlet.OptionsBehavior.Editable = False
        Me.ViewDetailTransOutlet.OptionsCustomization.AllowColumnMoving = False
        Me.ViewDetailTransOutlet.OptionsCustomization.AllowFilter = False
        Me.ViewDetailTransOutlet.OptionsCustomization.AllowGroup = False
        Me.ViewDetailTransOutlet.OptionsCustomization.AllowQuickHideColumns = False
        Me.ViewDetailTransOutlet.OptionsCustomization.AllowSort = False
        Me.ViewDetailTransOutlet.OptionsView.ShowGroupPanel = False
        '
        'ThisMemberOutlet
        '
        Me.ThisMemberOutlet.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        GridLevelNode1.LevelTemplate = Me.ViewDetailTransOutlet
        GridLevelNode1.RelationName = "Level1"
        Me.ThisMemberOutlet.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode1})
        Me.ThisMemberOutlet.Location = New System.Drawing.Point(3, 3)
        Me.ThisMemberOutlet.MainView = Me.ViewMasterTransOutlet
        Me.ThisMemberOutlet.Name = "ThisMemberOutlet"
        Me.ThisMemberOutlet.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.OutletChecker})
        Me.ThisMemberOutlet.Size = New System.Drawing.Size(527, 505)
        Me.ThisMemberOutlet.TabIndex = 4
        Me.ThisMemberOutlet.TabStop = False
        Me.ThisMemberOutlet.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.ViewMasterTransOutlet, Me.ViewDetailTransOutlet})
        '
        'ViewMasterTransOutlet
        '
        Me.ViewMasterTransOutlet.Appearance.HeaderPanel.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ViewMasterTransOutlet.Appearance.HeaderPanel.Options.UseFont = True
        Me.ViewMasterTransOutlet.Appearance.Row.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ViewMasterTransOutlet.Appearance.Row.Options.UseFont = True
        Me.ViewMasterTransOutlet.ColumnPanelRowHeight = 50
        Me.ViewMasterTransOutlet.GridControl = Me.ThisMemberOutlet
        Me.ViewMasterTransOutlet.Name = "ViewMasterTransOutlet"
        Me.ViewMasterTransOutlet.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewMasterTransOutlet.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewMasterTransOutlet.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewMasterTransOutlet.OptionsCustomization.AllowColumnMoving = False
        Me.ViewMasterTransOutlet.OptionsCustomization.AllowFilter = False
        Me.ViewMasterTransOutlet.OptionsCustomization.AllowGroup = False
        Me.ViewMasterTransOutlet.OptionsCustomization.AllowQuickHideColumns = False
        Me.ViewMasterTransOutlet.OptionsView.ShowFooter = True
        Me.ViewMasterTransOutlet.OptionsView.ShowGroupPanel = False
        Me.ViewMasterTransOutlet.RowHeight = 45
        '
        'OutletChecker
        '
        Me.OutletChecker.AutoHeight = False
        Me.OutletChecker.Name = "OutletChecker"
        '
        'ViewDetailTransOrder
        '
        Me.ViewDetailTransOrder.Appearance.HeaderPanel.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ViewDetailTransOrder.Appearance.HeaderPanel.Options.UseFont = True
        Me.ViewDetailTransOrder.Appearance.Row.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ViewDetailTransOrder.Appearance.Row.Options.UseFont = True
        Me.ViewDetailTransOrder.GridControl = Me.ThisMemberOrder
        Me.ViewDetailTransOrder.Name = "ViewDetailTransOrder"
        Me.ViewDetailTransOrder.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewDetailTransOrder.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewDetailTransOrder.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewDetailTransOrder.OptionsBehavior.Editable = False
        Me.ViewDetailTransOrder.OptionsCustomization.AllowColumnMoving = False
        Me.ViewDetailTransOrder.OptionsCustomization.AllowFilter = False
        Me.ViewDetailTransOrder.OptionsCustomization.AllowGroup = False
        Me.ViewDetailTransOrder.OptionsCustomization.AllowQuickHideColumns = False
        Me.ViewDetailTransOrder.OptionsView.ShowGroupPanel = False
        '
        'ThisMemberOrder
        '
        Me.ThisMemberOrder.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        GridLevelNode2.LevelTemplate = Me.ViewDetailTransOrder
        GridLevelNode2.RelationName = "Level1"
        Me.ThisMemberOrder.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode2})
        Me.ThisMemberOrder.Location = New System.Drawing.Point(3, 3)
        Me.ThisMemberOrder.MainView = Me.ViewMasterTransOrder
        Me.ThisMemberOrder.Name = "ThisMemberOrder"
        Me.ThisMemberOrder.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.OrderChecker})
        Me.ThisMemberOrder.Size = New System.Drawing.Size(527, 505)
        Me.ThisMemberOrder.TabIndex = 4
        Me.ThisMemberOrder.TabStop = False
        Me.ThisMemberOrder.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.ViewMasterTransOrder, Me.ViewDetailTransOrder})
        '
        'ViewMasterTransOrder
        '
        Me.ViewMasterTransOrder.Appearance.HeaderPanel.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ViewMasterTransOrder.Appearance.HeaderPanel.Options.UseFont = True
        Me.ViewMasterTransOrder.Appearance.Row.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ViewMasterTransOrder.Appearance.Row.Options.UseFont = True
        Me.ViewMasterTransOrder.ColumnPanelRowHeight = 50
        Me.ViewMasterTransOrder.GridControl = Me.ThisMemberOrder
        Me.ViewMasterTransOrder.Name = "ViewMasterTransOrder"
        Me.ViewMasterTransOrder.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewMasterTransOrder.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewMasterTransOrder.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewMasterTransOrder.OptionsCustomization.AllowColumnMoving = False
        Me.ViewMasterTransOrder.OptionsCustomization.AllowFilter = False
        Me.ViewMasterTransOrder.OptionsCustomization.AllowGroup = False
        Me.ViewMasterTransOrder.OptionsCustomization.AllowQuickHideColumns = False
        Me.ViewMasterTransOrder.OptionsView.ShowFooter = True
        Me.ViewMasterTransOrder.OptionsView.ShowGroupPanel = False
        Me.ViewMasterTransOrder.RowHeight = 45
        '
        'Step1
        '
        Me.Step1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Step1.AppearanceCaption.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.Step1.AppearanceCaption.Options.UseFont = True
        Me.Step1.Controls.Add(Me.ButClose1)
        Me.Step1.Controls.Add(Me.ButNext)
        Me.Step1.Controls.Add(Me.TabControlBenefit)
        Me.Step1.Location = New System.Drawing.Point(12, 12)
        Me.Step1.Name = "Step1"
        Me.Step1.Size = New System.Drawing.Size(855, 637)
        Me.Step1.TabIndex = 1
        Me.Step1.Text = "Daftar Gift dan Promo tersedia"
        '
        'ButClose1
        '
        Me.ButClose1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButClose1.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButClose1.Appearance.Options.UseFont = True
        Me.ButClose1.Image = Global.Laritta_Membership.My.Resources.Resources.close_32
        Me.ButClose1.Location = New System.Drawing.Point(764, 587)
        Me.ButClose1.Name = "ButClose1"
        Me.ButClose1.Size = New System.Drawing.Size(86, 45)
        Me.ButClose1.TabIndex = 8
        Me.ButClose1.Text = "&Tutup"
        '
        'ButNext
        '
        Me.ButNext.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButNext.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButNext.Appearance.Options.UseFont = True
        Me.ButNext.Image = Global.Laritta_Membership.My.Resources.Resources.apply_32
        Me.ButNext.Location = New System.Drawing.Point(672, 587)
        Me.ButNext.Name = "ButNext"
        Me.ButNext.Size = New System.Drawing.Size(86, 45)
        Me.ButNext.TabIndex = 2
        Me.ButNext.Text = "&OK"
        '
        'TabControlBenefit
        '
        Me.TabControlBenefit.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControlBenefit.AppearancePage.Header.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.TabControlBenefit.AppearancePage.Header.Options.UseFont = True
        Me.TabControlBenefit.Location = New System.Drawing.Point(5, 25)
        Me.TabControlBenefit.Name = "TabControlBenefit"
        Me.TabControlBenefit.SelectedTabPage = Me.PagePromo
        Me.TabControlBenefit.Size = New System.Drawing.Size(845, 556)
        Me.TabControlBenefit.TabIndex = 1
        Me.TabControlBenefit.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.PagePromo, Me.PageGift})
        '
        'PagePromo
        '
        Me.PagePromo.Controls.Add(Me.ThisMemberPromo)
        Me.PagePromo.Image = Global.Laritta_Membership.My.Resources.Resources.promo_32
        Me.PagePromo.Name = "PagePromo"
        Me.PagePromo.Size = New System.Drawing.Size(839, 511)
        Me.PagePromo.Text = "Promo"
        '
        'ThisMemberPromo
        '
        Me.ThisMemberPromo.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ThisMemberPromo.Location = New System.Drawing.Point(3, 3)
        Me.ThisMemberPromo.MainView = Me.ViewThisMemberPromo
        Me.ThisMemberPromo.Name = "ThisMemberPromo"
        Me.ThisMemberPromo.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepoMemo})
        Me.ThisMemberPromo.Size = New System.Drawing.Size(833, 503)
        Me.ThisMemberPromo.TabIndex = 3
        Me.ThisMemberPromo.TabStop = False
        Me.ThisMemberPromo.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.ViewThisMemberPromo})
        '
        'ViewThisMemberPromo
        '
        Me.ViewThisMemberPromo.Appearance.HeaderPanel.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ViewThisMemberPromo.Appearance.HeaderPanel.Options.UseFont = True
        Me.ViewThisMemberPromo.Appearance.Row.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ViewThisMemberPromo.Appearance.Row.Options.UseFont = True
        Me.ViewThisMemberPromo.ColumnPanelRowHeight = 50
        Me.ViewThisMemberPromo.GridControl = Me.ThisMemberPromo
        Me.ViewThisMemberPromo.Name = "ViewThisMemberPromo"
        Me.ViewThisMemberPromo.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewThisMemberPromo.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewThisMemberPromo.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewThisMemberPromo.OptionsBehavior.Editable = False
        Me.ViewThisMemberPromo.OptionsCustomization.AllowColumnMoving = False
        Me.ViewThisMemberPromo.OptionsCustomization.AllowColumnResizing = False
        Me.ViewThisMemberPromo.OptionsCustomization.AllowFilter = False
        Me.ViewThisMemberPromo.OptionsCustomization.AllowGroup = False
        Me.ViewThisMemberPromo.OptionsCustomization.AllowQuickHideColumns = False
        Me.ViewThisMemberPromo.OptionsCustomization.AllowSort = False
        Me.ViewThisMemberPromo.OptionsView.ShowFooter = True
        Me.ViewThisMemberPromo.OptionsView.ShowGroupPanel = False
        Me.ViewThisMemberPromo.RowHeight = 45
        '
        'RepoMemo
        '
        Me.RepoMemo.Name = "RepoMemo"
        '
        'PageGift
        '
        Me.PageGift.Controls.Add(Me.ThisMemberGift)
        Me.PageGift.Image = Global.Laritta_Membership.My.Resources.Resources.gift_32
        Me.PageGift.Name = "PageGift"
        Me.PageGift.Size = New System.Drawing.Size(839, 511)
        Me.PageGift.Text = "Gift"
        '
        'ThisMemberGift
        '
        Me.ThisMemberGift.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ThisMemberGift.Location = New System.Drawing.Point(3, 3)
        Me.ThisMemberGift.MainView = Me.ViewThisMemberGift
        Me.ThisMemberGift.Name = "ThisMemberGift"
        Me.ThisMemberGift.Size = New System.Drawing.Size(833, 487)
        Me.ThisMemberGift.TabIndex = 4
        Me.ThisMemberGift.TabStop = False
        Me.ThisMemberGift.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.ViewThisMemberGift})
        '
        'ViewThisMemberGift
        '
        Me.ViewThisMemberGift.Appearance.HeaderPanel.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ViewThisMemberGift.Appearance.HeaderPanel.Options.UseFont = True
        Me.ViewThisMemberGift.Appearance.Row.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ViewThisMemberGift.Appearance.Row.Options.UseFont = True
        Me.ViewThisMemberGift.ColumnPanelRowHeight = 50
        Me.ViewThisMemberGift.GridControl = Me.ThisMemberGift
        Me.ViewThisMemberGift.Name = "ViewThisMemberGift"
        Me.ViewThisMemberGift.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewThisMemberGift.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewThisMemberGift.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewThisMemberGift.OptionsBehavior.Editable = False
        Me.ViewThisMemberGift.OptionsCustomization.AllowColumnMoving = False
        Me.ViewThisMemberGift.OptionsCustomization.AllowColumnResizing = False
        Me.ViewThisMemberGift.OptionsCustomization.AllowFilter = False
        Me.ViewThisMemberGift.OptionsCustomization.AllowGroup = False
        Me.ViewThisMemberGift.OptionsCustomization.AllowQuickHideColumns = False
        Me.ViewThisMemberGift.OptionsCustomization.AllowSort = False
        Me.ViewThisMemberGift.OptionsView.ShowFooter = True
        Me.ViewThisMemberGift.OptionsView.ShowGroupPanel = False
        Me.ViewThisMemberGift.RowHeight = 45
        '
        'Step2
        '
        Me.Step2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Step2.AppearanceCaption.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.Step2.AppearanceCaption.Options.UseFont = True
        Me.Step2.Controls.Add(Me.PanelControl1)
        Me.Step2.Controls.Add(Me.ButBack)
        Me.Step2.Controls.Add(Me.ButOK)
        Me.Step2.Controls.Add(Me.ButClose2)
        Me.Step2.Controls.Add(Me.TransactionPages)
        Me.Step2.Enabled = False
        Me.Step2.Location = New System.Drawing.Point(12, 12)
        Me.Step2.Name = "Step2"
        Me.Step2.Size = New System.Drawing.Size(855, 637)
        Me.Step2.TabIndex = 3
        Me.Step2.Text = "Ketentuan syarat"
        Me.Step2.Visible = False
        '
        'PanelControl1
        '
        Me.PanelControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelControl1.Controls.Add(Me.LabelControl3)
        Me.PanelControl1.Controls.Add(Me.DenganSyarat)
        Me.PanelControl1.Controls.Add(Me.RewardTimes)
        Me.PanelControl1.Controls.Add(Me.LabelControl4)
        Me.PanelControl1.Controls.Add(Me.CekMultiplied)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Controls.Add(Me.use_order)
        Me.PanelControl1.Controls.Add(Me.LabelControl2)
        Me.PanelControl1.Controls.Add(Me.use_outlet)
        Me.PanelControl1.Controls.Add(Me.LabelControl5)
        Me.PanelControl1.Controls.Add(Me.SyaratUnlockValues)
        Me.PanelControl1.Controls.Add(Me.TransGrandTotal)
        Me.PanelControl1.Controls.Add(Me.SyaratUnlockName)
        Me.PanelControl1.Controls.Add(Me.SyaratUnlockOperator)
        Me.PanelControl1.Controls.Add(Me.use_expired)
        Me.PanelControl1.Controls.Add(Me.RewardName)
        Me.PanelControl1.Controls.Add(Me.ShapeContainer2)
        Me.PanelControl1.Location = New System.Drawing.Point(550, 25)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(300, 551)
        Me.PanelControl1.TabIndex = 113
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl3.Location = New System.Drawing.Point(232, 139)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(8, 14)
        Me.LabelControl3.TabIndex = 101
        Me.LabelControl3.Text = "x"
        '
        'DenganSyarat
        '
        Me.DenganSyarat.Location = New System.Drawing.Point(14, 7)
        Me.DenganSyarat.Name = "DenganSyarat"
        Me.DenganSyarat.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DenganSyarat.Properties.Appearance.Options.UseFont = True
        Me.DenganSyarat.Properties.Caption = "Syarat"
        Me.DenganSyarat.Properties.ReadOnly = True
        Me.DenganSyarat.Size = New System.Drawing.Size(75, 19)
        Me.DenganSyarat.TabIndex = 109
        Me.DenganSyarat.TabStop = False
        '
        'RewardTimes
        '
        Me.RewardTimes.EditValue = "0"
        Me.RewardTimes.Location = New System.Drawing.Point(240, 136)
        Me.RewardTimes.Name = "RewardTimes"
        Me.RewardTimes.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.RewardTimes.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.RewardTimes.Properties.Appearance.Options.UseFont = True
        Me.RewardTimes.Properties.Mask.EditMask = "n0"
        Me.RewardTimes.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.RewardTimes.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.RewardTimes.Properties.NullText = "0"
        Me.RewardTimes.Properties.ReadOnly = True
        Me.RewardTimes.Size = New System.Drawing.Size(43, 21)
        Me.RewardTimes.TabIndex = 105
        Me.RewardTimes.TabStop = False
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl4.Location = New System.Drawing.Point(16, 58)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(66, 14)
        Me.LabelControl4.TabIndex = 101
        Me.LabelControl4.Text = "Terpenuhi"
        '
        'CekMultiplied
        '
        Me.CekMultiplied.Location = New System.Drawing.Point(14, 162)
        Me.CekMultiplied.Name = "CekMultiplied"
        Me.CekMultiplied.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CekMultiplied.Properties.Appearance.Options.UseFont = True
        Me.CekMultiplied.Properties.Caption = "Berlaku kelipatan"
        Me.CekMultiplied.Properties.ReadOnly = True
        Me.CekMultiplied.Size = New System.Drawing.Size(137, 19)
        Me.CekMultiplied.TabIndex = 111
        Me.CekMultiplied.TabStop = False
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Location = New System.Drawing.Point(16, 201)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(134, 14)
        Me.LabelControl1.TabIndex = 101
        Me.LabelControl1.Text = "Expired penggunaan"
        '
        'use_order
        '
        Me.use_order.Location = New System.Drawing.Point(95, 265)
        Me.use_order.Name = "use_order"
        Me.use_order.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.use_order.Properties.Appearance.Options.UseFont = True
        Me.use_order.Properties.Caption = "Order"
        Me.use_order.Properties.ReadOnly = True
        Me.use_order.Size = New System.Drawing.Size(75, 19)
        Me.use_order.TabIndex = 110
        Me.use_order.TabStop = False
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl2.Location = New System.Drawing.Point(16, 246)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(67, 14)
        Me.LabelControl2.TabIndex = 101
        Me.LabelControl2.Text = "Berlaku di"
        '
        'use_outlet
        '
        Me.use_outlet.Location = New System.Drawing.Point(14, 265)
        Me.use_outlet.Name = "use_outlet"
        Me.use_outlet.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.use_outlet.Properties.Appearance.Options.UseFont = True
        Me.use_outlet.Properties.Caption = "Outlet"
        Me.use_outlet.Properties.ReadOnly = True
        Me.use_outlet.Size = New System.Drawing.Size(75, 19)
        Me.use_outlet.TabIndex = 109
        Me.use_outlet.TabStop = False
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl5.Location = New System.Drawing.Point(16, 117)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(51, 14)
        Me.LabelControl5.TabIndex = 101
        Me.LabelControl5.Text = "Reward"
        '
        'SyaratUnlockValues
        '
        Me.SyaratUnlockValues.EditValue = ""
        Me.SyaratUnlockValues.Location = New System.Drawing.Point(229, 32)
        Me.SyaratUnlockValues.Name = "SyaratUnlockValues"
        Me.SyaratUnlockValues.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.SyaratUnlockValues.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.SyaratUnlockValues.Properties.Appearance.Options.UseFont = True
        Me.SyaratUnlockValues.Properties.Appearance.Options.UseTextOptions = True
        Me.SyaratUnlockValues.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.SyaratUnlockValues.Properties.Mask.EditMask = "n0"
        Me.SyaratUnlockValues.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.SyaratUnlockValues.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.SyaratUnlockValues.Properties.NullText = "0"
        Me.SyaratUnlockValues.Properties.ReadOnly = True
        Me.SyaratUnlockValues.Size = New System.Drawing.Size(56, 21)
        Me.SyaratUnlockValues.TabIndex = 105
        Me.SyaratUnlockValues.TabStop = False
        '
        'TransGrandTotal
        '
        Me.TransGrandTotal.EditValue = ""
        Me.TransGrandTotal.Location = New System.Drawing.Point(16, 77)
        Me.TransGrandTotal.Name = "TransGrandTotal"
        Me.TransGrandTotal.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TransGrandTotal.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.TransGrandTotal.Properties.Appearance.Options.UseFont = True
        Me.TransGrandTotal.Properties.Appearance.Options.UseTextOptions = True
        Me.TransGrandTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.TransGrandTotal.Properties.Mask.EditMask = "n0"
        Me.TransGrandTotal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TransGrandTotal.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TransGrandTotal.Properties.NullText = "0"
        Me.TransGrandTotal.Properties.ReadOnly = True
        Me.TransGrandTotal.Size = New System.Drawing.Size(269, 21)
        Me.TransGrandTotal.TabIndex = 104
        Me.TransGrandTotal.TabStop = False
        '
        'SyaratUnlockName
        '
        Me.SyaratUnlockName.Location = New System.Drawing.Point(16, 32)
        Me.SyaratUnlockName.Name = "SyaratUnlockName"
        Me.SyaratUnlockName.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.SyaratUnlockName.Properties.Appearance.Options.UseFont = True
        Me.SyaratUnlockName.Properties.ReadOnly = True
        Me.SyaratUnlockName.Size = New System.Drawing.Size(163, 21)
        Me.SyaratUnlockName.TabIndex = 106
        Me.SyaratUnlockName.TabStop = False
        '
        'SyaratUnlockOperator
        '
        Me.SyaratUnlockOperator.Location = New System.Drawing.Point(185, 32)
        Me.SyaratUnlockOperator.Name = "SyaratUnlockOperator"
        Me.SyaratUnlockOperator.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.SyaratUnlockOperator.Properties.Appearance.Options.UseFont = True
        Me.SyaratUnlockOperator.Properties.ReadOnly = True
        Me.SyaratUnlockOperator.Size = New System.Drawing.Size(38, 21)
        Me.SyaratUnlockOperator.TabIndex = 106
        Me.SyaratUnlockOperator.TabStop = False
        '
        'use_expired
        '
        Me.use_expired.Location = New System.Drawing.Point(16, 220)
        Me.use_expired.Name = "use_expired"
        Me.use_expired.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.use_expired.Properties.Appearance.Options.UseFont = True
        Me.use_expired.Properties.Mask.EditMask = "dd MMMM yyyy"
        Me.use_expired.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.use_expired.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.use_expired.Properties.ReadOnly = True
        Me.use_expired.Size = New System.Drawing.Size(207, 21)
        Me.use_expired.TabIndex = 106
        Me.use_expired.TabStop = False
        '
        'RewardName
        '
        Me.RewardName.Location = New System.Drawing.Point(16, 136)
        Me.RewardName.Name = "RewardName"
        Me.RewardName.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.RewardName.Properties.Appearance.Options.UseFont = True
        Me.RewardName.Properties.ReadOnly = True
        Me.RewardName.Size = New System.Drawing.Size(210, 21)
        Me.RewardName.TabIndex = 106
        Me.RewardName.TabStop = False
        '
        'ShapeContainer2
        '
        Me.ShapeContainer2.Location = New System.Drawing.Point(2, 2)
        Me.ShapeContainer2.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer2.Name = "ShapeContainer2"
        Me.ShapeContainer2.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape1, Me.LineShape2})
        Me.ShapeContainer2.Size = New System.Drawing.Size(296, 547)
        Me.ShapeContainer2.TabIndex = 0
        Me.ShapeContainer2.TabStop = False
        '
        'LineShape1
        '
        Me.LineShape1.BorderColor = System.Drawing.SystemColors.ButtonShadow
        Me.LineShape1.Name = "LineShape1"
        Me.LineShape1.X1 = 7
        Me.LineShape1.X2 = 286
        Me.LineShape1.Y1 = 190
        Me.LineShape1.Y2 = 190
        '
        'LineShape2
        '
        Me.LineShape2.BorderColor = System.Drawing.SystemColors.ButtonShadow
        Me.LineShape2.Name = "LineShape2"
        Me.LineShape2.X1 = 7
        Me.LineShape2.X2 = 286
        Me.LineShape2.Y1 = 105
        Me.LineShape2.Y2 = 105
        '
        'ButBack
        '
        Me.ButBack.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButBack.Image = Global.Laritta_Membership.My.Resources.Resources.back_32
        Me.ButBack.Location = New System.Drawing.Point(5, 587)
        Me.ButBack.Name = "ButBack"
        Me.ButBack.Size = New System.Drawing.Size(86, 45)
        Me.ButBack.TabIndex = 5
        Me.ButBack.Text = "&Kembali"
        '
        'ButOK
        '
        Me.ButOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButOK.Image = Global.Laritta_Membership.My.Resources.Resources.apply_32
        Me.ButOK.Location = New System.Drawing.Point(672, 587)
        Me.ButOK.Name = "ButOK"
        Me.ButOK.Size = New System.Drawing.Size(86, 45)
        Me.ButOK.TabIndex = 6
        Me.ButOK.Text = "&OK"
        '
        'ButClose2
        '
        Me.ButClose2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButClose2.Image = Global.Laritta_Membership.My.Resources.Resources.close_32
        Me.ButClose2.Location = New System.Drawing.Point(764, 587)
        Me.ButClose2.Name = "ButClose2"
        Me.ButClose2.Size = New System.Drawing.Size(86, 45)
        Me.ButClose2.TabIndex = 7
        Me.ButClose2.Text = "&Tutup"
        '
        'TransactionPages
        '
        Me.TransactionPages.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TransactionPages.AppearancePage.Header.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.TransactionPages.AppearancePage.Header.Options.UseFont = True
        Me.TransactionPages.Location = New System.Drawing.Point(5, 25)
        Me.TransactionPages.Name = "TransactionPages"
        Me.TransactionPages.SelectedTabPage = Me.PageDaftarTransaksiOutlet
        Me.TransactionPages.Size = New System.Drawing.Size(539, 556)
        Me.TransactionPages.TabIndex = 3
        Me.TransactionPages.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.PageDaftarTransaksiOutlet, Me.PageDaftarTransaksiOrder})
        '
        'PageDaftarTransaksiOutlet
        '
        Me.PageDaftarTransaksiOutlet.Controls.Add(Me.ThisMemberOutlet)
        Me.PageDaftarTransaksiOutlet.Image = Global.Laritta_Membership.My.Resources.Resources.outlet_32
        Me.PageDaftarTransaksiOutlet.Name = "PageDaftarTransaksiOutlet"
        Me.PageDaftarTransaksiOutlet.Size = New System.Drawing.Size(533, 511)
        Me.PageDaftarTransaksiOutlet.Text = "Outlet"
        '
        'PageDaftarTransaksiOrder
        '
        Me.PageDaftarTransaksiOrder.Controls.Add(Me.ThisMemberOrder)
        Me.PageDaftarTransaksiOrder.Image = Global.Laritta_Membership.My.Resources.Resources.order_32
        Me.PageDaftarTransaksiOrder.Name = "PageDaftarTransaksiOrder"
        Me.PageDaftarTransaksiOrder.Size = New System.Drawing.Size(533, 511)
        Me.PageDaftarTransaksiOrder.Text = "Order"
        '
        'OrderChecker
        '
        Me.OrderChecker.AutoHeight = False
        Me.OrderChecker.Name = "OrderChecker"
        '
        'PenukaranGiftPromo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(879, 661)
        Me.Controls.Add(Me.Step1)
        Me.Controls.Add(Me.Step2)
        Me.Name = "PenukaranGiftPromo"
        CType(Me.ViewDetailTransOutlet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ThisMemberOutlet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewMasterTransOutlet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.OutletChecker, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewDetailTransOrder, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ThisMemberOrder, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewMasterTransOrder, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Step1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Step1.ResumeLayout(False)
        CType(Me.TabControlBenefit, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControlBenefit.ResumeLayout(False)
        Me.PagePromo.ResumeLayout(False)
        CType(Me.ThisMemberPromo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewThisMemberPromo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepoMemo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PageGift.ResumeLayout(False)
        CType(Me.ThisMemberGift, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewThisMemberGift, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Step2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Step2.ResumeLayout(False)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.DenganSyarat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RewardTimes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CekMultiplied.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.use_order.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.use_outlet.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SyaratUnlockValues.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TransGrandTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SyaratUnlockName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SyaratUnlockOperator.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.use_expired.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RewardName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TransactionPages, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TransactionPages.ResumeLayout(False)
        Me.PageDaftarTransaksiOutlet.ResumeLayout(False)
        Me.PageDaftarTransaksiOrder.ResumeLayout(False)
        CType(Me.OrderChecker, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Step1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents TabControlBenefit As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents PagePromo As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents PageGift As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents ButNext As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Step2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents ThisMemberPromo As DevExpress.XtraGrid.GridControl
    Friend WithEvents ViewThisMemberPromo As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents ThisMemberGift As DevExpress.XtraGrid.GridControl
    Friend WithEvents ViewThisMemberGift As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents TransactionPages As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents PageDaftarTransaksiOutlet As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents ThisMemberOutlet As DevExpress.XtraGrid.GridControl
    Friend WithEvents ViewMasterTransOutlet As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents PageDaftarTransaksiOrder As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents ThisMemberOrder As DevExpress.XtraGrid.GridControl
    Friend WithEvents ViewMasterTransOrder As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents TransGrandTotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SyaratUnlockValues As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ButOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButClose2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SyaratUnlockOperator As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SyaratUnlockName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ButBack As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents use_expired As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents use_order As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents use_outlet As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CekMultiplied As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents RewardName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents RewardTimes As DevExpress.XtraEditors.TextEdit
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents ShapeContainer2 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents ButClose1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LineShape2 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents RepoMemo As DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit
    Friend WithEvents ViewDetailTransOutlet As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents ViewDetailTransOrder As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents DenganSyarat As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LineShape1 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents OutletChecker As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents OrderChecker As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
End Class
