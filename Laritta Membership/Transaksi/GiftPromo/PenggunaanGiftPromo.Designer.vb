﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PenggunaanGiftPromo
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.KodePenukaran = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.ButClear = New DevExpress.XtraEditors.SimpleButton()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.RewardJumlah = New DevExpress.XtraEditors.TextEdit()
        Me.BenefitNama = New DevExpress.XtraEditors.TextEdit()
        Me.RewardNama = New DevExpress.XtraEditors.TextEdit()
        Me.RewardJenis = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.BenefitJenis = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.LineShape2 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.TransaksiNoOutlet = New DevExpress.XtraEditors.TextEdit()
        Me.TransaksiJenis = New DevExpress.XtraEditors.RadioGroup()
        Me.TransaksiNoOrder = New DevExpress.XtraEditors.TextEdit()
        Me.TransaksiNilai = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.CabangTransaksi = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.ViewCabangTransaksi = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.ButBatal = New DevExpress.XtraEditors.SimpleButton()
        Me.ButOK = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.KodePenukaran.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.RewardJumlah.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BenefitNama.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RewardNama.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RewardJenis.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BenefitJenis.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TransaksiNoOutlet.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TransaksiJenis.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TransaksiNoOrder.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TransaksiNilai.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.CabangTransaksi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewCabangTransaksi, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelControl1.Controls.Add(Me.KodePenukaran)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Controls.Add(Me.ButClear)
        Me.PanelControl1.Location = New System.Drawing.Point(12, 12)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(417, 39)
        Me.PanelControl1.TabIndex = 0
        '
        'KodePenukaran
        '
        Me.KodePenukaran.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.KodePenukaran.EditValue = ""
        Me.KodePenukaran.Location = New System.Drawing.Point(86, 5)
        Me.KodePenukaran.Name = "KodePenukaran"
        Me.KodePenukaran.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.KodePenukaran.Properties.Appearance.Options.UseFont = True
        Me.KodePenukaran.Properties.Mask.EditMask = "[PR|GF]{2}/[0-9]{2}/[0-9]{2}/[0-9]{4}"
        Me.KodePenukaran.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.KodePenukaran.Properties.ValidateOnEnterKey = True
        Me.KodePenukaran.Size = New System.Drawing.Size(238, 29)
        Me.KodePenukaran.TabIndex = 1
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Location = New System.Drawing.Point(19, 8)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(61, 23)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Kode :"
        '
        'ButClear
        '
        Me.ButClear.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButClear.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButClear.Appearance.Options.UseFont = True
        Me.ButClear.Location = New System.Drawing.Point(330, 5)
        Me.ButClear.Name = "ButClear"
        Me.ButClear.Size = New System.Drawing.Size(82, 29)
        Me.ButClear.TabIndex = 2
        Me.ButClear.Text = "&Clear"
        '
        'GridView1
        '
        Me.GridView1.Name = "GridView1"
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.Controls.Add(Me.LabelControl6)
        Me.GroupControl1.Controls.Add(Me.RewardJumlah)
        Me.GroupControl1.Controls.Add(Me.BenefitNama)
        Me.GroupControl1.Controls.Add(Me.RewardNama)
        Me.GroupControl1.Controls.Add(Me.RewardJenis)
        Me.GroupControl1.Controls.Add(Me.LabelControl7)
        Me.GroupControl1.Controls.Add(Me.LabelControl8)
        Me.GroupControl1.Controls.Add(Me.BenefitJenis)
        Me.GroupControl1.Controls.Add(Me.LabelControl5)
        Me.GroupControl1.Controls.Add(Me.LabelControl4)
        Me.GroupControl1.Controls.Add(Me.ShapeContainer1)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 57)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(417, 184)
        Me.GroupControl1.TabIndex = 1
        Me.GroupControl1.Text = "Benefit"
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl6.Location = New System.Drawing.Point(21, 157)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(59, 14)
        Me.LabelControl6.TabIndex = 1
        Me.LabelControl6.Text = "Kelipatan"
        '
        'RewardJumlah
        '
        Me.RewardJumlah.Location = New System.Drawing.Point(130, 154)
        Me.RewardJumlah.Name = "RewardJumlah"
        Me.RewardJumlah.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.RewardJumlah.Properties.Appearance.Options.UseFont = True
        Me.RewardJumlah.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.RewardJumlah.Properties.ReadOnly = True
        Me.RewardJumlah.Size = New System.Drawing.Size(135, 21)
        Me.RewardJumlah.TabIndex = 4
        Me.RewardJumlah.TabStop = False
        '
        'BenefitNama
        '
        Me.BenefitNama.Location = New System.Drawing.Point(130, 58)
        Me.BenefitNama.Name = "BenefitNama"
        Me.BenefitNama.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.BenefitNama.Properties.Appearance.Options.UseFont = True
        Me.BenefitNama.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.BenefitNama.Properties.ReadOnly = True
        Me.BenefitNama.Size = New System.Drawing.Size(235, 21)
        Me.BenefitNama.TabIndex = 1
        Me.BenefitNama.TabStop = False
        '
        'RewardNama
        '
        Me.RewardNama.Location = New System.Drawing.Point(130, 127)
        Me.RewardNama.Name = "RewardNama"
        Me.RewardNama.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.RewardNama.Properties.Appearance.Options.UseFont = True
        Me.RewardNama.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.RewardNama.Properties.ReadOnly = True
        Me.RewardNama.Size = New System.Drawing.Size(235, 21)
        Me.RewardNama.TabIndex = 3
        Me.RewardNama.TabStop = False
        '
        'RewardJenis
        '
        Me.RewardJenis.Location = New System.Drawing.Point(130, 100)
        Me.RewardJenis.Name = "RewardJenis"
        Me.RewardJenis.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.RewardJenis.Properties.Appearance.Options.UseFont = True
        Me.RewardJenis.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.RewardJenis.Properties.ReadOnly = True
        Me.RewardJenis.Size = New System.Drawing.Size(135, 21)
        Me.RewardJenis.TabIndex = 2
        Me.RewardJenis.TabStop = False
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl7.Location = New System.Drawing.Point(21, 61)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(84, 14)
        Me.LabelControl7.TabIndex = 1
        Me.LabelControl7.Text = "Nama benefit"
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl8.Location = New System.Drawing.Point(21, 130)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(48, 14)
        Me.LabelControl8.TabIndex = 1
        Me.LabelControl8.Text = "Reward"
        '
        'BenefitJenis
        '
        Me.BenefitJenis.Location = New System.Drawing.Point(130, 31)
        Me.BenefitJenis.Name = "BenefitJenis"
        Me.BenefitJenis.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.BenefitJenis.Properties.Appearance.Options.UseFont = True
        Me.BenefitJenis.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.BenefitJenis.Properties.ReadOnly = True
        Me.BenefitJenis.Size = New System.Drawing.Size(135, 21)
        Me.BenefitJenis.TabIndex = 0
        Me.BenefitJenis.TabStop = False
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl5.Location = New System.Drawing.Point(21, 103)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(80, 14)
        Me.LabelControl5.TabIndex = 1
        Me.LabelControl5.Text = "Jenis reward"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl4.Location = New System.Drawing.Point(21, 34)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(79, 14)
        Me.LabelControl4.TabIndex = 1
        Me.LabelControl4.Text = "Jenis benefit"
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(2, 24)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape2})
        Me.ShapeContainer1.Size = New System.Drawing.Size(413, 158)
        Me.ShapeContainer1.TabIndex = 0
        Me.ShapeContainer1.TabStop = False
        '
        'LineShape2
        '
        Me.LineShape2.BorderColor = System.Drawing.SystemColors.ButtonShadow
        Me.LineShape2.Name = "LineShape2"
        Me.LineShape2.X1 = 13
        Me.LineShape2.X2 = 398
        Me.LineShape2.Y1 = 65
        Me.LineShape2.Y2 = 65
        '
        'TransaksiNoOutlet
        '
        Me.TransaksiNoOutlet.Enabled = False
        Me.TransaksiNoOutlet.Location = New System.Drawing.Point(194, 53)
        Me.TransaksiNoOutlet.Name = "TransaksiNoOutlet"
        Me.TransaksiNoOutlet.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.TransaksiNoOutlet.Properties.Appearance.Options.UseFont = True
        Me.TransaksiNoOutlet.Properties.Mask.EditMask = "SO/[0-9]{2}/[0-9]{2}/[0-9]{6}"
        Me.TransaksiNoOutlet.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TransaksiNoOutlet.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TransaksiNoOutlet.Properties.ValidateOnEnterKey = True
        Me.TransaksiNoOutlet.Size = New System.Drawing.Size(171, 21)
        Me.TransaksiNoOutlet.TabIndex = 2
        '
        'TransaksiJenis
        '
        Me.TransaksiJenis.Enabled = False
        Me.TransaksiJenis.Location = New System.Drawing.Point(130, 44)
        Me.TransaksiJenis.Name = "TransaksiJenis"
        Me.TransaksiJenis.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.TransaksiJenis.Properties.Appearance.Options.UseBackColor = True
        Me.TransaksiJenis.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.TransaksiJenis.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Outlet"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Order")})
        Me.TransaksiJenis.Size = New System.Drawing.Size(58, 64)
        Me.TransaksiJenis.TabIndex = 1
        '
        'TransaksiNoOrder
        '
        Me.TransaksiNoOrder.Enabled = False
        Me.TransaksiNoOrder.Location = New System.Drawing.Point(194, 80)
        Me.TransaksiNoOrder.Name = "TransaksiNoOrder"
        Me.TransaksiNoOrder.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.TransaksiNoOrder.Properties.Appearance.Options.UseFont = True
        Me.TransaksiNoOrder.Properties.Mask.EditMask = "ORD/[0-9]{2}/[0-9]{2}/[0-9]{4}"
        Me.TransaksiNoOrder.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TransaksiNoOrder.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TransaksiNoOrder.Size = New System.Drawing.Size(171, 21)
        Me.TransaksiNoOrder.TabIndex = 3
        '
        'TransaksiNilai
        '
        Me.TransaksiNilai.Enabled = False
        Me.TransaksiNilai.Location = New System.Drawing.Point(130, 107)
        Me.TransaksiNilai.Name = "TransaksiNilai"
        Me.TransaksiNilai.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.TransaksiNilai.Properties.Appearance.Options.UseFont = True
        Me.TransaksiNilai.Properties.Mask.EditMask = "n0"
        Me.TransaksiNilai.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TransaksiNilai.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TransaksiNilai.Size = New System.Drawing.Size(171, 21)
        Me.TransaksiNilai.TabIndex = 4
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl2.Location = New System.Drawing.Point(21, 56)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(79, 14)
        Me.LabelControl2.TabIndex = 1
        Me.LabelControl2.Text = "No transaksi"
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl3.Location = New System.Drawing.Point(21, 110)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(88, 14)
        Me.LabelControl3.TabIndex = 1
        Me.LabelControl3.Text = "Nilai transaksi"
        '
        'GroupControl2
        '
        Me.GroupControl2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl2.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold)
        Me.GroupControl2.AppearanceCaption.Options.UseFont = True
        Me.GroupControl2.Controls.Add(Me.LabelControl9)
        Me.GroupControl2.Controls.Add(Me.LabelControl3)
        Me.GroupControl2.Controls.Add(Me.TransaksiNilai)
        Me.GroupControl2.Controls.Add(Me.LabelControl2)
        Me.GroupControl2.Controls.Add(Me.TransaksiNoOutlet)
        Me.GroupControl2.Controls.Add(Me.TransaksiNoOrder)
        Me.GroupControl2.Controls.Add(Me.CabangTransaksi)
        Me.GroupControl2.Controls.Add(Me.TransaksiJenis)
        Me.GroupControl2.Location = New System.Drawing.Point(12, 247)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(417, 137)
        Me.GroupControl2.TabIndex = 2
        Me.GroupControl2.Text = "Penggunaan"
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl9.Location = New System.Drawing.Point(21, 29)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(75, 14)
        Me.LabelControl9.TabIndex = 1
        Me.LabelControl9.Text = "Transaksi di"
        '
        'CabangTransaksi
        '
        Me.CabangTransaksi.Enabled = False
        Me.CabangTransaksi.Location = New System.Drawing.Point(130, 27)
        Me.CabangTransaksi.Name = "CabangTransaksi"
        Me.CabangTransaksi.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.CabangTransaksi.Properties.NullText = ""
        Me.CabangTransaksi.Properties.PopupSizeable = False
        Me.CabangTransaksi.Properties.View = Me.ViewCabangTransaksi
        Me.CabangTransaksi.Size = New System.Drawing.Size(235, 20)
        Me.CabangTransaksi.TabIndex = 0
        '
        'ViewCabangTransaksi
        '
        Me.ViewCabangTransaksi.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.ViewCabangTransaksi.Name = "ViewCabangTransaksi"
        Me.ViewCabangTransaksi.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.ViewCabangTransaksi.OptionsView.ShowGroupPanel = False
        '
        'ButBatal
        '
        Me.ButBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButBatal.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButBatal.Appearance.Options.UseFont = True
        Me.ButBatal.Image = Global.Laritta_Membership.My.Resources.Resources.back_32
        Me.ButBatal.Location = New System.Drawing.Point(339, 390)
        Me.ButBatal.Name = "ButBatal"
        Me.ButBatal.Size = New System.Drawing.Size(90, 40)
        Me.ButBatal.TabIndex = 4
        Me.ButBatal.Text = "&Batal"
        '
        'ButOK
        '
        Me.ButOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButOK.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButOK.Appearance.Options.UseFont = True
        Me.ButOK.Image = Global.Laritta_Membership.My.Resources.Resources.apply_32
        Me.ButOK.Location = New System.Drawing.Point(243, 390)
        Me.ButOK.Name = "ButOK"
        Me.ButOK.Size = New System.Drawing.Size(90, 40)
        Me.ButOK.TabIndex = 3
        Me.ButOK.Text = "&OK"
        '
        'PenggunaanGiftPromo
        '
        Me.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(441, 442)
        Me.Controls.Add(Me.ButBatal)
        Me.Controls.Add(Me.ButOK)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.PanelControl1)
        Me.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Name = "PenggunaanGiftPromo"
        Me.Text = "PenggunaanGiftPromo"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.KodePenukaran.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.RewardJumlah.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BenefitNama.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RewardNama.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RewardJenis.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BenefitJenis.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TransaksiNoOutlet.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TransaksiJenis.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TransaksiNoOrder.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TransaksiNilai.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.CabangTransaksi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewCabangTransaksi, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ButBatal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents KodePenukaran As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ButClear As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents RewardJumlah As DevExpress.XtraEditors.TextEdit
    Friend WithEvents BenefitNama As DevExpress.XtraEditors.TextEdit
    Friend WithEvents RewardNama As DevExpress.XtraEditors.TextEdit
    Friend WithEvents RewardJenis As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents BenefitJenis As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TransaksiNoOutlet As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TransaksiJenis As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents TransaksiNoOrder As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TransaksiNilai As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents LineShape2 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CabangTransaksi As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents ViewCabangTransaksi As DevExpress.XtraGrid.Views.Grid.GridView
End Class
