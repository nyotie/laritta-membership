﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CheckMember
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.KodeMember = New DevExpress.XtraEditors.TextEdit()
        Me.ButClear = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.TipeMember = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.KategoriMember = New DevExpress.XtraEditors.TextEdit()
        Me.CustName = New DevExpress.XtraEditors.TextEdit()
        Me.CustBday = New DevExpress.XtraEditors.TextEdit()
        Me.CustAddress = New DevExpress.XtraEditors.TextEdit()
        Me.CustCity = New DevExpress.XtraEditors.TextEdit()
        Me.CustPhoneHome = New DevExpress.XtraEditors.TextEdit()
        Me.CustPhoneMobile = New DevExpress.XtraEditors.TextEdit()
        Me.CustJob = New DevExpress.XtraEditors.TextEdit()
        Me.LineShape1 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage()
        Me.ShapeContainer2 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.MemberJmlTransOrder = New DevExpress.XtraEditors.TextEdit()
        Me.MemberTotalTransOrder = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.MemberJmlTransOutlet = New DevExpress.XtraEditors.TextEdit()
        Me.MemberTotalTransOutlet = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.MemberTotalTrans = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.MemberJmlTrans = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.MemberEmoney = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.MemberStamp = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.MemberPoint = New DevExpress.XtraEditors.TextEdit()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.LineShape4 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.LineShape3 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.LineShape2 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.XtraTabPage3 = New DevExpress.XtraTab.XtraTabPage()
        Me.GridGiftMember = New DevExpress.XtraGrid.GridControl()
        Me.ViewGiftMember = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.XtraTabPage4 = New DevExpress.XtraTab.XtraTabPage()
        Me.GridPromoMember = New DevExpress.XtraGrid.GridControl()
        Me.ViewPromoMember = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.ButUpgradeMember = New DevExpress.XtraEditors.SimpleButton()
        Me.ButPemberianPointStamp = New DevExpress.XtraEditors.SimpleButton()
        Me.ButPenukaranPointStamp = New DevExpress.XtraEditors.SimpleButton()
        Me.ButPenukaranPromoGift = New DevExpress.XtraEditors.SimpleButton()
        Me.ButBlessing = New DevExpress.XtraEditors.SimpleButton()
        Me.ButClose = New DevExpress.XtraEditors.SimpleButton()
        Me.ButVoidGift = New DevExpress.XtraEditors.SimpleButton()
        Me.ButVoidPoint = New DevExpress.XtraEditors.SimpleButton()
        Me.RibbonControl1 = New DevExpress.XtraBars.Ribbon.RibbonControl()
        Me.CardProgressStatus = New DevExpress.XtraBars.BarStaticItem()
        Me.CardProgressBar = New DevExpress.XtraBars.BarEditItem()
        Me.RepositoryItemProgressBar1 = New DevExpress.XtraEditors.Repository.RepositoryItemProgressBar()
        Me.RibbonStatusBar1 = New DevExpress.XtraBars.Ribbon.RibbonStatusBar()
        Me.CustNotes = New DevExpress.XtraEditors.MemoEdit()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.KodeMember.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TipeMember.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.KategoriMember.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CustName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CustBday.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CustAddress.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CustCity.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CustPhoneHome.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CustPhoneMobile.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CustJob.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.MemberJmlTransOrder.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemberTotalTransOrder.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemberJmlTransOutlet.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemberTotalTransOutlet.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemberTotalTrans.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemberJmlTrans.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemberEmoney.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemberStamp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemberPoint.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage3.SuspendLayout()
        CType(Me.GridGiftMember, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewGiftMember, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage4.SuspendLayout()
        CType(Me.GridPromoMember, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewPromoMember, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RibbonControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemProgressBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CustNotes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Location = New System.Drawing.Point(19, 8)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(131, 23)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Kode Member"
        '
        'PanelControl1
        '
        Me.PanelControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelControl1.Controls.Add(Me.KodeMember)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Controls.Add(Me.ButClear)
        Me.PanelControl1.Location = New System.Drawing.Point(12, 12)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(602, 39)
        Me.PanelControl1.TabIndex = 1
        '
        'KodeMember
        '
        Me.KodeMember.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.KodeMember.Location = New System.Drawing.Point(156, 5)
        Me.KodeMember.Name = "KodeMember"
        Me.KodeMember.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.KodeMember.Properties.Appearance.Options.UseFont = True
        Me.KodeMember.Properties.Mask.EditMask = "[0-9]{8} [0-9]{4}"
        Me.KodeMember.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.KodeMember.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.KodeMember.Size = New System.Drawing.Size(353, 29)
        Me.KodeMember.TabIndex = 1
        '
        'ButClear
        '
        Me.ButClear.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButClear.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButClear.Appearance.Options.UseFont = True
        Me.ButClear.Location = New System.Drawing.Point(515, 5)
        Me.ButClear.Name = "ButClear"
        Me.ButClear.Size = New System.Drawing.Size(82, 29)
        Me.ButClear.TabIndex = 2
        Me.ButClear.Text = "&Clear"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl2.Location = New System.Drawing.Point(37, 108)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(33, 14)
        Me.LabelControl2.TabIndex = 0
        Me.LabelControl2.Text = "Ultah"
        '
        'TipeMember
        '
        Me.TipeMember.Location = New System.Drawing.Point(124, 14)
        Me.TipeMember.Name = "TipeMember"
        Me.TipeMember.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.TipeMember.Properties.Appearance.Options.UseFont = True
        Me.TipeMember.Properties.ReadOnly = True
        Me.TipeMember.Size = New System.Drawing.Size(226, 21)
        Me.TipeMember.TabIndex = 4
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl3.Location = New System.Drawing.Point(37, 81)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(36, 14)
        Me.LabelControl3.TabIndex = 0
        Me.LabelControl3.Text = "Nama"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl4.Location = New System.Drawing.Point(37, 135)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(43, 14)
        Me.LabelControl4.TabIndex = 0
        Me.LabelControl4.Text = "Alamat"
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl5.Location = New System.Drawing.Point(37, 162)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(29, 14)
        Me.LabelControl5.TabIndex = 0
        Me.LabelControl5.Text = "Kota"
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl6.Location = New System.Drawing.Point(37, 189)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(70, 14)
        Me.LabelControl6.TabIndex = 0
        Me.LabelControl6.Text = "Telp rumah"
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl7.Location = New System.Drawing.Point(37, 216)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(47, 14)
        Me.LabelControl7.TabIndex = 0
        Me.LabelControl7.Text = "Telp HP"
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl8.Location = New System.Drawing.Point(37, 243)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(64, 14)
        Me.LabelControl8.TabIndex = 0
        Me.LabelControl8.Text = "Pekerjaan"
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl9.Location = New System.Drawing.Point(16, 44)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(53, 14)
        Me.LabelControl9.TabIndex = 0
        Me.LabelControl9.Text = "Kategori"
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl10.Location = New System.Drawing.Point(16, 17)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(80, 14)
        Me.LabelControl10.TabIndex = 0
        Me.LabelControl10.Text = "Tipe Member"
        '
        'KategoriMember
        '
        Me.KategoriMember.Location = New System.Drawing.Point(124, 41)
        Me.KategoriMember.Name = "KategoriMember"
        Me.KategoriMember.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.KategoriMember.Properties.Appearance.Options.UseFont = True
        Me.KategoriMember.Properties.ReadOnly = True
        Me.KategoriMember.Size = New System.Drawing.Size(226, 21)
        Me.KategoriMember.TabIndex = 5
        '
        'CustName
        '
        Me.CustName.Location = New System.Drawing.Point(145, 78)
        Me.CustName.Name = "CustName"
        Me.CustName.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.CustName.Properties.Appearance.Options.UseFont = True
        Me.CustName.Properties.ReadOnly = True
        Me.CustName.Size = New System.Drawing.Size(226, 21)
        Me.CustName.TabIndex = 6
        '
        'CustBday
        '
        Me.CustBday.Location = New System.Drawing.Point(145, 105)
        Me.CustBday.Name = "CustBday"
        Me.CustBday.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.CustBday.Properties.Appearance.Options.UseFont = True
        Me.CustBday.Properties.Mask.EditMask = "dd MMMM yyyy"
        Me.CustBday.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.CustBday.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.CustBday.Properties.ReadOnly = True
        Me.CustBday.Size = New System.Drawing.Size(178, 21)
        Me.CustBday.TabIndex = 7
        '
        'CustAddress
        '
        Me.CustAddress.Location = New System.Drawing.Point(145, 132)
        Me.CustAddress.Name = "CustAddress"
        Me.CustAddress.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.CustAddress.Properties.Appearance.Options.UseFont = True
        Me.CustAddress.Properties.ReadOnly = True
        Me.CustAddress.Size = New System.Drawing.Size(317, 21)
        Me.CustAddress.TabIndex = 8
        '
        'CustCity
        '
        Me.CustCity.Location = New System.Drawing.Point(145, 159)
        Me.CustCity.Name = "CustCity"
        Me.CustCity.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.CustCity.Properties.Appearance.Options.UseFont = True
        Me.CustCity.Properties.ReadOnly = True
        Me.CustCity.Size = New System.Drawing.Size(178, 21)
        Me.CustCity.TabIndex = 9
        '
        'CustPhoneHome
        '
        Me.CustPhoneHome.Location = New System.Drawing.Point(145, 186)
        Me.CustPhoneHome.Name = "CustPhoneHome"
        Me.CustPhoneHome.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.CustPhoneHome.Properties.Appearance.Options.UseFont = True
        Me.CustPhoneHome.Properties.ReadOnly = True
        Me.CustPhoneHome.Size = New System.Drawing.Size(226, 21)
        Me.CustPhoneHome.TabIndex = 10
        '
        'CustPhoneMobile
        '
        Me.CustPhoneMobile.Location = New System.Drawing.Point(145, 213)
        Me.CustPhoneMobile.Name = "CustPhoneMobile"
        Me.CustPhoneMobile.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.CustPhoneMobile.Properties.Appearance.Options.UseFont = True
        Me.CustPhoneMobile.Properties.ReadOnly = True
        Me.CustPhoneMobile.Size = New System.Drawing.Size(226, 21)
        Me.CustPhoneMobile.TabIndex = 11
        '
        'CustJob
        '
        Me.CustJob.Location = New System.Drawing.Point(145, 240)
        Me.CustJob.Name = "CustJob"
        Me.CustJob.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.CustJob.Properties.Appearance.Options.UseFont = True
        Me.CustJob.Properties.ReadOnly = True
        Me.CustJob.Size = New System.Drawing.Size(178, 21)
        Me.CustJob.TabIndex = 12
        '
        'LineShape1
        '
        Me.LineShape1.BorderColor = System.Drawing.SystemColors.ButtonShadow
        Me.LineShape1.Name = "LineShape1"
        Me.LineShape1.X1 = 17
        Me.LineShape1.X2 = 527
        Me.LineShape1.Y1 = 69
        Me.LineShape1.Y2 = 69
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.XtraTabControl1.AppearancePage.Header.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.XtraTabControl1.AppearancePage.Header.Options.UseFont = True
        Me.XtraTabControl1.Location = New System.Drawing.Point(14, 57)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage1
        Me.XtraTabControl1.Size = New System.Drawing.Size(600, 441)
        Me.XtraTabControl1.TabIndex = 3
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2, Me.XtraTabPage3, Me.XtraTabPage4})
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.LabelControl22)
        Me.XtraTabPage1.Controls.Add(Me.CustNotes)
        Me.XtraTabPage1.Controls.Add(Me.CustJob)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl10)
        Me.XtraTabPage1.Controls.Add(Me.CustPhoneMobile)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl2)
        Me.XtraTabPage1.Controls.Add(Me.CustPhoneHome)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl4)
        Me.XtraTabPage1.Controls.Add(Me.CustCity)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl5)
        Me.XtraTabPage1.Controls.Add(Me.CustAddress)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl6)
        Me.XtraTabPage1.Controls.Add(Me.CustBday)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl7)
        Me.XtraTabPage1.Controls.Add(Me.CustName)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl8)
        Me.XtraTabPage1.Controls.Add(Me.KategoriMember)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl9)
        Me.XtraTabPage1.Controls.Add(Me.TipeMember)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl3)
        Me.XtraTabPage1.Controls.Add(Me.ShapeContainer2)
        Me.XtraTabPage1.Image = Global.Laritta_Membership.My.Resources.Resources.info_cust_32
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(594, 396)
        Me.XtraTabPage1.Text = "Info Customer"
        '
        'ShapeContainer2
        '
        Me.ShapeContainer2.Location = New System.Drawing.Point(0, 0)
        Me.ShapeContainer2.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer2.Name = "ShapeContainer2"
        Me.ShapeContainer2.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape1})
        Me.ShapeContainer2.Size = New System.Drawing.Size(594, 396)
        Me.ShapeContainer2.TabIndex = 2
        Me.ShapeContainer2.TabStop = False
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.MemberJmlTransOrder)
        Me.XtraTabPage2.Controls.Add(Me.MemberTotalTransOrder)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl20)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl21)
        Me.XtraTabPage2.Controls.Add(Me.MemberJmlTransOutlet)
        Me.XtraTabPage2.Controls.Add(Me.MemberTotalTransOutlet)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl14)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl15)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl11)
        Me.XtraTabPage2.Controls.Add(Me.MemberTotalTrans)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl19)
        Me.XtraTabPage2.Controls.Add(Me.MemberJmlTrans)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl18)
        Me.XtraTabPage2.Controls.Add(Me.MemberEmoney)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl13)
        Me.XtraTabPage2.Controls.Add(Me.MemberStamp)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl12)
        Me.XtraTabPage2.Controls.Add(Me.MemberPoint)
        Me.XtraTabPage2.Controls.Add(Me.ShapeContainer1)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl16)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl17)
        Me.XtraTabPage2.Image = Global.Laritta_Membership.My.Resources.Resources.info_member_32
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.Size = New System.Drawing.Size(594, 396)
        Me.XtraTabPage2.Text = "Info Membership"
        '
        'MemberJmlTransOrder
        '
        Me.MemberJmlTransOrder.Location = New System.Drawing.Point(140, 322)
        Me.MemberJmlTransOrder.Name = "MemberJmlTransOrder"
        Me.MemberJmlTransOrder.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.MemberJmlTransOrder.Properties.Appearance.Options.UseFont = True
        Me.MemberJmlTransOrder.Properties.Appearance.Options.UseTextOptions = True
        Me.MemberJmlTransOrder.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.MemberJmlTransOrder.Properties.Mask.EditMask = "n0"
        Me.MemberJmlTransOrder.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.MemberJmlTransOrder.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.MemberJmlTransOrder.Properties.NullText = "0"
        Me.MemberJmlTransOrder.Properties.ReadOnly = True
        Me.MemberJmlTransOrder.Size = New System.Drawing.Size(121, 21)
        Me.MemberJmlTransOrder.TabIndex = 11
        '
        'MemberTotalTransOrder
        '
        Me.MemberTotalTransOrder.Location = New System.Drawing.Point(140, 349)
        Me.MemberTotalTransOrder.Name = "MemberTotalTransOrder"
        Me.MemberTotalTransOrder.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.MemberTotalTransOrder.Properties.Appearance.Options.UseFont = True
        Me.MemberTotalTransOrder.Properties.Appearance.Options.UseTextOptions = True
        Me.MemberTotalTransOrder.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.MemberTotalTransOrder.Properties.Mask.EditMask = "n2"
        Me.MemberTotalTransOrder.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.MemberTotalTransOrder.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.MemberTotalTransOrder.Properties.NullText = "0.00"
        Me.MemberTotalTransOrder.Properties.ReadOnly = True
        Me.MemberTotalTransOrder.Size = New System.Drawing.Size(121, 21)
        Me.MemberTotalTransOrder.TabIndex = 12
        '
        'LabelControl20
        '
        Me.LabelControl20.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl20.Location = New System.Drawing.Point(25, 352)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(100, 14)
        Me.LabelControl20.TabIndex = 2
        Me.LabelControl20.Text = "Total pembelian"
        '
        'LabelControl21
        '
        Me.LabelControl21.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl21.Location = New System.Drawing.Point(25, 325)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(105, 14)
        Me.LabelControl21.TabIndex = 2
        Me.LabelControl21.Text = "Jumlah transaksi"
        '
        'MemberJmlTransOutlet
        '
        Me.MemberJmlTransOutlet.Location = New System.Drawing.Point(140, 228)
        Me.MemberJmlTransOutlet.Name = "MemberJmlTransOutlet"
        Me.MemberJmlTransOutlet.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.MemberJmlTransOutlet.Properties.Appearance.Options.UseFont = True
        Me.MemberJmlTransOutlet.Properties.Appearance.Options.UseTextOptions = True
        Me.MemberJmlTransOutlet.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.MemberJmlTransOutlet.Properties.Mask.EditMask = "n0"
        Me.MemberJmlTransOutlet.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.MemberJmlTransOutlet.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.MemberJmlTransOutlet.Properties.NullText = "0"
        Me.MemberJmlTransOutlet.Properties.ReadOnly = True
        Me.MemberJmlTransOutlet.Size = New System.Drawing.Size(121, 21)
        Me.MemberJmlTransOutlet.TabIndex = 9
        '
        'MemberTotalTransOutlet
        '
        Me.MemberTotalTransOutlet.Location = New System.Drawing.Point(140, 255)
        Me.MemberTotalTransOutlet.Name = "MemberTotalTransOutlet"
        Me.MemberTotalTransOutlet.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.MemberTotalTransOutlet.Properties.Appearance.Options.UseFont = True
        Me.MemberTotalTransOutlet.Properties.Appearance.Options.UseTextOptions = True
        Me.MemberTotalTransOutlet.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.MemberTotalTransOutlet.Properties.Mask.EditMask = "n2"
        Me.MemberTotalTransOutlet.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.MemberTotalTransOutlet.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.MemberTotalTransOutlet.Properties.NullText = "0.00"
        Me.MemberTotalTransOutlet.Properties.ReadOnly = True
        Me.MemberTotalTransOutlet.Size = New System.Drawing.Size(121, 21)
        Me.MemberTotalTransOutlet.TabIndex = 10
        '
        'LabelControl14
        '
        Me.LabelControl14.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl14.Location = New System.Drawing.Point(25, 231)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(105, 14)
        Me.LabelControl14.TabIndex = 2
        Me.LabelControl14.Text = "Jumlah transaksi"
        '
        'LabelControl15
        '
        Me.LabelControl15.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl15.Location = New System.Drawing.Point(25, 258)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(100, 14)
        Me.LabelControl15.TabIndex = 2
        Me.LabelControl15.Text = "Total pembelian"
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl11.Location = New System.Drawing.Point(16, 17)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(32, 14)
        Me.LabelControl11.TabIndex = 3
        Me.LabelControl11.Text = "Point"
        '
        'MemberTotalTrans
        '
        Me.MemberTotalTrans.Location = New System.Drawing.Point(140, 154)
        Me.MemberTotalTrans.Name = "MemberTotalTrans"
        Me.MemberTotalTrans.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.MemberTotalTrans.Properties.Appearance.Options.UseFont = True
        Me.MemberTotalTrans.Properties.Appearance.Options.UseTextOptions = True
        Me.MemberTotalTrans.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.MemberTotalTrans.Properties.Mask.EditMask = "n2"
        Me.MemberTotalTrans.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.MemberTotalTrans.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.MemberTotalTrans.Properties.NullText = "0.00"
        Me.MemberTotalTrans.Properties.ReadOnly = True
        Me.MemberTotalTrans.Size = New System.Drawing.Size(121, 21)
        Me.MemberTotalTrans.TabIndex = 8
        '
        'LabelControl19
        '
        Me.LabelControl19.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl19.Location = New System.Drawing.Point(16, 157)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(100, 14)
        Me.LabelControl19.TabIndex = 2
        Me.LabelControl19.Text = "Total pembelian"
        '
        'MemberJmlTrans
        '
        Me.MemberJmlTrans.Location = New System.Drawing.Point(140, 127)
        Me.MemberJmlTrans.Name = "MemberJmlTrans"
        Me.MemberJmlTrans.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.MemberJmlTrans.Properties.Appearance.Options.UseFont = True
        Me.MemberJmlTrans.Properties.Appearance.Options.UseTextOptions = True
        Me.MemberJmlTrans.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.MemberJmlTrans.Properties.Mask.EditMask = "n0"
        Me.MemberJmlTrans.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.MemberJmlTrans.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.MemberJmlTrans.Properties.NullText = "0"
        Me.MemberJmlTrans.Properties.ReadOnly = True
        Me.MemberJmlTrans.Size = New System.Drawing.Size(121, 21)
        Me.MemberJmlTrans.TabIndex = 7
        '
        'LabelControl18
        '
        Me.LabelControl18.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl18.Location = New System.Drawing.Point(16, 130)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(105, 14)
        Me.LabelControl18.TabIndex = 2
        Me.LabelControl18.Text = "Jumlah transaksi"
        '
        'MemberEmoney
        '
        Me.MemberEmoney.Location = New System.Drawing.Point(140, 68)
        Me.MemberEmoney.Name = "MemberEmoney"
        Me.MemberEmoney.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.MemberEmoney.Properties.Appearance.Options.UseFont = True
        Me.MemberEmoney.Properties.Appearance.Options.UseTextOptions = True
        Me.MemberEmoney.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.MemberEmoney.Properties.Mask.EditMask = "n2"
        Me.MemberEmoney.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.MemberEmoney.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.MemberEmoney.Properties.NullText = "0.00"
        Me.MemberEmoney.Properties.ReadOnly = True
        Me.MemberEmoney.Size = New System.Drawing.Size(226, 21)
        Me.MemberEmoney.TabIndex = 6
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl13.Location = New System.Drawing.Point(16, 71)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(55, 14)
        Me.LabelControl13.TabIndex = 2
        Me.LabelControl13.Text = "E-money"
        '
        'MemberStamp
        '
        Me.MemberStamp.Location = New System.Drawing.Point(140, 41)
        Me.MemberStamp.Name = "MemberStamp"
        Me.MemberStamp.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.MemberStamp.Properties.Appearance.Options.UseFont = True
        Me.MemberStamp.Properties.Appearance.Options.UseTextOptions = True
        Me.MemberStamp.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.MemberStamp.Properties.Mask.EditMask = "n0"
        Me.MemberStamp.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.MemberStamp.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.MemberStamp.Properties.NullText = "0"
        Me.MemberStamp.Properties.ReadOnly = True
        Me.MemberStamp.Size = New System.Drawing.Size(226, 21)
        Me.MemberStamp.TabIndex = 5
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl12.Location = New System.Drawing.Point(16, 44)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(40, 14)
        Me.LabelControl12.TabIndex = 2
        Me.LabelControl12.Text = "Stamp"
        '
        'MemberPoint
        '
        Me.MemberPoint.Location = New System.Drawing.Point(140, 14)
        Me.MemberPoint.Name = "MemberPoint"
        Me.MemberPoint.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.MemberPoint.Properties.Appearance.Options.UseFont = True
        Me.MemberPoint.Properties.Appearance.Options.UseTextOptions = True
        Me.MemberPoint.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.MemberPoint.Properties.Mask.EditMask = "n2"
        Me.MemberPoint.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.MemberPoint.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.MemberPoint.Properties.NullText = "0.00"
        Me.MemberPoint.Properties.ReadOnly = True
        Me.MemberPoint.Size = New System.Drawing.Size(226, 21)
        Me.MemberPoint.TabIndex = 4
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(0, 0)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape4, Me.LineShape3, Me.LineShape2})
        Me.ShapeContainer1.Size = New System.Drawing.Size(594, 396)
        Me.ShapeContainer1.TabIndex = 6
        Me.ShapeContainer1.TabStop = False
        '
        'LineShape4
        '
        Me.LineShape4.BorderColor = System.Drawing.SystemColors.ButtonShadow
        Me.LineShape4.Name = "LineShape4"
        Me.LineShape4.X1 = 25
        Me.LineShape4.X2 = 530
        Me.LineShape4.Y1 = 315
        Me.LineShape4.Y2 = 315
        '
        'LineShape3
        '
        Me.LineShape3.BorderColor = System.Drawing.SystemColors.ButtonShadow
        Me.LineShape3.Name = "LineShape3"
        Me.LineShape3.X1 = 25
        Me.LineShape3.X2 = 530
        Me.LineShape3.Y1 = 220
        Me.LineShape3.Y2 = 220
        '
        'LineShape2
        '
        Me.LineShape2.BorderColor = System.Drawing.SystemColors.ButtonShadow
        Me.LineShape2.Name = "LineShape2"
        Me.LineShape2.X1 = 15
        Me.LineShape2.X2 = 530
        Me.LineShape2.Y1 = 108
        Me.LineShape2.Y2 = 108
        '
        'LabelControl16
        '
        Me.LabelControl16.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl16.Location = New System.Drawing.Point(25, 206)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(39, 14)
        Me.LabelControl16.TabIndex = 2
        Me.LabelControl16.Text = "Outlet"
        '
        'LabelControl17
        '
        Me.LabelControl17.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl17.Location = New System.Drawing.Point(25, 301)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(36, 14)
        Me.LabelControl17.TabIndex = 7
        Me.LabelControl17.Text = "Order"
        '
        'XtraTabPage3
        '
        Me.XtraTabPage3.Controls.Add(Me.GridGiftMember)
        Me.XtraTabPage3.Image = Global.Laritta_Membership.My.Resources.Resources.own_gift_32
        Me.XtraTabPage3.Name = "XtraTabPage3"
        Me.XtraTabPage3.Size = New System.Drawing.Size(594, 396)
        Me.XtraTabPage3.Text = "Benefit Gift"
        '
        'GridGiftMember
        '
        Me.GridGiftMember.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridGiftMember.Location = New System.Drawing.Point(3, 3)
        Me.GridGiftMember.MainView = Me.ViewGiftMember
        Me.GridGiftMember.Name = "GridGiftMember"
        Me.GridGiftMember.Size = New System.Drawing.Size(588, 390)
        Me.GridGiftMember.TabIndex = 4
        Me.GridGiftMember.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.ViewGiftMember})
        '
        'ViewGiftMember
        '
        Me.ViewGiftMember.Appearance.HeaderPanel.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ViewGiftMember.Appearance.HeaderPanel.Options.UseFont = True
        Me.ViewGiftMember.Appearance.Row.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ViewGiftMember.Appearance.Row.Options.UseFont = True
        Me.ViewGiftMember.ColumnPanelRowHeight = 40
        Me.ViewGiftMember.GridControl = Me.GridGiftMember
        Me.ViewGiftMember.Name = "ViewGiftMember"
        Me.ViewGiftMember.OptionsBehavior.Editable = False
        Me.ViewGiftMember.OptionsCustomization.AllowColumnMoving = False
        Me.ViewGiftMember.OptionsCustomization.AllowFilter = False
        Me.ViewGiftMember.OptionsCustomization.AllowGroup = False
        Me.ViewGiftMember.OptionsCustomization.AllowQuickHideColumns = False
        Me.ViewGiftMember.OptionsView.EnableAppearanceEvenRow = True
        Me.ViewGiftMember.OptionsView.ShowFooter = True
        Me.ViewGiftMember.OptionsView.ShowGroupPanel = False
        Me.ViewGiftMember.RowHeight = 30
        '
        'XtraTabPage4
        '
        Me.XtraTabPage4.Controls.Add(Me.GridPromoMember)
        Me.XtraTabPage4.Image = Global.Laritta_Membership.My.Resources.Resources.own_promo_32
        Me.XtraTabPage4.Name = "XtraTabPage4"
        Me.XtraTabPage4.Size = New System.Drawing.Size(594, 396)
        Me.XtraTabPage4.Text = "Benefit Promo"
        '
        'GridPromoMember
        '
        Me.GridPromoMember.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridPromoMember.Location = New System.Drawing.Point(3, 3)
        Me.GridPromoMember.MainView = Me.ViewPromoMember
        Me.GridPromoMember.Name = "GridPromoMember"
        Me.GridPromoMember.Size = New System.Drawing.Size(588, 390)
        Me.GridPromoMember.TabIndex = 4
        Me.GridPromoMember.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.ViewPromoMember})
        '
        'ViewPromoMember
        '
        Me.ViewPromoMember.Appearance.HeaderPanel.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ViewPromoMember.Appearance.HeaderPanel.Options.UseFont = True
        Me.ViewPromoMember.Appearance.Row.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ViewPromoMember.Appearance.Row.Options.UseFont = True
        Me.ViewPromoMember.ColumnPanelRowHeight = 40
        Me.ViewPromoMember.GridControl = Me.GridPromoMember
        Me.ViewPromoMember.Name = "ViewPromoMember"
        Me.ViewPromoMember.OptionsBehavior.Editable = False
        Me.ViewPromoMember.OptionsCustomization.AllowColumnMoving = False
        Me.ViewPromoMember.OptionsCustomization.AllowFilter = False
        Me.ViewPromoMember.OptionsCustomization.AllowGroup = False
        Me.ViewPromoMember.OptionsCustomization.AllowQuickHideColumns = False
        Me.ViewPromoMember.OptionsView.EnableAppearanceEvenRow = True
        Me.ViewPromoMember.OptionsView.ShowFooter = True
        Me.ViewPromoMember.OptionsView.ShowGroupPanel = False
        Me.ViewPromoMember.RowHeight = 30
        '
        'ButUpgradeMember
        '
        Me.ButUpgradeMember.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButUpgradeMember.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButUpgradeMember.Appearance.Options.UseFont = True
        Me.ButUpgradeMember.Image = Global.Laritta_Membership.My.Resources.Resources.member_32
        Me.ButUpgradeMember.Location = New System.Drawing.Point(620, 12)
        Me.ButUpgradeMember.Name = "ButUpgradeMember"
        Me.ButUpgradeMember.Size = New System.Drawing.Size(192, 55)
        Me.ButUpgradeMember.TabIndex = 13
        Me.ButUpgradeMember.Text = "&1 Upgrade" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Member"
        '
        'ButPemberianPointStamp
        '
        Me.ButPemberianPointStamp.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButPemberianPointStamp.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButPemberianPointStamp.Appearance.Options.UseFont = True
        Me.ButPemberianPointStamp.Image = Global.Laritta_Membership.My.Resources.Resources.get_pas_32
        Me.ButPemberianPointStamp.Location = New System.Drawing.Point(620, 73)
        Me.ButPemberianPointStamp.Name = "ButPemberianPointStamp"
        Me.ButPemberianPointStamp.Size = New System.Drawing.Size(192, 55)
        Me.ButPemberianPointStamp.TabIndex = 14
        Me.ButPemberianPointStamp.Text = "&2 Pemberian" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Point && Stamp"
        '
        'ButPenukaranPointStamp
        '
        Me.ButPenukaranPointStamp.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButPenukaranPointStamp.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButPenukaranPointStamp.Appearance.Options.UseFont = True
        Me.ButPenukaranPointStamp.Enabled = False
        Me.ButPenukaranPointStamp.Image = Global.Laritta_Membership.My.Resources.Resources.exc_pas_32
        Me.ButPenukaranPointStamp.Location = New System.Drawing.Point(620, 134)
        Me.ButPenukaranPointStamp.Name = "ButPenukaranPointStamp"
        Me.ButPenukaranPointStamp.Size = New System.Drawing.Size(192, 55)
        Me.ButPenukaranPointStamp.TabIndex = 15
        Me.ButPenukaranPointStamp.Text = "&3 Penukaran" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Point && Stamp"
        '
        'ButPenukaranPromoGift
        '
        Me.ButPenukaranPromoGift.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButPenukaranPromoGift.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButPenukaranPromoGift.Appearance.Options.UseFont = True
        Me.ButPenukaranPromoGift.Enabled = False
        Me.ButPenukaranPromoGift.Image = Global.Laritta_Membership.My.Resources.Resources.exc_gift_32
        Me.ButPenukaranPromoGift.Location = New System.Drawing.Point(620, 195)
        Me.ButPenukaranPromoGift.Name = "ButPenukaranPromoGift"
        Me.ButPenukaranPromoGift.Size = New System.Drawing.Size(192, 55)
        Me.ButPenukaranPromoGift.TabIndex = 16
        Me.ButPenukaranPromoGift.Text = "&4 Penukaran" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Promo && Gift"
        '
        'ButBlessing
        '
        Me.ButBlessing.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButBlessing.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButBlessing.Appearance.Options.UseFont = True
        Me.ButBlessing.Image = Global.Laritta_Membership.My.Resources.Resources.blessing_32
        Me.ButBlessing.Location = New System.Drawing.Point(620, 256)
        Me.ButBlessing.Name = "ButBlessing"
        Me.ButBlessing.Size = New System.Drawing.Size(192, 55)
        Me.ButBlessing.TabIndex = 17
        Me.ButBlessing.Text = "&5 Blessings"
        '
        'ButClose
        '
        Me.ButClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButClose.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButClose.Appearance.Options.UseFont = True
        Me.ButClose.Image = Global.Laritta_Membership.My.Resources.Resources.close_32
        Me.ButClose.Location = New System.Drawing.Point(620, 439)
        Me.ButClose.Name = "ButClose"
        Me.ButClose.Size = New System.Drawing.Size(192, 55)
        Me.ButClose.TabIndex = 20
        Me.ButClose.Text = "&8 Tutup"
        '
        'ButVoidGift
        '
        Me.ButVoidGift.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButVoidGift.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButVoidGift.Appearance.Options.UseFont = True
        Me.ButVoidGift.Image = Global.Laritta_Membership.My.Resources.Resources.void_gift_32
        Me.ButVoidGift.Location = New System.Drawing.Point(620, 378)
        Me.ButVoidGift.Name = "ButVoidGift"
        Me.ButVoidGift.Size = New System.Drawing.Size(192, 55)
        Me.ButVoidGift.TabIndex = 19
        Me.ButVoidGift.Text = "&7 Void Gift"
        '
        'ButVoidPoint
        '
        Me.ButVoidPoint.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButVoidPoint.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButVoidPoint.Appearance.Options.UseFont = True
        Me.ButVoidPoint.Image = Global.Laritta_Membership.My.Resources.Resources.void_point_32
        Me.ButVoidPoint.Location = New System.Drawing.Point(620, 317)
        Me.ButVoidPoint.Name = "ButVoidPoint"
        Me.ButVoidPoint.Size = New System.Drawing.Size(192, 55)
        Me.ButVoidPoint.TabIndex = 18
        Me.ButVoidPoint.Text = "&6 Void" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Penukaran Point"
        '
        'RibbonControl1
        '
        Me.RibbonControl1.ApplicationButtonText = Nothing
        '
        '
        '
        Me.RibbonControl1.ExpandCollapseItem.Id = 0
        Me.RibbonControl1.ExpandCollapseItem.Name = ""
        Me.RibbonControl1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.RibbonControl1.ExpandCollapseItem, Me.CardProgressStatus, Me.CardProgressBar})
        Me.RibbonControl1.Location = New System.Drawing.Point(0, 0)
        Me.RibbonControl1.MaxItemId = 3
        Me.RibbonControl1.Name = "RibbonControl1"
        Me.RibbonControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemProgressBar1})
        Me.RibbonControl1.Size = New System.Drawing.Size(824, 48)
        Me.RibbonControl1.StatusBar = Me.RibbonStatusBar1
        Me.RibbonControl1.Visible = False
        '
        'CardProgressStatus
        '
        Me.CardProgressStatus.Caption = "Waiting.."
        Me.CardProgressStatus.Id = 1
        Me.CardProgressStatus.Name = "CardProgressStatus"
        Me.CardProgressStatus.TextAlignment = System.Drawing.StringAlignment.Near
        '
        'CardProgressBar
        '
        Me.CardProgressBar.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.CardProgressBar.Edit = Me.RepositoryItemProgressBar1
        Me.CardProgressBar.EditValue = 0
        Me.CardProgressBar.Id = 2
        Me.CardProgressBar.Name = "CardProgressBar"
        Me.CardProgressBar.Width = 100
        '
        'RepositoryItemProgressBar1
        '
        Me.RepositoryItemProgressBar1.Name = "RepositoryItemProgressBar1"
        '
        'RibbonStatusBar1
        '
        Me.RibbonStatusBar1.ItemLinks.Add(Me.CardProgressStatus)
        Me.RibbonStatusBar1.ItemLinks.Add(Me.CardProgressBar)
        Me.RibbonStatusBar1.Location = New System.Drawing.Point(0, 504)
        Me.RibbonStatusBar1.Name = "RibbonStatusBar1"
        Me.RibbonStatusBar1.Ribbon = Me.RibbonControl1
        Me.RibbonStatusBar1.Size = New System.Drawing.Size(824, 27)
        '
        'CustNotes
        '
        Me.CustNotes.Location = New System.Drawing.Point(145, 267)
        Me.CustNotes.MenuManager = Me.RibbonControl1
        Me.CustNotes.Name = "CustNotes"
        Me.CustNotes.Properties.ReadOnly = True
        Me.CustNotes.Size = New System.Drawing.Size(317, 96)
        Me.CustNotes.TabIndex = 13
        '
        'LabelControl22
        '
        Me.LabelControl22.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl22.Location = New System.Drawing.Point(37, 269)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(51, 14)
        Me.LabelControl22.TabIndex = 0
        Me.LabelControl22.Text = "Catatan"
        '
        'CheckMember
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(824, 531)
        Me.Controls.Add(Me.RibbonStatusBar1)
        Me.Controls.Add(Me.ButVoidPoint)
        Me.Controls.Add(Me.ButVoidGift)
        Me.Controls.Add(Me.ButBlessing)
        Me.Controls.Add(Me.ButPenukaranPromoGift)
        Me.Controls.Add(Me.ButPenukaranPointStamp)
        Me.Controls.Add(Me.ButPemberianPointStamp)
        Me.Controls.Add(Me.ButUpgradeMember)
        Me.Controls.Add(Me.ButClose)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.Controls.Add(Me.PanelControl1)
        Me.Controls.Add(Me.RibbonControl1)
        Me.Name = "CheckMember"
        Me.Text = "CheckMember"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.KodeMember.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TipeMember.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.KategoriMember.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CustName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CustBday.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CustAddress.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CustCity.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CustPhoneHome.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CustPhoneMobile.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CustJob.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        Me.XtraTabPage1.PerformLayout()
        Me.XtraTabPage2.ResumeLayout(False)
        Me.XtraTabPage2.PerformLayout()
        CType(Me.MemberJmlTransOrder.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemberTotalTransOrder.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemberJmlTransOutlet.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemberTotalTransOutlet.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemberTotalTrans.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemberJmlTrans.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemberEmoney.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemberStamp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemberPoint.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage3.ResumeLayout(False)
        CType(Me.GridGiftMember, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewGiftMember, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage4.ResumeLayout(False)
        CType(Me.GridPromoMember, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewPromoMember, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RibbonControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemProgressBar1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CustNotes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents KodeMember As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TipeMember As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CustJob As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CustPhoneMobile As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CustPhoneHome As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CustCity As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CustAddress As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CustName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents KategoriMember As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LineShape1 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents ShapeContainer2 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents MemberTotalTrans As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents MemberJmlTrans As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents MemberEmoney As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents MemberStamp As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents MemberPoint As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents LineShape2 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents MemberJmlTransOrder As DevExpress.XtraEditors.TextEdit
    Friend WithEvents MemberTotalTransOrder As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents MemberJmlTransOutlet As DevExpress.XtraEditors.TextEdit
    Friend WithEvents MemberTotalTransOutlet As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LineShape4 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents LineShape3 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents XtraTabPage3 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage4 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GridGiftMember As DevExpress.XtraGrid.GridControl
    Friend WithEvents ViewGiftMember As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridPromoMember As DevExpress.XtraGrid.GridControl
    Friend WithEvents ViewPromoMember As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents ButClear As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButUpgradeMember As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButPemberianPointStamp As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButPenukaranPointStamp As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButPenukaranPromoGift As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButBlessing As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButClose As DevExpress.XtraEditors.SimpleButton
    Private WithEvents CustBday As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ButVoidGift As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButVoidPoint As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents RibbonControl1 As DevExpress.XtraBars.Ribbon.RibbonControl
    Friend WithEvents CardProgressStatus As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents CardProgressBar As DevExpress.XtraBars.BarEditItem
    Friend WithEvents RepositoryItemProgressBar1 As DevExpress.XtraEditors.Repository.RepositoryItemProgressBar
    Friend WithEvents RibbonStatusBar1 As DevExpress.XtraBars.Ribbon.RibbonStatusBar
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CustNotes As DevExpress.XtraEditors.MemoEdit
End Class
