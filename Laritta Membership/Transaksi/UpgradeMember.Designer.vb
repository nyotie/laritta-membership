﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UpgradeMember
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ButBatal = New DevExpress.XtraEditors.SimpleButton()
        Me.ButUpgrade = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.TotalBayar = New DevExpress.XtraEditors.TextEdit()
        Me.TotalBeli = New DevExpress.XtraEditors.TextEdit()
        Me.xTrans = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.LevelNow = New DevExpress.XtraEditors.TextEdit()
        Me.TipeMembershipNow = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.DaftarTipeMembership = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.ViewTipeMembership = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.HargaKartu = New DevExpress.XtraEditors.TextEdit()
        Me.SyaratKartu = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.TotalBayar.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TotalBeli.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xTrans.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LevelNow.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TipeMembershipNow.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DaftarTipeMembership.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewTipeMembership, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.HargaKartu.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SyaratKartu.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ButBatal
        '
        Me.ButBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButBatal.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButBatal.Appearance.Options.UseFont = True
        Me.ButBatal.Image = Global.Laritta_Membership.My.Resources.Resources.back_32
        Me.ButBatal.Location = New System.Drawing.Point(319, 330)
        Me.ButBatal.Name = "ButBatal"
        Me.ButBatal.Size = New System.Drawing.Size(90, 40)
        Me.ButBatal.TabIndex = 7
        Me.ButBatal.Text = "&Batal"
        '
        'ButUpgrade
        '
        Me.ButUpgrade.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButUpgrade.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.ButUpgrade.Appearance.Options.UseFont = True
        Me.ButUpgrade.Enabled = False
        Me.ButUpgrade.Image = Global.Laritta_Membership.My.Resources.Resources.member_upgrade_32
        Me.ButUpgrade.Location = New System.Drawing.Point(199, 330)
        Me.ButUpgrade.Name = "ButUpgrade"
        Me.ButUpgrade.Size = New System.Drawing.Size(114, 40)
        Me.ButUpgrade.TabIndex = 6
        Me.ButUpgrade.Text = "Upgrade"
        '
        'GroupControl2
        '
        Me.GroupControl2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl2.AppearanceCaption.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.GroupControl2.AppearanceCaption.Options.UseFont = True
        Me.GroupControl2.Controls.Add(Me.TotalBayar)
        Me.GroupControl2.Controls.Add(Me.TotalBeli)
        Me.GroupControl2.Controls.Add(Me.xTrans)
        Me.GroupControl2.Controls.Add(Me.LabelControl16)
        Me.GroupControl2.Controls.Add(Me.LabelControl9)
        Me.GroupControl2.Controls.Add(Me.LabelControl8)
        Me.GroupControl2.Controls.Add(Me.LabelControl15)
        Me.GroupControl2.Controls.Add(Me.LabelControl13)
        Me.GroupControl2.Controls.Add(Me.LabelControl10)
        Me.GroupControl2.Controls.Add(Me.LevelNow)
        Me.GroupControl2.Controls.Add(Me.TipeMembershipNow)
        Me.GroupControl2.Controls.Add(Me.LabelControl2)
        Me.GroupControl2.Controls.Add(Me.LabelControl1)
        Me.GroupControl2.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(397, 177)
        Me.GroupControl2.TabIndex = 5
        Me.GroupControl2.Text = "Kondisi saat ini"
        '
        'TotalBayar
        '
        Me.TotalBayar.Location = New System.Drawing.Point(215, 142)
        Me.TotalBayar.Name = "TotalBayar"
        Me.TotalBayar.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.TotalBayar.Properties.Appearance.Options.UseFont = True
        Me.TotalBayar.Properties.Appearance.Options.UseTextOptions = True
        Me.TotalBayar.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.TotalBayar.Properties.Mask.EditMask = "n2"
        Me.TotalBayar.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TotalBayar.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TotalBayar.Properties.ReadOnly = True
        Me.TotalBayar.Size = New System.Drawing.Size(126, 21)
        Me.TotalBayar.TabIndex = 106
        Me.TotalBayar.TabStop = False
        '
        'TotalBeli
        '
        Me.TotalBeli.Location = New System.Drawing.Point(215, 115)
        Me.TotalBeli.Name = "TotalBeli"
        Me.TotalBeli.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.TotalBeli.Properties.Appearance.Options.UseFont = True
        Me.TotalBeli.Properties.Appearance.Options.UseTextOptions = True
        Me.TotalBeli.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.TotalBeli.Properties.Mask.EditMask = "n2"
        Me.TotalBeli.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TotalBeli.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TotalBeli.Properties.ReadOnly = True
        Me.TotalBeli.Size = New System.Drawing.Size(126, 21)
        Me.TotalBeli.TabIndex = 108
        Me.TotalBeli.TabStop = False
        '
        'xTrans
        '
        Me.xTrans.Location = New System.Drawing.Point(187, 88)
        Me.xTrans.Name = "xTrans"
        Me.xTrans.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.xTrans.Properties.Appearance.Options.UseFont = True
        Me.xTrans.Properties.Appearance.Options.UseTextOptions = True
        Me.xTrans.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.xTrans.Properties.Mask.EditMask = "n0"
        Me.xTrans.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.xTrans.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.xTrans.Properties.ReadOnly = True
        Me.xTrans.Size = New System.Drawing.Size(55, 21)
        Me.xTrans.TabIndex = 107
        Me.xTrans.TabStop = False
        '
        'LabelControl16
        '
        Me.LabelControl16.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl16.Location = New System.Drawing.Point(20, 145)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(151, 14)
        Me.LabelControl16.TabIndex = 105
        Me.LabelControl16.Text = "Total transaksi terbayar"
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl9.Location = New System.Drawing.Point(20, 118)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(93, 14)
        Me.LabelControl9.TabIndex = 104
        Me.LabelControl9.Text = "Total transaksi"
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl8.Location = New System.Drawing.Point(20, 91)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(105, 14)
        Me.LabelControl8.TabIndex = 100
        Me.LabelControl8.Text = "Jumlah transaksi"
        '
        'LabelControl15
        '
        Me.LabelControl15.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl15.Location = New System.Drawing.Point(187, 145)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(20, 14)
        Me.LabelControl15.TabIndex = 103
        Me.LabelControl15.Text = "Rp."
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl13.Location = New System.Drawing.Point(248, 91)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl13.TabIndex = 102
        Me.LabelControl13.Text = "x"
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl10.Location = New System.Drawing.Point(187, 118)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(20, 14)
        Me.LabelControl10.TabIndex = 101
        Me.LabelControl10.Text = "Rp."
        '
        'LevelNow
        '
        Me.LevelNow.Location = New System.Drawing.Point(187, 61)
        Me.LevelNow.Name = "LevelNow"
        Me.LevelNow.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LevelNow.Properties.Appearance.Options.UseFont = True
        Me.LevelNow.Properties.ReadOnly = True
        Me.LevelNow.Size = New System.Drawing.Size(72, 21)
        Me.LevelNow.TabIndex = 99
        Me.LevelNow.TabStop = False
        '
        'TipeMembershipNow
        '
        Me.TipeMembershipNow.Location = New System.Drawing.Point(187, 34)
        Me.TipeMembershipNow.Name = "TipeMembershipNow"
        Me.TipeMembershipNow.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.TipeMembershipNow.Properties.Appearance.Options.UseFont = True
        Me.TipeMembershipNow.Properties.ReadOnly = True
        Me.TipeMembershipNow.Size = New System.Drawing.Size(192, 21)
        Me.TipeMembershipNow.TabIndex = 99
        Me.TipeMembershipNow.TabStop = False
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl2.Location = New System.Drawing.Point(20, 37)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(107, 14)
        Me.LabelControl2.TabIndex = 0
        Me.LabelControl2.Text = "Tipe membership"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl1.Location = New System.Drawing.Point(20, 64)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(33, 14)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Level"
        '
        'DaftarTipeMembership
        '
        Me.DaftarTipeMembership.Location = New System.Drawing.Point(186, 35)
        Me.DaftarTipeMembership.Name = "DaftarTipeMembership"
        Me.DaftarTipeMembership.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.DaftarTipeMembership.Properties.Appearance.Options.UseFont = True
        Me.DaftarTipeMembership.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DaftarTipeMembership.Properties.NullText = ""
        Me.DaftarTipeMembership.Properties.View = Me.ViewTipeMembership
        Me.DaftarTipeMembership.Size = New System.Drawing.Size(192, 21)
        Me.DaftarTipeMembership.TabIndex = 1
        '
        'ViewTipeMembership
        '
        Me.ViewTipeMembership.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.ViewTipeMembership.Name = "ViewTipeMembership"
        Me.ViewTipeMembership.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.ViewTipeMembership.OptionsView.ShowGroupPanel = False
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl7.Location = New System.Drawing.Point(19, 92)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(75, 14)
        Me.LabelControl7.TabIndex = 0
        Me.LabelControl7.Text = "Harga kartu"
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl6.Location = New System.Drawing.Point(19, 65)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(110, 14)
        Me.LabelControl6.TabIndex = 0
        Me.LabelControl6.Text = "Syarat pembelian"
        '
        'HargaKartu
        '
        Me.HargaKartu.Location = New System.Drawing.Point(214, 89)
        Me.HargaKartu.Name = "HargaKartu"
        Me.HargaKartu.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.HargaKartu.Properties.Appearance.Options.UseFont = True
        Me.HargaKartu.Properties.Appearance.Options.UseTextOptions = True
        Me.HargaKartu.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.HargaKartu.Properties.Mask.EditMask = "n2"
        Me.HargaKartu.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.HargaKartu.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.HargaKartu.Properties.ReadOnly = True
        Me.HargaKartu.Size = New System.Drawing.Size(126, 21)
        Me.HargaKartu.TabIndex = 99
        Me.HargaKartu.TabStop = False
        '
        'SyaratKartu
        '
        Me.SyaratKartu.Location = New System.Drawing.Point(214, 62)
        Me.SyaratKartu.Name = "SyaratKartu"
        Me.SyaratKartu.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.SyaratKartu.Properties.Appearance.Options.UseFont = True
        Me.SyaratKartu.Properties.Appearance.Options.UseTextOptions = True
        Me.SyaratKartu.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.SyaratKartu.Properties.Mask.EditMask = "n2"
        Me.SyaratKartu.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.SyaratKartu.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.SyaratKartu.Properties.ReadOnly = True
        Me.SyaratKartu.Size = New System.Drawing.Size(126, 21)
        Me.SyaratKartu.TabIndex = 99
        Me.SyaratKartu.TabStop = False
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl12.Location = New System.Drawing.Point(186, 92)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(20, 14)
        Me.LabelControl12.TabIndex = 0
        Me.LabelControl12.Text = "Rp."
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl11.Location = New System.Drawing.Point(186, 65)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(20, 14)
        Me.LabelControl11.TabIndex = 0
        Me.LabelControl11.Text = "Rp."
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl5.Location = New System.Drawing.Point(19, 38)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(107, 14)
        Me.LabelControl5.TabIndex = 0
        Me.LabelControl5.Text = "Tipe membership"
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.Controls.Add(Me.LabelControl5)
        Me.GroupControl1.Controls.Add(Me.LabelControl11)
        Me.GroupControl1.Controls.Add(Me.LabelControl12)
        Me.GroupControl1.Controls.Add(Me.SyaratKartu)
        Me.GroupControl1.Controls.Add(Me.HargaKartu)
        Me.GroupControl1.Controls.Add(Me.LabelControl6)
        Me.GroupControl1.Controls.Add(Me.LabelControl7)
        Me.GroupControl1.Controls.Add(Me.DaftarTipeMembership)
        Me.GroupControl1.Location = New System.Drawing.Point(13, 195)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(396, 129)
        Me.GroupControl1.TabIndex = 109
        Me.GroupControl1.Text = "Upgrade ke"
        '
        'UpgradeMember
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(421, 382)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.ButBatal)
        Me.Controls.Add(Me.ButUpgrade)
        Me.Controls.Add(Me.GroupControl2)
        Me.Name = "UpgradeMember"
        Me.Text = "UpgradeMember"
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.TotalBayar.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TotalBeli.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xTrans.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LevelNow.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TipeMembershipNow.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DaftarTipeMembership.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewTipeMembership, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.HargaKartu.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SyaratKartu.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ButBatal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButUpgrade As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents TipeMembershipNow As DevExpress.XtraEditors.TextEdit
    Friend WithEvents DaftarTipeMembership As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents ViewTipeMembership As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents HargaKartu As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SyaratKartu As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LevelNow As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TotalBayar As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TotalBeli As DevExpress.XtraEditors.TextEdit
    Friend WithEvents xTrans As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
End Class
